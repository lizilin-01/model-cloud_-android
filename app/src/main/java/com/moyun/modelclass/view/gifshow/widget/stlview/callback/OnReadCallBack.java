package com.moyun.modelclass.view.gifshow.widget.stlview.callback;

import javax.microedition.khronos.opengles.GL10;

/**
 * Author : xuan.
 * Date : 2017/12/12.
 * Description :抽象类回调
 */

public abstract class OnReadCallBack {
    public void onStart() {

    }

    public void onReading(int cur, int total) {

    }

    public void onCreateBitmap(GL10 gl, int mOutputWidth, int mOutputHeight){

    }

    public void onFinish() {

    }
}
