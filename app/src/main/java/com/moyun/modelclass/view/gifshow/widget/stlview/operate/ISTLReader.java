package com.moyun.modelclass.view.gifshow.widget.stlview.operate;


import com.moyun.modelclass.view.gifshow.widget.stlview.callback.OnReadListener;
import com.moyun.modelclass.view.gifshow.widget.stlview.model.STLModel;

/**
 * Author : xuan.
 * Date : 2017/12/10.
 * Description : interface of stlreader
 */

public interface ISTLReader {
    public STLModel parserBinStl(byte[] bytes);

    public STLModel parserAsciiStl(byte[] bytes);

    public void setCallBack(OnReadListener listener);
}
