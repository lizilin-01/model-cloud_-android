package com.moyun.modelclass.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 作者：Meteor
 * 日期：2022/1/21 00:53
 * tip：
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.divider};

    private Drawable mDivider;
    private int mMarginLeft = 0;
    private int mMarginRight = 0;
    private boolean mIsDrawLastDivider = false;  //是否画最后一条线

    /**
     * Default divider will be used
     */
    public DividerItemDecoration(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
        mDivider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    /**
     * Custom divider will be used
     */
    public DividerItemDecoration(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
    }

    public DividerItemDecoration(Context context, int dividerColor, int dividerHeight) {
        mDivider = new ColorDrawable(dividerColor);  //dividerColor 要用 getResource 获取，不能直接传资源 id
        mDivider.setBounds(0, 0, 0, dividerHeight);
    }

    public DividerItemDecoration(Context context, int dividerColor, int dividerHeight, int marginLeft, int marginRight) {
        this(context, dividerColor, dividerHeight);
        this.mMarginLeft = marginLeft;
        this.mMarginRight = marginRight;
    }

    public DividerItemDecoration(Context context, int dividerColor, int dividerHeight, int marginLeft, int marginRight, boolean isDrawLastDivider) {
        this(context, dividerColor, dividerHeight);
        this.mMarginLeft = marginLeft;
        this.mMarginRight = marginRight;
        this.mIsDrawLastDivider = isDrawLastDivider;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int length = parent.getChildCount();
        if (!mIsDrawLastDivider) {
            length = length - 1;   //不画最后一条线
        }
        for (int i = 0; i < length; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getBounds().height();

            mDivider.setBounds(left + mMarginLeft, top, right - mMarginRight, bottom);
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);
        int itemCount = parent.getAdapter().getItemCount();
        if (mIsDrawLastDivider) {
            outRect.set(0, 0, 0, mDivider.getBounds().height());
        } else {
            if (position != itemCount - 1) {  //不画最后一条线
                outRect.set(0, 0, 0, mDivider.getBounds().height());
            }
        }
    }
}
