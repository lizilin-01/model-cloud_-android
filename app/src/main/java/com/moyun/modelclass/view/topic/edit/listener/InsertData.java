package com.moyun.modelclass.view.topic.edit.listener;

import com.moyun.modelclass.view.topic.bean.FormatRange;

public interface InsertData {

    CharSequence charSequence();

    FormatRange.FormatData formatData();

    int color();
}
