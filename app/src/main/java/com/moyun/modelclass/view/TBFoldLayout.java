package com.moyun.modelclass.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moyun.modelclass.view.history.FlowListView;
import com.moyun.modelclass.R;

/**
 * @author zwl
 * @describe 折叠
 * @date on 2021/8/7
 */
public class TBFoldLayout extends FlowListView {
    private final View upFoldView;

    public TBFoldLayout(Context context) {
        this(context, null);
    }

    public TBFoldLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TBFoldLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        upFoldView = LayoutInflater.from(context).inflate(R.layout.view_item_fold_up, null);
        upFoldView.setOnClickListener(v -> {
            mFold = false;
            flowAdapter.notifyDataChanged();
        });

        setOnFoldChangedListener((canFold, fold, index, surplusWidth) -> {
            if (canFold) {
                if (fold) {
                    removeFromParent(upFoldView);
                    addView(upFoldView, index(index, surplusWidth));
                }
            }
        });
    }

    private int index(int index, int surplusWidth) {
        int upIndex = index;
        int upWidth = getViewWidth(upFoldView);
        //当剩余空间大于等于展开View宽度直接加入index+1
        if (surplusWidth >= upWidth) {
            upIndex = index + 1;
        } else { //找到对应的位置
            for (int i = index; i >= 0; i--) {
                View view = getChildAt(index);
                int viewWidth = getViewWidth(view);
                upWidth -= viewWidth;
                if (upWidth <= 0) {
                    upIndex = i;
                    break;
                }
            }
        }
        return upIndex;
    }

    public static int getViewWidth(View view) {
        view.measure(0, 0);
        return view.getMeasuredWidth();
    }

    /**
     * 移除父布局中的子布局
     *
     * @param view
     */
    public static void removeFromParent(View view) {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }
}
