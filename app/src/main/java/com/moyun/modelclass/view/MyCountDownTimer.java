package com.moyun.modelclass.view;

import android.os.CountDownTimer;
import android.util.Log;

import com.moyun.modelclass.utils.DataUtil;

public class MyCountDownTimer extends CountDownTimer {

    private OnTimerListener onTimerListener;

    public MyCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public void setOnTimerListener(OnTimerListener onTimerListener) {
        this.onTimerListener = onTimerListener;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        // 这里的代码会在倒计时过程中每隔一段时间（由构造函数的countDownInterval参数决定）执行一次
        // 计算剩余时间
        int secondsRemaining = (int) (millisUntilFinished / 1000);
        Log.d("CountDownTimer", "Seconds remaining: " + secondsRemaining);
        String a = DataUtil.secondsToHourMinute(secondsRemaining);
        if (onTimerListener != null) {
            onTimerListener.onItemSelected(a);
        }
        // 更新UI
        // updateUI(secondsRemaining);
    }

    @Override
    public void onFinish() {
        // 这里的代码会在倒计时结束时执行
        Log.d("CountDownTimer", "Countdown finished!");
        // 倒计时结束时的操作
        // countdownFinished();
    }

    public interface OnTimerListener {
        void onItemSelected(String secondsRemaining);//参数
    }
}