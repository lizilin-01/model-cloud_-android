package com.moyun.modelclass.view;

import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

public class AndroidBugInputBoxAndSoftKeyboard {
    // For more information, see https://code.google.com/p/android/issues/detail?id=5497
    // To use this class, simply invoke assistActivity() on an Activity that alreay has its content view set.


    public static void assistActivity(Activity activity, boolean isMoveMore, OnChangeListener listener) {

        new AndroidBugInputBoxAndSoftKeyboard(activity, isMoveMore, listener);
    }

    public OnChangeListener listener;
    private View mChildOfContent;
    private int usableHeightPrevious;
    private FrameLayout.LayoutParams frameLayoutParams;
    private Activity activity;

    private AndroidBugInputBoxAndSoftKeyboard(Activity activity, final boolean isMoveMore, OnChangeListener listener) {
        this.activity = activity;
        this.listener = listener;
        FrameLayout content = (FrameLayout) activity.findViewById(android.R.id.content);

        mChildOfContent = content.getChildAt(0);
        mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                possiblyResizeChildOfContent(isMoveMore);
            }
        });
        frameLayoutParams = (FrameLayout.LayoutParams) mChildOfContent.getLayoutParams();
    }

    private void possiblyResizeChildOfContent(boolean isMoveMore) {
        int usableHeightNow = computeUsableHeight();
        if (usableHeightNow != usableHeightPrevious) {
//            int usableHeightSansKeyboard = mChildOfContent.getRootView().getHeight();
            int usableHeightSansKeyboard = activity.getWindowManager().getDefaultDisplay().getHeight();//获取屏幕尺寸，不包括底部导航栏的高度
            int heightDifference = usableHeightSansKeyboard - usableHeightNow;
            if (heightDifference > (usableHeightSansKeyboard / 4)) {
                listener.showChange(true);
                // keyboard probably just became visible
                if (isMoveMore) {
                    frameLayoutParams.height = usableHeightSansKeyboard - heightDifference - 80;
                } else {
                    frameLayoutParams.height = usableHeightSansKeyboard - 200;
                }
            } else {
                listener.showChange(false);
                // keyboard probably just became hidden
                frameLayoutParams.height = usableHeightSansKeyboard;
            }
            mChildOfContent.requestLayout();
            usableHeightPrevious = usableHeightNow;
        }
    }

    private int computeUsableHeight() {
        Rect r = new Rect();
        mChildOfContent.getWindowVisibleDisplayFrame(r);
//        return (r.bottom - r.top);
        // 全屏模式下：
        return r.bottom;
    }

    public interface OnChangeListener {
        void showChange(boolean isShow);
    }


}
