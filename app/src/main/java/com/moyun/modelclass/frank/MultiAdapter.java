package com.moyun.modelclass.frank;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class MultiAdapter extends RecyclerView.Adapter<MultiAdapter.ViewHolder> {

    protected final List mDataList = new ArrayList<>();


    public void addData(List list) {
        if (list != null) {
            mDataList.addAll(list);
        }
        notifyDataSetChanged();
    }

    public List getData() {
        return mDataList;
    }

    public void setData(List list) {
        mDataList.clear();
        if (list != null) {
            mDataList.addAll(list);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MultiAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return createViewBindingHolder(parent, viewType).mHolder;
    }

    protected abstract ViewBindingHolder createViewBindingHolder(@NonNull ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(@NonNull MultiAdapter.ViewHolder holder, int position) {
        holder.viewBindingHolder.onBindData(mDataList.get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        return getItemViewType(position, mDataList.get(position));
    }

    protected abstract int getItemViewType(int position, Object o);

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public static abstract class ViewBindingHolder<V extends ViewDataBinding, M> {
        protected V binding;
        protected MultiAdapter.ViewHolder mHolder;

        public ViewBindingHolder(ViewGroup parent) {
            try {
                Class aClass = getClass();
                Type genericSuperclass = aClass.getGenericSuperclass();
                if (genericSuperclass instanceof ParameterizedType) {
                    Class type =
                            (Class) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
                    Method method = type.getMethod("inflate", LayoutInflater.class, ViewGroup.class,
                            boolean.class);
                    binding = (V) method.invoke(null, LayoutInflater.from(parent.getContext()),
                            parent, false);
                    mHolder = new MultiAdapter.ViewHolder(this);
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

        public void onBindData(M bean, int position) {
            binding.setVariable(1, bean);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ViewBindingHolder viewBindingHolder;

        public ViewHolder(@NonNull ViewBindingHolder v) {
            super(v.binding.getRoot());
            viewBindingHolder = v;
        }
    }

}
