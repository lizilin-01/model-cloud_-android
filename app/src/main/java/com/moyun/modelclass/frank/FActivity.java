package com.moyun.modelclass.frank;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ViewDataBinding;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class FActivity<V extends ViewDataBinding> extends AppCompatActivity {
    protected V mBinding;
    protected AppCompatActivity mActivity;

    public void start(Class<? extends Activity> clazz) {
        Intent starter = new Intent(mActivity, clazz);
        startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        try {
            Class<? extends FActivity> aClass = getClass();
            Type genericSuperclass = aClass.getGenericSuperclass();
            if (genericSuperclass instanceof ParameterizedType) {
                Type[] types =
                        ((ParameterizedType) genericSuperclass).getActualTypeArguments();
                Class type = (Class) types[0];
                Method method = type.getMethod("inflate", LayoutInflater.class);
                LayoutInflater layoutInflater = getLayoutInflater();
                mBinding = (V) method.invoke(null, layoutInflater);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(mBinding.getRoot());
        initView();
        initData();
    }

    protected void initData() {
    }

    protected void initView() {
    }
}