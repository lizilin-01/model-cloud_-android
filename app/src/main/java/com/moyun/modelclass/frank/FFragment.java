package com.moyun.modelclass.frank;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class FFragment<V extends ViewDataBinding> extends Fragment {
    protected V mBinding;
    protected FragmentActivity mActivity;
    protected final String TAG = getClass().getSimpleName();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        try {
            Class<? extends FFragment> aClass = getClass();
            Type genericSuperclass = aClass.getGenericSuperclass();
            if (genericSuperclass instanceof ParameterizedType) {
                Type[] types =
                        ((ParameterizedType) genericSuperclass).getActualTypeArguments();
                Class type =
                        (Class) types[0];
                Method method = type.getMethod("inflate", LayoutInflater.class, ViewGroup.class,
                        boolean.class);
                mBinding = (V) method.invoke(null, inflater, container, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called::" + getClass().getSimpleName() + ",isVisible:" + isVisible());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called::" + getClass().getSimpleName() + ",isVisible:" + isVisible());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d(TAG, "onHiddenChanged() called::" + getClass().getSimpleName() + ",hidden:" + hidden + ",isVisible:" + isVisible());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, "setUserVisibleHint() called::" + getClass().getSimpleName() + ",isVisibleToUser:" + isVisibleToUser + ",isVisible:" + isVisible());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }


    protected void initData() {
    }

}
