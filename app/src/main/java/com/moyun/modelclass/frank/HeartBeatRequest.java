package com.moyun.modelclass.frank;

import com.hjq.http.config.IRequestApi;

import java.util.List;

public class HeartBeatRequest implements IRequestApi {
    @Override
    public String getApi() {
        return "daemon/beat";
    }

    public static class Bean {

        public List<NewChat> newChat;
        public double nextExecuteSecond;

    }

    public static class NewChat {
        public String userId;
        public int unreadCount;
    }
}
