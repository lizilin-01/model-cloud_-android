package com.moyun.modelclass.frank;

import androidx.lifecycle.LifecycleOwner;

import com.hjq.http.EasyHttp;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.http.model.HttpData;

public class Http {
    public abstract static class Callback<T> {
        public abstract void onSuccess(T t);

        public void onFail(int code, String msg) {

        }
    }

    public static <T> void post(LifecycleOwner lifecycleOwner, IRequestApi requestApi, Callback<T> callback) {
        EasyHttp.post(lifecycleOwner)
                .api(requestApi)
                .request(new HttpCallback<HttpData<T>>(null) {
                    @Override
                    public void onSucceed(HttpData<T> result) {
                        if (result != null) {
                            if (result.getCode() == 1) {
                                callback.onSuccess(result.getData());
                            } else {
                                callback.onFail(result.getCode(), result.getMessage());
                            }
                        } else {
                            callback.onFail(-1, "网络异常");
                        }
                    }
                });
    }
}
