package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：13.修改支付密码：校验验证码
 */
public class AppVipJudgePayPasswordCodeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/judgePayPasswordCode";
    }

    //手机号
    @HttpRename("username")
    private String username;

    //验证码
    @HttpRename("code")
    private String code;

    public AppVipJudgePayPasswordCodeApi setUsername(String username) {
        this.username = username;
        return this;
    }

    public AppVipJudgePayPasswordCodeApi setCode(String code) {
        this.code = code;
        return this;
    }

    public final static class Bean {
        private String msg;

        public String getMsg() {
            return msg;
        }

    }
}
