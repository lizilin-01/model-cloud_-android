package com.moyun.modelclass.http;

import com.hjq.http.config.IRequestServer;
import com.hjq.http.model.BodyType;

/**
 * 作者：Meteor
 * 日期：2022/2/136:17 下午
 * tip：
 */
public class RequestServer implements IRequestServer {
    @Override
    public String getHost() {
        return CommonConfiguration.getApiUrl();
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.JSON;
    }
}
