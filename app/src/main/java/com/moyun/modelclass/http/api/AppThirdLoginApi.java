package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：三方：登录
 */
public class AppThirdLoginApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/third/login";
    }

    //三方：微信openId（两个不能都为空）
    @HttpRename("thirdWxOpenId")
    private String thirdWxOpenId;
    //三方：微信账号
    @HttpRename("thirdWxAccount")
    private String thirdWxAccount;
    //三方：支付宝openId（两个不能都为空）
    @HttpRename("thirdAliOpenId")
    private String thirdAliOpenId;
    //三方：支付宝账号
    @HttpRename("thirdAliAccount")
    private String thirdAliAccount;


    public AppThirdLoginApi setThirdWxOpenId(String thirdWxOpenId) {
        this.thirdWxOpenId = thirdWxOpenId;
        return this;
    }

    public AppThirdLoginApi setThirdWxAccount(String thirdWxAccount) {
        this.thirdWxAccount = thirdWxAccount;
        return this;
    }

    public AppThirdLoginApi setThirdAliOpenId(String thirdAliOpenId) {
        this.thirdAliOpenId = thirdAliOpenId;
        return this;
    }

    public AppThirdLoginApi setThirdAliAccount(String thirdAliAccount) {
        this.thirdAliAccount = thirdAliAccount;
        return this;
    }
}
