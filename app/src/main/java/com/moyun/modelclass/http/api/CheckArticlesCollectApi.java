package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.CommentResponse;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 作者：Meteor
 * tip：检查文章是否收藏,登录之后才能调用
 */
public class CheckArticlesCollectApi implements IRequestApi {
    @Override
    public String getApi() {
        return "likes/checkArticles";
    }

    //文章id列表
    @HttpRename("articleIdList")
    private List<String> articleIdList;

    public CheckArticlesCollectApi setArticleIdList(List<String> articleIdList) {
        this.articleIdList = articleIdList;
        return this;
    }

    public static class Bean implements Serializable {
        private List<BeanInfo> likes;

        public List<BeanInfo> getLikes() {
            return likes;
        }
    }

    public static class BeanInfo implements Serializable {
        private String objectId;
        private boolean likes = false;

        public String getObjectId() {
            return objectId;
        }

        public boolean isLikes() {
            return likes;
        }
    }
}
