package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AddressResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：获取会员信息描述，会员种类
 */
public class MemberSettingApi implements IRequestApi {
    @Override
    public String getApi() {
        return "member/setting";
    }

    public final static class Bean {
        private List<BeanInfo> list;
        private int currentLevel;

        public List<BeanInfo> getList() {
            return list;
        }

        public int getCurrentLevel() {
            return currentLevel;
        }
    }

    public final static class BeanInfo {
        private int level;//会员等级
        private String levelDesc;//会员等级描述
        private List<DescResponse> desc;//描述
        private int money = 0;//充值金额
        private int effectiveDays;//有效天数
        private String currentDate;//当前日期
        private String expireDate;//过期时间
        private String id;//
        private int price;//
        private int totalSpace;//总空间
        private boolean isDefault;//
        private boolean isEnable;//是否显示

        public int getLevel() {
            return level;
        }

        public String getLevelDesc() {
            return levelDesc;
        }

        public List<DescResponse> getDesc() {
            return desc;
        }

        public int getMoney() {
            return money;
        }

        public int getEffectiveDays() {
            return effectiveDays;
        }

        public String getCurrentDate() {
            return currentDate;
        }

        public String getExpireDate() {
            return expireDate;
        }

        public String getId() {
            return id;
        }

        public int getPrice() {
            return price;
        }

        public int getTotalSpace() {
            return totalSpace;
        }

        public boolean isDefault() {
            return isDefault;
        }

        public boolean isEnable() {
            return isEnable;
        }
    }

    public final static class DescResponse {
        private String logo;//描述logo
        private String title;//描述标题
        private String description;//描述

        public String getLogo() {
            return logo;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }
    }
}
