package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.CommentResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：评论列表
 */
public class AppCommentApi implements IRequestApi {
    @Override
    public String getApi() {
        return "comment/list";
    }

    //评论对象类型，文章1，模型2
    @HttpRename("type")
    private int type;
    //评论对象id
    @HttpRename("objectId")
    private String objectId;
    //最后一条的id
    @HttpRename("lastId")
    private String lastId;
    //分页大小
    @HttpRename("pageSize")
    private int pageSize;


    public AppCommentApi setType(int type) {
        this.type = type;
        return this;
    }

    public AppCommentApi setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public AppCommentApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public AppCommentApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public final static class Bean {
        private List<CommentResponse> comments;
        private int totalComment;//总数量

        public List<CommentResponse> getComments() {
            return comments;
        }

        public int getTotalComment() {
            return totalComment;
        }
    }
}
