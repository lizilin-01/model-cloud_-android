package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ArticleResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：用户相关接口
 */
public class UserSearchApi implements IRequestApi {
    @Override
    public String getApi() {
        return "user/search";
    }

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    //搜索内容，支持标题，话题，暂不支持正文
    @HttpRename("searchText")
    private String searchText;


    public UserSearchApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public UserSearchApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public UserSearchApi setSearchText(String searchText) {
        this.searchText = searchText;
        return this;
    }

    public static class Bean {
        private List<BeanInfo> list;

        public List<BeanInfo> getList() {
            return list;
        }
    }

    public static class BeanInfo {
        private String userId;//userId
        private String nickName;//用户昵称
        private String avatar;//头像地址
        private String desc;//描述

        private Boolean isFollow = false;//是否关注该用户

        public String getUserId() {
            return userId;
        }

        public String getNickName() {
            return nickName;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getDesc() {
            return desc;
        }

        public Boolean getFollow() {
            return isFollow;
        }

        public void setFollow(Boolean follow) {
            isFollow = follow;
        }
    }
}
