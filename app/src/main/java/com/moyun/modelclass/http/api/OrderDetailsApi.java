package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：订单详情
 */
public class OrderDetailsApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/detail";
    }

    //订单id
    @HttpRename("orderId")
    private String orderId;

    public OrderDetailsApi setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }
}
