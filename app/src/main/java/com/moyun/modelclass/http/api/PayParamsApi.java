package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AliPayRequest;
import com.moyun.modelclass.http.bean.WXPayRequest;

import java.util.List;

/**
 * 作者：Meteor
 * tip：获取支付参数
 */
public class PayParamsApi implements IRequestApi {
    @Override
    public String getApi() {
        return "pay/payParams";
    }

    //渠道id
    @HttpRename("channelId")
    private int channelId;

    //支付订单号
    @HttpRename("payOrderId")
    private String payOrderId;

    public PayParamsApi setChannelId(int channelId) {
        this.channelId = channelId;
        return this;
    }

    public PayParamsApi setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
        return this;
    }

    public final static class Bean {
        private WXPayRequest wxParams;//微信支付参数
        private AliPayRequest aliParams;//支付宝支付参数

        public WXPayRequest getWxParams() {
            return wxParams;
        }

        public AliPayRequest getAliParams() {
            return aliParams;
        }
    }
}
