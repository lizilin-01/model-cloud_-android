package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：点赞评论
 */
public class LikeCommentApi implements IRequestApi {
    @Override
    public String getApi() {
        return "likes/comment";
    }

    //评论id
    @HttpRename("commentId")
    private String commentId;

    //点赞或者取消点赞
    @HttpRename("likes")
    private boolean likes;

    public LikeCommentApi setCommentId(String commentId) {
        this.commentId = commentId;
        return this;
    }

    public LikeCommentApi setLikes(boolean likes) {
        this.likes = likes;
        return this;
    }
}
