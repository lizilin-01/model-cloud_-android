package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 作者：Meteor
 * 首页：检查评论是否被点赞
 */
public class CheckCommentsLikesApi implements IRequestApi {
    @Override
    public String getApi() {
        return "likes/checkComments";
    }

    //评论id列表
    @HttpRename("commentIdList")
    private List<String> commentIdList;

    public CheckCommentsLikesApi setCommentIdList(List<String> commentIdList) {
        this.commentIdList = commentIdList;
        return this;
    }

    public static class Bean implements Serializable {
        private List<BeanInfo> likes;

        public List<BeanInfo> getLikes() {
            return likes;
        }
    }

    public static class BeanInfo implements Serializable {
        private String objectId;
        private boolean likes = false;

        public String getObjectId() {
            return objectId;
        }

        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }

        public boolean isLikes() {
            return likes;
        }

        public void setLikes(boolean likes) {
            this.likes = likes;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BeanInfo beanInfo = (BeanInfo) o;
            return likes == beanInfo.likes && Objects.equals(objectId, beanInfo.objectId);
        }

        @Override
        public String toString() {
            return "BeanInfo{" +
                    "objectId='" + objectId + '\'' +
                    ", likes=" + likes +
                    '}';
        }
    }
}
