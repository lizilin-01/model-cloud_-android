package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：前端对文件进行md5，传上来md5之后，后台会匹配当前文章是否传过
 */
public class CheckFileApi implements IRequestApi {
    @Override
    public String getApi() {
        return "file/checkFile";
    }

    //文件的md5值
    @HttpRename("md5")
    private String md5;

    //文件的后缀名
    @HttpRename("extName")
    private String extName;

    public CheckFileApi setMd5(String md5) {
        this.md5 = md5;
        return this;
    }

    public CheckFileApi setExtName(String extName) {
        this.extName = extName;
        return this;
    }

    public static class Bean {
        private boolean exist;//是否存在
        private String fileName;//文件名称
        private String url;//文件地址
        private String thumbnail;//缩略图

        public boolean isExist() {
            return exist;
        }

        public String getFileName() {
            return fileName;
        }

        public String getUrl() {
            return url;
        }

        public String getThumbnail() {
            return thumbnail;
        }
    }
}
