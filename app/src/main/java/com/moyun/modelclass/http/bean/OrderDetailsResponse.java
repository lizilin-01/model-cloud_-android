package com.moyun.modelclass.http.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * tip：订单详情
 */
public class OrderDetailsResponse implements Serializable {
    private String id;//订单id
    private String orderNo;//订单号
    private String payOrderId;//支付中心订单号，这个字段用在订单详情页面唤起支付组件使用
    private String createTime;//创建时间
    private String remark;//备注
    private String expireTime;//过期时间
    private String payTime;//实际支付时间
    private int orderState;//订单状态
    private int orderStateVO;//对外展示的订单状态，分别为：待付款、待发货、待收货、待评价、已完成、已关闭
    private List<OrderModelResponse> modelList;//模型列表

    private int totalPrice;//总金额
    private int materialPrice;//总材料费用
    private String userId;//
    private int productPrice;//商品价格
    private String printParams;//打印参数
    private RecipientResponse recipient;//收件人信息


    private int payChannelId;//支付渠道id
    private int deliveryPrice;//快递费用
    private int refundAmount;//退款金额，单位分
    private String receiveOrderTime;//收货时间
    private String deliveryTime;//订单发货时间
    private String deliveryNo;//物流单号
    private String deliverySupplier;//物流供应商
    private OrderOperateResponse orderOperate;//订单可以进行哪些操作
    private boolean hasEvaluate;//是否已经评价过
    private String receiveManufacturerUserId;//制造商userId
    private int printHours;//
    private int deductionBalance;//抵扣余额，单位为分
    private String orderStateVODesc;//订单状态描述

    public String getId() {
        return id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public String getPayOrderId() {
        return payOrderId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getRemark() {
        return remark;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public int getOrderState() {
        return orderState;
    }

    public int getOrderStateVO() {
        return orderStateVO;
    }

    public List<OrderModelResponse> getModelList() {
        return modelList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public int getMaterialPrice() {
        return materialPrice;
    }

    public String getUserId() {
        return userId;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public String getPrintParams() {
        return printParams;
    }

    public RecipientResponse getRecipient() {
        return recipient;
    }

    public int getPayChannelId() {
        return payChannelId;
    }

    public int getDeliveryPrice() {
        return deliveryPrice;
    }

    public int getRefundAmount() {
        return refundAmount;
    }

    public String getReceiveOrderTime() {
        return receiveOrderTime;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public String getDeliverySupplier() {
        return deliverySupplier;
    }

    public OrderOperateResponse getOrderOperate() {
        return orderOperate;
    }

    public boolean isHasEvaluate() {
        return hasEvaluate;
    }

    public String getReceiveManufacturerUserId() {
        return receiveManufacturerUserId;
    }

    public int getPrintHours() {
        return printHours;
    }

    public int getDeductionBalance() {
        return deductionBalance;
    }

    public String getOrderStateVODesc() {
        return orderStateVODesc;
    }

    public static class RecipientResponse {
        private List<Double> location;//经纬度
        private String userName;//收件人

        private String mobile;//手机号
        private String address;//地址

        public List<Double> getLocation() {
            return location;
        }

        public String getUserName() {
            return userName;
        }

        public String getMobile() {
            return mobile;
        }

        public String getAddress() {
            return address;
        }
    }

    public static class OrderOperateResponse {
        private boolean canPay;//是否当前可以支付
        private boolean canEvaluate;//当前是否可以评论
        private boolean canCancel;//是否能取消
        private boolean canUrgeDelivery;//是否可以催发货

        public boolean isCanPay() {
            return canPay;
        }

        public boolean isCanEvaluate() {
            return canEvaluate;
        }

        public boolean isCanCancel() {
            return canCancel;
        }

        public boolean isCanUrgeDelivery() {
            return canUrgeDelivery;
        }
    }
}
