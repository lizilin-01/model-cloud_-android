package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.OrderRequestResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：获取支付单信息
 */
public class OrderInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "pay/orderInfo";
    }

    //支付订单号
    @HttpRename("payOrderId")
    private String payOrderId;

    public OrderInfoApi setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
        return this;
    }

    public final static class Bean {
        private String payOrderId;//支付订单号,在支付组件中使用此订单号
        private int totalAmount;//总价格
        private int orderState;//支付订单状态：1：待支付，2、已支付、3、已关闭
        private String expireTime;//过期时间
        private List<ChannelBean> channelList;//渠道列表
        private int payBusiness;//所属业务，目前业务只有两种，1、模型购买 2、会员购买
        private String orderId;//

        public String getPayOrderId() {
            return payOrderId;
        }

        public int getTotalAmount() {
            return totalAmount;
        }

        public int getOrderState() {
            return orderState;
        }

        public String getExpireTime() {
            return expireTime;
        }

        public List<ChannelBean> getChannelList() {
            return channelList;
        }

        public int getPayBusiness() {
            return payBusiness;
        }

        public String getOrderId() {
            return orderId;
        }
    }

    public final static class ChannelBean {
        private int channelId;//渠道id
        private String channelIcon;//渠道icon
        private String channelName;//渠道名称

        public int getChannelId() {
            return channelId;
        }

        public String getChannelIcon() {
            return channelIcon;
        }

        public String getChannelName() {
            return channelName;
        }
    }
}
