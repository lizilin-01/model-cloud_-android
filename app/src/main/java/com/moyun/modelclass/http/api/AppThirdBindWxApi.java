package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：三方：绑定微信账号
 */
public class AppThirdBindWxApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/third/bindWx";
    }

    //三方：微信openId（两个不能都为空）
    @HttpRename("thirdWxOpenId")
    private String thirdWxOpenId;
    //三方：微信账号
    @HttpRename("thirdWxAccount")
    private String thirdWxAccount;

    public AppThirdBindWxApi setThirdWxOpenId(String thirdWxOpenId) {
        this.thirdWxOpenId = thirdWxOpenId;
        return this;
    }

    public AppThirdBindWxApi setThirdWxAccount(String thirdWxAccount) {
        this.thirdWxAccount = thirdWxAccount;
        return this;
    }
}
