package com.moyun.modelclass.http.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 作者：Meteor
 * tip：用户信息
 */
public class LoginInfoResponse implements Parcelable {
    private String id; //用户id
    private String avatar;//头像
    private String mobile;//手机号码，自己查询自己的有手机号，别人查询此字段不返回
    private String nickName;//昵称
    private String backgroundImage;//背景图
    private int articleCount = 0;//文章数
    private int modelCount = 0;//模型树
    private String province;//省
    private String city;//市
    private String area;//区
    private int gender = 1;//性别
    private String birth;//生日
    private String career;//职业
    private List<String> speciality;//领域
    private String areaCode;//地址code
    private String desc;//自我描述
    private int followerCount;//关注人数
    private int fansCount;//粉丝人数
    private String appId;//AppId
    private int balance;//账户余额
    private boolean hasBindWechat;//是否绑定了微信
    private boolean canCheckInfo;//是否可以查看个人信息，true，表示可以点击头像
    private boolean canChat;//是否可以私聊,true：表示可以私聊


    protected LoginInfoResponse(Parcel in) {
        id = in.readString();
        avatar = in.readString();
        mobile = in.readString();
        nickName = in.readString();
        backgroundImage = in.readString();
        articleCount = in.readInt();
        modelCount = in.readInt();
        province = in.readString();
        city = in.readString();
        area = in.readString();
        gender = in.readInt();
        birth = in.readString();
        career = in.readString();
        speciality = in.createStringArrayList();
        areaCode = in.readString();
        desc = in.readString();
        followerCount = in.readInt();
        fansCount = in.readInt();
        appId = in.readString();
        balance = in.readInt();
        hasBindWechat = in.readByte() != 0;
        canCheckInfo = in.readByte() != 0;
        canChat = in.readByte() != 0;
    }

    public static final Creator<LoginInfoResponse> CREATOR = new Creator<LoginInfoResponse>() {
        @Override
        public LoginInfoResponse createFromParcel(Parcel in) {
            return new LoginInfoResponse(in);
        }

        @Override
        public LoginInfoResponse[] newArray(int size) {
            return new LoginInfoResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(avatar);
        dest.writeString(mobile);
        dest.writeString(nickName);
        dest.writeString(backgroundImage);
        dest.writeInt(articleCount);
        dest.writeInt(modelCount);
        dest.writeString(province);
        dest.writeString(city);
        dest.writeString(area);
        dest.writeInt(gender);
        dest.writeString(birth);
        dest.writeString(career);
        dest.writeStringList(speciality);
        dest.writeString(areaCode);
        dest.writeString(desc);
        dest.writeInt(followerCount);
        dest.writeInt(fansCount);
        dest.writeString(appId);
        dest.writeInt(balance);
        dest.writeByte((byte) (hasBindWechat ? 1 : 0));
        dest.writeByte((byte) (canCheckInfo ? 1 : 0));
        dest.writeByte((byte) (canChat ? 1 : 0));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public int getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(int articleCount) {
        this.articleCount = articleCount;
    }

    public int getModelCount() {
        return modelCount;
    }

    public void setModelCount(int modelCount) {
        this.modelCount = modelCount;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public List<String> getSpeciality() {
        return speciality;
    }

    public void setSpeciality(List<String> speciality) {
        this.speciality = speciality;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public boolean isHasBindWechat() {
        return hasBindWechat;
    }

    public void setHasBindWechat(boolean hasBindWechat) {
        this.hasBindWechat = hasBindWechat;
    }

    public boolean isCanCheckInfo() {
        return canCheckInfo;
    }

    public void setCanCheckInfo(boolean canCheckInfo) {
        this.canCheckInfo = canCheckInfo;
    }

    public boolean isCanChat() {
        return canChat;
    }

    public void setCanChat(boolean canChat) {
        this.canChat = canChat;
    }
}
