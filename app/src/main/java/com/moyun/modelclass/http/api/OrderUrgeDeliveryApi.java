package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：催发货
 */
public class OrderUrgeDeliveryApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/urgeDelivery";
    }

    //订单Id
    @HttpRename("orderId")
    private String orderId;

    public OrderUrgeDeliveryApi setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }
}
