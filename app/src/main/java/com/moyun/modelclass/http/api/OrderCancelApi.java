package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AddressResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：取消订单
 */
public class OrderCancelApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/cancel";
    }

    //订单Id
    @HttpRename("orderId")
    private String orderId;

    public OrderCancelApi setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }
}
