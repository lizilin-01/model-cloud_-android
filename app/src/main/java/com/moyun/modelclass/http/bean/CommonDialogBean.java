package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/1310:40 下午
 * tip：用户信息
 */
public class CommonDialogBean implements Serializable {
    private String code;

    private String message;

    public CommonDialogBean(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
