package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/5/610:34 下午
 * tip：
 */
public  class WeChatUserInfoApi implements  IRequestApi {

    @Override
    public String getApi() {
        return "sns/userinfo";
    }

    //调用凭证
    @HttpRename("access_token")
    private String access_token;
    //普通用户的标识，对当前开发者帐号唯一
    @HttpRename("openid")
    private String openid;
    //国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语，默认为 en
    @HttpRename("lang")
    private String lang;

    public String getAccess_token() {
        return access_token;
    }

    public WeChatUserInfoApi setAccess_token(String access_token) {
        this.access_token = access_token;
        return this;
    }

    public String getOpenid() {
        return openid;
    }

    public WeChatUserInfoApi setOpenid(String openid) {
        this.openid = openid;
        return this;
    }

    public String getLang() {
        return lang;
    }

    public WeChatUserInfoApi setLang(String lang) {
        this.lang = lang;
        return this;
    }

    public final static class Bean {
        private String openid;//普通用户的标识，对当前开发者帐号唯一
        private String nickname;//普通用户昵称
        private int sex;//普通用户性别，1 为男性，2 为女性
        private int province;//普通用户个人资料填写的省份
        private int city;//普通用户个人资料填写的城市
        private int country;//国家，如中国为 CN
        private int headimgurl;//用户头像，最后一个数值代表正方形头像大小（有 0、46、64、96、132 数值可选，0 代表 640*640 正方形头像），用户没有头像时该项为空
        private int privilege;//用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
        private int unionid;//用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。

        public String getOpenid() {
            return openid;
        }

        public String getNickname() {
            return nickname;
        }

        public int getSex() {
            return sex;
        }

        public int getProvince() {
            return province;
        }

        public int getCity() {
            return city;
        }

        public int getCountry() {
            return country;
        }

        public int getHeadimgurl() {
            return headimgurl;
        }

        public int getPrivilege() {
            return privilege;
        }

        public int getUnionid() {
            return unionid;
        }
    }
}
