package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AddressResponse;
import java.util.List;

/**
 * 作者：Meteor
 * tip：获取地址列表
 */
public class AddressListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "address/list";
    }

    public final static class Bean {
        private List<AddressResponse> list;

        public List<AddressResponse> getList() {
            return list;
        }
    }

}
