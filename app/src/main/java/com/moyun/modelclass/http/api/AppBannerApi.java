package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.BannerResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：轮播图：获取轮播图
 */
public class AppBannerApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/banner";
    }

    public static class Bean {
        private int totalElements;
        private List<BannerResponse> content;

        public int getTotalElements() {
            return totalElements;
        }

        public List<BannerResponse> getContent() {
            return content;
        }
    }
}
