package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：6.更新密码
 */
public class AppVipUpdatePasswordApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/updatePassword";
    }

    //手机号、邮箱
    @HttpRename("username")
    private String username;
    //验证码
    @HttpRename("code")
    private String code;
    //密码
    @HttpRename("password")
    private String password;

    public AppVipUpdatePasswordApi setUsername(String username) {
        this.username = username;
        return this;
    }

    public AppVipUpdatePasswordApi setCode(String code) {
        this.code = code;
        return this;
    }

    public AppVipUpdatePasswordApi setPassword(String password) {
        this.password = password;
        return this;
    }
}
