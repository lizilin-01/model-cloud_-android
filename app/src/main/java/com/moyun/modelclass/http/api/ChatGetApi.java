package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class ChatGetApi implements IRequestApi {
    @Override
    public String getApi() {
        return "chat/get";
    }

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;
    @HttpRename("receiveUserId")
    private String receiveUserId;


    public ChatGetApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public ChatGetApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public ChatGetApi setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
        return this;
    }

    public static class Bean {

        public List<Chats> chats;
        public FromUser fromUser;
        public ReceiveUser receiveUser;
        public boolean isOnline;

        public static class FromUser {
            public String userId;
            public String avatar;
            public String nickName;
        }

        public static class ReceiveUser {
            public String userId;
            public String avatar;
            public String nickName;
        }


    }

    public static class Chats {
        public String id;
        public String receiveUserId;
        public String fromUserId;
        public String content;
        public String sendTime;
    }

}
