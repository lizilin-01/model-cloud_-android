package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：资金提现记录
 */
public class FundWithdrawalOrderListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "fund/withdrawalOrderList";
    }

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    public FundWithdrawalOrderListApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public FundWithdrawalOrderListApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public static class Bean {
        private List<BeanInfo> list;

        public List<BeanInfo> getList() {
            return list;
        }
    }

    public static class BeanInfo {
        public String withdrawalReason;//提现原因
        public String id;//提现订单id
        public String withdrawalOrderNo;//提现订单号
        public String userId;//提现UserId
        public String transactionId;//渠道流水号
        public String realName;//提现昵称
        public int amount;//提现金额
        public int toAccountMoney;//到账金额
        public String failReason;//失败原因
        public int commission;//手续费
        public String createTime;//创建时间
        public String reviewTime;//审核时间
        public String successTime;//成功时间
        public int orderState;//订单状态
        public String orderStateDesc;//订单状态描述

        public String getWithdrawalReason() {
            return withdrawalReason;
        }

        public String getId() {
            return id;
        }

        public String getWithdrawalOrderNo() {
            return withdrawalOrderNo;
        }

        public String getUserId() {
            return userId;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public String getRealName() {
            return realName;
        }

        public int getAmount() {
            return amount;
        }

        public int getToAccountMoney() {
            return toAccountMoney;
        }

        public String getFailReason() {
            return failReason;
        }

        public int getCommission() {
            return commission;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getReviewTime() {
            return reviewTime;
        }

        public String getSuccessTime() {
            return successTime;
        }

        public int getOrderState() {
            return orderState;
        }

        public String getOrderStateDesc() {
            return orderStateDesc;
        }
    }

}
