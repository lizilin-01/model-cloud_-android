package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * tip：文章保存
 */
public class EditArticleApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/save";
    }

    //文章id，为空就表示新增
    @HttpRename("id")
    private String id;

    //标题
    @HttpRename("title")
    private String title;

    //头图
    @HttpRename("simpleImage")
    private String simpleImage;

    //文章正文
    @HttpRename("content")
    private String content;

    //模型列表
    @HttpRename("modelList")
    private List<String> modelList;

    //文章话题
    @HttpRename("tags")
    private List<String> tags;

    //是否作为草稿
    @HttpRename("isDraft")
    private boolean isDraft;

    public EditArticleApi setId(String id) {
        this.id = id;
        return this;
    }

    public EditArticleApi setTitle(String title) {
        this.title = title;
        return this;
    }

    public EditArticleApi setSimpleImage(String simpleImage) {
        this.simpleImage = simpleImage;
        return this;
    }

    public EditArticleApi setContent(String content) {
        this.content = content;
        return this;
    }

    public EditArticleApi setModelList(List<String> modelList) {
        this.modelList = modelList;
        return this;
    }

    public EditArticleApi setTags(List<String> tags) {
        this.tags = tags;
        return this;
    }

    public EditArticleApi setIsDraft(boolean isDraft) {
        this.isDraft = isDraft;
        return this;
    }
}
