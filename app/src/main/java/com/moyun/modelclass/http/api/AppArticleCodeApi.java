package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：文章：文章详情
 */
public class AppArticleCodeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/article/"+code;
    }

    //文章编码（url参数）
    @HttpRename("code")
    private String code;

    public AppArticleCodeApi setCode(String code) {
        this.code = code;
        return this;
    }

    public final static class Bean {
        private String id;
        private String title;//标题
        private String code;
        private String content;//内容
        private String url;//地址

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getCode() {
            return code;
        }

        public String getContent() {
            return content;
        }

        public String getUrl() {
            return url;
        }
    }
}
