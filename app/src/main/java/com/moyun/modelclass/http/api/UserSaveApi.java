package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 保存用户信息
 */
public class UserSaveApi implements IRequestApi {
    @Override
    public String getApi() {
        return "user/save";
    }

    //昵称
    @HttpRename("nickName")
    private String nickName;

    //头像
    @HttpRename("avatar")
    private String avatar;

    //背景图像
    @HttpRename("backgroundImage")
    private String backgroundImage;
    //男女
    @HttpRename("gender")
    private String gender;
    //生日
    @HttpRename("birth")
    private String birth;
    //职业
    @HttpRename("career")
    private String career;

    //专业领域
    @HttpRename("speciality")
    private List<String> speciality;

    //地址code
    @HttpRename("areaCode")
    private String areaCode;

    //自我描述
    @HttpRename("desc")
    private String desc;


    public UserSaveApi setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public UserSaveApi setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public UserSaveApi setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        return this;
    }

    public UserSaveApi setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public UserSaveApi setBirth(String birth) {
        this.birth = birth;
        return this;
    }

    public UserSaveApi setCareer(String career) {
        this.career = career;
        return this;
    }

    public UserSaveApi setSpeciality(List<String> speciality) {
        this.speciality = speciality;
        return this;
    }

    public UserSaveApi setAreaCode(String areaCode) {
        this.areaCode = areaCode;
        return this;
    }

    public UserSaveApi setDesc(String desc) {
        this.desc = desc;
        return this;
    }
}
