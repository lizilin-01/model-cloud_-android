package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ModelResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：通过模型id列表批量获取模型数据
 */
public class AppModelListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/batchGet";
    }

    //模型id集合
    @HttpRename("modelIdList")
    private List<String> modelIdList;

    //场景:1、文章，2、购物车，3、模型库 、订单确认页 5、订单详情
    @HttpRename("scene")
    private int scene;

    public AppModelListApi setModelIdList(List<String> modelIdList) {
        this.modelIdList = modelIdList;
        return this;
    }

    public AppModelListApi setScene(int scene) {
        this.scene = scene;
        return this;
    }

    public final static class Bean {
        private List<ModelResponse> list;

        public List<ModelResponse> getList() {
            return list;
        }
    }
}
