package com.moyun.modelclass.http;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.moyun.modelclass.activity.ArticleDetailsActivity;
import com.moyun.modelclass.activity.MainActivity;
import com.moyun.modelclass.activity.ModelDetailsActivity;
import com.moyun.modelclass.activity.WebActivity;
import com.tencent.mmkv.MMKV;

public class CommonConfiguration {
    public static final boolean isRelease = true;

    public static final int page = 10;//分页
    public static final String releaseApiUrl = "https://m.mo-yun.com/";//api域名
    public static final String releaseUploadUrl = "https://upload.mo-yun.com/";//上传域名
    public static final String releaseResUrl = "https://res.mo-yun.com/";//资源文件域名
    public static final String releaseShareUrl = "https://m.mo-yun.com/h5/index.html";//分享链接

    public static final String debugApiUrl = "http://m-t.mo-yun.com/";//api域名
    public static final String debugUploadUrl = "http://upload-t.mo-yun.com/";//上传域名
    public static final String debugResUrl = "http://res-t.mo-yun.com/";//资源文件域名
    public static final String debugShareUrl = "http://m-t.mo-yun.com/h5/index.html";//分享链接

    public static final String mSecretKey = "&MoYunKeJi888,!&";

    public static String getApiUrl() {
        return isRelease ? releaseApiUrl : debugApiUrl;
    }

    public static String getUploadUrl() {
        return isRelease ? releaseUploadUrl : debugUploadUrl;
    }

    public static String getShareUrl(){
        return isRelease ? releaseShareUrl : debugShareUrl;
    }


    public static String splitResUrl(@IntRange(from = 1, to = 6) int type, @NonNull String image) {
        String imageAddress = (isRelease ? releaseResUrl : debugResUrl) + image;

        if (type == 1) {
            //确定高度，宽度按比例缩放
            return imageAddress + "-0*100.jpg";
        } else if (type == 2) {
            //确定宽度，高度按比例缩放
            return imageAddress + "-100*0.jpg";
        } else if (type == 3) {
            //展示原尺寸三种
            return imageAddress + "-0*0.jpg";
        } else if (type == 4) {
            //确定宽度，高度按比例缩放
            int width = (ScreenUtils.getScreenWidth() - SizeUtils.dp2px(44)) / 2;
            return imageAddress + "-" + width + "*" + 0 + ".jpg";
        } else if (type == 5) {
            //确定宽度，高度按比例缩放
            int width = (ScreenUtils.getScreenWidth() - SizeUtils.dp2px(24));
            int height = (int) (width * 0.5625);
            return imageAddress + "-" + width + "*" + height + ".jpg";
        }
        else if (type == 6) {
            //确定宽度，高度按比例缩放
            int width = ScreenUtils.getScreenWidth();
            int height = SizeUtils.dp2px(310);
            return imageAddress + "-" + width + "*" + height + ".jpg";
        }
        return imageAddress + "-0*0.jpg";
    }


    public static String splitResUrl(@NonNull String image) {
        String imageAddress = (isRelease ? releaseResUrl : debugResUrl) + image;
        //确定宽度，高度按比例缩放
        int width = SizeUtils.dp2px(90);
        int height =SizeUtils.dp2px(90);
        return imageAddress + "-" + width + "*" + height + ".jpg";
    }

    public static String splitResUrlTwo(@NonNull String image) {
        String imageAddress = (isRelease ? releaseResUrl : debugResUrl) + image;
        //确定宽度，高度按比例缩放
        int width = SizeUtils.dp2px(304);
        int height =SizeUtils.dp2px(322);
        return imageAddress + "-" + width + "*" + height + ".jpg";
    }

    public static String splitResStl(String stl) {
        String stlAddress = (isRelease ? releaseResUrl : debugResUrl) + stl;
        return stlAddress + ".mystl";
    }


    public static Boolean isLogin(MMKV kv) {
        String tokenInfo = null;
        if (kv != null) {
            tokenInfo = kv.decodeString("userTokenInfo");
        }
        return tokenInfo != null;
    }

    public static String getUseId(MMKV kv) {
        String userIdInfo = null;
        if (kv != null) {
            userIdInfo = kv.decodeString("userIdInfo");
        }
        return userIdInfo;
    }

    public static void skipNewPage(Context context, String mUrl) {
        if (mUrl == null || mUrl.isEmpty()) {
            return;
        }
        // 创建Uri对象
        Uri uri = Uri.parse(mUrl);
        String id = uri.getQueryParameter("id");
        String path = uri.getPath();
        if (mUrl.contains(getApiUrl())) {
            if (path != null && path.contains("article/home")) {
                //首页
                ((MainActivity) context).setTabSelect(0);
            } else if (path != null && path.contains("article/detail")) {
                //文章详情页
                ArticleDetailsActivity.start(context, id, false);
            } else if (path != null && path.contains("model/home")) {
                //模型首页
                ((MainActivity) context).setTabSelect(1);
            } else if (path != null && path.contains("model/detail")) {
                //	模型详情页
                ModelDetailsActivity.start(context, id,false);
            }
        } else {
            WebActivity.start(context, mUrl);
        }
    }
}
