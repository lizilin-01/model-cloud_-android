package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.http.bean.OrderRequestResponse;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：预计算商品价格
 */
public class OrderTrialApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/trial";
    }

    //地址id
    @HttpRename("addressId")
    private String addressId;

    //模型id集合
    @HttpRename("modelList")
    private List<OrderRequestResponse> modelList;

    //打印参数
    @HttpRename("printParams")
    private String printParams;

    public OrderTrialApi setAddressId(String addressId) {
        this.addressId = addressId;
        return this;
    }

    public OrderTrialApi setModelList(List<OrderRequestResponse> modelList) {
        this.modelList = modelList;
        return this;
    }

    public OrderTrialApi setPrintParams(String printParams) {
        this.printParams = printParams;
        return this;
    }

    public final static class Bean {
        private List<BeanInfo> list;
        private int totalPrice;//总价
        private int deductionBalance;//抵扣余额，单位为分

        public List<BeanInfo> getList() {
            return list;
        }

        public int getTotalPrice() {
            return totalPrice;
        }

        public int getDeductionBalance() {
            return deductionBalance;
        }
    }


    public final static class BeanInfo {
        private String priceLabel;//
        private String priceDesc;//
        private int price;//
        private int index;//

        public String getPriceLabel() {
            return priceLabel;
        }

        public String getPriceDesc() {
            return priceDesc;
        }

        public int getPrice() {
            return price;
        }

        public int getIndex() {
            return index;
        }
    }
}
