package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ModelResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：购物车相关接口
 */
public class AppAddShoppingApi implements IRequestApi {
    @Override
    public String getApi() {
        return "cart/add";
    }

    //模型id集合
    @HttpRename("modelIdList")
    private List<String> modelIdList;

    public AppAddShoppingApi setModelIdList(List<String> modelIdList) {
        this.modelIdList = modelIdList;
        return this;
    }


    public final static class Bean {
        private List<String> list;

        public List<String> getList() {
            return list;
        }
    }
}
