package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

import okhttp3.MultipartBody;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：反馈：提交反馈
 */
public class AppFeedbackCommitApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/feedback/commit";
    }

    private List<MultipartBody.Part> images;//汇款凭证

    //其他参数
    @HttpRename("param")
    private String param;

    public AppFeedbackCommitApi setImage(List<MultipartBody.Part> images) {
        this.images = images;
        return this;
    }

    public AppFeedbackCommitApi setParam(String param) {
        this.param = param;
        return this;
    }

    public final static class Bean {
        private String msg;//固定为：“提交成功！”

        public String getMsg() {
            return msg;
        }
    }
}
