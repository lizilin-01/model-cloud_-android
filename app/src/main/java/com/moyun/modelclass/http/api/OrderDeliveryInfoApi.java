package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：获取物流信息
 */
public class OrderDeliveryInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/getDeliveryInfo";
    }

    //订单id
    @HttpRename("orderId")
    private String orderId;

    public OrderDeliveryInfoApi setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public final static class Bean {
        private String id;//订单id
        private String orderId;//订单号
        private String userId;//用户userId
        private String deliveryNo;//快递编号
        private int deliveryState;//快递当前状态
        private String deliveryType;//快递公司类型
        private String updateTime;//最后更新时间
        private List<BeanInfo> itemList;//快递项
        private boolean isSign;//是否已签收
        private String takeTime;//截止最新轨迹总时长
        private String expName;//快递公司名字
        private String expSite;//快递公司官网
        private String expPhone;//快递公司电话
        private String courier;//快递员姓名
        private String courierPhone;//快递员电话
        private String logo;//快递公司logo

        public String getId() {
            return id;
        }

        public String getOrderId() {
            return orderId;
        }

        public String getUserId() {
            return userId;
        }

        public String getDeliveryNo() {
            return deliveryNo;
        }

        public int getDeliveryState() {
            return deliveryState;
        }

        public String getDeliveryType() {
            return deliveryType;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public List<BeanInfo> getItemList() {
            return itemList;
        }

        public boolean isSign() {
            return isSign;
        }

        public String getTakeTime() {
            return takeTime;
        }

        public String getExpName() {
            return expName;
        }

        public String getExpSite() {
            return expSite;
        }

        public String getExpPhone() {
            return expPhone;
        }

        public String getCourier() {
            return courier;
        }

        public String getCourierPhone() {
            return courierPhone;
        }

        public String getLogo() {
            return logo;
        }
    }

    public final static class BeanInfo {
        private String time;//item时间
        private String statusDesc;//状态描述，比如：【广州市】 快件到达 【广州中心】


        public String getTime() {
            return time;
        }

        public String getStatusDesc() {
            return statusDesc;
        }
    }
}
