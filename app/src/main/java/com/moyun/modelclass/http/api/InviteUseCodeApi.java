package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：使用邀请码
 */
public class InviteUseCodeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "invite/useCode";
    }

    //填入的邀请码，邀请码最大长度是6位
    @HttpRename("inviteCode")
    private String inviteCode;


    public InviteUseCodeApi setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
        return this;
    }

    public final static class Bean {
        private String useId;
        private int balance;

        public String getUseId() {
            return useId;
        }

        public int getBalance() {
            return balance;
        }
    }
}
