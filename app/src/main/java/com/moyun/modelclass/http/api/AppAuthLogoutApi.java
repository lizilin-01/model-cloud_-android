package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：退出登录
 */
public class AppAuthLogoutApi implements IRequestApi {
    @Override
    public String getApi() {
        return "user/logout";
    }

}
