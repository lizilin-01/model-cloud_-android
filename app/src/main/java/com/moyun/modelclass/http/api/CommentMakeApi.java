package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AddressResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：发表评论
 */
public class CommentMakeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "comment/make";
    }

    //评价类型：1、文章、2、模型
    @HttpRename("type")
    private int type;

    //支持多次评论一起发送
    @HttpRename("commentObjectList")
    private List<CommentMakeRequest> commentObjectList;

    public CommentMakeApi setType(int type) {
        this.type = type;
        return this;
    }

    public CommentMakeApi setCommentObjectList(List<CommentMakeRequest> commentObjectList) {
        this.commentObjectList = commentObjectList;
        return this;
    }

    public final static class CommentMakeRequest {
        //评论内容
        @HttpRename("commentContent")
        private String commentContent;

        //评论内容
        @HttpRename("images")
        private List<String> images;

        //对象id，模型id，文章id、订单id等等
        @HttpRename("objectId")
        private String objectId;

        //对象id，这个id在回复评论的时候使用，传入需要回复的评论id
        @HttpRename("targetId")
        private String targetId;

        //评论星级
        @HttpRename("star")
        private int star;

        public void setCommentContent(String commentContent) {
            this.commentContent = commentContent;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }

        public void setTargetId(String targetId) {
            this.targetId = targetId;
        }

        public void setStar(int star) {
            this.star = star;
        }
    }
}
