package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：搜索:热门话题
 */
public class AppVipSearchPopularApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/hotTags";
    }

    public static class Bean {
        private List<String> tags;

        public List<String> getTags() {
            return tags;
        }
    }
}
