package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：注销账号
 */
public class AppAuthLogoffApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/logoff";
    }

    public static class Bean {
        private String msg;

        public String getMsg() {
            return msg;
        }
    }
}
