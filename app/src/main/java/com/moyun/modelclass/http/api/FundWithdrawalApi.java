package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：资金提现到微信
 */
public class FundWithdrawalApi implements IRequestApi {
    @Override
    public String getApi() {
        return "fund/withdrawal";
    }

    //用户真实姓名
    @HttpRename("realName")
    private String realName;

    //提现金额，单位分
    @HttpRename("money")
    private double money;

    //支付宝收款账号
    @HttpRename("channelAccount")
    private String channelAccount;

    public FundWithdrawalApi setRealName(String realName) {
        this.realName = realName;
        return this;
    }

    public FundWithdrawalApi setMoney(double money) {
        this.money = money;
        return this;
    }

    public FundWithdrawalApi setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
        return this;
    }
}
