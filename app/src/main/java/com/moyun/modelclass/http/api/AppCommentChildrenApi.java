package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.comment.bean.SecondLevelBean;
import com.moyun.modelclass.http.bean.CommentResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：获取子评论
 */
public class AppCommentChildrenApi implements IRequestApi {
    @Override
    public String getApi() {
        return "comment/childrenList";
    }

    //评论对象类型，文章1，模型2
    @HttpRename("type")
    private int type;
    //评论对象id
    @HttpRename("objectId")
    private String objectId;
    //父评论id
    @HttpRename("parentCommentId")
    private String parentCommentId;
    //最后一条的id
    @HttpRename("lastId")
    private String lastId;
    //分页大小
    @HttpRename("pageSize")
    private int pageSize;


    public AppCommentChildrenApi setType(int type) {
        this.type = type;
        return this;
    }

    public AppCommentChildrenApi setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public AppCommentChildrenApi setParentCommentId(String parentCommentId) {
        this.parentCommentId = parentCommentId;
        return this;
    }

    public AppCommentChildrenApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public AppCommentChildrenApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public final static class Bean {
        private List<SecondLevelBean> comments;
        private int totalComment;//总数量

        public List<SecondLevelBean> getComments() {
            return comments;
        }

        public int getTotalComment() {
            return totalComment;
        }
    }
}
