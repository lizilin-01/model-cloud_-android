package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：用户保证金：充值
 */
public class AppVipWalletPayDepositPayApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/wallet/pay/depositPay";
    }

    //订单金额
    @HttpRename("payTotal")
    private double payTotal;
    //支付方式：1微信 2支付宝
    @HttpRename("paymentType")
    private int paymentType;

    public AppVipWalletPayDepositPayApi setPayTotal(double payTotal) {
        this.payTotal = payTotal;
        return this;
    }

    public AppVipWalletPayDepositPayApi setPaymentType(int paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public final static class Bean {
        private int paymentType;//支付方式：1微信 2支付宝 4对公汇款
        private String tradeNo;//流水号
        private int payStatus;//支付状态 -2支付失败 2支付中 3支付完成（这里理论上只有支付完成）
        private String errorMessage;//失败原因
        private String outTradeNo;
        private long payTime;
        private String appPayBody;
        private boolean success;
        private boolean fail;
        private boolean paying;

        public int getPaymentType() {
            return paymentType;
        }

        public String getTradeNo() {
            return tradeNo;
        }

        public int getPayStatus() {
            return payStatus;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public String getOutTradeNo() {
            return outTradeNo;
        }

        public long getPayTime() {
            return payTime;
        }

        public String getAppPayBody() {
            return appPayBody;
        }

        public boolean isSuccess() {
            return success;
        }

        public boolean isFail() {
            return fail;
        }

        public boolean isPaying() {
            return paying;
        }
    }
}
