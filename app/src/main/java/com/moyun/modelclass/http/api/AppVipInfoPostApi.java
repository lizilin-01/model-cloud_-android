package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import okhttp3.MultipartBody;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：修改个人信息
 */
public class AppVipInfoPostApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/info";
    }

    private MultipartBody.Part avatarFile;//头像文件

    //其他参数
    @HttpRename("param")
    private String param;

    public AppVipInfoPostApi setAvatarFile(MultipartBody.Part avatarFile) {
        this.avatarFile = avatarFile;
        return this;
    }

    public AppVipInfoPostApi setParam(String param) {
        this.param = param;
        return this;
    }

    public final static class Bean {
        private String msg;

        public String getMsg() {
            return msg;
        }
    }
}
