package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class ChatRemoveApi implements IRequestApi {
    public ChatRemoveApi(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    @Override
    public String getApi() {
        return "chat/remove";
    }

    @HttpRename("receiveUserId")
    private String receiveUserId;


    public static class Bean {
        private List<ChatBean> list;

        public List<ChatBean> getList() {
            return list;
        }
    }

    public static class ChatBean {
        public String receiveUserId;
        public String lastChat;
        public String lastTime;
        public int unReadCount;
    }

}
