package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：三方：绑定手机号：发送验证码
 */
public class AppCodeBindThirdUserNameApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/code/bindThirdUserName";
    }

    //手机号、邮箱
    @HttpRename("username")
    private String username;

    public AppCodeBindThirdUserNameApi setUsername(String username) {
        this.username = username;
        return this;
    }

    public final static class Bean {
        private String msg;
        private String code;
        private int expiration;

        public String getMsg() {
            return msg;
        }

        public String getCode() {
            return code;
        }

        public int getExpiration() {
            return expiration;
        }
    }
}
