package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：文章详情
 */
public class AppArticleDetailsApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/get";
    }

    //文章id
    @HttpRename("articleId")
    private String articleId;

    public AppArticleDetailsApi setArticleId(String articleId) {
        this.articleId = articleId;
        return this;
    }

    public final static class Bean {
        private String id;//文章id
        private String summary;//文章摘要
        private List<String> tags;//文章话题列表
        private String title;//标题
        private int scanCount;//浏览数量
        private int commentCount;//评论数量
        private int likeCount;//点赞数量
        private String content;//内容
        private String simpleImage;//头图
        private List<String> modelList;//模型列表
        private int articleState;//文章状态，1:草稿，2，待审核，3、已发布
        private String createUser;//作者的userId
        private String createUserAvatar;//作者的头像
        private String nickName;//作者昵称
        private String createTime;//创建时间
        private String updateTime;//最后更新时间

        public String getId() {
            return id;
        }

        public String getSummary() {
            return summary;
        }

        public List<String> getTags() {
            return tags;
        }

        public String getTitle() {
            return title;
        }

        public int getScanCount() {
            return scanCount;
        }

        public int getCommentCount() {
            return commentCount;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public String getContent() {
            return content;
        }

        public String getSimpleImage() {
            return simpleImage;
        }

        public List<String> getModelList() {
            return modelList;
        }

        public int getArticleState() {
            return articleState;
        }

        public String getCreateUser() {
            return createUser;
        }

        public String getCreateUserAvatar() {
            return createUserAvatar;
        }

        public String getNickName() {
            return nickName;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }
    }
}
