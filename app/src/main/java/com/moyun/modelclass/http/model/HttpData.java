package com.moyun.modelclass.http.model;

/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/EasyHttp
 * time   : 2019/05/19
 * desc   : 统一接口数据结构
 */
public class HttpData<T> {
    /**
     * 返回码
     */
    private int code;//返回码，code=1代表成功，其余都是代表失败
    /**
     * 提示语
     */
    private String message;//错误码提示内容
    /**
     * 数据
     */
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

}