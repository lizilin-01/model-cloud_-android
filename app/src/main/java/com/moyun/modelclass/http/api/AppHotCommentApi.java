package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.CommentResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：热评
 */
public class AppHotCommentApi implements IRequestApi {
    @Override
    public String getApi() {
        return "comment/hotComment";
    }

    //评论对象类型，文章1，模型2
    @HttpRename("type")
    private int type;
    //评论对象id
    @HttpRename("objectId")
    private String objectId;


    public AppHotCommentApi setType(int type) {
        this.type = type;
        return this;
    }

    public AppHotCommentApi setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }


    public final static class Bean {
        private List<CommentResponse> comments;
        private int totalComment;//总数量

        public List<CommentResponse> getComments() {
            return comments;
        }

        public int getTotalComment() {
            return totalComment;
        }
    }
}
