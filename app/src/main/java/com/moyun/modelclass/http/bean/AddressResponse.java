package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：收货地址
 */
public class AddressResponse implements Serializable {
    private String id;//地址id
    private String name;//收货人姓名
    private String mobile;//收货人手机号码
    private String areaCode;//地址code，请调用接口解析
    private String province;//省
    private String city;//市
    private String area;//区
    private String detail;//详细地址
    private boolean defaultFlag = false;//是否默认

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getArea() {
        return area;
    }

    public String getDetail() {
        return detail;
    }

    public boolean isDefaultFlag() {
        return defaultFlag;
    }
}
