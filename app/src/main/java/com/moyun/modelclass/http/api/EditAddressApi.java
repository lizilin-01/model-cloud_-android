package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：地址保存
 */
public class EditAddressApi implements IRequestApi {
    @Override
    public String getApi() {
        return "address/save";
    }

    //地址id，如果是空表示新增
    @HttpRename("id")
    private String id;
    //收件人
    @HttpRename("name")
    private String name;
    //收件人手机号码
    @HttpRename("mobile")
    private String mobile;
    //收件人地址code
    @HttpRename("areaCode")
    private String areaCode;
    //收件人详细地址
    @HttpRename("detail")
    private String detail;
    //是否默认
    @HttpRename("defaultFlag")
    private boolean defaultFlag = false;

    public EditAddressApi setId(String id) {
        this.id = id;
        return this;
    }

    public EditAddressApi setName(String name) {
        this.name = name;
        return this;
    }

    public EditAddressApi setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public EditAddressApi setAreaCode(String areaCode) {
        this.areaCode = areaCode;
        return this;
    }

    public EditAddressApi setDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public EditAddressApi setDefaultFlag(boolean defaultFlag) {
        this.defaultFlag = defaultFlag;
        return this;
    }

}
