package com.moyun.modelclass.http.api;

import android.os.Parcel;
import android.os.Parcelable;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：资源共享：查询资源共享列表
 */
public class AppSupplierApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/supplier";
    }

    //页数，从0开始
    @HttpRename("page")
    private int page;
    //条数
    @HttpRename("size")
    private int size;
    //关键词
    @HttpRename("name")
    private String name;
    public AppSupplierApi setPage(int page) {
        this.page = page;
        return this;
    }

    public AppSupplierApi setSize(int size) {
        this.size = size;
        return this;
    }

    public AppSupplierApi setName(String name) {
        this.name = name;
        return this;
    }


    public static class Bean {
        private int totalElements;
        private List<BeanInfo> content;

        public int getTotalElements() {
            return totalElements;
        }

        public List<BeanInfo> getContent() {
            return content;
        }
    }

    public static class BeanInfo implements Parcelable {
        private String id;//资源共享ID
        private Long createdTime;//创建时间
        private String name;//关键词
        private String link;//链接
        private Long establishTime;//成立时间
        private String image;//封面图
        private String bannerImages;//轮播图
        private String content;//图文详情

        public BeanInfo(){

        }

        protected BeanInfo(Parcel in) {
            id = in.readString();
            if (in.readByte() == 0) {
                createdTime = null;
            } else {
                createdTime = in.readLong();
            }
            name = in.readString();
            link = in.readString();
            if (in.readByte() == 0) {
                establishTime = null;
            } else {
                establishTime = in.readLong();
            }
            image = in.readString();
            bannerImages = in.readString();
            content = in.readString();
        }

        public static final Creator<BeanInfo> CREATOR = new Creator<BeanInfo>() {
            @Override
            public BeanInfo createFromParcel(Parcel in) {
                return new BeanInfo(in);
            }

            @Override
            public BeanInfo[] newArray(int size) {
                return new BeanInfo[size];
            }
        };

        public String getId() {
            return id;
        }

        public Long getCreatedTime() {
            return createdTime;
        }

        public String getName() {
            return name;
        }

        public String getLink() {
            return link;
        }

        public Long getEstablishTime() {
            return establishTime;
        }

        public String getImage() {
            return image;
        }

        public String getBannerImages() {
            return bannerImages;
        }

        public String getContent() {
            return content;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            if (createdTime == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeLong(createdTime);
            }
            dest.writeString(name);
            dest.writeString(link);
            if (establishTime == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeLong(establishTime);
            }
            dest.writeString(image);
            dest.writeString(bannerImages);
            dest.writeString(content);
        }
    }
}
