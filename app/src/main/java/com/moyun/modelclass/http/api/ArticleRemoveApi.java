package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：删除文章
 */
public class ArticleRemoveApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/remove";
    }

    //文章id
    @HttpRename("articleId")
    private String articleId;

    public ArticleRemoveApi setArticleId(String articleId) {
        this.articleId = articleId;
        return this;
    }
}
