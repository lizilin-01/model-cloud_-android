package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ModelResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：获取关联模型
 */
public class ModelRelateModelListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/relateModelList";
    }

    //注意这边不是模型Id，是返回值里面的index
    @HttpRename("index")
    private int index;

    //模型关联类型，1：全部，2：已上传，3：已购买
    @HttpRename("modelRelateType")
    private int modelRelateType;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    public ModelRelateModelListApi setIndex(int index) {
        this.index = index;
        return  this;
    }

    public ModelRelateModelListApi setModelRelateType(int modelRelateType) {
        this.modelRelateType = modelRelateType;
        return  this;
    }

    public ModelRelateModelListApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return  this;
    }

    public static class Bean {
        private List<BeanInfo> list;

        public List<BeanInfo> getList() {
            return list;
        }
    }
    public static class BeanInfo {
        private int index;
        private String modelId;
        private String time;

        public int getIndex() {
            return index;
        }

        public String getModelId() {
            return modelId;
        }

        public String getTime() {
            return time;
        }
    }
}
