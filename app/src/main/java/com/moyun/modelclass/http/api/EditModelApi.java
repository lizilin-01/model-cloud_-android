package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * tip：模型保存
 */
public class EditModelApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/save";
    }

    //文章id，为空就表示新增
    @HttpRename("id")
    private String id;

    //模型封面(头图)
    @HttpRename("coverImage")
    private String coverImage;

    //标题
    @HttpRename("title")
    private String title;

    //模型描述
    @HttpRename("desc")
    private String desc;

    //模型图片
    @HttpRename("images")
    private List<String> images;

    //模型文件
    @HttpRename("files")
    private List<String> files;

    //完整件或者组合件
    @HttpRename("onePiece")
    private List<String> onePiece;

    //额外文件
    @HttpRename("otherFile")
    private List<String> otherFile;

    //是否开放别人浏览
    @HttpRename("openFlag")
    private boolean openFlag;

    //模型价格
    @HttpRename("price")
    private double price;

    //是否原创
    @HttpRename("original")
    private boolean original;

    //类别
    @HttpRename("category")
    private int category;


    public EditModelApi setId(String id) {
        this.id = id;
        return this;
    }

    public EditModelApi setCoverImage(String coverImage) {
        this.coverImage = coverImage;
        return this;
    }

    public EditModelApi setTitle(String title) {
        this.title = title;
        return this;
    }

    public EditModelApi setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public EditModelApi setImages(List<String> images) {
        this.images = images;
        return this;
    }

    public EditModelApi setFiles(List<String> files) {
        this.files = files;
        return this;
    }

    public EditModelApi setOnePiece(List<String> onePiece) {
        this.onePiece = onePiece;
        return this;
    }

    public EditModelApi setOtherFile(List<String> otherFile) {
        this.otherFile = otherFile;
        return this;
    }

    public EditModelApi setOpenFlag(boolean openFlag) {
        this.openFlag = openFlag;
        return this;
    }

    public EditModelApi setPrice(double price) {
        this.price = price;
        return this;
    }

    public EditModelApi setOriginal(boolean original) {
        this.original = original;
        return this;
    }

    public EditModelApi setCategory(int category) {
        this.category = category;
        return this;
    }
}
