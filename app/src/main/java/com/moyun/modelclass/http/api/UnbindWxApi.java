package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：解绑微信
 */
public class UnbindWxApi implements IRequestApi {
    @Override
    public String getApi() {
        return "user/unbindWechat";
    }

    //验证码
    @HttpRename("verifyCode")
    private String verifyCode;

    //guid
    @HttpRename("guid")
    private String guid;


    public UnbindWxApi setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
        return this;
    }

    public UnbindWxApi setGuid(String guid) {
        this.guid = guid;
        return this;
    }
}
