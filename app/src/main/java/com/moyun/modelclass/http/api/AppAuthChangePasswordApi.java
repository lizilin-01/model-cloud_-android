package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：忘记密码：修改密码
 */
public class AppAuthChangePasswordApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/auth/changePassword";
    }

    //手机号、邮箱
    @HttpRename("username")
    private String username;
    //验证码
    @HttpRename("code")
    private String code;
    //密码
    @HttpRename("password")
    private String password;

    public AppAuthChangePasswordApi setUsername(String username) {
        this.username = username;
        return this;
    }

    public AppAuthChangePasswordApi setCode(String code) {
        this.code = code;
        return this;
    }

    public AppAuthChangePasswordApi setPassword(String password) {
        this.password = password;
        return this;
    }

    public final static class Bean {
        private String msg;

        public String getMsg() {
            return msg;
        }

    }
}
