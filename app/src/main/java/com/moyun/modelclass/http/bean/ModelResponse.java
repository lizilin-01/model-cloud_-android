package com.moyun.modelclass.http.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：模型
 */
public class ModelResponse implements Serializable {
    private String id;//模型id
    private String title;//模型标题
    private String coverImage;//模型封面图
    private String createUser;//
    private boolean openFlag = false;//是否开放其他人浏览
    private int price;//价格，单位分
    private boolean original = true;//是否原创
    private int saleCount;//售卖数量
    private int scanCount;//浏览数量
    private int likeCount;//收藏数量
    private boolean canBuy = true;//模型是否可以购买，商品title会显示具体原因
    private List<ModelSizeResponse> modelSizeList;//可选模型尺寸
    private List<Integer> xyz;//长宽高，单位是mm
    private String updateTime;//最后更新时间
    private boolean descOk;//模型基础数据是否已经生成好，生成好就可以下单了
    private List<String> files;
    private List<String> onePiece;
    private List<String> otherFile;

    private ModelSizeResponse modelSize;//选中的尺寸
    private int selectPosition = 0;//选中的尺寸的位置

    private int addCount = 1;//添加数量

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public boolean isOpenFlag() {
        return openFlag;
    }

    public int getPrice() {
        return price;
    }

    public boolean isOriginal() {
        return original;
    }

    public int getSaleCount() {
        return saleCount;
    }

    public int getScanCount() {
        return scanCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public boolean isCanBuy() {
        return canBuy;
    }

    public boolean isChecked = false;
    public boolean canDelete = false;

    public List<ModelSizeResponse> getModelSizeList() {
        return modelSizeList;
    }

    public List<Integer> getXyz() {
        return xyz;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setAddCount(int addCount) {
        this.addCount = addCount;
    }

    public int getAddCount() {
        return addCount;
    }

    public ModelSizeResponse getModelSize() {
        return modelSize;
    }

    public void setModelSize(ModelSizeResponse modelSize) {
        this.modelSize = modelSize;
    }

    public int getSelectPosition() {
        return selectPosition;
    }

    public void setSelectPosition(int selectPosition) {
        this.selectPosition = selectPosition;
    }

    public String getCreateUser() {
        return createUser;
    }

    public boolean isDescOk() {
        return descOk;
    }

    public List<String> getFiles() {
        return files;
    }

    public List<String> getOnePiece() {
        return onePiece;
    }

    public List<String> getOtherFile() {
        return otherFile;
    }

    public static class ModelSizeResponse {
        private float size;//选中的大小
        private String sizeLabel;//选中的字段显示
        private String sizeDesc;//选中的描述

        public float getSize() {
            return size;
        }

        public String getSizeLabel() {
            return sizeLabel;
        }

        public String getSizeDesc() {
            return sizeDesc;
        }
    }
}
