package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * tip：发表评论
 */
public class OrderEvaluateApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/evaluate";
    }

    //订单id
    @HttpRename("orderId")
    private String orderId;

    //支持多次评论一起发送
    @HttpRename("commentObjectList")
    private List<OrderEvaluateRequest> commentObjectList;

    public OrderEvaluateApi setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public OrderEvaluateApi setCommentObjectList(List<OrderEvaluateRequest> commentObjectList) {
        this.commentObjectList = commentObjectList;
        return this;
    }

    public final static class OrderEvaluateRequest {
        //评论内容
        @HttpRename("commentContent")
        private String commentContent;

        //评论内容
        @HttpRename("images")
        private List<String> images;

        //评价类型：1、制造工艺 2、物流 3、模型
        @HttpRename("type")
        private int type;

        //对象id，这个id在回复评论的时候使用，传入需要回复的评论id
        @HttpRename("targetId")
        private String targetId;

        //评论星级
        @HttpRename("star")
        private int star;

        public void setCommentContent(String commentContent) {
            this.commentContent = commentContent;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public void setType(int type) {
            this.type = type;
        }

        public void setTargetId(String targetId) {
            this.targetId = targetId;
        }

        public void setStar(int star) {
            this.star = star;
        }
    }
}
