package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：获取文件上传token
 */
public class GenerateStsTokenApi implements IRequestApi {

    @Override
    public String getApi() {
        return "file/generateStsToken";
    }

    //上传文件大小
    @HttpRename("fileSize")
    private int fileSize;

    //是否是其他文件
    @HttpRename("isOther")
    private boolean isOther;

    public GenerateStsTokenApi setFileSize(int fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public GenerateStsTokenApi setOther(boolean other) {
        isOther = other;
        return this;
    }

    public static class Bean {
        private String accessKeyId;//
        private String accessKeySecret;//
        private String expiration;//
        private String securityToken;//
        private String bucketName;//
        private String fileDir;//
        private String objectKey;//
        private String fileId;//
        private String callbackServer;//

        public String getAccessKeyId() {
            return accessKeyId;
        }

        public String getAccessKeySecret() {
            return accessKeySecret;
        }

        public String getExpiration() {
            return expiration;
        }

        public String getSecurityToken() {
            return securityToken;
        }

        public String getBucketName() {
            return bucketName;
        }

        public String getFileDir() {
            return fileDir;
        }

        public String getObjectKey() {
            return objectKey;
        }

        public String getFileId() {
            return fileId;
        }

        public String getCallbackServer() {
            return callbackServer;
        }
    }
}
