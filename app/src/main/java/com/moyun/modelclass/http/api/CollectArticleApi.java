package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 首页：点赞或者收藏文章
 */
public class CollectArticleApi implements IRequestApi {
    @Override
    public String getApi() {
        return "likes/article";
    }

    //文章id
    @HttpRename("articleId")
    private String articleId;

    //收藏或者取消收藏
    @HttpRename("likes")
    private boolean likes;

    public CollectArticleApi setArticleId(String articleId) {
        this.articleId = articleId;
        return this;
    }

    public CollectArticleApi setLikes(boolean likes) {
        this.likes = likes;
        return this;
    }

    public static class Bean {
        private boolean follow;

        public boolean isFollow() {
            return follow;
        }
    }
}
