package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：1.获取个人信息
 */
public class AppAuthInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/auth/info";
    }
}
