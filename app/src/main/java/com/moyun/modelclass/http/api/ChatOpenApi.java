package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/5/610:34 下午
 * tip：
 */
public class ChatOpenApi implements IRequestApi {

    @Override
    public String getApi() {
        return "chat/openChat";
    }

    @HttpRename("receiveUserId")
    private String receiveUserId;

    public ChatOpenApi setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
        return this;
    }

    public final static class Bean {

        public UserInfoVO userInfoVO;
        public boolean canCheckInfo;
        public boolean canChat;
        public String phone;

        public static class UserInfoVO {
            public String userId;
            public String nickName;
            public String avatar;
            public String desc;
        }
    }
}
