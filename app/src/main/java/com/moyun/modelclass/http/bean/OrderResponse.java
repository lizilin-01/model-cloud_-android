package com.moyun.modelclass.http.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：订单
 */
public class OrderResponse implements Serializable {
    private String id;//订单id
    private String orderNo;//订单号
    private String createTime;//创建时间
    private String expireTime;//过期时间
    private String payTime;//支付时间
    private int orderState;//订单状态
    private int  orderStateVO;//对外展示的订单状态，分别为：待付款、待发货、待收货、待评价、已完成、已关闭
    private List<OrderModelResponse> modelList;//模型列表
    private int totalPrice;//总金额
    private String orderStateVODesc;//订单状态描述

    public String getId() {
        return id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public int getOrderState() {
        return orderState;
    }

    public int getOrderStateVO() {
        return orderStateVO;
    }

    public List<OrderModelResponse> getModelList() {
        return modelList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public String getOrderStateVODesc() {
        return orderStateVODesc;
    }
}
