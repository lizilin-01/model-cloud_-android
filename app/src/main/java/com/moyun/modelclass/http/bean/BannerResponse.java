package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：banner
 */
public class BannerResponse implements Serializable {
    private String id;//项目宣传轮播图ID
    private Long createdTime;//创建时间
    private String image;//图片
    private int sort;//排序
    private int jump;//是否跳转项目宣传 1是0否
    private String publicityId;//项目宣传ID

    private int type;//跳转类型 0无跳转 2页面跳转
    private String url;//外部链接（type=1有效）


    public String getId() {
        return id;
    }

    public Long getCreatedTime() {
        return createdTime;
    }

    public String getImage() {
        return image;
    }

    public int getSort() {
        return sort;
    }

    public int getJump() {
        return jump;
    }

    public String getPublicityId() {
        return publicityId;
    }

    public int getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }
}
