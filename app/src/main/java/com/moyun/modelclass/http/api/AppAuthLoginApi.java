package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：验证码登录
 */
public class AppAuthLoginApi implements IRequestApi {
    @Override
    public String getApi() {
        return "user/registerOrLogin";
    }

    //手机号
    @HttpRename("mobile")
    private String mobile;
    //随机id
    @HttpRename("guid")
    private String guid;
    //验证码
    @HttpRename("verifyCode")
    private String verifyCode;


    public AppAuthLoginApi setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public AppAuthLoginApi setGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public AppAuthLoginApi setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
        return this;
    }

    public final static class Bean {
        private String token;//后续请求使用token
        private String expireTime;//过期时间
        private String userId;//用户userId,app可以不用缓存，所有的接口都支持传入token转userId
//        private boolean isNew;//是否新用户

        public String getToken() {
            return token;
        }

        public String getExpireTime() {
            return expireTime;
        }

        public String getUserId() {
            return userId;
        }

//        public Boolean getNew() {
//            return isNew;
//        }
    }
}
