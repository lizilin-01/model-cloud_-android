package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class ShoppingCartListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "cart/get";
    }


    public static class Bean {
        public List<ShoppingItem> modelList;
    }

    public static class ShoppingItem {
        public String modelId;
        public String createTime;
    }

}
