package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ModelResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：获取个人模型列表
 */
public class MyModelApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/onesModel";
    }

    //查询的用户id
    @HttpRename("userId")
    private String userId;

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //搜索内容
    @HttpRename("search")
    private String search;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    public MyModelApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public MyModelApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public MyModelApi setSearch(String search) {
        this.search = search;
        return this;
    }

    public MyModelApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public static class Bean {
        private List<ModelResponse> list;

        public List<ModelResponse> getList() {
            return list;
        }
    }
}
