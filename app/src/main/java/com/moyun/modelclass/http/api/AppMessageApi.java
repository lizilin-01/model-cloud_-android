package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：系统消息：消息列表
 */
public class AppMessageApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/message";
    }

    //页数，从0开始
    @HttpRename("page")
    private int page;

    //条数
    @HttpRename("size")
    private int size;

    public AppMessageApi setPage(int page) {
        this.page = page;
        return this;
    }

    public AppMessageApi setSize(int size) {
        this.size = size;
        return this;
    }

    public final static class Bean {
        private List<BeanInfo> content;
        private int totalElements;

        public List<BeanInfo> getContent() {
            return content;
        }

        public int getTotalElements() {
            return totalElements;
        }
    }

    public final static class BeanInfo {
        private String id;//消息ID
        private Long createdTime;//创建时间
        private String createdBy;//createdBy
        private int isDeleted;//isDeleted
        private int type;//消息类型 1后台消息
        private int toType;//用户类型 10全部 11指定
        private int status;//状态 1已发布
        private String publishBy;//publishBy
        private Long publishTime;//发布时间
        private String title;//标题
        private String content;//详情
        private int messageTos;//messageTos
        private int readCount;//已阅读次数
        private String url;

        public String getId() {
            return id;
        }

        public Long getCreatedTime() {
            return createdTime;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public int getType() {
            return type;
        }

        public int getToType() {
            return toType;
        }

        public int getStatus() {
            return status;
        }

        public String getPublishBy() {
            return publishBy;
        }

        public Long getPublishTime() {
            return publishTime;
        }

        public String getTitle() {
            return title;
        }

        public String getContent() {
            return content;
        }

        public int getMessageTos() {
            return messageTos;
        }

        public int getReadCount() {
            return readCount;
        }

        public String getUrl() {
            return url;
        }
    }
}
