package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/188:48 下午
 * tip：反馈：提交反馈
 */
public class FeedbackCommitResponse implements Serializable {
    private String content;//反馈详情

    public FeedbackCommitResponse(String content) {
        this.content = content;
    }
}
