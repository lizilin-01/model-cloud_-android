package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ArticleResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class HomeFindApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/list";
    }

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    //搜索内容，支持标题，话题，暂不支持正文
    @HttpRename("searchText")
    private String searchText;

    //作者id，可以用作个人简介中
    @HttpRename("authorUserId")
    private String authorUserId;

    //排序方式,1、默认 2、热度 3、发布日期
    @HttpRename("sortType")
    private int sortType;


    public HomeFindApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public HomeFindApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public HomeFindApi setSearchText(String searchText) {
        this.searchText = searchText;
        return this;
    }

    public HomeFindApi setAuthorUserId(String authorUserId) {
        this.authorUserId = authorUserId;
        return this;
    }

    public HomeFindApi setSortType(int sortType) {
        this.sortType = sortType;
        return this;
    }

    public static class Bean {
        private List<ArticleResponse> list;

        public List<ArticleResponse> getList() {
            return list;
        }
    }

}
