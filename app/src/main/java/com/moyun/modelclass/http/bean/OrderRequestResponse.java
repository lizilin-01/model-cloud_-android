package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：订单请求
 */
public class OrderRequestResponse implements Serializable {
    private String modelId;//模型id
    private int count;//购买数量
    private float size;//尺寸，注意请使用接口返回的参数,前端显示50%，80%，100%，150%，200%

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setSize(float size) {
        this.size = size;
    }
}
