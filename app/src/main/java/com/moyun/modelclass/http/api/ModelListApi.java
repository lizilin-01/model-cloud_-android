package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ModelResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：获取模型列表
 */
public class ModelListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/list";
    }

    //搜索类别,1:推荐，2、分类1、3、分类2
    @HttpRename("type")
    private int type;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //搜索内容，仅支持title搜索
    @HttpRename("searchText")
    private String searchText;

    public ModelListApi setType(int type) {
        this.type = type;
        return this;
    }

    public ModelListApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public ModelListApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public ModelListApi setSearchText(String searchText) {
        this.searchText = searchText;
        return this;
    }

    public static class Bean {
        private List<ModelResponse> list;

        public List<ModelResponse> getList() {
            return list;
        }
    }
}
