package com.moyun.modelclass.http.api;

import androidx.annotation.NonNull;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.moyun.modelclass.http.CommonConfiguration;

import java.io.File;

/**
 * 作者：Meteor
 * tip：文件上传相关接口：上传STL文件
 */
public class UploadMainStlFileApi implements IRequestServer, IRequestApi  {
    @NonNull
    @Override
    public String getHost() {
        return CommonConfiguration.getUploadUrl();
    }

    @Override
    public String getApi() {
        return "file/uploadSTL";
    }

    //文件名
    @HttpRename("fileName")
    private String fileName;

    //文件
    @HttpRename("file")
    private File file;

    public UploadMainStlFileApi setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public UploadMainStlFileApi setFile(File file) {
        this.file = file;
        return this;
    }

    public static class Bean {
        private String fileId;//stl  id
        private String fileName;//stl 文件名
        private String url;//stl 地址

        public String getFileId() {
            return fileId;
        }

        public String getFileName() {
            return fileName;
        }

        public String getUrl() {
            return url;
        }
    }
}
