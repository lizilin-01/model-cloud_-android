package com.moyun.modelclass.http.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：用工大厅：需求列表
 */
public class ReleaseEmploymentResponse implements Serializable {
    private List<BeanInfo> content;
    private int totalElements;

    public List<BeanInfo> getContent() {
        return content;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public static class BeanInfo implements Parcelable {
        private String id;//用工需求ID
        private String vipId;//发包方用户id
        private int vipType;//发包方用户类型
        private String vipNickName;//发包方昵称
        private String vipName;//发包方用户实名
        private String vipUserName;//发包方实名
        private String vipAvatar;//发包方头像
        private long createdTime;//创建时间
        private long checkTime;//审核时间
        private long signTime;//发起签约时间
        private long constructionTime;//开始施工时间
        private long cancelTime;//取消时间
        private int type;//类型：1大包(总包) 2长期 3零散 4技术(小时) 5商务
        private int status;//状态 -2已取消 -1审核失败 0审核中 1审核通过/招标中 2审核通过/派单中 5签约中 6施工中
        private String workerType1Id;//工作范围一级id
        private String workerType1Name;//工作范围一级名称
        private String workerType2Id;//工作范围二级id
        private String workerType2Name;//工作范围二级名称
        private String workerType3Id;//工作范围三级id
        private String workerType3Name;//工作范围三级名称
        private String workerProvince;//工作地址：省
        private String workerCity;//工作地址：市
        private String workerArea;//工作地址：区
        private String workerAddress;//工作地址：地址
        private double workerAddressLatitude;//工作地址：纬度
        private double workerAddressLongitude;//工作地址：经度
        private String workerTitle;//用工标题
        private String workerContent;//用工内容
        private int workerAccommodationType;//食宿问题 1包吃包住 2包吃不包住 3不包吃包住 4不包吃不包住
        private int workerBidLimit;//投标名额
        private String workerSchedule;//截止日期(老：用工工期)
        private String planCabinet;//进场计划：柜体
        private String planElement;//进场计划：元件
        private String planCopper;//进场计划：铜排
        private String planAccessory;//进场计划：辅料
        private double workerAmount;//用工工资

        private String workerStartTime;//开始时间
        private String workerEndTime;//完工工期

        //长期
        private int level1Count;//用工等级：一次高级工人数
        private Double level1Price;//用工等级：一次高级工单价
        private int level2Count;//用工等级：一次中级工人数
        private Double level2Price;//用工等级：一次中级工单价
        private int level3Count;//用工等级：二次高级工人数
        private Double level3Price;//用工等级：二次高级工单价

        private int level4Count;//用工等级：二次中级工人数
        private Double level4Price;//用工等级：二次中级工单价
        private int level5Count;//用工等级：初级工人数
        private Double level5Price;//用工等级：初级工单价
        private int level6Count;//用工等级：技术商务员人数
        private Double level6Price;//用工等级：技术商务员单价

        private int bidCount;//已投标人数
        private int projectAdmin;//是否为发包方 1是0否
        private int hasBid;//是否已投标
        private int hasCollect;//是否已收藏
        private int saleCount;//订单数量
        private int hasSale;//当前用户是否已生成订单:1已投 ，其他未投
        private int saleLevel1Count;//订单数量：高级
        private int saleLevel2Count;//订单数量：中级
        private int saleLevel3Count;//订单数量：低级
        private int enableBid;//是否允许投标
        private int enableGrab;//是否允许抢单
        private double bidAmount;//投标金额

        private String rejectReason;//拒绝理由
        private String vipWalletLogId;//
        private int recommend;//是否推荐  0否1是
        private long recommendTime;//是否推荐  时间
        private long applyCheckTime;//
        private long doneTime;//
        private String workerImages;//图片列表


        protected BeanInfo(Parcel in) {
            id = in.readString();
            vipId = in.readString();
            vipType = in.readInt();
            vipNickName = in.readString();
            vipName = in.readString();
            vipUserName = in.readString();
            vipAvatar = in.readString();
            createdTime = in.readLong();
            checkTime = in.readLong();
            signTime = in.readLong();
            constructionTime = in.readLong();
            cancelTime = in.readLong();
            type = in.readInt();
            status = in.readInt();
            workerType1Id = in.readString();
            workerType1Name = in.readString();
            workerType2Id = in.readString();
            workerType2Name = in.readString();
            workerType3Id = in.readString();
            workerType3Name = in.readString();
            workerProvince = in.readString();
            workerCity = in.readString();
            workerArea = in.readString();
            workerAddress = in.readString();
            workerAddressLatitude = in.readDouble();
            workerAddressLongitude = in.readDouble();
            workerTitle = in.readString();
            workerContent = in.readString();
            workerAccommodationType = in.readInt();
            workerBidLimit = in.readInt();
            workerSchedule = in.readString();
            planCabinet = in.readString();
            planElement = in.readString();
            planCopper = in.readString();
            planAccessory = in.readString();
            workerAmount = in.readDouble();
            workerStartTime = in.readString();
            workerEndTime = in.readString();
            level1Count = in.readInt();
            if (in.readByte() == 0) {
                level1Price = null;
            } else {
                level1Price = in.readDouble();
            }
            level2Count = in.readInt();
            if (in.readByte() == 0) {
                level2Price = null;
            } else {
                level2Price = in.readDouble();
            }
            level3Count = in.readInt();
            if (in.readByte() == 0) {
                level3Price = null;
            } else {
                level3Price = in.readDouble();
            }
            level4Count = in.readInt();
            if (in.readByte() == 0) {
                level4Price = null;
            } else {
                level4Price = in.readDouble();
            }
            level5Count = in.readInt();
            if (in.readByte() == 0) {
                level5Price = null;
            } else {
                level5Price = in.readDouble();
            }
            level6Count = in.readInt();
            if (in.readByte() == 0) {
                level6Price = null;
            } else {
                level6Price = in.readDouble();
            }
            bidCount = in.readInt();
            projectAdmin = in.readInt();
            hasBid = in.readInt();
            hasCollect = in.readInt();
            saleCount = in.readInt();
            hasSale = in.readInt();
            saleLevel1Count = in.readInt();
            saleLevel2Count = in.readInt();
            saleLevel3Count = in.readInt();
            enableBid = in.readInt();
            enableGrab = in.readInt();
            bidAmount = in.readDouble();
            rejectReason = in.readString();
            vipWalletLogId = in.readString();
            recommend = in.readInt();
            recommendTime = in.readLong();
            applyCheckTime = in.readLong();
            doneTime = in.readLong();
            workerImages = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(vipId);
            dest.writeInt(vipType);
            dest.writeString(vipNickName);
            dest.writeString(vipName);
            dest.writeString(vipUserName);
            dest.writeString(vipAvatar);
            dest.writeLong(createdTime);
            dest.writeLong(checkTime);
            dest.writeLong(signTime);
            dest.writeLong(constructionTime);
            dest.writeLong(cancelTime);
            dest.writeInt(type);
            dest.writeInt(status);
            dest.writeString(workerType1Id);
            dest.writeString(workerType1Name);
            dest.writeString(workerType2Id);
            dest.writeString(workerType2Name);
            dest.writeString(workerType3Id);
            dest.writeString(workerType3Name);
            dest.writeString(workerProvince);
            dest.writeString(workerCity);
            dest.writeString(workerArea);
            dest.writeString(workerAddress);
            dest.writeDouble(workerAddressLatitude);
            dest.writeDouble(workerAddressLongitude);
            dest.writeString(workerTitle);
            dest.writeString(workerContent);
            dest.writeInt(workerAccommodationType);
            dest.writeInt(workerBidLimit);
            dest.writeString(workerSchedule);
            dest.writeString(planCabinet);
            dest.writeString(planElement);
            dest.writeString(planCopper);
            dest.writeString(planAccessory);
            dest.writeDouble(workerAmount);
            dest.writeString(workerStartTime);
            dest.writeString(workerEndTime);
            dest.writeInt(level1Count);
            if (level1Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(level1Price);
            }
            dest.writeInt(level2Count);
            if (level2Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(level2Price);
            }
            dest.writeInt(level3Count);
            if (level3Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(level3Price);
            }
            dest.writeInt(level4Count);
            if (level4Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(level4Price);
            }
            dest.writeInt(level5Count);
            if (level5Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(level5Price);
            }
            dest.writeInt(level6Count);
            if (level6Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(level6Price);
            }
            dest.writeInt(bidCount);
            dest.writeInt(projectAdmin);
            dest.writeInt(hasBid);
            dest.writeInt(hasCollect);
            dest.writeInt(saleCount);
            dest.writeInt(hasSale);
            dest.writeInt(saleLevel1Count);
            dest.writeInt(saleLevel2Count);
            dest.writeInt(saleLevel3Count);
            dest.writeInt(enableBid);
            dest.writeInt(enableGrab);
            dest.writeDouble(bidAmount);
            dest.writeString(rejectReason);
            dest.writeString(vipWalletLogId);
            dest.writeInt(recommend);
            dest.writeLong(recommendTime);
            dest.writeLong(applyCheckTime);
            dest.writeLong(doneTime);
            dest.writeString(workerImages);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<BeanInfo> CREATOR = new Creator<BeanInfo>() {
            @Override
            public BeanInfo createFromParcel(Parcel in) {
                return new BeanInfo(in);
            }

            @Override
            public BeanInfo[] newArray(int size) {
                return new BeanInfo[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVipId() {
            return vipId;
        }

        public void setVipId(String vipId) {
            this.vipId = vipId;
        }

        public int getVipType() {
            return vipType;
        }

        public void setVipType(int vipType) {
            this.vipType = vipType;
        }

        public String getVipNickName() {
            return vipNickName;
        }

        public void setVipNickName(String vipNickName) {
            this.vipNickName = vipNickName;
        }

        public String getVipName() {
            return vipName;
        }

        public void setVipName(String vipName) {
            this.vipName = vipName;
        }

        public String getVipUserName() {
            return vipUserName;
        }

        public void setVipUserName(String vipUserName) {
            this.vipUserName = vipUserName;
        }

        public String getVipAvatar() {
            return vipAvatar;
        }

        public void setVipAvatar(String vipAvatar) {
            this.vipAvatar = vipAvatar;
        }

        public long getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(long createdTime) {
            this.createdTime = createdTime;
        }

        public long getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(long checkTime) {
            this.checkTime = checkTime;
        }

        public long getSignTime() {
            return signTime;
        }

        public void setSignTime(long signTime) {
            this.signTime = signTime;
        }

        public long getConstructionTime() {
            return constructionTime;
        }

        public void setConstructionTime(long constructionTime) {
            this.constructionTime = constructionTime;
        }

        public long getCancelTime() {
            return cancelTime;
        }

        public void setCancelTime(long cancelTime) {
            this.cancelTime = cancelTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getWorkerType1Id() {
            return workerType1Id;
        }

        public void setWorkerType1Id(String workerType1Id) {
            this.workerType1Id = workerType1Id;
        }

        public String getWorkerType1Name() {
            return workerType1Name;
        }

        public void setWorkerType1Name(String workerType1Name) {
            this.workerType1Name = workerType1Name;
        }

        public String getWorkerType2Id() {
            return workerType2Id;
        }

        public void setWorkerType2Id(String workerType2Id) {
            this.workerType2Id = workerType2Id;
        }

        public String getWorkerType2Name() {
            return workerType2Name;
        }

        public void setWorkerType2Name(String workerType2Name) {
            this.workerType2Name = workerType2Name;
        }

        public String getWorkerType3Id() {
            return workerType3Id;
        }

        public void setWorkerType3Id(String workerType3Id) {
            this.workerType3Id = workerType3Id;
        }

        public String getWorkerType3Name() {
            return workerType3Name;
        }

        public void setWorkerType3Name(String workerType3Name) {
            this.workerType3Name = workerType3Name;
        }

        public String getWorkerProvince() {
            return workerProvince;
        }

        public void setWorkerProvince(String workerProvince) {
            this.workerProvince = workerProvince;
        }

        public String getWorkerCity() {
            return workerCity;
        }

        public void setWorkerCity(String workerCity) {
            this.workerCity = workerCity;
        }

        public String getWorkerArea() {
            return workerArea;
        }

        public void setWorkerArea(String workerArea) {
            this.workerArea = workerArea;
        }

        public String getWorkerAddress() {
            return workerAddress;
        }

        public void setWorkerAddress(String workerAddress) {
            this.workerAddress = workerAddress;
        }

        public double getWorkerAddressLatitude() {
            return workerAddressLatitude;
        }

        public void setWorkerAddressLatitude(double workerAddressLatitude) {
            this.workerAddressLatitude = workerAddressLatitude;
        }

        public double getWorkerAddressLongitude() {
            return workerAddressLongitude;
        }

        public void setWorkerAddressLongitude(double workerAddressLongitude) {
            this.workerAddressLongitude = workerAddressLongitude;
        }

        public String getWorkerTitle() {
            return workerTitle;
        }

        public void setWorkerTitle(String workerTitle) {
            this.workerTitle = workerTitle;
        }

        public String getWorkerContent() {
            return workerContent;
        }

        public void setWorkerContent(String workerContent) {
            this.workerContent = workerContent;
        }

        public int getWorkerAccommodationType() {
            return workerAccommodationType;
        }

        public void setWorkerAccommodationType(int workerAccommodationType) {
            this.workerAccommodationType = workerAccommodationType;
        }

        public int getWorkerBidLimit() {
            return workerBidLimit;
        }

        public void setWorkerBidLimit(int workerBidLimit) {
            this.workerBidLimit = workerBidLimit;
        }

        public String getWorkerSchedule() {
            return workerSchedule;
        }

        public void setWorkerSchedule(String workerSchedule) {
            this.workerSchedule = workerSchedule;
        }

        public String getPlanCabinet() {
            return planCabinet;
        }

        public void setPlanCabinet(String planCabinet) {
            this.planCabinet = planCabinet;
        }

        public String getPlanElement() {
            return planElement;
        }

        public void setPlanElement(String planElement) {
            this.planElement = planElement;
        }

        public String getPlanCopper() {
            return planCopper;
        }

        public void setPlanCopper(String planCopper) {
            this.planCopper = planCopper;
        }

        public String getPlanAccessory() {
            return planAccessory;
        }

        public void setPlanAccessory(String planAccessory) {
            this.planAccessory = planAccessory;
        }

        public double getWorkerAmount() {
            return workerAmount;
        }

        public void setWorkerAmount(double workerAmount) {
            this.workerAmount = workerAmount;
        }

        public String getWorkerStartTime() {
            return workerStartTime;
        }

        public void setWorkerStartTime(String workerStartTime) {
            this.workerStartTime = workerStartTime;
        }

        public String getWorkerEndTime() {
            return workerEndTime;
        }

        public void setWorkerEndTime(String workerEndTime) {
            this.workerEndTime = workerEndTime;
        }

        public int getLevel1Count() {
            return level1Count;
        }

        public void setLevel1Count(int level1Count) {
            this.level1Count = level1Count;
        }

        public Double getLevel1Price() {
            return level1Price;
        }

        public void setLevel1Price(Double level1Price) {
            this.level1Price = level1Price;
        }

        public int getLevel2Count() {
            return level2Count;
        }

        public void setLevel2Count(int level2Count) {
            this.level2Count = level2Count;
        }

        public Double getLevel2Price() {
            return level2Price;
        }

        public void setLevel2Price(Double level2Price) {
            this.level2Price = level2Price;
        }

        public int getLevel3Count() {
            return level3Count;
        }

        public void setLevel3Count(int level3Count) {
            this.level3Count = level3Count;
        }

        public Double getLevel3Price() {
            return level3Price;
        }

        public void setLevel3Price(Double level3Price) {
            this.level3Price = level3Price;
        }

        public int getLevel4Count() {
            return level4Count;
        }

        public void setLevel4Count(int level4Count) {
            this.level4Count = level4Count;
        }

        public Double getLevel4Price() {
            return level4Price;
        }

        public void setLevel4Price(Double level4Price) {
            this.level4Price = level4Price;
        }

        public int getLevel5Count() {
            return level5Count;
        }

        public void setLevel5Count(int level5Count) {
            this.level5Count = level5Count;
        }

        public Double getLevel5Price() {
            return level5Price;
        }

        public void setLevel5Price(Double level5Price) {
            this.level5Price = level5Price;
        }

        public int getLevel6Count() {
            return level6Count;
        }

        public void setLevel6Count(int level6Count) {
            this.level6Count = level6Count;
        }

        public Double getLevel6Price() {
            return level6Price;
        }

        public void setLevel6Price(Double level6Price) {
            this.level6Price = level6Price;
        }

        public int getBidCount() {
            return bidCount;
        }

        public void setBidCount(int bidCount) {
            this.bidCount = bidCount;
        }

        public int getProjectAdmin() {
            return projectAdmin;
        }

        public void setProjectAdmin(int projectAdmin) {
            this.projectAdmin = projectAdmin;
        }

        public int getHasBid() {
            return hasBid;
        }

        public void setHasBid(int hasBid) {
            this.hasBid = hasBid;
        }

        public int getHasCollect() {
            return hasCollect;
        }

        public void setHasCollect(int hasCollect) {
            this.hasCollect = hasCollect;
        }

        public int getSaleCount() {
            return saleCount;
        }

        public void setSaleCount(int saleCount) {
            this.saleCount = saleCount;
        }

        public int getHasSale() {
            return hasSale;
        }

        public void setHasSale(int hasSale) {
            this.hasSale = hasSale;
        }

        public int getSaleLevel1Count() {
            return saleLevel1Count;
        }

        public void setSaleLevel1Count(int saleLevel1Count) {
            this.saleLevel1Count = saleLevel1Count;
        }

        public int getSaleLevel2Count() {
            return saleLevel2Count;
        }

        public void setSaleLevel2Count(int saleLevel2Count) {
            this.saleLevel2Count = saleLevel2Count;
        }

        public int getSaleLevel3Count() {
            return saleLevel3Count;
        }

        public void setSaleLevel3Count(int saleLevel3Count) {
            this.saleLevel3Count = saleLevel3Count;
        }

        public int getEnableBid() {
            return enableBid;
        }

        public void setEnableBid(int enableBid) {
            this.enableBid = enableBid;
        }

        public int getEnableGrab() {
            return enableGrab;
        }

        public void setEnableGrab(int enableGrab) {
            this.enableGrab = enableGrab;
        }

        public double getBidAmount() {
            return bidAmount;
        }

        public void setBidAmount(double bidAmount) {
            this.bidAmount = bidAmount;
        }

        public String getRejectReason() {
            return rejectReason;
        }

        public void setRejectReason(String rejectReason) {
            this.rejectReason = rejectReason;
        }

        public String getVipWalletLogId() {
            return vipWalletLogId;
        }

        public void setVipWalletLogId(String vipWalletLogId) {
            this.vipWalletLogId = vipWalletLogId;
        }

        public int getRecommend() {
            return recommend;
        }

        public void setRecommend(int recommend) {
            this.recommend = recommend;
        }

        public long getRecommendTime() {
            return recommendTime;
        }

        public void setRecommendTime(long recommendTime) {
            this.recommendTime = recommendTime;
        }

        public long getApplyCheckTime() {
            return applyCheckTime;
        }

        public void setApplyCheckTime(long applyCheckTime) {
            this.applyCheckTime = applyCheckTime;
        }

        public long getDoneTime() {
            return doneTime;
        }

        public void setDoneTime(long doneTime) {
            this.doneTime = doneTime;
        }

        public String getWorkerImages() {
            return workerImages;
        }

        public void setWorkerImages(String workerImages) {
            this.workerImages = workerImages;
        }

        public static Creator<BeanInfo> getCREATOR() {
            return CREATOR;
        }
    }
}
