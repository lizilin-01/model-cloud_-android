package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：删除地址
 */
public class RemoveAddressApi implements IRequestApi {
    @Override
    public String getApi() {
        return "address/remove";
    }

    //地址id
    @HttpRename("addressId")
    private String addressId;


    public RemoveAddressApi setAddressId(String addressId) {
        this.addressId = addressId;
        return this;
    }
}
