package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class ChatListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "chat/list";
    }

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;


    public ChatListApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public ChatListApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public static class Bean {
        private List<ChatBean> list;

        public List<ChatBean> getList() {
            return list;
        }
    }

    public static class ChatBean {

        public String receiveUserId;
        public String lastChat;
        public String lastTime;
        public int unReadCount;
        public ToUser toUser;
        public long id;

        public static class ToUser {
            public String userId;
            public String nickName;
            public String avatar;
            public String desc;
        }
    }

}
