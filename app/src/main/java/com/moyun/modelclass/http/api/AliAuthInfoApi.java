package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：13.三方：支付宝换取授权访问令牌
 */
public class AliAuthInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/third/aliAuth";
    }

    //支付宝authCode
    @HttpRename("authCode")
    private String authCode;

    public AliAuthInfoApi setAuthCode(String authCode) {
        this.authCode = authCode;
        return this;
    }
}
