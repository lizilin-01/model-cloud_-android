package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/188:48 下午
 * tip：修改个人信息
 */
public class VipUserInfoResponse implements Serializable {
    private String nickName;//昵称

    public VipUserInfoResponse(String nickName) {
        this.nickName = nickName;
    }
}
