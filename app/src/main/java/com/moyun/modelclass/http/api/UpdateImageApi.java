package com.moyun.modelclass.http.api;

import androidx.annotation.NonNull;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.moyun.modelclass.http.CommonConfiguration;

import java.io.File;

/**
 * 作者: Meteor
 * 日期: 2024/4/30 15:51
 * 描述：
 */
public final class UpdateImageApi implements IRequestServer, IRequestApi {
    @NonNull
    @Override
    public String getHost() {
        return CommonConfiguration.getUploadUrl();
    }

    @NonNull
    @Override
    public String getApi() {
        return "file/uploadImage";
    }

    //其他参数
    @HttpRename("fileName")
    private String fileName;
    //图片文件
    private File file;
    //水印
    @HttpRename("watermark")
    private boolean watermark;

    public UpdateImageApi setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public UpdateImageApi setFile(File file) {
        this.file = file;
        return this;
    }

    public UpdateImageApi setWatermark(boolean watermark) {
        this.watermark = watermark;
        return this;
    }

    public static class Bean {
        private String fileId;//图片  id
        private String fileName;//图片名称
        private String url;//图片地址

        public String getFileId() {
            return fileId;
        }
        public String getFileName() {
            return fileName;
        }

        public String getUrl() {
            return url;
        }
    }
}
