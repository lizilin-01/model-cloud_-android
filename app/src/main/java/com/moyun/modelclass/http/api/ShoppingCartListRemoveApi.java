package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class ShoppingCartListRemoveApi implements IRequestApi {
    @Override
    public String getApi() {
        return "cart/batchRemove";
    }

    public List<String> modelIdList;

    public static class Bean {
    }

}
