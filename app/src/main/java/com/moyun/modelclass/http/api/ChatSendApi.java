package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 首页：发现列表
 */
public class ChatSendApi implements IRequestApi {
    @Override
    public String getApi() {
        return "chat/send";
    }

    @HttpRename("content")
    private String content;
    @HttpRename("receiveUserId")
    private String receiveUserId;


    public ChatSendApi setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
        return this;
    }

    public ChatSendApi setContent(String content) {
        this.content = content;
        return this;
    }

    public static class Bean {


    }


}
