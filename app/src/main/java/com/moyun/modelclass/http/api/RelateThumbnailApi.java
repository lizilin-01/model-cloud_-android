package com.moyun.modelclass.http.api;

import androidx.annotation.NonNull;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.moyun.modelclass.http.CommonConfiguration;

import java.io.File;

/**
 * 作者: Meteor
 * 日期: 2024/4/30 15:51
 * 描述：文件关联缩略图
 */
public final class RelateThumbnailApi implements IRequestApi {

    @NonNull
    @Override
    public String getApi() {
        return "file/relateThumbnail";
    }

    //STL文件
    @HttpRename("fileId")
    private String fileId;

    //缩略图
    @HttpRename("thumbnail")
    private String thumbnail;


    public RelateThumbnailApi setFileId(String fileId) {
        this.fileId = fileId;
        return this;
    }

    public RelateThumbnailApi setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }
}
