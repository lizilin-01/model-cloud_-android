package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ArticleResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：我的文章列表
 */
public class ArticleOnesPostApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/onesPost";
    }

    //查询的用户id
    @HttpRename("userId")
    private String userId;

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    //文章状态   0：全部, 1:草稿，2，待审核，3、已发布,4、审核驳回
    @HttpRename("articleState")
    private int articleState;

    public ArticleOnesPostApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public ArticleOnesPostApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public ArticleOnesPostApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public ArticleOnesPostApi setArticleState(int articleState) {
        this.articleState = articleState;
        return this;
    }

    public static class Bean {
        private List<ArticleResponse> list;

        public List<ArticleResponse> getList() {
            return list;
        }
    }

}
