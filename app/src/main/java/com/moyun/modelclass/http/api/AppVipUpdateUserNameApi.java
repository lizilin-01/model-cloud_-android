package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：4.修改登录账号
 */
public class AppVipUpdateUserNameApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/updateUserName";
    }

    //登录账号（老）
    @HttpRename("oldUsername")
    private String oldUsername;

    //验证码（老）
    @HttpRename("oldCode")
    private String oldCode;

    //登录账号（新）
    @HttpRename("newUsername")
    private String newUsername;

    //验证码（新）
    @HttpRename("newCode")
    private String newCode;


    public AppVipUpdateUserNameApi setOldUsername(String oldUsername) {
        this.oldUsername = oldUsername;
        return this;
    }

    public AppVipUpdateUserNameApi setOldCode(String oldCode) {
        this.oldCode = oldCode;
        return this;
    }

    public AppVipUpdateUserNameApi setNewUsername(String newUsername) {
        this.newUsername = newUsername;
        return this;
    }

    public AppVipUpdateUserNameApi setNewCode(String newCode) {
        this.newCode = newCode;
        return this;
    }
}
