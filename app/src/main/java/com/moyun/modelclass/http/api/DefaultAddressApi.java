package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.io.Serializable;

/**
 * 作者：Meteor
 * tip：获取用户默认地址
 */
public class DefaultAddressApi implements IRequestApi {
    @Override
    public String getApi() {
        return "address/getDefault";
    }

}
