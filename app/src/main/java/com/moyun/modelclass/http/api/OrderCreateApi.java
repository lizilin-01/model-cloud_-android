package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.OrderRequestResponse;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：订单创建
 */
public class OrderCreateApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/create";
    }

    //地址id
    @HttpRename("addressId")
    private String addressId;

    //模型id集合
    @HttpRename("modelList")
    private List<OrderRequestResponse> modelList;

    //打印参数
    @HttpRename("printParams")
    private String printParams;

    //备注
    @HttpRename("remark")
    private String remark;

    //场景:1、文章，2、购物车，3、模型库 、订单确认页 5、订单详情
    @HttpRename("scene")
    private String scene;

    //抵扣余额，单位为分
    @HttpRename("deductionBalance")
    private int deductionBalance;

    public OrderCreateApi setAddressId(String addressId) {
        this.addressId = addressId;
        return this;
    }

    public OrderCreateApi setModelList(List<OrderRequestResponse> modelList) {
        this.modelList = modelList;
        return this;
    }

    public OrderCreateApi setPrintParams(String printParams) {
        this.printParams = printParams;
        return this;
    }

    public OrderCreateApi setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public OrderCreateApi setScene(String scene) {
        this.scene = scene;
        return this;
    }

    public OrderCreateApi setDeductionBalance(int deductionBalance) {
        this.deductionBalance = deductionBalance;
        return this;
    }

    public final static class Bean {
        private String orderId;//订单号
        private String payOrderId;//支付订单号,在支付组件中使用此订单号

        public String getOrderId() {
            return orderId;
        }

        public String getPayOrderId() {
            return payOrderId;
        }
    }
}
