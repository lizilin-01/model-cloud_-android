package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：模型详情
 */
public class AppModelDetailsApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/get";
    }

    //模型id
    @HttpRename("modelId")
    private String modelId;

    public AppModelDetailsApi setArticleId(String modelId) {
        this.modelId = modelId;
        return this;
    }

    public final static class Bean {
        private String id;//模型id
        private String modelNo;//模型编号
        private String updateTime;//更新时间
        private String title;//标题
        private String coverImage;//模型封面图
        private List<String> images;//模型图片
        private List<String> files;//模型文件
        private List<String> onePiece;//完整件或组合件

        private List<StlFilesBean> stlFiles;//模型文件
        private List<StlFilesBean> oneStlPieces;//完整件

        private List<String> otherFile;//额外文件，主要都是一些辅助打印的文件

        private List<OtherFilesBean> otherFiles;//额外文件，主要都是一些辅助打印的文件
        private boolean openFlag;//是否开放别人浏览
        private int price;//模型价格
        private String desc;//模型描述
        private boolean original;//是否原创
        private String category;//模型类别
        private int saleCount;//售卖数量
        private int scanCount;//浏览数量
        private int likeCount;//收藏数量
        private int spaceUsed;//空间使用量
        private String createUser;//创建人userId
        private String createUserAvatar;//创建人头像
        private String nickName;//作者昵称


        public String getId() {
            return id;
        }

        public String getModelNo() {
            return modelNo;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public String getTitle() {
            return title;
        }

        public String getCoverImage() {
            return coverImage;
        }

        public List<String> getImages() {
            return images;
        }

        public List<String> getFiles() {
            return files;
        }

        public List<String> getOnePiece() {
            return onePiece;
        }

        public List<StlFilesBean> getStlFiles() {
            return stlFiles;
        }

        public List<StlFilesBean> getOneStlPieces() {
            return oneStlPieces;
        }

        public List<String> getOtherFile() {
            return otherFile;
        }

        public List<OtherFilesBean> getOtherFiles() {
            return otherFiles;
        }

        public boolean isOpenFlag() {
            return openFlag;
        }

        public int getPrice() {
            return price;
        }

        public String getDesc() {
            return desc;
        }

        public boolean isOriginal() {
            return original;
        }

        public String getCategory() {
            return category;
        }

        public int getSaleCount() {
            return saleCount;
        }

        public int getScanCount() {
            return scanCount;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public int getSpaceUsed() {
            return spaceUsed;
        }

        public String getCreateUser() {
            return createUser;
        }

        public String getCreateUserAvatar() {
            return createUserAvatar;
        }

        public String getNickName() {
            return nickName;
        }
    }

    public final static class StlFilesBean {
        private String fileId;//文件id，用来关联缩略图
        private String thumbnail;//缩略图
        private String file;//3d文件
        private String remark;//注意事项
        private boolean needReGetThumbnail = false;//需要重新获取缩略图，如果是需要重新获取缩略图，请在点开模型的时候，重新渲染缩略图

        private List<Integer> xyz;//长宽高，单位是mm

        public String getFileId() {
            return fileId;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public String getFile() {
            return file;
        }

        public String getRemark() {
            return remark;
        }

        public List<Integer> getXyz() {
            return xyz;
        }

        public boolean isNeedReGetThumbnail() {
            return needReGetThumbnail;
        }
    }
    public final static class OtherFilesBean {
        private String fileId;//文件id，用来关联缩略图
        private String fileName;//文件名称
        private String file;//文件地址

        public String getFileId() {
            return fileId;
        }

        public String getFileName() {
            return fileName;
        }

        public String getFile() {
            return file;
        }
    }
}
