package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：订单删除
 */
public class OrderDeleteApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/remove";
    }

    //订单Id
    @HttpRename("orderId")
    private String orderId;

    public OrderDeleteApi setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }
}
