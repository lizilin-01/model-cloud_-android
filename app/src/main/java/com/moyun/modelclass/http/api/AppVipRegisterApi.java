package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：注册账号
 */
public class AppVipRegisterApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/register";
    }

    //手机号、邮箱
    @HttpRename("username")
    private String username;
    //验证码
    @HttpRename("code")
    private String code;
    //密码
    @HttpRename("password")
    private String password;

    public AppVipRegisterApi setUsername(String username) {
        this.username = username;
        return this;
    }

    public AppVipRegisterApi setCode(String code) {
        this.code = code;
        return this;
    }

    public AppVipRegisterApi setPassword(String password) {
        this.password = password;
        return this;
    }
}
