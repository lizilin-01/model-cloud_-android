package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.FansAndFollowResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：关注相关接口
 */
public class GetFollowApi implements IRequestApi {
    @Override
    public String getApi() {
        return "follow/getFollows";
    }

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId = "";

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    public GetFollowApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public GetFollowApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public static class Bean {
        private List<FansAndFollowResponse> list;

        public List<FansAndFollowResponse> getList() {
            return list;
        }
    }
}
