package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AddressResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：获取文案
 */
public class DocumentGetApi implements IRequestApi {
    @Override
    public String getApi() {
        return "document/get";
    }

    //
    @HttpRename("key")
    private String key;

    public DocumentGetApi setKey(String key) {
        this.key = key;
        return this;
    }

    public final static class Bean {
        private String key;//文案key
        private String content;//文案内容，注意是富文本
        private String title;//文案标题，请前端放入标题位
        private String version;//当前文案版本号
        private String updateTime;//最后更新时间

        public String getKey() {
            return key;
        }

        public String getContent() {
            return content;
        }

        public String getTitle() {
            return title;
        }

        public String getVersion() {
            return version;
        }

        public String getUpdateTime() {
            return updateTime;
        }
    }

}
