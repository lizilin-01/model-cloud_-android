package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：购买会员
 */
public class VipOrderCreateApi implements IRequestApi {
    @Override
    public String getApi() {
        return "member/buyVip";
    }

    //VIP等级
    @HttpRename("level")
    private int level;

    public VipOrderCreateApi setLevel(int level) {
        this.level = level;
        return this;
    }

    public final static class Bean {
        private String memberOrderId;//会员订单id
        private String payOrderId;//支付订单号,在支付组件中使用此订单号

        public String getMemberOrderId() {
            return memberOrderId;
        }

        public String getPayOrderId() {
            return payOrderId;
        }
    }
}
