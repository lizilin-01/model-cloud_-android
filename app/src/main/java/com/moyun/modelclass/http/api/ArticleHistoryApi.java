package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ArticleResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：我的浏览记录
 */
public class ArticleHistoryApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/history";
    }

    //页码
    @HttpRename("pageIndex")
    private int pageIndex;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;


    public ArticleHistoryApi setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
        return this;
    }

    public ArticleHistoryApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public static class Bean {
        private List<ArticleResponse> list;

        public List<ArticleResponse> getList() {
            return list;
        }
    }

}
