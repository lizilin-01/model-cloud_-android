package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：提现之前需要绑定到微信
 */
public class UserBindWechatApi implements IRequestApi {

    @Override
    public String getApi() {
        return "user/bindWechat";
    }

    //微信授权code
    @HttpRename("code")
    private String code;

    public UserBindWechatApi setCode(String code) {
        this.code = code;
        return this;
    }
}
