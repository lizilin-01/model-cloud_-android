package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 首页：关注用户
 */
public class FollowUserApi implements IRequestApi {
    @Override
    public String getApi() {
        return "follow/user";
    }

    //被关注人用户id
    @HttpRename("userId")
    private String userId;

    //关注还是取消关注
    @HttpRename("follow")
    private boolean follow;

    public FollowUserApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public FollowUserApi setFollow(boolean follow) {
        this.follow = follow;
        return this;
    }

    public static class Bean {
        private boolean follow;

        public boolean isFollow() {
            return follow;
        }
    }
}
