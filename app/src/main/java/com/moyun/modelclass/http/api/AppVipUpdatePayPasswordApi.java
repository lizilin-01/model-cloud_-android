package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：15.修改支付密码：修改密码
 */
public class AppVipUpdatePayPasswordApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/vip/updatePayPassword";
    }

    //支付密码
    @HttpRename("payPassword")
    private String payPassword;

    public AppVipUpdatePayPasswordApi setPayPassword(String payPassword) {
        this.payPassword = payPassword;
        return this;
    }

    public final static class Bean {
        private String msg;

        public String getMsg() {
            return msg;
        }
    }
}
