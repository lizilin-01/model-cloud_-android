package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;
import java.util.List;

/**
 * 作者：Meteor
 * tip：打印相关参数
 */
public class ConfigPrintParamsApi implements IRequestApi {
    @Override
    public String getApi() {
        return "config/printParams";
    }

    public static class Bean {
        private List<BeanInfo> list;

        public List<BeanInfo> getList() {
            return list;
        }
    }

    public static class BeanInfo{
        public String paramId;//参数id
        public String paramLabel;//参数描述
        public int paramType;//参数类型:可以是输入框1，可以是下拉菜单2
        public String paramDesc;//参数描述,请显示在下来列表中
        private List<BeanInfo> optionValue;//当paramType=2是，此值为可选值

        public String getParamId() {
            return paramId;
        }

        public String getParamLabel() {
            return paramLabel;
        }

        public int getParamType() {
            return paramType;
        }

        public String getParamDesc() {
            return paramDesc;
        }

        public List<BeanInfo> getOptionValue() {
            return optionValue;
        }
    }
}
