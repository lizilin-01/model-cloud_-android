package com.moyun.modelclass.http.api;
import com.hjq.http.config.IRequestApi;
import java.util.List;

/**
 * 作者：Meteor
 * 首页：获取模型分类
 */
public class ModelTypeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/category";
    }

    public static class Bean {
        private List<BeanInfo> list;

        public List<BeanInfo> getList() {
            return list;
        }
    }

    public static class BeanInfo {
        private String categoryId;
        private String categoryName;
        private int categoryIndex;

        public String getCategoryId() {
            return categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public int getCategoryIndex() {
            return categoryIndex;
        }
    }
}
