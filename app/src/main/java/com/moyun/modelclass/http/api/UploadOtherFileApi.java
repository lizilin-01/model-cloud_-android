package com.moyun.modelclass.http.api;

import androidx.annotation.NonNull;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.moyun.modelclass.http.CommonConfiguration;

import java.io.File;

/**
 * 作者：Meteor
 * tip：文件上传相关接口
 */
public class UploadOtherFileApi implements IRequestServer, IRequestApi {
    @NonNull
    @Override
    public String getHost() {
        return CommonConfiguration.getUploadUrl();
    }

    @Override
    public String getApi() {
        return "file/uploadOther";
    }

    //文件名
    @HttpRename("fileName")
    private String fileName;

    //文件
    @HttpRename("file")
    private File file;

    public UploadOtherFileApi setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public UploadOtherFileApi setFile(File file) {
        this.file = file;
        return this;
    }

    public static class Bean {
        private String fileName;
        private String url;

        public String getFileName() {
            return fileName;
        }

        public String getUrl() {
            return url;
        }
    }
}
