package com.moyun.modelclass.http.api;

import java.io.Serializable;

public class CommonRequestBody implements Serializable {
    private String source;//设备来源，网页端1，ios:2,android:3,微信小程序：4
    private String uuid;//身份id，请进app的时候时候自己生成，并存入缓存中，为32位随机数

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
