package com.moyun.modelclass.http.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：文章
 */
public class ArticleResponse implements Serializable {
    private String id;//文章id，字符串
    private String createUser;//创建人id
    private UserInfoResponse userInfo;//创建人的信息
    private String summary;//摘要
    private String title;//文章标题
    private List<String> tags;//话题列表
    private int scanCount;//浏览数量
    private int commentCount;//评论数量
    private int likeCount;//点赞数量
    private String simpleImage;//图片，可能是空
    private String createTime;//创建时间
    private String updateTime;//最后更新时间
    private int articleState;//文章的状态

    private Boolean isFollow = false;//是否关注该用户

    private Boolean isCollect = false;//是否收藏文章

    public String getId() {
        return id;
    }

    public String getCreateUser() {
        return createUser;
    }

    public UserInfoResponse getUserInfo() {
        return userInfo;
    }

    public String getSummary() {
        return summary;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getTags() {
        return tags;
    }

    public int getScanCount() {
        return scanCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public String getSimpleImage() {
        return simpleImage;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public int getArticleState() {
        return articleState;
    }

    public Boolean getFollow() {
        return isFollow;
    }

    public void setFollow(Boolean follow) {
        isFollow = follow;
    }

    public Boolean getCollect() {
        return isCollect;
    }

    public void setCollect(Boolean collect) {
        isCollect = collect;
    }



    public static class UserInfoResponse {
        private String userId;//创建人的userId
        private String nickName;//创建人的userId
        private String avatar;//创建人的userId
        private String desc;//创建人的userId

        public String getUserId() {
            return userId;
        }

        public String getNickName() {
            return nickName;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getDesc() {
            return desc;
        }
    }
}
