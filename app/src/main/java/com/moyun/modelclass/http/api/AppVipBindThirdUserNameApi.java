package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：三方：绑定手机号
 */
public class AppVipBindThirdUserNameApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/third/bindThirdUserName";
    }

    //手机号、邮箱
    @HttpRename("userName")
    private String userName;
    //验证码
    @HttpRename("code")
    private String code;

    public AppVipBindThirdUserNameApi setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public AppVipBindThirdUserNameApi setCode(String code) {
        this.code = code;
        return this;
    }
}
