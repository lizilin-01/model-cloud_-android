package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：三方：支付宝获取infoStr
 */
public class AppThirdAliInfoStrApi implements IRequestApi {
    @Override
    public String getApi() {
        return "app/third/aliInfoStr";
    }


    public final static class Bean {
        private String infoStr;//

        public String getInfoStr() {
            return infoStr;
        }
    }
}
