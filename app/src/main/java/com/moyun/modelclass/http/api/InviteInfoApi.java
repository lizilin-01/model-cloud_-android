package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * 作者：Meteor
 * tip：获取用户邀请码的信息
 */
public class InviteInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "invite/info";
    }

    public final static class Bean {
        private boolean hasUse;
        private String inviteCode;
        private String useTime;
        private String content;

        public boolean isHasUse() {
            return hasUse;
        }

        public String getInviteCode() {
            return inviteCode;
        }

        public String getUseTime() {
            return useTime;
        }

        public String getContent() {
            return content;
        }
    }

}
