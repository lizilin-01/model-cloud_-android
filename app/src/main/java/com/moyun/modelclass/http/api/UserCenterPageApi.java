package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.AddressResponse;

import java.util.List;

/**
 * 作者：Meteor
 * tip：用户中心信息补充，比如客服电话、个人中心菜单页补充等。
 */
public class UserCenterPageApi implements IRequestApi {
    @Override
    public String getApi() {
        return "config/userCenterPage";
    }

    public final static class Bean {
        private CustomerServiceConfigBean customerServiceConfig;

        private List<MenuItemConfigListBean> menuItemConfigList;

        public CustomerServiceConfigBean getCustomerServiceConfig() {
            return customerServiceConfig;
        }

        public List<MenuItemConfigListBean> getMenuItemConfigList() {
            return menuItemConfigList;
        }
    }

    public final static class CustomerServiceConfigBean{
        private String userId;//客服userId
        private String phone;//客服电话
        private String desc;//菜单说明

        public String getUserId() {
            return userId;
        }

        public String getPhone() {
            return phone;
        }

        public String getDesc() {
            return desc;
        }
    }

    public final static class MenuItemConfigListBean{
        private String label;//
        private String url;//
        private String desc;//
        private String index;//

        public String getLabel() {
            return label;
        }

        public String getUrl() {
            return url;
        }

        public String getDesc() {
            return desc;
        }

        public String getIndex() {
            return index;
        }
    }

}
