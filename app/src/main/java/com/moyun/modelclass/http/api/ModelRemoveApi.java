package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：删除模型
 */
public class ModelRemoveApi implements IRequestApi {
    @Override
    public String getApi() {
        return "model/remove";
    }

    //文章id
    @HttpRename("modelId")
    private String modelId;

    public ModelRemoveApi setModelId(String modelId) {
        this.modelId = modelId;
        return this;
    }
}
