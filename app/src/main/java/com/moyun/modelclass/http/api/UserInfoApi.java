package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.ArticleResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 获取用户信息
 */
public class UserInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "user/get";
    }

    //查询的用户id
    @HttpRename("userId")
    private String userId;

    public UserInfoApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }
}
