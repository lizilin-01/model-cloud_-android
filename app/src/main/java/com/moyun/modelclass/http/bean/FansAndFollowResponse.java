package com.moyun.modelclass.http.bean;

public class FansAndFollowResponse {
    private String userId;//查询用户的userId
    private String nickName;//用户昵称
    private String avatar;//头像地址
    private String desc;//描述

    public String getUserId() {
        return userId;
    }

    public String getNickName() {
        return nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getDesc() {
        return desc;
    }
}
