package com.moyun.modelclass.http.bean;

import java.io.Serializable;
import java.util.List;

public class OrderModelResponse implements Serializable {
    private String modelId;//模型id
    private int price;//模型价格
    private String image;//模型图片
    private String size;//模型规格
    private int count;//模型购买数量
    private String title;//模型标题

    private List<String> files;//
    private List<String> onePieceFiles;//
    private List<String> otherFiles;//

    private String commentContent;//评论内容
    private List<String> commentImages;//评论图片
    private int star = 5;//评论星级

    public String getModelId() {
        return modelId;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public String getSize() {
        return size;
    }

    public int getCount() {
        return count;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getFiles() {
        return files;
    }

    public List<String> getOnePieceFiles() {
        return onePieceFiles;
    }

    public List<String> getOtherFiles() {
        return otherFiles;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public List<String> getCommentImages() {
        return commentImages;
    }

    public void setCommentImages(List<String> commentImages) {
        this.commentImages = commentImages;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }
}
