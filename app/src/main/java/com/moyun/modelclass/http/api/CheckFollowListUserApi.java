package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 作者：Meteor
 * 首页：检查是否关注了用户
 */
public class CheckFollowListUserApi implements IRequestApi {
    @Override
    public String getApi() {
        return "follow/checkUser";
    }

    //用户id列表
    @HttpRename("userIdList")
    private List<String> userIdList;

    public CheckFollowListUserApi setUserIdList(List<String> userIdList) {
        this.userIdList = userIdList;
        return this;
    }

    public static class Bean implements Serializable {
        private List<BeanInfo> list;

        public List<BeanInfo> getList() {
            return list;
        }
    }

    public static class BeanInfo implements Serializable {
        private String userId;
        private boolean isFollow = false;

        public String getUserId() {
            return userId;
        }

        public boolean isFollow() {
            return isFollow;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setFollow(boolean follow) {
            isFollow = follow;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BeanInfo beanInfo = (BeanInfo) o;
            return isFollow == beanInfo.isFollow && Objects.equals(userId, beanInfo.userId);
        }

        @Override
        public String toString() {
            return "BeanInfo{" +
                    "userId='" + userId + '\'' +
                    ", isFollow=" + isFollow +
                    '}';
        }
    }
}
