package com.moyun.modelclass.http.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：评论/热评
 */
public class CommentResponse implements Serializable {
    private String id;//评论id
    private UserInfoResponse userInfo;//创建人的信息
    private String content;//评论内容
    private int star;//评论星级
    private int type;//评论内容类型
    private String objectId;//评论对象
    private ParentUserInfoResponse parentUserInfo;//创建人的信息
    private String[] images;//图片集合
    private String createTime;//评论时间
    private int likes = 0;//点赞数
    private List<CommentResponse> children;//子评论

    private boolean isSelfLike = false;//自己是否点赞

    public String getId() {
        return id;
    }

    public UserInfoResponse getUserInfo() {
        return userInfo;
    }

    public String getContent() {
        return content;
    }

    public int getStar() {
        return star;
    }

    public int getType() {
        return type;
    }

    public String getObjectId() {
        return objectId;
    }

    public ParentUserInfoResponse getParentUserInfo() {
        return parentUserInfo;
    }

    public String[] getImages() {
        return images;
    }

    public String getCreateTime() {
        return createTime;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public List<CommentResponse> getChildren() {
        return children;
    }

    public boolean isSelfLike() {
        return isSelfLike;
    }

    public void setSelfLike(boolean selfLike) {
        isSelfLike = selfLike;
    }

    public static class UserInfoResponse {
        private String userId;//用户userId
        private String nickName;//用户昵称
        private String avatar;//用户头像
        private String desc;//用户描述

        public String getUserId() {
            return userId;
        }

        public String getNickName() {
            return nickName;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getDesc() {
            return desc;
        }
    }

    public static class ParentUserInfoResponse {
        private String userId;//用户userId
        private String nickName;//用户昵称
        private String avatar;//用户头像
        private String desc;//用户描述

        public String getUserId() {
            return userId;
        }

        public String getNickName() {
            return nickName;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getDesc() {
            return desc;
        }
    }
}
