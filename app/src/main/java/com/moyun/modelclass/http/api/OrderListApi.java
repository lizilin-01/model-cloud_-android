package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;
import com.moyun.modelclass.http.bean.OrderResponse;

import java.util.List;

/**
 * 作者：Meteor
 * 首页：订单列表
 */
public class OrderListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "order/list";
    }

    //订单状态：0:全部，1、代付款，2、待发货、3、待收货、4、待评价
    @HttpRename("orderStateVO")
    private int orderStateVO;

    //每页查询数量
    @HttpRename("pageSize")
    private int pageSize;

    //最后一次查询Id
    @HttpRename("lastId")
    private String lastId;

    //搜索，只支持title搜索
    @HttpRename("searchText")
    private String searchText;

    public OrderListApi setOrderStateVO(int orderStateVO) {
        this.orderStateVO = orderStateVO;
        return this;
    }

    public OrderListApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public OrderListApi setLastId(String lastId) {
        this.lastId = lastId;
        return this;
    }

    public OrderListApi setSearchText(String searchText) {
        this.searchText = searchText;
        return this;
    }

    public static class Bean {
        private List<OrderResponse> list;

        public List<OrderResponse> getList() {
            return list;
        }
    }
}
