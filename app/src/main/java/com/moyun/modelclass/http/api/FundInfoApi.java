package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 首页：查看资金详情
 */
public class FundInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "fund/info";
    }

    public static class Bean {
        private int totalFund;//总收益
        private int monthFund;//月收益
        private int canWithdrawal;//可提现金额

        public int getTotalFund() {
            return totalFund;
        }

        public int getMonthFund() {
            return monthFund;
        }

        public int getCanWithdrawal() {
            return canWithdrawal;
        }
    }
}
