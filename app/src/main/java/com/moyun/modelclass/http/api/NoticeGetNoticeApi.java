package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：公告接口
 */
public class NoticeGetNoticeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "notice/getNotice";
    }

    public final static class Bean {
        private String id;
        private String title;
        private String image;
        private String url;

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getImage() {
            return image;
        }

        public String getUrl() {
            return url;
        }
    }
}
