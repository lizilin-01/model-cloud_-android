package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * tip：退回草稿
 */
public class ArticleBackToDraftApi implements IRequestApi {
    @Override
    public String getApi() {
        return "article/backToDraft";
    }

    //文章id
    @HttpRename("articleId")
    private String articleId;

    public ArticleBackToDraftApi setArticleId(String articleId) {
        this.articleId = articleId;
        return this;
    }
}
