package com.moyun.modelclass.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 首页：获取用户会员信息
 */
public class MemberInfoApi implements IRequestApi {
    @Override
    public String getApi() {
        return "member/info";
    }

    public static class Bean {
        private long totalSpace;//总空间:总空间可能小于用户使用空间
        private long useSpace;//用户使用空间
        private int level;//会员等级：普通会员1、vip2
        private String buyTime;//购买时间
        private String expireTime;//过期时间

        public long getTotalSpace() {
            return totalSpace;
        }

        public long getUseSpace() {
            return useSpace;
        }

        public int getLevel() {
            return level;
        }

        public String getBuyTime() {
            return buyTime;
        }

        public String getExpireTime() {
            return expireTime;
        }
    }
}
