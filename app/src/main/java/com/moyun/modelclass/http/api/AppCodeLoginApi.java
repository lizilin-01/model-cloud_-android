package com.moyun.modelclass.http.api;

import com.hjq.http.annotation.HttpRename;
import com.hjq.http.config.IRequestApi;

/**
 * 作者：Meteor
 * 日期：2022/2/136:43 下午
 * tip：登录：发送验证码
 */
public class AppCodeLoginApi implements IRequestApi {
    @Override
    public String getApi() {
        return "verify/sendSMS";
    }

    //手机号
    @HttpRename("mobile")
    private String mobile;

    //短信目的：1、注册 2、解绑微信
    @HttpRename("smsType")
    private int smsType;

    public AppCodeLoginApi setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public AppCodeLoginApi setSmsType(int smsType) {
        this.smsType = smsType;
        return this;
    }

    public final static class Bean {
        private String mobile;//手机号
        private String guid;//guid，后续验证短信对应关系

        public String getMobile() {
            return mobile;
        }

        public String getGuid() {
            return guid;
        }
    }
}
