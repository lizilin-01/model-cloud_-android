package com.moyun.modelclass.http.bean;

import java.io.Serializable;

/**
 * 作者：Meteor
 * 日期：2022/2/2810:30 下午
 * tip：banner
 */
public class WXPayRequest implements Serializable {
//     "package": "Sign=WXPay",
//             "appid": "wx899aaa3c58960347",
//             "sign": "CB40D30A06CFB0787CFF59C224225E63",
//             "partnerid": "1622701053",
//             "prepayid": "wx10231019249290523bf629298b30170000",
//             "noncestr": "fhsaK9L8xiLLm5Em",
//             "timestamp": "1646925019"


//    private String package;//
    private String appId;//
    private String partnerid;//
    private String prepayid;//
    private String packagee;//
    private String noncestr;//
    private String timestamp;//
    private String sign;//

    public String getAppId() {
        return appId;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public String getPackagee() {
        return packagee;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getSign() {
        return sign;
    }
}
