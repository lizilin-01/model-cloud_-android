package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.R;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.OrderModelResponse;
import com.moyun.modelclass.view.MyEditText;
import java.text.DecimalFormat;
import java.util.List;

/**
 * 作者：Meteor
 * 首页：我的订单：评论模型
 */
public class OrderEvaluateModelAdapter extends BaseQuickAdapter<OrderModelResponse, BaseViewHolder> {
    private DecimalFormat df = new DecimalFormat("￥##0.00");
    private OnAddNewImageClick onAddNewImageClick;

    public void setOnAddNewImageClick(OnAddNewImageClick onAddNewImageClick) {
        this.onAddNewImageClick = onAddNewImageClick;
    }

    public OrderEvaluateModelAdapter() {
        super(R.layout.item_order_evaluate);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OrderModelResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_my_order_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(beanInfo.getImage()))
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        baseViewHolder.setText(R.id.tv_my_order_name, beanInfo.getTitle());
        baseViewHolder.setText(R.id.tv_my_order_price, df.format(beanInfo.getPrice() * 0.01));
        baseViewHolder.setText(R.id.tv_my_order_count, "x" + beanInfo.getCount());
        baseViewHolder.setText(R.id.tv_my_order_size, "规格："+beanInfo.getSize());

//        switch (beanInfo.getSize()) {
//            case "0.5":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：标准规格");
//                break;
//            case "0.8":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：80%");
//                break;
//            case "1.0":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：100%");
//                break;
//            case "1.2":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：120%");
//                break;
//            case "1.5":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：150%");
//                break;
//        }


        RatingBar rating = baseViewHolder.getView(R.id.rating_order_evaluate_two);//评分

        FrameLayout addImage = baseViewHolder.getView(R.id.fl_order_evaluate_image_add_two);//添加图片
        RecyclerView showImage = baseViewHolder.getView(R.id.recycler_order_evaluate_image_show_two);//添加图片

        MyEditText commentDetails = baseViewHolder.getView(R.id.et_order_evaluate_title_two);//评价描述

        rating.setRating((float) beanInfo.getStar());

        if (beanInfo.getCommentImages() != null && beanInfo.getCommentImages().size() > 0) {
            OrderEvaluateImageAdapter imageTwoAdapter = new OrderEvaluateImageAdapter();
            showImage.setLayoutManager(new GridLayoutManager(getContext(), 3));
            showImage.setAdapter(imageTwoAdapter);
            imageTwoAdapter.setList(beanInfo.getCommentImages());
            imageTwoAdapter.setOnImageClick(new OrderEvaluateImageAdapter.OnImageClick() {
                @Override
                public void delete(int position) {
                    List<String> temp = getData().get(baseViewHolder.getLayoutPosition()).getCommentImages();
                    temp.remove(position);
                    imageTwoAdapter.setList(temp);
                }
            });
        }

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                getData().get(baseViewHolder.getLayoutPosition()).setStar((int) ratingBar.getRating());
            }
        });

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onAddNewImageClick != null) {
                    onAddNewImageClick.addNewImage(baseViewHolder.getLayoutPosition());
                }
            }
        });

        //请填写评价描述
        TextWatcher priceUnitWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                getData().get(baseViewHolder.getLayoutPosition()).setCommentContent(commentDetails.getText().toString());
            }
        };
        commentDetails.addTextChangedListener(priceUnitWatcher);
        commentDetails.setTag(priceUnitWatcher);
    }

    public interface OnAddNewImageClick {
        void addNewImage(int currentPosition);
    }
}
