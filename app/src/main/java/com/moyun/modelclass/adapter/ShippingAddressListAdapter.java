package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.bean.AddressResponse;
import com.moyun.modelclass.R;

public class ShippingAddressListAdapter extends BaseQuickAdapter<AddressResponse, BaseViewHolder> {
    private int selectedPosition = 0; // 默认选择第一个
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;

    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener mOnItemSelectedChangedListener) {
        this.mOnItemSelectedChangedListener = mOnItemSelectedChangedListener;
    }

    public ShippingAddressListAdapter() {
        super(R.layout.item_shipping_address_list);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, AddressResponse beanInfo) {
        CheckedTextView addressCheck = baseViewHolder.getView(R.id.ct_shipping_address_check);
        ImageView defaultLabel = baseViewHolder.getView(R.id.iv_shipping_address_default);
        addressCheck.setChecked(selectedPosition == baseViewHolder.getLayoutPosition());

        if (beanInfo.isDefaultFlag()) {
            defaultLabel.setVisibility(View.VISIBLE);
        } else {
            defaultLabel.setVisibility(View.INVISIBLE);
        }

        baseViewHolder.setText(R.id.tv_shipping_address_name, beanInfo.getName());
        baseViewHolder.setText(R.id.tv_shipping_address_phone, beanInfo.getMobile());
        baseViewHolder.setText(R.id.tv_shipping_address_address, beanInfo.getProvince() + " " + beanInfo.getCity() + " " + beanInfo.getArea() + " " + beanInfo.getDetail());


        addressCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseViewHolder.getLayoutPosition() != selectedPosition) {
                    notifyItemChanged(selectedPosition); // 可选，如果有复杂动画
                    selectedPosition = baseViewHolder.getLayoutPosition();
                    notifyItemChanged(baseViewHolder.getLayoutPosition());
                }
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemSelected(beanInfo);
                }
            }
        });

        baseViewHolder.getView(R.id.rl_shipping_address_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemEdit(beanInfo.getId(), beanInfo.isDefaultFlag());
                }
            }
        });
    }


    public interface OnItemSelectedChangedListener {
        void onItemSelected(AddressResponse beanInfo);//选中地址

        void onItemEdit(String addressId, boolean isDefaultFlag);//编辑地址
    }
}
