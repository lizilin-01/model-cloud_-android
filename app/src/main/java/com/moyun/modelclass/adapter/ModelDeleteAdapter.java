package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;

import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 文章：模型文章列表：删除
 */
public class ModelDeleteAdapter extends BaseQuickAdapter<ModelResponse, BaseViewHolder> {
    private OnDeleteClick onDeleteClick;
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    public void setOnDeleteClick(OnDeleteClick onDeleteClick) {
        this.onDeleteClick = onDeleteClick;
    }

    public ModelDeleteAdapter() {
        super(R.layout.item_model_delete);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_model_delete_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(3, beanInfo.getCoverImage()))
                .placeholder(R.drawable.icon_head_portrait)
                .error(R.drawable.icon_head_portrait)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                    }
                });

        baseViewHolder.setText(R.id.tv_model_delete_time, beanInfo.getUpdateTime());
        baseViewHolder.setText(R.id.tv_model_delete_sold, "已售" + beanInfo.getSaleCount());
        baseViewHolder.setText(R.id.tv_model_delete_name, beanInfo.getTitle());
        baseViewHolder.setText(R.id.tv_model_delete_price, df.format(beanInfo.getPrice() * 0.01));

        baseViewHolder.getView(R.id.tv_model_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onDeleteClick != null) {
                    onDeleteClick.delete(baseViewHolder.getLayoutPosition());
                }
            }
        });
    }

    public interface OnDeleteClick {
        void delete(int position);
    }
}
