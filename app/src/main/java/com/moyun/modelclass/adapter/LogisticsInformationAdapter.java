package com.moyun.modelclass.adapter;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.OrderDeliveryInfoApi;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 首页：文章状态
 */
public class LogisticsInformationAdapter extends BaseQuickAdapter<OrderDeliveryInfoApi.BeanInfo, BaseViewHolder> {

    public LogisticsInformationAdapter() {
        super(R.layout.item_logistics_information);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OrderDeliveryInfoApi.BeanInfo beanInfo) {
        if (baseViewHolder.getLayoutPosition() == getData().size() - 1) {//如果是最后一个条目，隐藏掉竖线
            baseViewHolder.setGone(R.id.item_logistics_information_line, true);
        }

        baseViewHolder.setText(R.id.item_logistics_information_time, beanInfo.getTime());
        baseViewHolder.setText(R.id.item_logistics_information_address, beanInfo.getStatusDesc());
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected(int position, String response);
    }
}
