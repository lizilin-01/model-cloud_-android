package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.widget.ImageView;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 文章：模型文章列表
 */
public class ArticleDetailDetailsAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public ArticleDetailDetailsAdapter() {
        super(R.layout.item_article_detail_details);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String beanInfo) {
        ImageView articleImage = baseViewHolder.getView(R.id.iv_article_detail_image);
        if (beanInfo.startsWith("image:image/",0)){
            Glide.with(getContext())
                    .asBitmap()
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.replace("image:","")))
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new BitmapImageViewTarget(articleImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                            articleImage.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                        }
                    });

//            Glide.with(getContext())
//                    .load(CommonConfiguration.splitResUrl(3, beanInfo.replace("image:","")))
//                    .centerInside()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(articleImage);
        }else {
            baseViewHolder.setText(R.id.tv_article_detail_details, beanInfo);
        }
    }
}
