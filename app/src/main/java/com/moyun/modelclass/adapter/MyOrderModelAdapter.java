package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.activity.OrderDetailsActivity;
import com.moyun.modelclass.activity.OrderSearchActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.OrderModelResponse;
import com.moyun.modelclass.R;

import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 首页：我的订单：模型列表
 */
public class MyOrderModelAdapter extends BaseQuickAdapter<OrderModelResponse, BaseViewHolder> {
    private DecimalFormat df = new DecimalFormat("￥##0.00");
    private String moOrderId = "";//订单id

    public MyOrderModelAdapter() {
        super(R.layout.item_my_order_model);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OrderModelResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_my_order_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(beanInfo.getImage()))
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        baseViewHolder.setText(R.id.tv_my_order_name, beanInfo.getTitle());
        baseViewHolder.setText(R.id.tv_my_order_price, df.format(beanInfo.getPrice() * 0.01));
        baseViewHolder.setText(R.id.tv_my_order_count, "x" + beanInfo.getCount());
        baseViewHolder.setText(R.id.tv_my_order_size, "规格："+beanInfo.getSize());
//        switch (beanInfo.getSize()) {
//            case "0.5":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：标准规格");
//                break;
//            case "0.8":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：80%");
//                break;
//            case "1.0":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：100%");
//                break;
//            case "1.2":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：120%");
//                break;
//            case "1.5":
//                baseViewHolder.setText(R.id.tv_my_order_size, "规格：150%");
//                break;
//        }

        //查看详情
        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (moOrderId != null && !moOrderId.isEmpty()) {
                    OrderDetailsActivity.start(getContext(), moOrderId);
                }
            }
        });
    }

    public void setOrderId(String orderId) {
        this.moOrderId = orderId;
    }
}
