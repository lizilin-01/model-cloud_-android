package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.CheckedTextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.ModelTypeApi;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 日期：2022/1/26 23:28
 * tip：选择模型弹窗分类 适配器
 */
public class ModelTypeDialogAdapter extends BaseQuickAdapter<ModelTypeApi.BeanInfo, BaseViewHolder> {
    private int selectedPosition = 0; // 默认选择第一个
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;


    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener mOnItemSelectedChangedListener) {
        this.mOnItemSelectedChangedListener = mOnItemSelectedChangedListener;
    }

    public ModelTypeDialogAdapter() {
        super(R.layout.item_model_type_dialog);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelTypeApi.BeanInfo beanInfo) {
        baseViewHolder.setText(R.id.tv_model_type_dialog_title, beanInfo.getCategoryName());
        CheckedTextView addressCheck = baseViewHolder.getView(R.id.ct_model_type_dialog_check);
        addressCheck.setChecked(selectedPosition == baseViewHolder.getLayoutPosition());
        addressCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseViewHolder.getLayoutPosition() != selectedPosition) {
                    notifyItemChanged(selectedPosition); // 可选，如果有复杂动画
                    selectedPosition = baseViewHolder.getLayoutPosition();
                    notifyItemChanged(baseViewHolder.getLayoutPosition());
                }
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemSelected(beanInfo, selectedPosition);
                }
            }
        });
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected(ModelTypeApi.BeanInfo beanInfo, int position);//参数
    }
}
