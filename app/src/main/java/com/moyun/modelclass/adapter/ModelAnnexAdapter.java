package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 文章：附加文件
 */
public class ModelAnnexAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private OnFileDeleteClick onFileDeleteClick;

    public void setOnFileDeleteClick(OnFileDeleteClick onFileDeleteClick) {
        this.onFileDeleteClick = onFileDeleteClick;
    }

    public ModelAnnexAdapter() {
        super(R.layout.item_model_annex);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String beanInfo) {
        baseViewHolder.setText(R.id.tv_model_annex_name, beanInfo);

        baseViewHolder.getView(R.id.tv_model_annex_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFileDeleteClick != null) {
                    onFileDeleteClick.delete(baseViewHolder.getLayoutPosition());
                }
            }
        });
    }

    public interface OnFileDeleteClick {
        void delete(int position);
    }
}
