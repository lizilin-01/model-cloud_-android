package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckCollectListUserApi;
import com.moyun.modelclass.http.api.CheckFollowListUserApi;
import com.moyun.modelclass.http.bean.ArticleResponse;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.DataUtil;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * 首页：发现
 */
public class MyArticleAdapter extends BaseQuickAdapter<ArticleResponse, BaseViewHolder> {
    private OnFindClick onFindClick;
    protected MMKV kv;

    public void setOnFindClick(OnFindClick onFindClick) {
        this.onFindClick = onFindClick;
    }

    public MyArticleAdapter() {
        super(R.layout.item_my_article);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ArticleResponse beanInfo) {
//        1:草稿，2，待审核，3、已发布,4、审核驳回

        TextView articleState = baseViewHolder.getView(R.id.tv_my_article_state);
        if (beanInfo.getArticleState() == 1) {
            baseViewHolder.setVisible(R.id.tv_my_article_edit, true);
            articleState.setText("【草稿】");
            articleState.setTextColor(Color.parseColor("#767676"));
            baseViewHolder.setGone(R.id.view_my_article_line, true);
            baseViewHolder.setGone(R.id.ll_my_article_number_all, true);
        } else if (beanInfo.getArticleState() == 2) {
            baseViewHolder.setVisible(R.id.tv_my_article_edit, false);
            articleState.setText("【审核中】");
            articleState.setTextColor(Color.parseColor("#FF6600"));
            baseViewHolder.setGone(R.id.view_my_article_line, true);
            baseViewHolder.setGone(R.id.ll_my_article_number_all, true);
        } else if (beanInfo.getArticleState() == 3) {
            baseViewHolder.setVisible(R.id.tv_my_article_edit, true);
            articleState.setText("【已发布】");
            articleState.setTextColor(Color.parseColor("#00BC77"));
            baseViewHolder.setGone(R.id.view_my_article_line, false);
            baseViewHolder.setGone(R.id.ll_my_article_number_all, false);
        } else if (beanInfo.getArticleState() == 4) {
            baseViewHolder.setVisible(R.id.tv_my_article_edit, true);
            articleState.setText("【审核驳回】");
            articleState.setTextColor(Color.parseColor("#E62B2F"));
            baseViewHolder.setGone(R.id.view_my_article_line, true);
            baseViewHolder.setGone(R.id.ll_my_article_number_all, true);
        }

        baseViewHolder.setText(R.id.tv_my_article_title, beanInfo.getTitle());

        if (beanInfo.getSummary().isEmpty()) {
            baseViewHolder.setGone(R.id.tv_my_article_details, true);
        } else {
            baseViewHolder.setGone(R.id.tv_my_article_details, false);
            baseViewHolder.setText(R.id.tv_my_article_details, beanInfo.getSummary());
        }

        //文章图片
        ImageView articleImage = baseViewHolder.getView(R.id.iv_my_article_image);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(5, beanInfo.getSimpleImage()))
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(articleImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        articleImage.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        articleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.imageUrl(CommonConfiguration.splitResUrl(3, beanInfo.getSimpleImage()));
                }
            }
        });

        StringBuilder tags = new StringBuilder();
        if (beanInfo.getTags() != null && beanInfo.getTags().size() > 0) {
            baseViewHolder.setVisible(R.id.tv_my_article_subject, true);
            for (int i = 0; i < beanInfo.getTags().size(); i++) {
                //证明是最后一个
                if (i == beanInfo.getTags().size() - 1) {
                    tags.append("#").append(beanInfo.getTags().get(i));
                } else {
                    tags.append("#").append(beanInfo.getTags().get(i)).append("，");
                }
            }
        } else {
            baseViewHolder.setVisible(R.id.tv_my_article_subject, false);
        }
        baseViewHolder.setText(R.id.tv_my_article_subject, tags);

        baseViewHolder.setText(R.id.tv_my_article_time, DataUtil.getTimeShowString(TimeUtils.string2Millis(beanInfo.getUpdateTime()), false));

        baseViewHolder.setText(R.id.tv_my_article_read, String.valueOf(beanInfo.getScanCount()));
        baseViewHolder.setText(R.id.tv_my_article_reply, String.valueOf(beanInfo.getCommentCount()));
        baseViewHolder.setText(R.id.tv_my_article_collect, String.valueOf(beanInfo.getLikeCount()));

        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    if (beanInfo.getArticleState() == 2 || beanInfo.getArticleState() == 3 || beanInfo.getArticleState() == 4) {
                        onFindClick.details(beanInfo.getId());
                    }
                }
            }
        });

        baseViewHolder.getView(R.id.tv_my_article_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.edit(beanInfo.getId());
                }
            }
        });
    }

    public interface OnFindClick {
        void details(String articleId);

        void edit(String articleId);

        void imageUrl(String url);
    }
}
