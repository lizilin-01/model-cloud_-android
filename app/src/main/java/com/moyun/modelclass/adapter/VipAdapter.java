package com.moyun.modelclass.adapter;

import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.MemberSettingApi;
import com.moyun.modelclass.R;

import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 首页：vip
 */
public class VipAdapter extends BaseQuickAdapter<MemberSettingApi.BeanInfo, BaseViewHolder> {
    private int mSelectPosition = 0; // 默认选择第一个
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    public VipAdapter() {
        super(R.layout.item_vip);
    }

    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener onItemSelectedChangedListener) {
        mOnItemSelectedChangedListener = onItemSelectedChangedListener;
    }

    public void setSelected(int position) {
        if (mSelectPosition == position) return;
        this.mSelectPosition = position;
        if (mOnItemSelectedChangedListener != null) {
            mOnItemSelectedChangedListener.onItemSelected(getItem(position));
        }
        notifyDataSetChanged();
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MemberSettingApi.BeanInfo beanInfo) {
        LinearLayout llVipAll = baseViewHolder.getView(R.id.ll_vip_all);
        ImageView ivVipImage = (ImageView) baseViewHolder.getView(R.id.iv_vip_image);
        TextView ivVipTitle = (TextView) baseViewHolder.getView(R.id.iv_vip_title);
        TextView ivVipPrice = (TextView) baseViewHolder.getView(R.id.iv_vip_price);
        TextView ivVipLabel = (TextView) baseViewHolder.getView(R.id.iv_vip_label);

        ivVipPrice.setText(df.format(beanInfo.getMoney() * 0.01));
        ivVipLabel.setText(beanInfo.getLevelDesc());

        if (baseViewHolder.getLayoutPosition() == mSelectPosition) {
            FrameLayout.LayoutParams paramsOne = (FrameLayout.LayoutParams) llVipAll.getLayoutParams();
            paramsOne.height = SizeUtils.sp2px(118);//设置布局的高度
            paramsOne.width = SizeUtils.sp2px(160);//设置布局宽度
            llVipAll.setLayoutParams(paramsOne);//将设置好的布局参数应用到控件中


            llVipAll.setBackgroundResource(R.drawable.background_fff7ec_stroke_10);

            ivVipImage.setVisibility(View.VISIBLE);
            ivVipTitle.setVisibility(View.GONE);

            ivVipPrice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);

            ViewGroup.LayoutParams params = ivVipLabel.getLayoutParams();
            params.width = SizeUtils.sp2px(98);
            params.height = SizeUtils.sp2px(23);
            ivVipLabel.setLayoutParams(params);
        } else {
            FrameLayout.LayoutParams paramsTwo = (FrameLayout.LayoutParams) llVipAll.getLayoutParams();
            paramsTwo.height = SizeUtils.sp2px(87);//设置布局的高度
            paramsTwo.width = SizeUtils.sp2px(118);//设置布局宽度
            llVipAll.setLayoutParams(paramsTwo);//将设置好的布局参数应用到控件中

            llVipAll.setBackgroundResource(R.drawable.background_f4f4f4_10);

            ivVipImage.setVisibility(View.GONE);
            ivVipTitle.setVisibility(View.VISIBLE);

            ivVipPrice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

            ViewGroup.LayoutParams params = ivVipLabel.getLayoutParams();
            params.width = SizeUtils.sp2px(54);
            params.height = SizeUtils.sp2px(16);
            ivVipLabel.setLayoutParams(params);
        }
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected(MemberSettingApi.BeanInfo beanInfo);
    }
}
