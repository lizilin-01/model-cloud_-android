package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.UserSearchApi;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 搜索-搜索结果
 */
public class RelatedUsersAdapter extends BaseQuickAdapter<UserSearchApi.BeanInfo, BaseViewHolder> {

    public RelatedUsersAdapter() {
        super(R.layout.item_related_users);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, UserSearchApi.BeanInfo beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_related_users_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(3, beanInfo.getAvatar()))
                .placeholder(R.drawable.icon_head_portrait)
                .error(R.drawable.icon_head_portrait)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                    }
                });

        baseViewHolder.setText(R.id.tv_related_users_name, beanInfo.getNickName());
    }
}
