package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.CheckedTextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.ConfigPrintParamsApi;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 日期：2022/1/26 23:28
 * tip：打印相关参数 适配器
 */
public class ConfigPrintParamsAdapter extends BaseQuickAdapter<ConfigPrintParamsApi.BeanInfo, BaseViewHolder> {
    private int selectedPosition = 0; // 默认选择第一个
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;


    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener mOnItemSelectedChangedListener) {
        this.mOnItemSelectedChangedListener = mOnItemSelectedChangedListener;
    }

    public ConfigPrintParamsAdapter() {
        super(R.layout.item_config_print_params);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ConfigPrintParamsApi.BeanInfo beanInfo) {
        baseViewHolder.setText(R.id.tv_config_print_params_title, beanInfo.getParamLabel());
        baseViewHolder.setText(R.id.tv_config_print_params_content, beanInfo.getParamDesc());

        CheckedTextView addressCheck = baseViewHolder.getView(R.id.ct_config_print_params_check);
        addressCheck.setChecked(selectedPosition == baseViewHolder.getLayoutPosition());
        addressCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseViewHolder.getLayoutPosition() != selectedPosition) {
                    notifyItemChanged(selectedPosition); // 可选，如果有复杂动画
                    selectedPosition = baseViewHolder.getLayoutPosition();
                    notifyItemChanged(baseViewHolder.getLayoutPosition());
                }
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemSelected(beanInfo, selectedPosition);
                }
            }
        });
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected(ConfigPrintParamsApi.BeanInfo beanInfo, int position);//参数
    }
}
