package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 作者：Meteor
 * 文章：模型文章列表：关联模型
 */
public class ModelAssociationAdapter extends BaseQuickAdapter<ModelResponse, BaseViewHolder> {
    private HashSet<ModelResponse> selectedItems = new HashSet<>();
    private OnCheckedClick onCheckedClick;
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    public void setOnCheckedClick(OnCheckedClick onCheckedClick) {
        this.onCheckedClick = onCheckedClick;
    }

    public ModelAssociationAdapter() {
        super(R.layout.item_model_association);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_model_association_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(3, beanInfo.getCoverImage()))
                .placeholder(R.drawable.icon_head_portrait)
                .error(R.drawable.icon_head_portrait)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                    }
                });

        baseViewHolder.setText(R.id.tv_model_association_time, beanInfo.getUpdateTime());
        baseViewHolder.setText(R.id.tv_model_association_sold, "已售" + beanInfo.getSaleCount());
        baseViewHolder.setText(R.id.tv_model_association_name, beanInfo.getTitle());
        baseViewHolder.setText(R.id.tv_model_association_price, df.format(beanInfo.getPrice() * 0.01));

        CheckedTextView addressCheck = baseViewHolder.getView(R.id.ct_model_association_check);
        addressCheck.setChecked(selectedItems.contains(beanInfo));
        addressCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedItems.contains(beanInfo)) {
                    selectedItems.remove(beanInfo);
                } else {
                    selectedItems.add(beanInfo);
                }
                notifyItemChanged(baseViewHolder.getLayoutPosition());
                if (onCheckedClick != null) {
                    onCheckedClick.Checked(new ArrayList<>(selectedItems));
                }
            }
        });
    }

    public void setSelectedItems(){
        selectedItems.clear();
    }

    public interface OnCheckedClick {
        void Checked(List<ModelResponse> ModelList);
    }
}
