package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.bean.AddressResponse;
import com.moyun.modelclass.R;

public class AddressListAdapter extends BaseQuickAdapter<AddressResponse, BaseViewHolder> {
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;

    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener mOnItemSelectedChangedListener) {
        this.mOnItemSelectedChangedListener = mOnItemSelectedChangedListener;
    }

    public AddressListAdapter() {
        super(R.layout.item_address_list);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, AddressResponse beanInfo) {
        ImageView defaultLabel = baseViewHolder.getView(R.id.iv_address_default);

        if (beanInfo.isDefaultFlag()) {
            defaultLabel.setVisibility(View.VISIBLE);
        } else {
            defaultLabel.setVisibility(View.INVISIBLE);
        }

        baseViewHolder.setText(R.id.tv_address_name, beanInfo.getName());
        baseViewHolder.setText(R.id.tv_address_phone, beanInfo.getMobile());
        baseViewHolder.setText(R.id.tv_address_address, beanInfo.getProvince() + " " + beanInfo.getCity() + " " + beanInfo.getArea() + " " + beanInfo.getDetail());


        baseViewHolder.getView(R.id.rl_address_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemEdit(beanInfo.getId(), beanInfo.isDefaultFlag());
                }
            }
        });
    }


    public interface OnItemSelectedChangedListener {
        void onItemEdit(String addressId, boolean isDefaultFlag);//编辑地址
    }
}
