package com.moyun.modelclass.adapter;

import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.UserCenterPageApi;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 首页：文章状态
 */
public class MineMoreAdapter extends BaseQuickAdapter<UserCenterPageApi.MenuItemConfigListBean, BaseViewHolder> {
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;

    public MineMoreAdapter() {
        super(R.layout.item_mine_more);
    }

    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener onItemSelectedChangedListener) {
        this.mOnItemSelectedChangedListener = onItemSelectedChangedListener;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, UserCenterPageApi.MenuItemConfigListBean beanInfo) {
        baseViewHolder.setText(R.id.tv_mine_more_label, beanInfo.getLabel());
        baseViewHolder.setText(R.id.tv_mine_more_desc, beanInfo.getDesc());
        baseViewHolder.getView(R.id.rl_mine_more_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemSelected(beanInfo.getUrl());
                }
            }
        });
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected(String url);
    }
}
