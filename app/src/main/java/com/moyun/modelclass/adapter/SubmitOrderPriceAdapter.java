package com.moyun.modelclass.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.OrderTrialApi;
import com.moyun.modelclass.R;

import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 文章：提交订单 价格列表
 */
public class SubmitOrderPriceAdapter extends BaseQuickAdapter<OrderTrialApi.BeanInfo, BaseViewHolder> {
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    public SubmitOrderPriceAdapter() {
        super(R.layout.item_submit_order_price);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OrderTrialApi.BeanInfo beanInfo) {
        baseViewHolder.setText(R.id.tv_submit_order_price_title, beanInfo.getPriceLabel());
        baseViewHolder.setText(R.id.tv_submit_order_price_content, beanInfo.getPriceDesc());
        baseViewHolder.setText(R.id.tv_tv_submit_order_price_price, df.format(beanInfo.getPrice() * 0.01));
    }
}
