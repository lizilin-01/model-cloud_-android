package com.moyun.modelclass.adapter;

import android.graphics.Color;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.ModelTypeApi;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 首页：模型
 */
public class ModelTypeAdapter extends BaseQuickAdapter<ModelTypeApi.BeanInfo, BaseViewHolder> {
    private int mSelectPosition = 0; // 默认选择第一个
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;

    public ModelTypeAdapter() {
        super(R.layout.item_main_model_type);
    }

    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener onItemSelectedChangedListener) {
        mOnItemSelectedChangedListener = onItemSelectedChangedListener;
    }

    public void setSelected(int position) {
        if (mSelectPosition == position) return;
        this.mSelectPosition = position;
        if (mOnItemSelectedChangedListener != null) {
            mOnItemSelectedChangedListener.onItemSelected(position, getItem(position));
        }
        notifyDataSetChanged();
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelTypeApi.BeanInfo beanInfo) {
        TextView modelType = baseViewHolder.getView(R.id.tv_main_model_type);
        if (baseViewHolder.getLayoutPosition() == mSelectPosition) {
            modelType.setText(beanInfo.getCategoryName());
            modelType.setTextColor(Color.parseColor("#055D6C"));
        } else {
            modelType.setText(beanInfo.getCategoryName());
            modelType.setTextColor(Color.parseColor("#767676"));
        }
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected(int position, ModelTypeApi.BeanInfo response);
    }
}
