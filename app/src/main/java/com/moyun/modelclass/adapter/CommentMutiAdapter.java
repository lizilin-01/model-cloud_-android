package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.activity.LoginVerificationCodeActivity;
import com.moyun.modelclass.comment.bean.CommentEntity;
import com.moyun.modelclass.comment.bean.CommentMoreBean;
import com.moyun.modelclass.comment.bean.FirstLevelBean;
import com.moyun.modelclass.comment.bean.SecondLevelBean;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckCommentsLikesApi;
import com.moyun.modelclass.R;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ganhuanhui
 * 时间：2019/12/13 0013
 * 描述：
 */
public class CommentMutiAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    private OnCommentClick onCommentClick;
    protected MMKV kv;

    private List<CheckCommentsLikesApi.BeanInfo> mCommonLikeList = new ArrayList<>();
    private CheckCommentsLikesApi.BeanInfo commentsLikes = new CheckCommentsLikesApi.BeanInfo();
    private Drawable likeYes = null, likeNot = null;


    public void setOnCommentClick(OnCommentClick onCommentClick) {
        this.onCommentClick = onCommentClick;
        commentsLikes.setLikes(true);
    }

    public CommentMutiAdapter(List list) {
        super(list);
        addItemType(CommentEntity.TYPE_COMMENT_PARENT, R.layout.item_parent_comment);
        addItemType(CommentEntity.TYPE_COMMENT_CHILD, R.layout.item_child_comment);
        addItemType(CommentEntity.TYPE_COMMENT_MORE, R.layout.load_more_comment_layout);
        addItemType(CommentEntity.TYPE_COMMENT_EMPTY, R.layout.empty_comment_layout);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        kv = MMKV.defaultMMKV();
        likeYes = ContextCompat.getDrawable(getContext(), R.drawable.icon_zan_yes);
        likeNot = ContextCompat.getDrawable(getContext(), R.drawable.icon_zan_not);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, MultiItemEntity item) {
        switch (item.getItemType()) {
            case CommentEntity.TYPE_COMMENT_PARENT:
                bindCommentParent(helper, (FirstLevelBean) item);
                break;
            case CommentEntity.TYPE_COMMENT_CHILD:
                bindCommentChild(helper, (SecondLevelBean) item);
                break;
            case CommentEntity.TYPE_COMMENT_MORE:
                bindCommonMore(helper, (CommentMoreBean) item);
                break;
            case CommentEntity.TYPE_COMMENT_EMPTY:
                bindEmpty(helper, item);
                break;
        }
    }

    private void bindCommentParent(BaseViewHolder baseViewHolder, FirstLevelBean beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_parent_comment_head_portrait);
        if (beanInfo.getUserInfo() != null) {
            Glide.with(getContext())
                    .asBitmap()
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getUserInfo().getAvatar()))
                    .placeholder(R.drawable.icon_head_portrait)
                    .error(R.drawable.icon_head_portrait)
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                    .into(new BitmapImageViewTarget(headPortrait) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                            headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                        }
                    });

            baseViewHolder.setText(R.id.tv_parent_comment_users_name, beanInfo.getUserInfo().getNickName());
        }


        baseViewHolder.setText(R.id.tv_parent_comment_content, beanInfo.getContent());
        ImageView ParentCommentImage = baseViewHolder.getView(R.id.tv_parent_comment_image);
        if (beanInfo.getImages() != null && beanInfo.getImages().length > 0 && (beanInfo.getImages()[0]) != null && !((beanInfo.getImages()[0]).isEmpty())) {
            ParentCommentImage.setVisibility(View.VISIBLE);
            Glide.with(getContext())
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getImages()[0]))
                    .centerInside()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ParentCommentImage);
        } else {
            ParentCommentImage.setVisibility(View.GONE);
        }

        ParentCommentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    ScanImageDialog.getInstance(getContext(), CommonConfiguration.splitResUrl(3, beanInfo.getImages()[0])).builder().setNegativeButton().show();
                } else {
//                    ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(getContext());
                }
            }
        });

        baseViewHolder.setText(R.id.tv_parent_comment_time, beanInfo.getCreateTime());

        TextView commentLike = baseViewHolder.getView(R.id.tv_parent_comment_like);
        commentsLikes.setObjectId(beanInfo.getId());
        if (mCommonLikeList.contains(commentsLikes)) {
            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeYes, null, null, null);
            commentLike.setTextColor(Color.parseColor("#FF6600"));
            ((FirstLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setSelfLike(true);
        } else {
            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeNot, null, null, null);
            commentLike.setTextColor(Color.parseColor("#9DB5BA"));
            ((FirstLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setSelfLike(false);
        }
        commentLike.setText(String.valueOf(beanInfo.getLikes()));
        commentLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    if (CommonConfiguration.isLogin(kv)) {
                        onCommentClick.zan(beanInfo.getId(), !beanInfo.isSelfLike());

                        ((FirstLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setSelfLike(!beanInfo.isSelfLike());
                        if (beanInfo.isSelfLike()) {
                            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeYes, null, null, null);
                            commentLike.setTextColor(Color.parseColor("#FF6600"));

                            ((FirstLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setLikes(beanInfo.getLikes() + 1);
                            commentLike.setText(String.valueOf(beanInfo.getLikes()));
                        } else {
                            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeNot, null, null, null);
                            commentLike.setTextColor(Color.parseColor("#9DB5BA"));
                            ((FirstLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setLikes(beanInfo.getLikes() - 1);
                            commentLike.setText(String.valueOf(beanInfo.getLikes()));
                        }
                    } else {
//                        ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                        LoginVerificationCodeActivity.start(getContext());
                    }
                }
            }
        });

        baseViewHolder.getView(R.id.tv_parent_comment_replay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    onCommentClick.replay(beanInfo.getId());
                }
            }
        });

    }

    private void bindCommentChild(final BaseViewHolder baseViewHolder, SecondLevelBean beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_child_comment_head_portrait);
        if (beanInfo.getUserInfo() != null) {
            Glide.with(getContext())
                    .asBitmap()
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getUserInfo().getAvatar()))
                    .placeholder(R.drawable.icon_head_portrait)
                    .error(R.drawable.icon_head_portrait)
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                    .into(new BitmapImageViewTarget(headPortrait) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                            headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                        }
                    });
            baseViewHolder.setText(R.id.tv_child_comment_users_name_one, beanInfo.getUserInfo().getNickName());
        }

        if (beanInfo.getParentUserInfo() != null) {
            baseViewHolder.setText(R.id.tv_child_comment_users_name_two, beanInfo.getParentUserInfo().getNickName());
        }

        baseViewHolder.setText(R.id.tv_child_comment_content, beanInfo.getContent());

        ImageView childCommentImage = baseViewHolder.getView(R.id.tv_child_comment_image);
        if (beanInfo.getImages() != null && beanInfo.getImages().length > 0 && (beanInfo.getImages()[0]) != null && !((beanInfo.getImages()[0]).isEmpty())) {
            childCommentImage.setVisibility(View.VISIBLE);
            Glide.with(getContext())
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getImages()[0]))
                    .centerInside()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(childCommentImage);
        } else {
            childCommentImage.setVisibility(View.GONE);
        }

        childCommentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    ScanImageDialog.getInstance(getContext(), CommonConfiguration.splitResUrl(3, beanInfo.getImages()[0])).builder().setNegativeButton().show();
                } else {
//                    ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(getContext());
                }
            }
        });

        baseViewHolder.setText(R.id.tv_child_comment_time, beanInfo.getCreateTime());

        TextView commentLike = baseViewHolder.getView(R.id.tv_child_comment_like);
        commentsLikes.setObjectId(beanInfo.getId());
        if (mCommonLikeList.contains(commentsLikes)) {
            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeYes, null, null, null);
            commentLike.setTextColor(Color.parseColor("#FF6600"));
            ((SecondLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setSelfLike(true);
        } else {
            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeNot, null, null, null);
            commentLike.setTextColor(Color.parseColor("#9DB5BA"));
            ((SecondLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setSelfLike(false);
        }
        commentLike.setText(String.valueOf(beanInfo.getLikes()));
        commentLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    if (CommonConfiguration.isLogin(kv)) {
                        onCommentClick.zan(beanInfo.getId(), !beanInfo.isSelfLike());
                        ((SecondLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setSelfLike(!beanInfo.isSelfLike());
                        if (beanInfo.isSelfLike()) {
                            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeYes, null, null, null);
                            commentLike.setTextColor(Color.parseColor("#FF6600"));

                            ((SecondLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setLikes(beanInfo.getLikes() + 1);
                            commentLike.setText(String.valueOf(beanInfo.getLikes()));
                        } else {
                            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeNot, null, null, null);
                            commentLike.setTextColor(Color.parseColor("#9DB5BA"));
                            ((SecondLevelBean) getData().get(baseViewHolder.getLayoutPosition())).setLikes(beanInfo.getLikes() - 1);
                            commentLike.setText(String.valueOf(beanInfo.getLikes()));
                        }
                    } else {
//                        ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                        LoginVerificationCodeActivity.start(getContext());
                    }
                }
            }
        });
        baseViewHolder.getView(R.id.tv_child_comment_replay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    onCommentClick.replay(beanInfo.getId());
                }
            }
        });

    }

    private void bindCommonMore(BaseViewHolder baseViewHolder, CommentMoreBean item) {
        LinearLayout llGroup = baseViewHolder.getView(R.id.ll_group);
//        llGroup.setTag(item.getItemType());
//        baseViewHolder.addOnClickListener(R.id.ll_group);

        llGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    onCommentClick.moreChildren(item.getParentCommentId(), item.getObjectId(), baseViewHolder.getLayoutPosition());
                }
            }
        });
    }

    private void bindEmpty(BaseViewHolder helper, MultiItemEntity item) {

    }


    public void setZanState(List<CheckCommentsLikesApi.BeanInfo> commonLikeList) {
        mCommonLikeList.clear();
        this.mCommonLikeList = commonLikeList;
        notifyDataSetChanged();
    }

    public interface OnCommentClick {
        void zan(String commentId, boolean isLike);

        void replay(String commentId);

        void moreChildren(String parentUserId, String lastId, int position);
    }
}
