package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;
import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 文章：模型文章列表
 */
public class ModelBuyAdapter extends BaseQuickAdapter<ModelResponse, BaseViewHolder> {
    private OnBuyClick onBuyClick;
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    public void setOnBuyClick(OnBuyClick onBuyClick) {
        this.onBuyClick = onBuyClick;
    }

    public ModelBuyAdapter() {
        super(R.layout.item_model_buy);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_model_buy_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(beanInfo.getCoverImage()))
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        baseViewHolder.setText(R.id.tv_model_buy_time, beanInfo.getUpdateTime());
        baseViewHolder.setText(R.id.tv_model_buy_sold, "已售" + beanInfo.getSaleCount());
        baseViewHolder.setText(R.id.tv_model_buy_name, beanInfo.getTitle());
        baseViewHolder.setText(R.id.tv_model_buy_price, df.format(beanInfo.getPrice() * 0.01));

        baseViewHolder.getView(R.id.tv_model_buy_add_shopping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onBuyClick != null) {
                    onBuyClick.buy(beanInfo.getId());
                }
            }
        });
        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBuyClick != null) {
                    onBuyClick.scanModel(beanInfo.getId());
                }
            }
        });
    }

    public interface OnBuyClick {
        void buy(String modelId);

        void scanModel(String modelId);
    }
}
