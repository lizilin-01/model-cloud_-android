package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.blankj.utilcode.util.ClickUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 搜索：热搜
 */
public class SearchHotAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private OnChoiceClick onChoiceClick;

    public void setOnChoiceClick(OnChoiceClick onChoiceClick) {
        this.onChoiceClick = onChoiceClick;
    }
    public SearchHotAdapter() {
        super(R.layout.item_search_hot);
    }
    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String beanInfo) {
        TextView hotTitle = baseViewHolder.getView(R.id.tv_search_hot_title);
//        ImageView quantity = baseViewHolder.getView(R.id.iv_search_hot_image);

        hotTitle.setText(beanInfo);

        ClickUtils.applySingleDebouncing(hotTitle, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onChoiceClick != null) {
                    onChoiceClick.choice(beanInfo);
                }
            }
        });
    }

    public interface OnChoiceClick {
        void choice(String search);
    }
}
