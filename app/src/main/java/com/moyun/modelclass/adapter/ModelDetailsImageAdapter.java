package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 文章：模型详情图片
 */
public class ModelDetailsImageAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private OnImageClick onImageClick;

    public void setOnImageClick(OnImageClick onImageClick) {
        this.onImageClick = onImageClick;
    }

    public ModelDetailsImageAdapter() {
        super(R.layout.item_model_details_image);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String beanInfo) {
        //图片
        ImageView modelDetailsImage = baseViewHolder.getView(R.id.iv_model_details_image);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(beanInfo))
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(modelDetailsImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        modelDetailsImage.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        baseViewHolder.getView(R.id.iv_model_details_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onImageClick != null) {
                    onImageClick.showImage(CommonConfiguration.splitResUrl(3, beanInfo));
                }
            }
        });
    }

    public interface OnImageClick {
        void showImage(String imageUrl);
    }
}
