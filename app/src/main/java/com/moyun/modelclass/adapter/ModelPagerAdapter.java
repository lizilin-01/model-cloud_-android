package com.moyun.modelclass.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import java.util.List;

public class ModelPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mList;
    private String mArticleId = "";

    public ModelPagerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.mList = list;
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position);//显示第几个页面
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return mList.size();//有几个页面
    }
}
