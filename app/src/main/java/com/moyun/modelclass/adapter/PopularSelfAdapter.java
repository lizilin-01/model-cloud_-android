package com.moyun.modelclass.adapter;

import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 已选择话题:删除
 */
public class PopularSelfAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private OnDeleteClick onDeleteClick;

    public void setOnDeleteClick(OnDeleteClick onDeleteClick) {
        this.onDeleteClick = onDeleteClick;
    }

    public PopularSelfAdapter() {
        super(R.layout.item_popular_delete);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String beanInfo) {
        baseViewHolder.setText(R.id.tv_popular_delete_time, beanInfo);

        baseViewHolder.getView(R.id.iv_popular_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onDeleteClick != null) {
                    onDeleteClick.delete(baseViewHolder.getLayoutPosition());
                }
            }
        });
    }

    public interface OnDeleteClick {
        void delete(int position);
    }
}
