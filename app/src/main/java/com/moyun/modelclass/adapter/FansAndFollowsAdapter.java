package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.FansAndFollowResponse;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 首页：关注和发现
 */
public class FansAndFollowsAdapter extends BaseQuickAdapter<FansAndFollowResponse, BaseViewHolder> {
    public FansAndFollowsAdapter() {
        super(R.layout.item_fans_and_follows);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, FansAndFollowResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_fans_and_follows_portrait);
        if (beanInfo.getAvatar() != null) {
            Glide.with(getContext())
                    .asBitmap()
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getAvatar()))
                    .placeholder(R.drawable.icon_head_portrait)
                    .error(R.drawable.icon_head_portrait)
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                    .into(new BitmapImageViewTarget(headPortrait) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                            headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                        }
                    });
        }

        baseViewHolder.setText(R.id.tv_fans_and_follows_name, beanInfo.getNickName());
    }
}
