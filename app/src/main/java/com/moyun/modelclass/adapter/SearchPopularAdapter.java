package com.moyun.modelclass.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.ClickUtils;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.history.FlowAdapter;


/**
 * @author zwl
 * 搜索：热门话题
 */
public class SearchPopularAdapter extends FlowAdapter<String> {
    private OnChoiceClick onChoiceClick;

    public void setOnChoiceClick(OnChoiceClick onChoiceClick) {
        this.onChoiceClick = onChoiceClick;
    }

    @Override
    public View getView(ViewGroup parent, String item, int position) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_popular, null);
    }

    @Override
    public void initView(View view, String item, int position) {
        AppCompatTextView popularTitle = view.findViewById(R.id.tv_search_popular_title);
        popularTitle.setText("#" + item);
        ClickUtils.applySingleDebouncing(popularTitle, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onChoiceClick != null) {
                    onChoiceClick.choice(item);
                }
            }
        });
    }

    public interface OnChoiceClick {
        void choice(String search);
    }
}
