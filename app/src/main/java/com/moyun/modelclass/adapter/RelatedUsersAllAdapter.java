package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckFollowListUserApi;
import com.moyun.modelclass.http.api.UserSearchApi;
import com.moyun.modelclass.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * 搜索-搜索结果-全部用户
 */
public class RelatedUsersAllAdapter extends BaseQuickAdapter<UserSearchApi.BeanInfo, BaseViewHolder> {
    private List<CheckFollowListUserApi.BeanInfo> mUserFollowList = new ArrayList<>();
    private CheckFollowListUserApi.BeanInfo UserFollow = new CheckFollowListUserApi.BeanInfo();

    private OnFindClick onFindClick;

    public void setOnFindClick(OnFindClick onFindClick) {
        this.onFindClick = onFindClick;
        UserFollow.setFollow(true);
    }

    public RelatedUsersAllAdapter() {
        super(R.layout.item_related_users_all);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, UserSearchApi.BeanInfo beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_related_users_all_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(3, beanInfo.getAvatar()))
                .placeholder(R.drawable.icon_head_portrait)
                .error(R.drawable.icon_head_portrait)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                    }
                });

        baseViewHolder.setText(R.id.tv_related_users_all_name, beanInfo.getNickName());
        baseViewHolder.setText(R.id.tv_related_users_all_title, beanInfo.getDesc());

        ImageView isFollow = baseViewHolder.getView(R.id.tv_related_users_all_is_follow);

        UserFollow.setUserId(beanInfo.getUserId());
        if (mUserFollowList.contains(UserFollow)) {
            isFollow.setBackgroundResource(R.drawable.icon_follow_not);
            getData().get(baseViewHolder.getLayoutPosition()).setFollow(true);
        } else {
            isFollow.setBackgroundResource(R.drawable.icon_follow_yes);
            getData().get(baseViewHolder.getLayoutPosition()).setFollow(false);
        }

        headPortrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onFindClick != null) {
                    onFindClick.userInfo(beanInfo.getUserId());
                }
            }
        });

        isFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.follow(beanInfo.getUserId(),!beanInfo.getFollow());
//                    getData().get(baseViewHolder.getLayoutPosition()).setFollow(!beanInfo.getFollow());
//                    if (beanInfo.getFollow()){
//                        isFollow.setBackgroundResource(R.drawable.icon_follow_not);
//                    }else {
//                        isFollow.setBackgroundResource(R.drawable.icon_follow_yes);
//                    }
                }
            }
        });
    }

    public void setFollowState(List<CheckFollowListUserApi.BeanInfo> userFollowList) {
        mUserFollowList.clear();
        this.mUserFollowList = userFollowList;
        notifyDataSetChanged();
    }
    public interface OnFindClick {
        void userInfo(String userId);

        void follow(String userId,boolean isFollow);
    }
}
