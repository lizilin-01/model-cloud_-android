package com.moyun.modelclass.adapter;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.bean.OrderModelResponse;
import com.moyun.modelclass.http.bean.OrderResponse;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;
import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 首页：我的订单
 */
public class MyOrderAdapter extends BaseQuickAdapter<OrderResponse, BaseViewHolder> {
    private OnFindClick onFindClick;
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    public void setOnFindClick(OnFindClick onFindClick) {
        this.onFindClick = onFindClick;
    }

    public MyOrderAdapter() {
        super(R.layout.item_my_order);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OrderResponse beanInfo) {
        //1、待付款，2、待发货、3、待收货、4、待评价
        TextView orderState = baseViewHolder.getView(R.id.tv_my_order_state);
        if (beanInfo.getOrderStateVO() == 1) {
            baseViewHolder.setText(R.id.tv_my_order_pay_time, "下单时间：" + beanInfo.getCreateTime());
            orderState.setText("待付款");
            orderState.setTextColor(Color.parseColor("#FF6600"));

            baseViewHolder.setGone(R.id.tv_my_order_close, false);
            baseViewHolder.setGone(R.id.tv_my_order_pay, false);
            baseViewHolder.setGone(R.id.tv_my_order_details, true);
            baseViewHolder.setGone(R.id.tv_my_order_delete, true);
            baseViewHolder.setGone(R.id.tv_my_order_evaluate, true);
        } else if (beanInfo.getOrderStateVO() == 2) {
            baseViewHolder.setText(R.id.tv_my_order_pay_time, "下单时间：" + beanInfo.getCreateTime());
            orderState.setText("待发货");
            orderState.setTextColor(Color.parseColor("#1C3049"));
            baseViewHolder.setGone(R.id.tv_my_order_close, true);
            baseViewHolder.setGone(R.id.tv_my_order_pay, true);
            baseViewHolder.setGone(R.id.tv_my_order_details, true);
            baseViewHolder.setGone(R.id.tv_my_order_delete, true);
            baseViewHolder.setGone(R.id.tv_my_order_evaluate, true);
        } else if (beanInfo.getOrderStateVO() == 3) {
            baseViewHolder.setText(R.id.tv_my_order_pay_time, "下单时间：" + beanInfo.getCreateTime());
            orderState.setText("待收货");
            orderState.setTextColor(Color.parseColor("#1C3049"));
            baseViewHolder.setGone(R.id.tv_my_order_close, true);
            baseViewHolder.setGone(R.id.tv_my_order_pay, true);
            baseViewHolder.setGone(R.id.tv_my_order_details, true);
            baseViewHolder.setGone(R.id.tv_my_order_delete, true);
            baseViewHolder.setGone(R.id.tv_my_order_evaluate, true);
        } else if (beanInfo.getOrderStateVO() == 4) {
            baseViewHolder.setText(R.id.tv_my_order_pay_time, "下单时间：" + beanInfo.getCreateTime());
            orderState.setText("待评价");
            orderState.setTextColor(Color.parseColor("#1C3049"));
            baseViewHolder.setGone(R.id.tv_my_order_close, true);
            baseViewHolder.setGone(R.id.tv_my_order_pay, true);
            baseViewHolder.setGone(R.id.tv_my_order_details, true);
            baseViewHolder.setGone(R.id.tv_my_order_delete, false);
            baseViewHolder.setGone(R.id.tv_my_order_evaluate, false);
        }else {
            baseViewHolder.setText(R.id.tv_my_order_pay_time, "下单时间：" + beanInfo.getCreateTime());
            orderState.setText(beanInfo.getOrderStateVODesc());
            orderState.setTextColor(Color.parseColor("#1C3049"));
            baseViewHolder.setGone(R.id.tv_my_order_close, true);
            baseViewHolder.setGone(R.id.tv_my_order_pay, true);
            baseViewHolder.setGone(R.id.tv_my_order_details, true);
            baseViewHolder.setGone(R.id.tv_my_order_delete, true);
            baseViewHolder.setGone(R.id.tv_my_order_evaluate, true);
        }

        RecyclerView modelListRefresh = baseViewHolder.getView(R.id.recycler_my_order_list);

        if (beanInfo.getModelList() != null && beanInfo.getModelList().size() > 0) {
            MyOrderModelAdapter myOrderModelAdapter = new MyOrderModelAdapter();
            modelListRefresh.setLayoutManager(new LinearLayoutManager(getContext()));
            modelListRefresh.addItemDecoration(new DividerItemDecoration(getContext(), Color.parseColor("#EFEFEF"), 1, SizeUtils.dp2px(6), SizeUtils.dp2px(6), false));
            modelListRefresh.setAdapter(myOrderModelAdapter);
            myOrderModelAdapter.setOrderId(beanInfo.getId());
            myOrderModelAdapter.setList(beanInfo.getModelList());
        }

        int modelCount = 0;
        for (OrderModelResponse modelResponse : beanInfo.getModelList()) {
            modelCount = modelCount + modelResponse.getCount();
        }

        baseViewHolder.setText(R.id.tv_my_order_model_count, "共" + modelCount + "件商品");
        baseViewHolder.setText(R.id.tv_my_order_total_price, "合计：" + df.format(beanInfo.getTotalPrice() * 0.01));

        //关闭订单
        baseViewHolder.getView(R.id.tv_my_order_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.close(beanInfo.getId());
                }
            }
        });

        //立即支付
        baseViewHolder.getView(R.id.tv_my_order_pay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.pay(beanInfo.getId());
                }
            }
        });

        //查看详情
        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.details(beanInfo.getId());
                }
            }
        });

        //删除订单
        baseViewHolder.getView(R.id.tv_my_order_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.delete(beanInfo.getId());
                }
            }
        });

        //立即评价
        baseViewHolder.getView(R.id.tv_my_order_evaluate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onFindClick != null) {
                    onFindClick.evaluate(beanInfo.getId());
                }
            }
        });
    }

    public interface OnFindClick {
        void close(String orderId);

        void pay(String orderId);

        void details(String orderId);

        void delete(String orderId);

        void evaluate(String orderId);
    }
}
