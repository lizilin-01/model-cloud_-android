package com.moyun.modelclass.adapter;

import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.listener.OnContentListener;
import com.moyun.modelclass.http.bean.CommonDialogBean;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * tip：公共共用弹窗
 */
public class CommonBottomAdapter extends BaseQuickAdapter<CommonDialogBean, BaseViewHolder> {
    private OnContentListener onContentListener;

    public CommonBottomAdapter() {
        super(R.layout.item_common_bottom);

    }

    public void setOnContentListener(OnContentListener onContentListener) {
        this.onContentListener = onContentListener;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, CommonDialogBean content) {
        baseViewHolder.setText(R.id.common_bottom_content, content.getMessage());
        ClickUtils.applySingleDebouncing(baseViewHolder.itemView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onContentListener != null) {
                    onContentListener.content(1, content);
                }
            }
        });
    }
}
