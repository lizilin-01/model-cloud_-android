package com.moyun.modelclass.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.dialog.ModelSizeDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 文章：模型订单列表
 */
public class ModelOrderAdapter extends BaseQuickAdapter<ModelResponse, BaseViewHolder> {
    private int mMinLimit = 1;//最小数量
    private int mMaxLimit = 999999;//最大数量
    private int mQuantity = 0;//数量
    private Activity mActivity;

    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;


    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener mOnItemSelectedChangedListener) {
        this.mOnItemSelectedChangedListener = mOnItemSelectedChangedListener;
    }

    public ModelOrderAdapter(Activity activity) {
        super(R.layout.item_model_order);
        this.mActivity = activity;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_model_order_head_portrait);
        Glide.with(getContext())
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(beanInfo.getCoverImage()))
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(headPortrait) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        headPortrait.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        baseViewHolder.setText(R.id.tv_model_order_time, beanInfo.getUpdateTime());
        baseViewHolder.setText(R.id.tv_model_order_sold, "已售" + beanInfo.getSaleCount());
        baseViewHolder.setText(R.id.tv_model_order_name, beanInfo.getTitle());
        final TextView num = baseViewHolder.getView(R.id.tv_model_order_number);//单个规格总数量

        final TextView size = baseViewHolder.getView(R.id.tv_model_order_size);//单个规格尺寸

        size.setText("规格：" + beanInfo.getModelSizeList().get(0).getSize() + " >");

        size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModelSizeDialog dialog = ModelSizeDialog.getInstance(mActivity, beanInfo.getSelectPosition(), beanInfo.getModelSizeList(), new ModelSizeDialog.OnProjectTypeListener() {
                    @Override
                    public void content(ModelResponse.ModelSizeResponse checkBeanInfo, int position) {
                        getData().get(baseViewHolder.getLayoutPosition()).setSelectPosition(position);
                        size.setText("规格：" + beanInfo.getModelSizeList().get(position).getSize() + " >");
                        if (mOnItemSelectedChangedListener != null) {
                            mOnItemSelectedChangedListener.onItemSelected();
                        }
                    }

                    @Override
                    public void cancel() {

                    }
                }).builder();
                dialog.show();
            }
        });

        baseViewHolder.getView(R.id.iv_model_order_minus_shopping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int curNum = Integer.parseInt(TextUtils.isEmpty(num.getText().toString()) ? "0" : num.getText().toString());
                if (curNum <= mMinLimit) {
                    ToastUtils.showShort("数量不能再少了噢~");
                } else {
                    num.setText(String.valueOf(curNum = (curNum - 1)));
                    mQuantity = curNum;
                }
                beanInfo.setAddCount(mQuantity);
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemSelected();
                }
            }
        });

        baseViewHolder.getView(R.id.iv_model_buy_add_shopping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(num.getText().toString())) {
                    num.setText(String.valueOf(mMinLimit));
                }
                int curNum = Integer.parseInt(num.getText().toString());
                if (curNum >= mMaxLimit) {
                    ToastUtils.showShort(getContext().getString(R.string.shop_cart_max_limit_tips, mMaxLimit));
                } else {
                    num.setText(String.valueOf(curNum = curNum + 1));
                    mQuantity = curNum;
                }
                beanInfo.setAddCount(mQuantity);
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.onItemSelected();
                }
            }
        });
    }

    public interface OnItemSelectedChangedListener {
        void onItemSelected();//参数
    }
}
