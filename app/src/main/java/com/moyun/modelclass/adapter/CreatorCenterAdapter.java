package com.moyun.modelclass.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.api.FundWithdrawalOrderListApi;
import com.moyun.modelclass.R;

import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * 文章：创作者中心
 */
public class CreatorCenterAdapter extends BaseQuickAdapter<FundWithdrawalOrderListApi.BeanInfo, BaseViewHolder> {
    private final DecimalFormat df = new DecimalFormat("￥##0.00");


    public CreatorCenterAdapter() {
        super(R.layout.item_creator_center);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, FundWithdrawalOrderListApi.BeanInfo beanInfo) {
        baseViewHolder.setText(R.id.tv_creator_center_state, beanInfo.getOrderStateDesc());//订单状态
        baseViewHolder.setText(R.id.tv_creator_center_ddbh, beanInfo.getWithdrawalOrderNo());//订单编号
        baseViewHolder.setText(R.id.tv_creator_center_txrxm, beanInfo.getRealName());//提现人姓名
        baseViewHolder.setText(R.id.tv_creator_center_txsj, beanInfo.getCreateTime());//提现时间
        if (beanInfo.getSuccessTime() == null || beanInfo.getSuccessTime().isEmpty()) {
            baseViewHolder.setGone(R.id.rl_creator_center_txcgsj, true);
            baseViewHolder.setGone(R.id.line_creator_center_txcgsj, true);

        } else {
            baseViewHolder.setGone(R.id.rl_creator_center_txcgsj, false);
            baseViewHolder.setGone(R.id.line_creator_center_txcgsj, false);
            baseViewHolder.setText(R.id.tv_creator_center_txcgsj, beanInfo.getSuccessTime());//提现成功时间
        }

        baseViewHolder.setText(R.id.tv_creator_center_sxfje, df.format(beanInfo.getCommission() * 0.01));//手续费
        if (beanInfo.getTransactionId() == null || beanInfo.getTransactionId().isEmpty()) {
            baseViewHolder.setGone(R.id.rl_creator_center_qdlsh, true);
            baseViewHolder.setGone(R.id.line_creator_center_qdlsh, true);
        } else {
            baseViewHolder.setGone(R.id.rl_creator_center_qdlsh, false);
            baseViewHolder.setGone(R.id.line_creator_center_qdlsh, false);
            baseViewHolder.setText(R.id.tv_creator_center_qdlsh, beanInfo.getTransactionId());//渠道流水号
        }

        if (beanInfo.getFailReason() == null || beanInfo.getFailReason().isEmpty()) {
            baseViewHolder.setGone(R.id.rl_creator_center_sbyy, true);
            baseViewHolder.setGone(R.id.line_creator_center_sbyy, true);
        } else {
            baseViewHolder.setGone(R.id.rl_creator_center_sbyy, false);
            baseViewHolder.setGone(R.id.line_creator_center_sbyy, false);
            baseViewHolder.setText(R.id.tv_creator_center_sbyy, beanInfo.getFailReason());//失败原因
        }
        baseViewHolder.setText(R.id.tv_creator_center_txdz, df.format(beanInfo.getAmount() * 0.01));//提现到账
    }
}
