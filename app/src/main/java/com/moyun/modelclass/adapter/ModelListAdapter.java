package com.moyun.modelclass.adapter;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 首页：模型列表适配器
 */
public class ModelListAdapter extends BaseQuickAdapter<ModelResponse, BaseViewHolder> {
    private OnItemSelectedChangedListener mOnItemSelectedChangedListener;

    public ModelListAdapter() {
        super(R.layout.item_model_list);
    }

    public void setOnItemSelectedChangedListener(OnItemSelectedChangedListener onItemSelectedChangedListener) {
        mOnItemSelectedChangedListener = onItemSelectedChangedListener;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ModelResponse beanInfo) {
        //模型图片
        ImageView modelImage = baseViewHolder.getView(R.id.iv_model_list_image);
        Glide.with(getContext())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(CommonConfiguration.splitResUrl(4,beanInfo.getCoverImage()))
                .into(modelImage);

        baseViewHolder.setText(R.id.tv_model_list_title, beanInfo.getTitle());

        int stlNum = 0;
        if (beanInfo.getFiles() != null && beanInfo.getFiles().size() > 0) {
            stlNum = stlNum + beanInfo.getFiles().size();
        }
        if (beanInfo.getOnePiece() != null && beanInfo.getOnePiece().size() > 0) {
            stlNum = stlNum + beanInfo.getOnePiece().size();
        }

        baseViewHolder.setText(R.id.tv_model_list_file_quantity, stlNum + "个文件");

        baseViewHolder.setText(R.id.tv_model_list_sold, "已售" + beanInfo.getSaleCount());

        baseViewHolder.getView(R.id.rl_model_list_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemSelectedChangedListener != null) {
                    mOnItemSelectedChangedListener.details(beanInfo.getId());
                }
            }
        });
    }

    public interface OnItemSelectedChangedListener {
        void details(String modelId);
    }
}
