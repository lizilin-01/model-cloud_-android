package com.moyun.modelclass.adapter;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 文章：领域
 */
public class SpecialityAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public SpecialityAdapter() {
        super(R.layout.item_speciality);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String beanInfo) {
        baseViewHolder.setText(R.id.tv_speciality_tip, beanInfo);
    }
}
