package com.moyun.modelclass.adapter;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.moyun.modelclass.activity.LoginVerificationCodeActivity;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckCommentsLikesApi;
import com.moyun.modelclass.http.bean.CommentResponse;
import com.moyun.modelclass.R;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * 首页：子评论
 */
public class ChildCommentAdapter extends BaseQuickAdapter<CommentResponse, BaseViewHolder> {
    private OnCommentClick onCommentClick;
    protected MMKV kv;

    private List<CheckCommentsLikesApi.BeanInfo> mCommonLikeList = new ArrayList<>();
    private CheckCommentsLikesApi.BeanInfo commentsLikes = new CheckCommentsLikesApi.BeanInfo();

    private Drawable likeYes = null, likeNot = null;

    public void setOnCommentClick(OnCommentClick onCommentClick) {
        this.onCommentClick = onCommentClick;
        commentsLikes.setLikes(true);
    }

    public ChildCommentAdapter() {
        super(R.layout.item_child_comment_one);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        kv = MMKV.defaultMMKV();
        likeYes = ContextCompat.getDrawable(getContext(), R.drawable.icon_zan_yes);
        likeNot = ContextCompat.getDrawable(getContext(), R.drawable.icon_zan_not);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, CommentResponse beanInfo) {
        //头像
        ImageView headPortrait = baseViewHolder.getView(R.id.iv_child_comment_head_portrait);
        if (beanInfo.getUserInfo() != null) {
            Glide.with(getContext())
                    .asBitmap()
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getUserInfo().getAvatar()))
                    .placeholder(R.drawable.icon_head_portrait)
                    .error(R.drawable.icon_head_portrait)
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                    .into(new BitmapImageViewTarget(headPortrait) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                            headPortrait.setImageBitmap(ImageUtils.toRound(resource));
                        }
                    });
            baseViewHolder.setText(R.id.tv_child_comment_users_name_one, beanInfo.getUserInfo().getNickName());
        }

        if (beanInfo.getParentUserInfo() != null) {
            baseViewHolder.setText(R.id.tv_child_comment_users_name_two, beanInfo.getParentUserInfo().getNickName());
        }

        baseViewHolder.setText(R.id.tv_child_comment_content, beanInfo.getContent());

        ImageView commentImage = baseViewHolder.getView(R.id.tv_child_comment_image);
        if (beanInfo.getImages() != null && beanInfo.getImages().length > 0) {
            commentImage.setVisibility(View.VISIBLE);

            Glide.with(getContext())
                    .load(CommonConfiguration.splitResUrl(3, beanInfo.getImages()[0]))
                    .centerInside()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(commentImage);
        } else {
            commentImage.setVisibility(View.GONE);
        }

        commentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    ScanImageDialog.getInstance(getContext(), CommonConfiguration.splitResUrl(3, beanInfo.getImages()[0])).builder().setNegativeButton().show();
                }else {
//                    ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(getContext());
                }
            }
        });

        baseViewHolder.setText(R.id.tv_child_comment_time, beanInfo.getCreateTime());

        TextView commentLike = baseViewHolder.getView(R.id.tv_child_comment_like);
        commentsLikes.setObjectId(beanInfo.getId());
        if (mCommonLikeList.contains(commentsLikes)) {
            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeYes, null, null, null);
            commentLike.setTextColor(Color.parseColor("#FF6600"));
            getData().get(baseViewHolder.getLayoutPosition()).setSelfLike(true);
        } else {
            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeNot, null, null, null);
            commentLike.setTextColor(Color.parseColor("#9DB5BA"));
            getData().get(baseViewHolder.getLayoutPosition()).setSelfLike(false);
        }
        commentLike.setText(String.valueOf(beanInfo.getLikes()));
        commentLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    if (CommonConfiguration.isLogin(kv)) {
                        onCommentClick.zan(beanInfo.getId(), !beanInfo.isSelfLike());
                        getData().get(baseViewHolder.getLayoutPosition()).setSelfLike(!beanInfo.isSelfLike());
                        if (beanInfo.isSelfLike()) {
                            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeYes, null, null, null);
                            commentLike.setTextColor(Color.parseColor("#FF6600"));

                            getData().get(baseViewHolder.getLayoutPosition()).setLikes(beanInfo.getLikes() + 1);
                            commentLike.setText(String.valueOf(beanInfo.getLikes()));
                        } else {
                            commentLike.setCompoundDrawablesWithIntrinsicBounds(likeNot, null, null, null);
                            commentLike.setTextColor(Color.parseColor("#9DB5BA"));
                            getData().get(baseViewHolder.getLayoutPosition()).setLikes(beanInfo.getLikes() - 1);
                            commentLike.setText(String.valueOf(beanInfo.getLikes()));
                        }
                    } else {
//                        ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                        LoginVerificationCodeActivity.start(getContext());
                    }
                }
            }
        });
        baseViewHolder.getView(R.id.tv_child_comment_replay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCommentClick != null) {
                    onCommentClick.replay(beanInfo.getId());
                }
            }
        });
    }

    public void setZanState(List<CheckCommentsLikesApi.BeanInfo> commonLikeList) {
        mCommonLikeList.clear();
        this.mCommonLikeList = commonLikeList;
        notifyDataSetChanged();
    }

    public interface OnCommentClick {
        void zan(String commentId, boolean isLike);

        void replay(String commentId);
    }
}
