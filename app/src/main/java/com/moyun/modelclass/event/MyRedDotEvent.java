package com.moyun.modelclass.event;

/**
 * 作者：Meteor
 * tip：红点
 */
public class MyRedDotEvent {
    private int type = 0;//“我的”是否有提醒 1是0否

    public MyRedDotEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
