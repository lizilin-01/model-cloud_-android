package com.moyun.modelclass.event;

/**
 * 作者：Meteor
 * tip：登录身份授权
 */
public class WxLoginEvent {
    private String code;
    private int type;

    public WxLoginEvent(String code, int type) {
        this.code = code;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
