package com.moyun.modelclass.event;

/**
 * 作者：Meteor
 * tip：登录状态
 */
public class LoginStateEvent {
    private int state = 0;//1：登录；-1：退出登录

    public LoginStateEvent(int type) {
        this.state = type;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
