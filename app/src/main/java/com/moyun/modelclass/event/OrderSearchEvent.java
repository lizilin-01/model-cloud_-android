package com.moyun.modelclass.event;

/**
 * 作者：Meteor
 * tip：订单搜索词
 */
public class OrderSearchEvent {
    private String search = "";//搜索词

    public OrderSearchEvent(String search) {
        this.search = search;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
