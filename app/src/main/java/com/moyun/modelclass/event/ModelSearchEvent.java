package com.moyun.modelclass.event;

/**
 * 作者：Meteor
 * tip：模型搜索词
 */
public class ModelSearchEvent {
    private String search = "";//搜索词

    public ModelSearchEvent(String search) {
        this.search = search;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
