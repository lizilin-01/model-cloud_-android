package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.http.listener.OnHttpListener;
import com.moyun.modelclass.activity.LoginVerificationCodeActivity;
import com.moyun.modelclass.http.api.AppCodeLoginApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.PwdEditText;

/**
 * 作者：Meteor
 * 日期：2022/2/49:38 下午
 * tip：解除绑定微信、支付宝
 */
public class UnbindDialog {
    private TextView tvUnbindTip;
    private PwdEditText etUnbindPwd;
    private TextView tvUnbindSend;
    private TextView unbindCancel;
    private TextView unbindDetermine;

    private static UnbindDialog alertDialog = null;
    private Context context;
    private Dialog dialog;
    private Display display;
    private String mPhoneNumber = "";//手机号
    private String mGuid = "";

    public static UnbindDialog getInstance(Context context, String phoneNumber) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new UnbindDialog(context, phoneNumber);
                }
            }
        }
        return alertDialog;
    }

    public UnbindDialog(Context context, String phoneNumber) {
        this.context = context;
        this.mPhoneNumber = phoneNumber;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    @SuppressLint("SetTextI18n")
    public UnbindDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_unbind, null);
        tvUnbindTip = (TextView) view.findViewById(R.id.tv_unbind_tip);
        etUnbindPwd = (PwdEditText) view.findViewById(R.id.et_unbind_pwd);
        tvUnbindSend = (TextView) view.findViewById(R.id.tv_unbind_send);
        unbindCancel = (TextView) view.findViewById(R.id.unbind_cancel);
        unbindDetermine = (TextView) view.findViewById(R.id.unbind_determine);
        KeyboardUtils.showSoftInput(etUnbindPwd);

        tvUnbindTip.setText("手机尾号 " + mPhoneNumber.substring(mPhoneNumber.length() - 4));

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.85);
        dialog.getWindow().setAttributes(attributes);

        tvUnbindSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegexUtils.isMobileSimple(mPhoneNumber)) {
                    EasyHttp.post((LifecycleOwner) context)
                            .api(new AppCodeLoginApi().setMobile(mPhoneNumber).setSmsType(2))
                            .request(new HttpCallback<HttpData<AppCodeLoginApi.Bean>>((OnHttpListener) context) {
                                @SuppressLint("SetTextI18n")
                                @Override
                                public void onSucceed(HttpData<AppCodeLoginApi.Bean> result) {
                                    mGuid = result.getData().getGuid();
                                    ToastUtils.showShort("发送成功");
                                }
                            });
                }
            }
        });
        return this;
    }

    //取消
    public UnbindDialog setNegativeButton() {
        ClickUtils.applySingleDebouncing(unbindCancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftInput(etUnbindPwd);
                alertDialog = null;
                dialog.dismiss();
            }
        });
        return this;
    }


    //确定
    public UnbindDialog setPositiveButton(OnPositiveListener listener) {
        ClickUtils.applySingleDebouncing(unbindDetermine, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null && !mGuid.isEmpty()) {
                    KeyboardUtils.hideSoftInput(etUnbindPwd);
                    alertDialog = null;
                    dialog.dismiss();
                    listener.positiveButton(etUnbindPwd.getText().toString().trim(), mGuid);
                }
            }
        });
        return this;
    }

    public void show() {
        dialog.show();
    }

    public interface OnPositiveListener {
        void positiveButton(String code, String guid);
    }
}
