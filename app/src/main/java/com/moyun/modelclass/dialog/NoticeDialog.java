package com.moyun.modelclass.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.moyun.modelclass.R;
import com.moyun.modelclass.http.CommonConfiguration;

/**
 * 作者：Meteor
 * tip：公告
 */
public class NoticeDialog {
    private ImageView ivNoticeClose;
    private ImageView ivNoticeImage;

    private static NoticeDialog alertDialog = null;
    private Context context;
    private Dialog dialog;
    private Display display;
    private String mImage;
    private String mUrl;

    public static NoticeDialog getInstance(Context context, String image, String url) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new NoticeDialog(context, image, url);
                }
            }
        }
        return alertDialog;
    }

    public NoticeDialog(Context context, String image, String url) {
        this.context = context;
        this.mImage = image;
        this.mUrl = url;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public NoticeDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_notice, null);
        ivNoticeClose = (ImageView) view.findViewById(R.id.iv_notice_close);
        ivNoticeImage = (ImageView) view.findViewById(R.id.iv_notice_image);


        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.9);
        dialog.getWindow().setAttributes(attributes);

        Glide.with(context)
                .asBitmap()
                .load(mImage)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(ivNoticeImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        ivNoticeImage.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        ivNoticeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonConfiguration.skipNewPage(context, mUrl);
                alertDialog = null;
                dialog.dismiss();
            }
        });

        return this;
    }

    //取消
    public NoticeDialog setNegativeButton() {
        ClickUtils.applySingleDebouncing(ivNoticeClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = null;
                dialog.dismiss();
            }
        });
        return this;
    }

    public void show() {
        dialog.show();
    }
}
