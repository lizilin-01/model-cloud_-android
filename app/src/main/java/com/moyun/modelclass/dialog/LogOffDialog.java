package com.moyun.modelclass.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 日期：2022/2/49:38 下午
 * tip：注销账号
 */
public class LogOffDialog {
    private TextView logOffCancel;
    private TextView logOffDetermine;

    private static LogOffDialog alertDialog = null;
    private Context context;
    private Dialog dialog;
    private Display display;

    public static LogOffDialog getInstance(Context context) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new LogOffDialog(context);
                }
            }
        }
        return alertDialog;
    }

    public LogOffDialog(Context context) {
        this.context = context;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public LogOffDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_log_off, null);
        logOffCancel = (TextView) view.findViewById(R.id.log_off_cancel);
        logOffDetermine = (TextView) view.findViewById(R.id.log_off_determine);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.85);
        dialog.getWindow().setAttributes(attributes);

        return this;
    }

    //取消
    public LogOffDialog setNegativeButton() {
        ClickUtils.applySingleDebouncing(logOffCancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = null;
                dialog.dismiss();
            }
        });
        return this;
    }


    //确定
    public LogOffDialog setPositiveButton(OnPositiveListener listener) {
        ClickUtils.applySingleDebouncing(logOffDetermine, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    listener.positiveButton();
                }
            }
        });
        return this;
    }

    public void show() {
        dialog.show();
    }

    public interface OnPositiveListener {
        void positiveButton();
    }
}
