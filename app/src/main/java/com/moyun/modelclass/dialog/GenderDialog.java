package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.http.listener.OnHttpListener;
import com.moyun.modelclass.adapter.ModelAssociationAdapter;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.AppModelListApi;
import com.moyun.modelclass.http.api.ModelRelateModelListApi;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：性别选择
 */
public class GenderDialog {
    private TextView tvGenderSave;
    private RadioGroup rgGenderType;

    @SuppressLint("StaticFieldLeak")
    private static GenderDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private OnGenderListener onGenderListener;
    private int mGender = 1;//1：男，2：女

    public static GenderDialog getInstance(Activity context, int gender, OnGenderListener onGenderListener) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new GenderDialog(context, gender, onGenderListener);
                }
            }
        }
        return alertDialog;
    }

    public GenderDialog(Activity context, int gender, OnGenderListener onGenderListener) {
        this.context = context;
        this.mGender = gender;
        this.onGenderListener = onGenderListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public GenderDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_gender, null);
        tvGenderSave = (TextView) view.findViewById(R.id.tv_gender_save);
        rgGenderType = (RadioGroup) view.findViewById(R.id.rg_gender_type);
        if (mGender == 1) {
            rgGenderType.check(R.id.rb_gender_man);
        } else {
            rgGenderType.check(R.id.rb_gender_woman);
        }

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕 dialog不消失
        dialog.setCancelable(false);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);

        rgGenderType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_gender_man) {
                    mGender = 1;
                } else if (checkedId == R.id.rb_gender_woman) {
                    mGender = 2;
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvGenderSave, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onGenderListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onGenderListener.content(mGender);
                }
            }
        });
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    public interface OnGenderListener {
        void content(int gender);
    }
}
