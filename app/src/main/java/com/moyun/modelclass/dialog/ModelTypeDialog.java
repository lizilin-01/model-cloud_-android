package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.adapter.ModelSizeAdapter;
import com.moyun.modelclass.adapter.ModelTypeDialogAdapter;
import com.moyun.modelclass.http.api.ModelTypeApi;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.util.List;

/**
 * 作者：Meteor
 * tip：选择模型分类
 */
public class ModelTypeDialog implements ModelTypeDialogAdapter.OnItemSelectedChangedListener {
    private ImageView dialogModelTypeClose;
    private RecyclerView dialogModelTypeRecycler;
    private TextView tvModelTypeSave;

    @SuppressLint("StaticFieldLeak")
    private static ModelTypeDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private int mPosition = 0;
    private List<ModelTypeApi.BeanInfo> contentList;
    private ModelTypeDialogAdapter modelTypeDialogAdapter = new ModelTypeDialogAdapter();
    private OnProjectTypeListener onProjectTypeListener;

    private ModelTypeApi.BeanInfo mBeanInfo;
    private int currentPosition = 0;

    public static ModelTypeDialog getInstance(Activity context, int position, List<ModelTypeApi.BeanInfo> contentList, OnProjectTypeListener onProjectTypeListener) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new ModelTypeDialog(context, position, contentList, onProjectTypeListener);
                }
            }
        }
        return alertDialog;
    }

    public ModelTypeDialog(Activity context, int position, List<ModelTypeApi.BeanInfo> contentList, OnProjectTypeListener onProjectTypeListener) {
        this.context = context;
        this.mPosition = position;
        this.currentPosition = position;
        this.contentList = contentList;
        if (contentList.size() > 0) {
            this.mBeanInfo = contentList.get(currentPosition);
        }
        this.onProjectTypeListener = onProjectTypeListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ModelTypeDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_model_type, null);


        dialogModelTypeClose = (ImageView) view.findViewById(R.id.dialog_model_type_close);
        dialogModelTypeRecycler = (RecyclerView) view.findViewById(R.id.dialog_model_type_recycler);
        tvModelTypeSave = (TextView) view.findViewById(R.id.tv_model_type_save);

        dialogModelTypeRecycler.setLayoutManager(new LinearLayoutManager(context));
        dialogModelTypeRecycler.addItemDecoration(new DividerItemDecoration(
                context,
                Color.parseColor("#EEEEEE"),
                1,
                SizeUtils.dp2px(17),
                SizeUtils.dp2px(17)));
        dialogModelTypeRecycler.setAdapter(modelTypeDialogAdapter);
        modelTypeDialogAdapter.setOnItemSelectedChangedListener(this);
        modelTypeDialogAdapter.setSelectedPosition(mPosition);
        modelTypeDialogAdapter.setList(contentList);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕 dialog不消失
        dialog.setCancelable(false);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);

        ClickUtils.applySingleDebouncing(dialogModelTypeClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onProjectTypeListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onProjectTypeListener.cancel();
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvModelTypeSave, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBeanInfo == null) {
                    ToastUtils.showShort("选择模型分类");
                    return;
                }
                if (onProjectTypeListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onProjectTypeListener.content(mBeanInfo, currentPosition);
                }
            }
        });
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onItemSelected(ModelTypeApi.BeanInfo beanInfo, int position) {
        mBeanInfo = beanInfo;
        currentPosition = position;
    }

    public interface OnProjectTypeListener {
        void content(ModelTypeApi.BeanInfo beanInfo, int position);

        void cancel();
    }
}
