package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.moyun.modelclass.R;
import com.moyun.modelclass.http.CommonConfiguration;

/**
 * 作者：Meteor
 * tip：微信分享
 */
public class ShareDialog {
    private ImageView ivShareClose;
    private ImageView ivShareImage;
    private TextView tvShareTitle;
    private TextView tvShareWechat;
    private TextView tvSharePyq;

    @SuppressLint("StaticFieldLeak")
    private static ShareDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private String mImageUrl, mTitle;
    private OnShareListener onShareListener;
    private Bitmap mResource;

    public static ShareDialog getInstance(Activity context, String imageUrl, String title, OnShareListener onShareListener) {
        alertDialog = new ShareDialog(context, imageUrl, title, onShareListener);
        return alertDialog;
    }

    public ShareDialog(Activity context, String imageUrl, String title, OnShareListener onShareListener) {
        this.context = context;
        this.mImageUrl = imageUrl;
        this.mTitle = title;
        this.onShareListener = onShareListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ShareDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_share, null);

        ivShareClose = (ImageView) view.findViewById(R.id.iv_share_close);
        ivShareImage = (ImageView) view.findViewById(R.id.iv_share_image);
        tvShareTitle = (TextView) view.findViewById(R.id.tv_share_title);
        tvShareWechat = (TextView) view.findViewById(R.id.tv_share_wechat);
        tvSharePyq = (TextView) view.findViewById(R.id.tv_share_pyq);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(true);//点击屏幕 dialog不消失
        dialog.setCancelable(true);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);

        Glide.with(context)
                .asBitmap()
                .load(mImageUrl)
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(ivShareImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        mResource = ImageUtils.toRoundCorner(resource,24);
                        ivShareImage.setImageBitmap(resource);
                    }
                });

        tvShareTitle.setText(mTitle);

        ClickUtils.applySingleDebouncing(tvShareWechat, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onShareListener != null && mResource != null) {
                    onShareListener.wx(mResource);
                    alertDialog = null;
                    dialog.dismiss();

                }
            }
        });

        ClickUtils.applySingleDebouncing(tvSharePyq, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onShareListener != null && mResource != null) {
                    onShareListener.pyq(mResource);
                    alertDialog = null;
                    dialog.dismiss();
                }
            }
        });
        return this;
    }

    //取消
    public ShareDialog setNegativeButton() {
        ClickUtils.applySingleDebouncing(ivShareClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = null;
                dialog.dismiss();
            }
        });
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    public interface OnShareListener {
        void wx(Bitmap resource);

        void pyq(Bitmap resource);
    }
}
