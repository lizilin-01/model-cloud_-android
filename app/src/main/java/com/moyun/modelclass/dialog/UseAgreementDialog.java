package com.moyun.modelclass.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.style.ClickableSpan;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 日期：2022/2/49:38 下午
 * tip：用户使用协议
 */
public class UseAgreementDialog {
    private TextView dialogUseAgreementContent;
    private TextView dialogUseAgreementAgreement;
    private TextView dialogUseAgreementCancel;

    private static UseAgreementDialog alertDialog = null;
    private Activity context;
    private Dialog dialog;
    private Display display;

    public static UseAgreementDialog getInstance(Activity context) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new UseAgreementDialog(context);
                }
            }
        }
        return alertDialog;
    }

    public UseAgreementDialog(Activity context) {
        this.context = context;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public UseAgreementDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_use_agreement, null);
        dialogUseAgreementContent = (TextView) view.findViewById(R.id.dialog_use_agreement_content);
        dialogUseAgreementAgreement = (TextView) view.findViewById(R.id.dialog_use_agreement_agreement);
        dialogUseAgreementCancel = (TextView) view.findViewById(R.id.dialog_use_agreement_cancel);


        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕 dialog不消失
        dialog.setCancelable(false);//点击屏幕或返回按钮 dialog不消失
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.85);
        dialog.getWindow().setAttributes(attributes);
        return this;
    }

    //协议
    public UseAgreementDialog setAgreementListener(OnAgreementListener listener) {
        SpanUtils.with(dialogUseAgreementContent)
                .append("请您阅读").appendSpace(14)
                .append("《用户协议》").setForegroundColor(Color.parseColor("#055D6C"))
                .setClickSpan(new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View view) {
                        if (listener != null) {
                            listener.userAgreement();
                        }
                    }
                }).appendSpace(14)
                .append("和").appendSpace(14)
                .append("《隐私政策》").setForegroundColor(Color.parseColor("#055D6C"))
                .setClickSpan(new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View view) {
                        if (listener != null) {
                            listener.privacyAgreement();
                        }
                    }
                }).create();
        return this;
    }

    //确定
    public UseAgreementDialog setPositiveButton(OnPositiveListener listener) {
        ClickUtils.applySingleDebouncing(dialogUseAgreementAgreement, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    listener.positiveButton();
                }
            }
        });
        return this;
    }

    //取消
    public UseAgreementDialog setNegativeButton(OnNegativeListener listener) {
        ClickUtils.applySingleDebouncing(dialogUseAgreementCancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    listener.negativeButton();
                }
            }
        });
        return this;
    }

    public void show() {
        dialog.show();
    }

    public interface OnPositiveListener {
        void positiveButton();
    }

    public interface OnNegativeListener {
        void negativeButton();
    }

    public interface OnAgreementListener {
        void userAgreement();//用户协议

        void privacyAgreement();//隐私协议
    }
}
