package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.adapter.ConfigPrintParamsAdapter;
import com.moyun.modelclass.http.api.ConfigPrintParamsApi;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.util.List;

/**
 * 作者：Meteor
 * tip：打印相关参数
 */
public class ConfigPrintParamsDialog implements ConfigPrintParamsAdapter.OnItemSelectedChangedListener {
    private TextView dialogConfigPrintParamsTitle;
    private ImageView dialogConfigPrintParamsClose;
    private RecyclerView dialogConfigPrintParamsRecycler;
    private TextView tvShippingAddressSave;

    @SuppressLint("StaticFieldLeak")
    private static ConfigPrintParamsDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private String mTitle = "";
    private int mPosition = 0;
    private List<ConfigPrintParamsApi.BeanInfo> contentList;
    private ConfigPrintParamsAdapter configPrintParamsAdapter = new ConfigPrintParamsAdapter();
    private OnProjectTypeListener onProjectTypeListener;

    private ConfigPrintParamsApi.BeanInfo mBeanInfo;
    private int currentPosition = 0;

    public static ConfigPrintParamsDialog getInstance(Activity context, String title,int position, List<ConfigPrintParamsApi.BeanInfo> contentList, OnProjectTypeListener onProjectTypeListener) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new ConfigPrintParamsDialog(context, title,position, contentList, onProjectTypeListener);
                }
            }
        }
        return alertDialog;
    }

    public ConfigPrintParamsDialog(Activity context, String title, int position, List<ConfigPrintParamsApi.BeanInfo> contentList, OnProjectTypeListener onProjectTypeListener) {
        this.context = context;
        this.mTitle = title;
        this.mPosition = position;
        this.contentList = contentList;
        if (contentList.size() > 0) {
            this.mBeanInfo = contentList.get(0);
        }
        this.onProjectTypeListener = onProjectTypeListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ConfigPrintParamsDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_config_print_params, null);
        dialogConfigPrintParamsTitle = (TextView) view.findViewById(R.id.dialog_config_print_params_title);
        dialogConfigPrintParamsClose = (ImageView) view.findViewById(R.id.dialog_config_print_params_close);
        dialogConfigPrintParamsRecycler = (RecyclerView) view.findViewById(R.id.dialog_config_print_params_recycler);
        tvShippingAddressSave = (TextView) view.findViewById(R.id.tv_shipping_address_save);

        dialogConfigPrintParamsTitle.setText(mTitle);
        dialogConfigPrintParamsRecycler.setLayoutManager(new LinearLayoutManager(context));
        dialogConfigPrintParamsRecycler.addItemDecoration(new DividerItemDecoration(
                context,
                Color.parseColor("#EEEEEE"),
                1,
                SizeUtils.dp2px(17),
                SizeUtils.dp2px(17)));
        dialogConfigPrintParamsRecycler.setAdapter(configPrintParamsAdapter);
        configPrintParamsAdapter.setOnItemSelectedChangedListener(this);
        configPrintParamsAdapter.setSelectedPosition(mPosition);
        configPrintParamsAdapter.setList(contentList);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕 dialog不消失
        dialog.setCancelable(false);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);

        ClickUtils.applySingleDebouncing(dialogConfigPrintParamsClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onProjectTypeListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onProjectTypeListener.cancel();
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvShippingAddressSave, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBeanInfo == null) {
                    ToastUtils.showShort("选择打印方式");
                    return;
                }
                if (onProjectTypeListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onProjectTypeListener.content(mBeanInfo, currentPosition);
                }
            }
        });
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onItemSelected(ConfigPrintParamsApi.BeanInfo beanInfo, int position) {
        mBeanInfo = beanInfo;
        currentPosition = position;
    }

    public interface OnProjectTypeListener {
        void content(ConfigPrintParamsApi.BeanInfo beanInfo, int position);

        void cancel();
    }
}
