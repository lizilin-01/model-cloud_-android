package com.moyun.modelclass.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * tip：查看图片
 */
public class ScanImageDialog {
    private ImageView ivScanImageClose;
    private ImageView ivScanImageImage;

    private static ScanImageDialog alertDialog = null;
    private Context context;
    private Dialog dialog;
    private Display display;
    private String mUrl;

    public static ScanImageDialog getInstance(Context context, String url) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new ScanImageDialog(context, url);
                }
            }
        }
        return alertDialog;
    }

    public ScanImageDialog(Context context, String url) {
        this.context = context;
        this.mUrl = url;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ScanImageDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_scan_image, null);
        ivScanImageClose = (ImageView) view.findViewById(R.id.iv_scan_image_close);
        ivScanImageImage = (ImageView) view.findViewById(R.id.iv_scan_image_image);


        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.9);
        dialog.getWindow().setAttributes(attributes);

        Glide.with(context)
                .asBitmap()
                .load(mUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(ivScanImageImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        ivScanImageImage.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        return this;
    }

    //取消
    public ScanImageDialog setNegativeButton() {
        ClickUtils.applySingleDebouncing(ivScanImageClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = null;
                dialog.dismiss();
            }
        });
        return this;
    }

    public void show() {
        dialog.show();
    }
}
