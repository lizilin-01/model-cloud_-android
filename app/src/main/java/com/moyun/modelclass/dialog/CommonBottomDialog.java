package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SizeUtils;
import com.moyun.modelclass.adapter.CommonBottomAdapter;
import com.moyun.modelclass.listener.OnContentListener;
import com.moyun.modelclass.http.bean.CommonDialogBean;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.util.List;

/**
 * 作者：Meteor
 * tip：公共共用弹窗
 */
public class CommonBottomDialog implements OnContentListener {
    private TextView commonBottomDialogTitle;
    private RecyclerView commonBottomDialogRecycler;

    @SuppressLint("StaticFieldLeak")
    private static CommonBottomDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private String title;
    private List<CommonDialogBean> contentList;
    private CommonBottomAdapter commonBottomAdapter = new CommonBottomAdapter();
    private OnContentListener onContentListener;

    public static CommonBottomDialog getInstance(Activity context, String title, List<CommonDialogBean> contentList, OnContentListener onContentListener) {
        alertDialog = new CommonBottomDialog(context, title, contentList, onContentListener);
        return alertDialog;
    }

    public CommonBottomDialog(Activity context, String title, List<CommonDialogBean> contentList, OnContentListener onContentListener) {
        this.context = context;
        this.title = title;
        this.contentList = contentList;
        this.onContentListener = onContentListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public CommonBottomDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_common_bottom, null);
        commonBottomDialogTitle = (TextView) view.findViewById(R.id.common_bottom_dialog_title);
        commonBottomDialogRecycler = (RecyclerView) view.findViewById(R.id.common_bottom_dialog_recycler);

        if (title.isEmpty()) {
            commonBottomDialogTitle.setVisibility(View.GONE);
        } else {
            commonBottomDialogTitle.setVisibility(View.VISIBLE);
            commonBottomDialogTitle.setText(title);
        }
        commonBottomDialogRecycler.setLayoutManager(new LinearLayoutManager(context));
        commonBottomDialogRecycler.addItemDecoration(new DividerItemDecoration(
                context,
                Color.parseColor("#EEEEEE"),
                1,
                SizeUtils.dp2px(15),
                SizeUtils.dp2px(15)));
        commonBottomDialogRecycler.setAdapter(commonBottomAdapter);
        commonBottomAdapter.setOnContentListener(this);
        commonBottomAdapter.setList(contentList);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(true);//点击屏幕 dialog不消失
        dialog.setCancelable(true);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void content(int id, CommonDialogBean content) {
        if (onContentListener != null) {
            alertDialog = null;
            dialog.dismiss();
            onContentListener.content(id, content);
        }
    }
}
