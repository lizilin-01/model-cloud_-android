package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SizeUtils;
import com.moyun.modelclass.adapter.CommonBottomAdapter;
import com.moyun.modelclass.listener.OnContentListener;
import com.moyun.modelclass.http.bean.CommonDialogBean;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/2/49:38 下午
 * tip：
 */
public class CommonCenterDialog implements OnContentListener {
    private TextView commonCenterDialogTitle;
    private RecyclerView commonCenterDialogRecycler;

    @SuppressLint("StaticFieldLeak")
    private static CommonCenterDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private String title;
    private List<CommonDialogBean> contentList;
    private CommonBottomAdapter commonCenterAdapter = new CommonBottomAdapter();
    private OnContentListener onContentListener;

    public static CommonCenterDialog getInstance(Activity context, String title, List<CommonDialogBean> contentList, OnContentListener onContentListener) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new CommonCenterDialog(context, title, contentList, onContentListener);
                }
            }
        }
        return alertDialog;
    }

    public CommonCenterDialog(Activity context, String title, List<CommonDialogBean> contentList, OnContentListener onContentListener) {
        this.context = context;
        this.title = title;
        this.contentList = contentList;
        this.onContentListener = onContentListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public CommonCenterDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_common_center, null);
        commonCenterDialogTitle = (TextView) view.findViewById(R.id.common_center_dialog_title);
        commonCenterDialogRecycler = (RecyclerView) view.findViewById(R.id.common_center_dialog_recycler);
        if (TextUtils.isEmpty(title)) {
            commonCenterDialogTitle.setVisibility(View.GONE);
        } else {
            commonCenterDialogTitle.setText(title);
            commonCenterDialogTitle.setVisibility(View.VISIBLE);
        }

        commonCenterDialogRecycler.setLayoutManager(new LinearLayoutManager(context));
        commonCenterDialogRecycler.addItemDecoration(new DividerItemDecoration(
                context,
                Color.parseColor("#EEEEEE"),
                1,
                SizeUtils.dp2px(15),
                SizeUtils.dp2px(15)));
        commonCenterDialogRecycler.setAdapter(commonCenterAdapter);
        commonCenterAdapter.setOnContentListener(this);
        commonCenterAdapter.setList(contentList);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.85);
        dialog.getWindow().setAttributes(attributes);
        return this;
    }

    public void show() {
        if (context == null || context.isDestroyed() || context.isFinishing()) {
            return;
        }
        dialog.show();
    }

    @Override
    public void content(int id, CommonDialogBean content) {
        if (onContentListener != null) {
            alertDialog = null;
            dialog.dismiss();
            onContentListener.content(id, content);
        }
    }
}
