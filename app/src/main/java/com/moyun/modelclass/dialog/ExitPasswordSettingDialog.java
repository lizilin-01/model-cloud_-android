package com.moyun.modelclass.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.moyun.modelclass.R;

/**
 * 作者：Meteor
 * 日期：2022/2/49:38 下午
 * tip：退出密码设置
 */
public class ExitPasswordSettingDialog {
    private TextView exitPasswordSettingContinueToModify;
    private TextView exitPasswordSettingDiscardModification;

    private static ExitPasswordSettingDialog alertDialog = null;
    private Context context;
    private Dialog dialog;
    private Display display;

    public static ExitPasswordSettingDialog getInstance(Context context) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new ExitPasswordSettingDialog(context);
                }
            }
        }
        return alertDialog;
    }

    public ExitPasswordSettingDialog(Context context) {
        this.context = context;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ExitPasswordSettingDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_exit_password_setting, null);
        exitPasswordSettingContinueToModify = (TextView) view.findViewById(R.id.exit_password_setting_continue_to_modify);
        exitPasswordSettingDiscardModification = (TextView) view.findViewById(R.id.exit_password_setting_discard_modification);


        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth() * 0.85);
        dialog.getWindow().setAttributes(attributes);

        return this;
    }

    //继续修改
    public ExitPasswordSettingDialog setNegativeButton(OnContinueToModifyListener listener) {
        ClickUtils.applySingleDebouncing(exitPasswordSettingContinueToModify, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    listener.continueToModify();
                }
            }
        });
        return this;
    }

    //放弃修改
    public ExitPasswordSettingDialog setPositiveButton(OnDiscardModificationListener listener) {
        ClickUtils.applySingleDebouncing(exitPasswordSettingDiscardModification, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.discardModification();
                }
            }
        });
        return this;
    }

    public void show() {
        dialog.show();
    }

    public interface OnContinueToModifyListener {
        void continueToModify();//继续修改
    }

    public interface OnDiscardModificationListener {
        void discardModification();//放弃修改
    }
}
