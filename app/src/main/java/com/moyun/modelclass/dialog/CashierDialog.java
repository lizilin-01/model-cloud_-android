package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.moyun.modelclass.adapter.ConfigPrintParamsAdapter;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.ConfigPrintParamsApi;
import com.moyun.modelclass.R;
import com.moyun.modelclass.http.api.OrderInfoApi;
import com.moyun.modelclass.utils.DataUtil;
import com.moyun.modelclass.view.MyCountDownTimer;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.text.DecimalFormat;
import java.util.List;

/**
 * 作者：Meteor
 * tip：收银台
 */
public class CashierDialog {
    private ImageView dialogCashierClose;
    private TextView dialogCashierAmountMoney;
    private TextView dialogCashierTime;
    private ImageView dialogCashierWechatIcon;
    private TextView dialogCashierWechat;
    private TextView tvCashierPay;
    private long millisInFuture = 15 * 60 * 1000; // 60秒
    private long countDownInterval = 1000; // 每秒更新一次
    private MyCountDownTimer countDownTimer;

    @SuppressLint("StaticFieldLeak")
    private static CashierDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private OnCashierListener onCashierListener;
    private DecimalFormat df = new DecimalFormat("￥##0.00");
    private int mTotalPrice = 0;
    private OrderInfoApi.ChannelBean mChannelBean = null;

    public static CashierDialog getInstance(Activity context, int totalPrice, OrderInfoApi.ChannelBean channelBean, OnCashierListener onCashierListener) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new CashierDialog(context, totalPrice, channelBean, onCashierListener);
                }
            }
        }
        return alertDialog;
    }

    public CashierDialog(Activity context, int totalPrice, OrderInfoApi.ChannelBean channelBean, OnCashierListener onCashierListener) {
        this.context = context;
        this.mTotalPrice = totalPrice;
        this.mChannelBean = channelBean;
        this.onCashierListener = onCashierListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
        countDownTimer = new MyCountDownTimer(millisInFuture, countDownInterval);
    }

    public CashierDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_cashier, null);
        dialogCashierClose = (ImageView) view.findViewById(R.id.dialog_cashier_close);
        dialogCashierAmountMoney = (TextView) view.findViewById(R.id.dialog_cashier_amount_money);
        dialogCashierTime = (TextView) view.findViewById(R.id.dialog_cashier_time);
        dialogCashierWechatIcon = (ImageView) view.findViewById(R.id.dialog_cashier_wechat_icon);
        dialogCashierWechat = (TextView) view.findViewById(R.id.dialog_cashier_wechat);
        tvCashierPay = (TextView) view.findViewById(R.id.tv_cashier_pay);
        dialogCashierAmountMoney.setText(df.format(mTotalPrice * 0.01));
        countDownTimer.setOnTimerListener(new MyCountDownTimer.OnTimerListener() {
            @Override
            public void onItemSelected(String secondsRemaining) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogCashierTime.setText("支付有效时间：" + secondsRemaining);
                    }
                });
            }
        });

        countDownTimer.start();

        Glide.with(context)
                .asBitmap()
                .load(CommonConfiguration.splitResUrl(5, mChannelBean.getChannelIcon()))
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(dialogCashierWechatIcon) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        dialogCashierWechatIcon.setImageBitmap(ImageUtils.toRoundCorner(resource, 10f));
                    }
                });

        dialogCashierWechat.setText(mChannelBean.getChannelName());

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog_two);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕 dialog不消失
        dialog.setCancelable(false);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);

        ClickUtils.applySingleDebouncing(dialogCashierClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCashierListener != null) {
                    countDownTimer.cancel();
                    alertDialog = null;
                    dialog.dismiss();
                    onCashierListener.cancel();
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvCashierPay, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCashierListener != null) {
                    countDownTimer.cancel();
                    alertDialog = null;
                    dialog.dismiss();
                    onCashierListener.pay();
                }
            }
        });
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    public interface OnCashierListener {
        void pay();

        void cancel();
    }
}
