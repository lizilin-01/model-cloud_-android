package com.moyun.modelclass.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.adapter.ModelSizeAdapter;
import com.moyun.modelclass.http.bean.ModelResponse;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.util.List;

/**
 * 作者：Meteor
 * tip：选择模型尺寸
 */
public class ModelSizeDialog implements ModelSizeAdapter.OnItemSelectedChangedListener {
    private ImageView dialogModelSizeClose;
    private RecyclerView dialogModelSizeRecycler;
    private TextView tvModelSizeSave;

    @SuppressLint("StaticFieldLeak")
    private static ModelSizeDialog alertDialog = null;
    private final Activity context;
    private Dialog dialog;
    private final Display display;
    private int mPosition = 0;
    private List<ModelResponse.ModelSizeResponse> contentList;
    private ModelSizeAdapter modelSizeAdapter = new ModelSizeAdapter();
    private OnProjectTypeListener onProjectTypeListener;

    private ModelResponse.ModelSizeResponse mBeanInfo;
    private int currentPosition = 0;

    public static ModelSizeDialog getInstance(Activity context, int position, List<ModelResponse.ModelSizeResponse> contentList, OnProjectTypeListener onProjectTypeListener) {
        if (alertDialog == null) {
            synchronized (AlertDialog.class) {
                if (alertDialog == null) {
                    alertDialog = new ModelSizeDialog(context,position, contentList, onProjectTypeListener);
                }
            }
        }
        return alertDialog;
    }

    public ModelSizeDialog(Activity context, int position, List<ModelResponse.ModelSizeResponse> contentList, OnProjectTypeListener onProjectTypeListener) {
        this.context = context;
        this.currentPosition = position;
        this.mPosition = position;
        this.contentList = contentList;
        if (contentList.size() > 0) {
            this.mBeanInfo = contentList.get(currentPosition);
        }
        this.onProjectTypeListener = onProjectTypeListener;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ModelSizeDialog builder() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_model_size, null);
        dialogModelSizeClose = (ImageView) view.findViewById(R.id.dialog_model_size_close);
        dialogModelSizeRecycler = (RecyclerView) view.findViewById(R.id.dialog_model_size_recycler);
        tvModelSizeSave = (TextView)view. findViewById(R.id.tv_model_size_save);

        dialogModelSizeRecycler.setLayoutManager(new LinearLayoutManager(context));
        dialogModelSizeRecycler.addItemDecoration(new DividerItemDecoration(
                context,
                Color.parseColor("#EEEEEE"),
                1,
                SizeUtils.dp2px(17),
                SizeUtils.dp2px(17)));
        dialogModelSizeRecycler.setAdapter(modelSizeAdapter);
        modelSizeAdapter.setOnItemSelectedChangedListener(this);
        modelSizeAdapter.setSelectedPosition(mPosition);
        modelSizeAdapter.setList(contentList);

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.common_dialog);
        dialog.setContentView(view);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCanceledOnTouchOutside(false);//点击屏幕 dialog不消失
        dialog.setCancelable(false);//点击屏幕或返回按钮 dialog不消失
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width = (int) (display.getWidth());
        dialog.getWindow().setAttributes(attributes);

        ClickUtils.applySingleDebouncing(dialogModelSizeClose, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onProjectTypeListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onProjectTypeListener.cancel();
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvModelSizeSave, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBeanInfo == null) {
                    ToastUtils.showShort("选择模型尺寸");
                    return;
                }
                if (onProjectTypeListener != null) {
                    alertDialog = null;
                    dialog.dismiss();
                    onProjectTypeListener.content(mBeanInfo, currentPosition);
                }
            }
        });
        return this;
    }

    public void show() {
        if (!context.isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onItemSelected(ModelResponse.ModelSizeResponse beanInfo, int position) {
        mBeanInfo = beanInfo;
        currentPosition = position;
    }

    public interface OnProjectTypeListener {
        void content(ModelResponse.ModelSizeResponse beanInfo, int position);

        void cancel();
    }
}
