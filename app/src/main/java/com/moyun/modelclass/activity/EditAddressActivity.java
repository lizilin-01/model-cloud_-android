package com.moyun.modelclass.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.gzuliyujiang.wheelpicker.AddressPicker;
import com.github.gzuliyujiang.wheelpicker.annotation.AddressMode;
import com.github.gzuliyujiang.wheelpicker.contract.OnAddressPickedListener;
import com.github.gzuliyujiang.wheelpicker.entity.CityEntity;
import com.github.gzuliyujiang.wheelpicker.entity.CountyEntity;
import com.github.gzuliyujiang.wheelpicker.entity.ProvinceEntity;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.EditAddressApi;
import com.moyun.modelclass.http.api.RemoveAddressApi;
import com.moyun.modelclass.http.api.SingleAddressApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;

/**
 * 作者：Meteor
 * tip：新建收货地址
 */
public class EditAddressActivity extends BaseActivity implements OnAddressPickedListener {
    private RelativeLayout rlEditAddressTop;
    private ImageView ivEditAddressBack;
    private TextView tvEditAddressTitle;
    private ImageView ivEditAddressDelete;
    private EditText etEditAddressPersonName;
    private EditText etEditAddressPhone;
    private EditText etEditAddressAddress;
    private EditText etEditAddressDetail;
    private CheckedTextView ctEditAddressChecked;
    private TextView tvEditAddressSave;

    private String mAddressId = "";//文章id
    private String mAreaCode = "110101";
    private boolean isDefaultAddress = false;//是否是默认地址

    public static void start(Context context, String addressId, boolean isDefaultAddress) {
        Intent starter = new Intent(context, EditAddressActivity.class);
        starter.putExtra("addressId", addressId);
        starter.putExtra("isDefaultAddress", isDefaultAddress);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_address;
    }

    @Override
    protected void assignViews() {
        rlEditAddressTop = (RelativeLayout) findViewById(R.id.rl_edit_address_top);
        ivEditAddressBack = (ImageView) findViewById(R.id.iv_edit_address_back);
        tvEditAddressTitle = (TextView) findViewById(R.id.tv_edit_address_title);
        ivEditAddressDelete = (ImageView) findViewById(R.id.iv_edit_address_delete);
        etEditAddressPersonName = (EditText) findViewById(R.id.et_edit_address_person_name);
        etEditAddressPhone = (EditText) findViewById(R.id.et_edit_address_phone);
        etEditAddressAddress = (EditText) findViewById(R.id.et_edit_address_address);
        etEditAddressDetail = (EditText) findViewById(R.id.et_edit_address_detail);
        ctEditAddressChecked = (CheckedTextView) findViewById(R.id.ct_edit_address_checked);
        tvEditAddressSave = (TextView) findViewById(R.id.tv_edit_address_save);

        StatusBarUtil.setPaddingSmart(this, rlEditAddressTop);
        etEditAddressAddress.setInputType(InputType.TYPE_NULL);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivEditAddressBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(ivEditAddressDelete, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAddress();
            }
        });

        ClickUtils.applySingleDebouncing(etEditAddressAddress, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProvinceCityCounty();
            }
        });

        ClickUtils.applySingleDebouncing(tvEditAddressSave, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressSave();
            }
        });

        ClickUtils.applySingleDebouncing(ctEditAddressChecked, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctEditAddressChecked.setChecked(!ctEditAddressChecked.isChecked());
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mAddressId = bundle.getString("addressId", "");
        isDefaultAddress = bundle.getBoolean("isDefaultAddress", false);
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        if (mAddressId != null && !mAddressId.isEmpty()) {
            tvEditAddressTitle.setText("编辑收货地址");
            ivEditAddressDelete.setVisibility(View.VISIBLE);
            singleAddress();
        } else {
            tvEditAddressTitle.setText("新建收货地址");
            ivEditAddressDelete.setVisibility(View.GONE);
        }
        ctEditAddressChecked.setChecked(isDefaultAddress);
    }

    public void onProvinceCityCounty() {
        AddressPicker picker = new AddressPicker(this);
        picker.setAddressMode(AddressMode.PROVINCE_CITY_COUNTY);
        picker.setTitle("选择所在地");
        picker.setDefaultValue("北京市", "北京市", "东城区");
        picker.setOnAddressPickedListener(this);
        picker.show();
    }

    //地址保存
    private void addressSave() {
        String name = etEditAddressPersonName.getText().toString();
        String phone = etEditAddressPhone.getText().toString();
        String address = mAreaCode;
        String addressDetail = etEditAddressDetail.getText().toString();
        if (TextUtils.isEmpty(name)) {
            ToastUtils.showShort("收货人姓名");
            return;
        }
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort("收货人号码");
            return;
        }
        if (TextUtils.isEmpty(address)) {
            ToastUtils.showShort("所在地区");
            return;
        }
        if (TextUtils.isEmpty(addressDetail)) {
            ToastUtils.showShort("请填写地址详情");
            return;
        }
        if (addressDetail.length() < 5) {
            ToastUtils.showShort("地址不能少于5个字");
            return;
        }

        EasyHttp.post(this)
                .api(new EditAddressApi().setId(mAddressId).setName(name).setMobile(phone).setAreaCode(address).setDetail(addressDetail).setDefaultFlag(ctEditAddressChecked.isChecked()))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        if (mAddressId != null && !mAddressId.isEmpty()) {
                            ToastUtils.showShort("保存成功");
                        }else {
                            ToastUtils.showShort("新建收货地址成功");
                        }
                        finish();
                    }
                });
    }

    //获取单个地址信息
    private void singleAddress() {
        EasyHttp.post(this)
                .api(new SingleAddressApi().setAddressId(mAddressId))
                .request(new HttpCallback<HttpData<SingleAddressApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<SingleAddressApi.Bean> result) {
                        if (result != null) {
                            mAreaCode = result.getData().getAreaCode();
                            etEditAddressPersonName.setText(result.getData().getName());
                            etEditAddressPhone.setText(result.getData().getMobile());
                            etEditAddressAddress.setText(result.getData().getProvince() + " " + result.getData().getCity() + " " + result.getData().getArea());
                            etEditAddressDetail.setText(result.getData().getDetail());
                            ctEditAddressChecked.setChecked(result.getData().isDefaultFlag());
                        }
                    }
                });
    }

    //删除地址
    private void removeAddress() {
        if (mAddressId == null || mAddressId.isEmpty()) {
            return;
        }
        EasyHttp.post(this)
                .api(new RemoveAddressApi().setAddressId(mAddressId))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        finish();
                    }
                });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onAddressPicked(ProvinceEntity province, CityEntity city, CountyEntity county) {
        mAreaCode = county.getCode();
        etEditAddressAddress.setText(province.provideText() + " " + city.provideText() + " " + county.provideText());
    }
}
