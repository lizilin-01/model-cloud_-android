package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.baidu.mobstat.StatService;
import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.app.BaseApplication;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.dialog.UseAgreementDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.RequestServer;
import com.moyun.modelclass.R;
import com.tencent.smtt.sdk.QbSdk;

/**
 * 作者：Meteor
 * 日期：2022/1/15 17:25
 * tip：启动页
 */
public class SplashActivity extends BaseActivity {

    public static void start(Context mContext) {
        Intent starter = new Intent(mContext, SplashActivity.class);
        mContext.startActivity(starter);
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void assignViews() {
    }

    @Override
    protected void registerListeners() {

    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        if (!kv.decodeBool("isShowDialog", false)) {
            UseAgreementDialog.getInstance(mActivity).builder()
                    .setAgreementListener(new UseAgreementDialog.OnAgreementListener() {
                        @Override
                        public void userAgreement() {
                            WebRichActivity.start(mActivity, "ServiceUsageAgreement");
                        }

                        @Override
                        public void privacyAgreement() {
                            WebRichActivity.start(mActivity, "PrivacyAgreement");
                        }
                    })
                    .setPositiveButton(new UseAgreementDialog.OnPositiveListener() {
                        @Override
                        public void positiveButton() {
                            kv.encode("isShowDialog", true);
                            ((BaseApplication) getApplication()).initTBS();
                            ((BaseApplication) getApplication()).initBaidu();
                            ((BaseApplication) getApplication()).initEasyConfig();
                            // setSendLogStrategy已经@deprecated，建议使用新的start接口
                            // 如果没有页面和自定义事件统计埋点，此代码一定要设置，否则无法完成统计
                            // 进程第一次执行此代码，会导致发送上次缓存的统计数据；若无上次缓存数据，则发送空启动日志
                            // 由于多进程等可能造成Application多次执行，建议此代码不要埋点在Application中，否则可能造成启动次数偏高
                            // 建议此代码埋点在统计路径触发的第一个页面中，若可能存在多个则建议都埋点
                            StatService.start(SplashActivity.this);
                            MainActivity.start(mActivity);
                            finish();
                        }
                    }).setNegativeButton(new UseAgreementDialog.OnNegativeListener() {
                        @Override
                        public void negativeButton() {
                            finish();
                        }
                    }).show();
        } else {
            MainActivity.start(mActivity);
            finish();
        }
    }

    @Override
    public void onFail(Exception e) {
        super.onFail(e);
        EasyConfig.getInstance().setServer(new RequestServer());
    }
}
