package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.ModelListAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.MyModelApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;

/**
 * 作者：Meteor
 * tip：模型
 */
public class CommentModelActivity extends BaseActivity implements ModelListAdapter.OnItemSelectedChangedListener {
    private RelativeLayout llCommentModelTop;
    private ImageView ivCommentModelBack;
    private EditText etCommentModelContent;
    private TextView tvCommentModelButton;
    private SmartRefreshLayout commentModelRefresh;
    private RecyclerView commentModelRecycler;

    private ModelListAdapter modelListAdapter = new ModelListAdapter();
    private String mLastId = "";
    private String mUserId = "";

    public static void start(Context context,String userId) {
        Intent starter = new Intent(context, CommentModelActivity.class);
        starter.putExtra("userId", userId);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_comment_model;
    }

    @Override
    protected void assignViews() {
        llCommentModelTop = (RelativeLayout) findViewById(R.id.ll_comment_model_top);
        ivCommentModelBack = (ImageView) findViewById(R.id.iv_comment_model_back);
        etCommentModelContent = (EditText) findViewById(R.id.et_comment_model_content);
        tvCommentModelButton = (TextView) findViewById(R.id.tv_comment_model_button);
        commentModelRefresh = (SmartRefreshLayout) findViewById(R.id.comment_model_refresh);
        commentModelRecycler = (RecyclerView) findViewById(R.id.comment_model_recycler);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        commentModelRecycler.setLayoutManager(layoutManager);
        modelListAdapter.setOnItemSelectedChangedListener(this);
        commentModelRecycler.setAdapter(modelListAdapter);

        StatusBarUtil.setPaddingSmart(this, llCommentModelTop);
    }


    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivCommentModelBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        commentModelRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                modelListData(true, "");
            }
        });
        commentModelRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                modelListData(false, "");
            }
        });

        ClickUtils.applySingleDebouncing(tvCommentModelButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchResult = etCommentModelContent.getText().toString().trim();
                KeyboardUtils.hideSoftInput(etCommentModelContent);
                etCommentModelContent.clearFocus();
                modelListData(true, searchResult);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mUserId = bundle.getString("userId","");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        modelListData(true, "");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        modelListData(true, "");
    }

    private void modelListData(boolean isRefresh, String searchString) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new MyModelApi().setUserId(mUserId).setLastId(mLastId).setSearch(searchString).setPageSize(CommonConfiguration.page))
                .request(new HttpCallback<HttpData<MyModelApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<MyModelApi.Bean> result) {
                        commentModelRefresh.finishRefresh();
                        commentModelRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                modelListAdapter.setList(result.getData().getList());
                            } else {
                                modelListAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                modelListAdapter.setList(new ArrayList<>());
                                modelListAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }

    @Override
    public void details(String modelId) {
        ModelDetailsActivity.start(this, modelId,false);
    }
}
