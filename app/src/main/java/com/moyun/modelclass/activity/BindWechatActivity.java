package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.base.BaseConfig;
import com.moyun.modelclass.event.WxLoginEvent;
import com.moyun.modelclass.http.api.UserBindWechatApi;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * 作者：Meteor
 * tip：绑定微信
 */
public class BindWechatActivity extends BaseActivity {
    private RelativeLayout rlBindWechatTop;
    private ImageView ivBindWechatBack;
    private TextView tvBindWechatSkip;

    public static void start(Context context) {
        Intent starter = new Intent(context, BindWechatActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_bind_wechat;
    }

    @Override
    protected void assignViews() {
        rlBindWechatTop = (RelativeLayout) findViewById(R.id.rl_bind_wechat_top);
        ivBindWechatBack = (ImageView) findViewById(R.id.iv_bind_wechat_back);
        tvBindWechatSkip = (TextView) findViewById(R.id.tv_bind_wechat_skip);

        StatusBarUtil.setPaddingSmart(this, rlBindWechatTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivBindWechatBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvBindWechatSkip, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IWXAPI api = WXAPIFactory.createWXAPI(mActivity, BaseConfig.WX_APPID, true);
                boolean clientValid = api.isWXAppInstalled();
                if (clientValid) {
                    final SendAuth.Req req = new SendAuth.Req();
                    req.scope = "snsapi_userinfo";
                    req.state = "carjob_wx_login";
                    api.sendReq(req);
                } else {
                    ToastUtils.showShort("请安装微信客户端");
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return true;
    }

    @Override
    protected void doAction() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWechatLoginEvent(WxLoginEvent event) {
        EasyHttp.post(BindWechatActivity.this)
                .api(new UserBindWechatApi().setCode(event.getCode()))
                .request(new HttpCallback<String>(BindWechatActivity.this) {
                    @Override
                    public void onSucceed(String openIdResult) {
                        finish();
                    }
                });
    }
}
