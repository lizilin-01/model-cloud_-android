package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.dialog.UnbindDialog;
import com.moyun.modelclass.http.api.FundWithdrawalApi;
import com.moyun.modelclass.http.api.UnbindWxApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.MyEditText;

import java.text.DecimalFormat;

/**
 * 作者：Meteor
 * tip：微信提现
 */
public class WithdrawalActivity extends BaseActivity {
    private RelativeLayout rlWithdrawalTop;
    private ImageView ivWithdrawalBack;
    private EditText tvWithdrawalTxje;
    private MyEditText tvWithdrawalZsxm;
    private EditText etWithdrawalLxfs;
    private TextView tvWithdrawalButton;
    private TextView tvWithdrawalUnbindWechat;

    private int mWithdrawalMoney = 0;//可提现金额
    private String mPhoneNumber = "";//手机号
    private final DecimalFormat df = new DecimalFormat("￥##0.00");

    public static void start(Context context, int withdrawalMoney, String phoneNumber) {
        Intent starter = new Intent(context, WithdrawalActivity.class);
        starter.putExtra("withdrawalMoney", withdrawalMoney);
        starter.putExtra("phoneNumber", phoneNumber);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdrawal;
    }

    @Override
    protected void assignViews() {
        rlWithdrawalTop = (RelativeLayout) findViewById(R.id.rl_withdrawal_top);
        ivWithdrawalBack = (ImageView) findViewById(R.id.iv_withdrawal_back);
        tvWithdrawalTxje = (EditText) findViewById(R.id.tv_withdrawal_txje);
        tvWithdrawalZsxm = (MyEditText) findViewById(R.id.tv_withdrawal_zsxm);
        etWithdrawalLxfs = (EditText) findViewById(R.id.et_withdrawal_lxfs);
        tvWithdrawalButton = (TextView) findViewById(R.id.tv_withdrawal_button);
        tvWithdrawalUnbindWechat = (TextView) findViewById(R.id.tv_withdrawal_unbind_wechat);

        StatusBarUtil.setPaddingSmart(this, rlWithdrawalTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivWithdrawalBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvWithdrawalButton, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initFundWithdrawal();
            }
        });

        ClickUtils.applySingleDebouncing(tvWithdrawalUnbindWechat, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnbindDialog.getInstance(mContext, mPhoneNumber).builder().setNegativeButton().setPositiveButton(new UnbindDialog.OnPositiveListener() {
                    @Override
                    public void positiveButton(String code, String guid) {
                        EasyHttp.post(WithdrawalActivity.this).api(new UnbindWxApi().setVerifyCode(code).setGuid(guid))
                                .request(new HttpCallback<HttpData<String>>(WithdrawalActivity.this) {
                                    @Override
                                    public void onSucceed(HttpData<String> result) {
                                        ToastUtils.showShort("微信解绑成功");
                                        finish();
                                    }
                                });
                    }
                }).show();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mWithdrawalMoney = bundle.getInt("withdrawalMoney", 0);
        mPhoneNumber = bundle.getString("phoneNumber", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
//        tvWithdrawalTxje.setText(df.format(mWithdrawalMoney * 0.01));
//        tvWithdrawalLxfs.setText(mPhoneNumber);
    }

    private void initFundWithdrawal() {
        String price = tvWithdrawalTxje.getText().toString().trim();
        String name = tvWithdrawalZsxm.getText().toString().trim();
        String account = etWithdrawalLxfs.getText().toString().trim();

        if (price.isEmpty()) {
            ToastUtils.showShort("请填写提现金额");
            return;
        }

        if (mWithdrawalMoney < Double.parseDouble(price) * 100) {
            ToastUtils.showShort("提现金额不足");
            return;
        }

        if (name.isEmpty()) {
            ToastUtils.showShort("请填写真实姓名");
            return;
        }

        if (account.isEmpty()) {
            ToastUtils.showShort("请填写支付宝收款账号");
            return;
        }
        KeyboardUtils.hideSoftInput(this);
        EasyHttp.post(this)
                .api(new FundWithdrawalApi().setMoney(Double.parseDouble(price) * 100).setRealName(name).setChannelAccount(account))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        ToastUtils.showShort("提交成功");
                        finish();
                    }
                });
    }
}
