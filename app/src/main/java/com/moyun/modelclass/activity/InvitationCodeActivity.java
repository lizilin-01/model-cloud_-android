package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.InviteInfoApi;
import com.moyun.modelclass.http.api.InviteUseCodeApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.MyEditText;

/**
 * 作者：Meteor
 * tip：邀请码
 */
public class InvitationCodeActivity extends BaseActivity {
    private RelativeLayout rlInvitationCodeTop;
    private ImageView ivInvitationCodeBack;
    private MyEditText etInvitationCodeInput;
    private TextView tvInvitationCodeLogin;
    private TextView tvInvitationCodeTip;

    public static void start(Context context) {
        Intent starter = new Intent(context, InvitationCodeActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_invitation_code;
    }

    @Override
    protected void assignViews() {
        rlInvitationCodeTop = (RelativeLayout) findViewById(R.id.rl_invitation_code_top);
        ivInvitationCodeBack = (ImageView) findViewById(R.id.iv_invitation_code_back);
        etInvitationCodeInput = (MyEditText) findViewById(R.id.et_invitation_code_input);
        tvInvitationCodeLogin = (TextView) findViewById(R.id.tv_invitation_code_login);
        tvInvitationCodeTip = (TextView) findViewById(R.id.tv_invitation_code_tip);


        StatusBarUtil.setPaddingSmart(this, rlInvitationCodeTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivInvitationCodeBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvInvitationCodeLogin, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = etInvitationCodeInput.getText().toString().trim();
                if (code.isEmpty()) {
                    ToastUtils.showShort("请输入邀请码");
                } else {
                    InviteUseCode(code);
                    KeyboardUtils.hideSoftInput(InvitationCodeActivity.this);
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        initInviteInfo();
    }

    //获取用户邀请码的信息
    private void initInviteInfo() {
        EasyHttp.post(this)
                .api(new InviteInfoApi())
                .request(new HttpCallback<HttpData<InviteInfoApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<InviteInfoApi.Bean> result) {
                        if (result != null && result.getData() != null) {
                            tvInvitationCodeTip.setText(result.getData().getContent());
                        }
                    }
                });
    }

    //使用邀请码
    private void InviteUseCode(String code) {
        EasyHttp.post(this)
                .api(new InviteUseCodeApi().setInviteCode(code))
                .request(new HttpCallback<HttpData<InviteUseCodeApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<InviteUseCodeApi.Bean> result) {
                        if (result != null && result.getData() != null) {
                            ToastUtils.showShort("使用邀请码成功");
                        }
                    }
                });
    }
}
