package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.frank.BaseAdapter;
import com.moyun.modelclass.frank.FActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.GetFollowApi;
import com.moyun.modelclass.http.bean.FansAndFollowResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.databinding.ActivityChatFavouriteBinding;
import com.moyun.modelclass.databinding.ItemChatFavouriteBinding;

import java.util.List;

public class ChatFavouriteActivity extends FActivity<ActivityChatFavouriteBinding> {

    private BaseAdapter<ItemChatFavouriteBinding, FansAndFollowResponse> adapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, ChatFavouriteActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void initView() {
        super.initView();
        adapter = new BaseAdapter<ItemChatFavouriteBinding, FansAndFollowResponse>() {
            @Override
            protected void onBindData(FansAndFollowResponse bean, ItemChatFavouriteBinding binding, int position, View itemView) {
                super.onBindData(bean, binding, position, itemView);
                binding.getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChatDetailActivity.start(mActivity, bean.getUserId());
                    }
                });
                Glide.with(mActivity)
                        .asBitmap()
                        .load(CommonConfiguration.splitResUrl(3, bean.getAvatar()))
                        .placeholder(R.drawable.icon_head_portrait)
                        .error(R.drawable.icon_head_portrait)
                        .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                        .into(new BitmapImageViewTarget(binding.icon) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                super.setResource(resource);
                                binding.icon.setImageBitmap(ImageUtils.toRound(resource));
                            }
                        });
            }
        };
        mBinding.recyclerSearchHot.setAdapter(adapter);
    }

    @Override
    protected void initData() {
        super.initData();
        EasyHttp.post(this)
                .api(new GetFollowApi())
                .request(new HttpCallback<HttpData<GetFollowApi.Bean>>(null) {
                    @Override
                    public void onSucceed(HttpData<GetFollowApi.Bean> result) {
                        if (result == null) {
                            return;
                        }
                        GetFollowApi.Bean data = result.getData();
                        if (data == null) {
                            return;
                        }
                        List<FansAndFollowResponse> list = data.getList();
                        if (list == null || list.isEmpty()) {
                            return;
                        }
                        adapter.setData(list);
                        mBinding.title.setText("我的关注(" + list.size() + ")");
                    }
                });
    }

    public void back(View view) {
        finish();
    }
}