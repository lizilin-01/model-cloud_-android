package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.blankj.utilcode.util.ClickUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.fragment.user.FansFragment;
import com.moyun.modelclass.fragment.user.FollowsFragment;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.NoScrollViewPager;

import java.util.Objects;

/**
 * 作者：Meteor
 * tip：粉丝和关注者
 */
public class FansAndFollowsActivity extends BaseActivity {
    private AppBarLayout fansAndFollowsAppbar;
    private TabLayout fansAndFollowsTabs;
    private ImageView fansAndFollowsBack;
    private NoScrollViewPager fansAndFollowsViewpager;
    private MinePagerAdapter minePagerAdapter;

    private int mType = 1;

    public static void start(Context context, int type) {
        Intent starter = new Intent(context, FansAndFollowsActivity.class);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fans_and_follows;
    }

    @Override
    protected void assignViews() {
        fansAndFollowsAppbar = (AppBarLayout) findViewById(R.id.fans_and_follows_appbar);
        fansAndFollowsTabs = (TabLayout) findViewById(R.id.fans_and_follows_tabs);
        fansAndFollowsBack = (ImageView) findViewById(R.id.fans_and_follows_back);
        fansAndFollowsViewpager = (NoScrollViewPager) findViewById(R.id.fans_and_follows_viewpager);

        StatusBarUtil.setPaddingSmart(this, fansAndFollowsAppbar);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(fansAndFollowsBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mType = bundle.getInt("type", 1);
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        minePagerAdapter = new MinePagerAdapter(getSupportFragmentManager());
        fansAndFollowsViewpager.setOffscreenPageLimit(2);
        fansAndFollowsViewpager.setAdapter(minePagerAdapter);
        fansAndFollowsTabs.setupWithViewPager(fansAndFollowsViewpager);
        setTabSelect(mType);
    }

    private void setTabSelect(int position) {
        fansAndFollowsTabs.getTabAt(position).select();
    }

    public static class MinePagerAdapter extends FragmentPagerAdapter {
        private Fragment[] fragments = null;
        private String[] titles = new String[]{"关注", "粉丝"};

        public MinePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getCount() {
            fragments = new Fragment[]{FollowsFragment.newInstance(), FansFragment.newInstance()};
            return fragments.length;
        }
    }
}
