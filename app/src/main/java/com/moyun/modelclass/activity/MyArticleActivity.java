package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.adapter.ArticleTypeAdapter;
import com.moyun.modelclass.adapter.ModelPagerAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.fragment.article.ArticleListFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.listener.RecyclerItemClickListener;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：我的文章
 */
public class MyArticleActivity extends BaseActivity {
    private RelativeLayout rlMainModelTop;
    private ImageView ivMyArticleBack;
    private ImageView ivMyArticleEdit;
    private RecyclerView myArticleRecycler;
    private NoScrollViewPager myArticleVp;

    private ModelPagerAdapter modelPagerAdapter = null;

    private ArticleTypeAdapter articleTypeAdapter = new ArticleTypeAdapter();
    private List<Fragment> mFragmentArrays = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, MyArticleActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_article;
    }

    @Override
    protected void assignViews() {
        rlMainModelTop = (RelativeLayout) findViewById(R.id.rl_main_model_top);
        ivMyArticleBack = (ImageView) findViewById(R.id.iv_my_article_back);
        ivMyArticleEdit = (ImageView) findViewById(R.id.iv_my_article_edit);
        myArticleRecycler = (RecyclerView) findViewById(R.id.my_article_recycler);
        myArticleVp = (NoScrollViewPager) findViewById(R.id.my_article_vp);


        myArticleRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        myArticleRecycler.setAdapter(articleTypeAdapter);

        StatusBarUtil.setPaddingSmart(this, rlMainModelTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivMyArticleBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(ivMyArticleEdit, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    EditArticleActivity.start(MyArticleActivity.this, "");
                } else {
//                    ToastUtils.showShort(getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(MyArticleActivity.this);
                }
            }
        });

        myArticleRecycler.addOnItemTouchListener(new RecyclerItemClickListener(this, myArticleRecycler, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                articleTypeAdapter.setSelected(position);
                myArticleVp.setCurrentItem(position, true);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        modelTypeData();
    }

    private void modelTypeData() {
        List<String> articleType = new ArrayList<>();
        articleType.add("全部");
        articleType.add("审核中");
        articleType.add("已发布");
        articleType.add("审核驳回");
        articleType.add("草稿箱");
        articleTypeAdapter.setList(articleType);
        mFragmentArrays.clear();
        for (String data : articleType) {
            mFragmentArrays.add(ArticleListFragment.newInstance(data));
        }

        modelPagerAdapter = new ModelPagerAdapter(getSupportFragmentManager(), mFragmentArrays);
        myArticleVp.setAdapter(modelPagerAdapter);
    }
}
