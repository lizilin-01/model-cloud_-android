package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.HomeFollowAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckCollectListUserApi;
import com.moyun.modelclass.http.api.CollectArticleApi;
import com.moyun.modelclass.http.api.MyCollectApi;
import com.moyun.modelclass.http.bean.ArticleResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：我的收藏
 */
public class MyCollectActivity extends BaseActivity implements HomeFollowAdapter.OnFollowClick {
    private RelativeLayout rlMyCollectTop;
    private ImageView ivMyCollectBack;
    private SmartRefreshLayout srlMyCollectRefresh;
    private RecyclerView rlMyCollectRecycler;

    private HomeFollowAdapter myCollectAdapter = new HomeFollowAdapter();
    private List<String> articleIdList = new ArrayList<>();
    private String mLastId = "";

    public static void start(Context context) {
        Intent starter = new Intent(context, MyCollectActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_collect;
    }

    @Override
    protected void assignViews() {
        rlMyCollectTop = (RelativeLayout) findViewById(R.id.rl_my_collect_top);
        ivMyCollectBack = (ImageView) findViewById(R.id.iv_my_collect_back);
        srlMyCollectRefresh = (SmartRefreshLayout) findViewById(R.id.srl_my_collect_refresh);
        rlMyCollectRecycler = (RecyclerView) findViewById(R.id.rl_my_collect_recycler);


        rlMyCollectRecycler.setLayoutManager(new LinearLayoutManager(this));
        myCollectAdapter.setOnFollowClick(this);
        rlMyCollectRecycler.setAdapter(myCollectAdapter);

        StatusBarUtil.setPaddingSmart(this, rlMyCollectTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivMyCollectBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        srlMyCollectRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                articleIdList.clear();
                homeFollowData(true);
            }
        });
        srlMyCollectRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                homeFollowData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        homeFollowData(true);
    }

    private void homeFollowData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new MyCollectApi().setLastId(mLastId).setPageSize(CommonConfiguration.page).setSearchText("").setAuthorUserId("").setSortType(1))
                .request(new HttpCallback<HttpData<MyCollectApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<MyCollectApi.Bean> result) {
                        srlMyCollectRefresh.finishRefresh();
                        srlMyCollectRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                myCollectAdapter.setList(result.getData().getList());
                            } else {
                                myCollectAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                myCollectAdapter.setList(new ArrayList<>());
                                myCollectAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }

                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            for (ArticleResponse item : result.getData().getList()) {
                                articleIdList.add(item.getId());
                            }
                        }
                        checkUserListCollect();
                    }
                });
    }

    private void checkUserListCollect() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckCollectListUserApi().setArticleIdList(articleIdList))
                    .request(new HttpCallback<HttpData<CheckCollectListUserApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckCollectListUserApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getLikes().size() > 0) {
                                myCollectAdapter.setArticleState(result.getData().getLikes());
                            }
                        }
                    });
        }
    }


    @Override
    public void userInfo(String userId) {
        UserInfoActivity.start(MyCollectActivity.this, userId);
    }

    @Override
    public void details(String articleId) {
        ArticleDetailsActivity.start(this, articleId, false);
    }

    @Override
    public void imageUrl(String url) {
        ScanImageDialog.getInstance(this, url).builder().setNegativeButton().show();
    }

    @Override
    public void collect(String articleId, boolean isCollect) {
        EasyHttp.post(this)
                .api(new CollectArticleApi().setArticleId(articleId).setLikes(isCollect))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        checkUserListCollect();
                        if (isCollect) {
                            ToastUtils.showShort("已收藏");
                        } else {
                            ToastUtils.showShort("取消收藏");
                        }
                    }
                });
    }
}
