package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.moyun.modelclass.adapter.ModelPagerAdapter;
import com.moyun.modelclass.adapter.OrderTypeAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.event.OrderSearchEvent;
import com.moyun.modelclass.fragment.order.OrderListFragment;
import com.moyun.modelclass.listener.RecyclerItemClickListener;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.NoScrollViewPager;
import org.greenrobot.eventbus.EventBus;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：我的订单
 */
public class MyOrderActivity extends BaseActivity {
    private RelativeLayout rlMyOrderTop;
    private ImageView ivMyOrderBack;
    private EditText etMyOrderContent;
    private TextView tvMyOrderButton;
    private RecyclerView myOrderRecycler;
    private NoScrollViewPager myOrderVp;

    private ModelPagerAdapter modelPagerAdapter = null;

    private OrderTypeAdapter orderTypeAdapter = new OrderTypeAdapter();
    private List<Fragment> mFragmentArrays = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, MyOrderActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_order;
    }

    @Override
    protected void assignViews() {
        rlMyOrderTop = (RelativeLayout) findViewById(R.id.rl_my_order_top);
        ivMyOrderBack = (ImageView) findViewById(R.id.iv_my_order_back);
        etMyOrderContent = (EditText) findViewById(R.id.et_my_order_content);
        tvMyOrderButton = (TextView) findViewById(R.id.tv_my_order_button);
        myOrderRecycler = (RecyclerView) findViewById(R.id.my_order_recycler);
        myOrderVp = (NoScrollViewPager) findViewById(R.id.my_order_vp);

        myOrderRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        myOrderRecycler.setAdapter(orderTypeAdapter);
        myOrderVp.setOffscreenPageLimit(5);

        StatusBarUtil.setPaddingSmart(this, rlMyOrderTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivMyOrderBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvMyOrderButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftInput(etMyOrderContent);
                EventBus.getDefault().post(new OrderSearchEvent(etMyOrderContent.getText().toString().trim()));
                etMyOrderContent.clearFocus();
            }
        });

        etMyOrderContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        KeyboardUtils.hideSoftInput(etMyOrderContent);
                        EventBus.getDefault().post(new OrderSearchEvent(etMyOrderContent.getText().toString().trim()));
                        etMyOrderContent.clearFocus();
                        break;
                }
                return true;
            }
        });

        myOrderRecycler.addOnItemTouchListener(new RecyclerItemClickListener(this, myOrderRecycler, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                orderTypeAdapter.setSelected(position);
                myOrderVp.setCurrentItem(position, true);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        modelTypeData();
    }

    private void modelTypeData() {
        List<String> orderType = new ArrayList<>();
        orderType.add("全部");
        orderType.add("待付款");
        orderType.add("待发货");
        orderType.add("待收货");
        orderType.add("待评价");
        orderTypeAdapter.setList(orderType);
        mFragmentArrays.clear();
        for (String data : orderType) {
            mFragmentArrays.add(OrderListFragment.newInstance(data));
        }

        modelPagerAdapter = new ModelPagerAdapter(getSupportFragmentManager(), mFragmentArrays);
        myOrderVp.setAdapter(modelPagerAdapter);
    }
}
