package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.HomeFollowAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.ArticleHistoryApi;
import com.moyun.modelclass.http.api.CheckCollectListUserApi;
import com.moyun.modelclass.http.api.CollectArticleApi;
import com.moyun.modelclass.http.bean.ArticleResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：浏览记录
 */
public class ArticleHistoryActivity extends BaseActivity implements HomeFollowAdapter.OnFollowClick {
    private RelativeLayout rlArticleHistoryTop;
    private ImageView ivArticleHistoryBack;
    private SmartRefreshLayout srlArticleHistoryRefresh;
    private RecyclerView rlArticleHistoryRecycler;

    private HomeFollowAdapter articleHistoryAdapter = new HomeFollowAdapter();
    private List<String> articleIdList = new ArrayList<>();
    private int mLastId = 1;

    public static void start(Context context) {
        Intent starter = new Intent(context, ArticleHistoryActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_article_history;
    }

    @Override
    protected void assignViews() {
        rlArticleHistoryTop = (RelativeLayout) findViewById(R.id.rl_article_history_top);
        ivArticleHistoryBack = (ImageView) findViewById(R.id.iv_article_history_back);
        srlArticleHistoryRefresh = (SmartRefreshLayout) findViewById(R.id.srl_article_history_refresh);
        rlArticleHistoryRecycler = (RecyclerView) findViewById(R.id.rl_article_history_recycler);

        rlArticleHistoryRecycler.setLayoutManager(new LinearLayoutManager(this));
        articleHistoryAdapter.setOnFollowClick(this);
        rlArticleHistoryRecycler.setAdapter(articleHistoryAdapter);

        StatusBarUtil.setPaddingSmart(this, rlArticleHistoryTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivArticleHistoryBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        srlArticleHistoryRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = 1;
                articleIdList.clear();
                homeFollowData(true);
            }
        });
        srlArticleHistoryRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                homeFollowData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        homeFollowData(true);
    }

    private void homeFollowData(boolean isRefresh) {
        mLastId = isRefresh ? 1 : mLastId;
        EasyHttp.post(this)
                .api(new ArticleHistoryApi().setPageIndex(mLastId).setPageSize(CommonConfiguration.page))
                .request(new HttpCallback<HttpData<ArticleHistoryApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<ArticleHistoryApi.Bean> result) {
                        srlArticleHistoryRefresh.finishRefresh();
                        srlArticleHistoryRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = mLastId + 1;
                            if (isRefresh) {
                                articleHistoryAdapter.setList(result.getData().getList());
                            } else {
                                articleHistoryAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                articleHistoryAdapter.setList(new ArrayList<>());
                                articleHistoryAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }

                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            for (ArticleResponse item : result.getData().getList()) {
                                articleIdList.add(item.getId());
                            }
                        }
                        checkUserListCollect();
                    }
                });
    }

    private void checkUserListCollect() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckCollectListUserApi().setArticleIdList(articleIdList))
                    .request(new HttpCallback<HttpData<CheckCollectListUserApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckCollectListUserApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getLikes().size() > 0) {
                                articleHistoryAdapter.setArticleState(result.getData().getLikes());
                            }
                        }
                    });
        }
    }


    @Override
    public void userInfo(String userId) {
        UserInfoActivity.start(ArticleHistoryActivity.this, userId);
    }

    @Override
    public void details(String articleId) {
        ArticleDetailsActivity.start(this, articleId, false);
    }

    @Override
    public void imageUrl(String url) {
        ScanImageDialog.getInstance(this, url).builder().setNegativeButton().show();
    }

    @Override
    public void collect(String articleId, boolean isCollect) {
        EasyHttp.post(this)
                .api(new CollectArticleApi().setArticleId(articleId).setLikes(isCollect))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        checkUserListCollect();
                        if (isCollect) {
                            ToastUtils.showShort("已收藏");
                        } else {
                            ToastUtils.showShort("取消收藏");
                        }
                    }
                });
    }
}
