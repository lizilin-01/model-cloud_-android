package com.moyun.modelclass.activity;

import static com.moyun.modelclass.activity.EditArticleActivity.KEY_POPULAR_ID;
import static com.moyun.modelclass.activity.PersonalInformationActivity.KEY_PERSONAL_INFORMATION_ID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.PopularSelfAdapter;
import com.moyun.modelclass.adapter.SearchPopularAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.AppVipSearchPopularApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.TBFoldLayout;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：编辑话题
 */
public class EditPopularActivity extends BaseActivity implements PopularSelfAdapter.OnDeleteClick, SearchPopularAdapter.OnChoiceClick {
    private RelativeLayout llEditPopularTop;
    private ImageView ivEditPopularBack;
    private EditText etEditPopularContent;
    private ImageView ivEditPopularClean;
    private TextView tvEditPopularButton;
    private TextView tvEditPopularSelf;
    private RecyclerView recyclerEditPopularSelf;
    private View lineEditPopularPopular;
    private TextView tvEditPopularPopular;
    private TBFoldLayout foldEditPopularPopular;
    private PopularSelfAdapter popularSelfAdapter = new PopularSelfAdapter();
    private SearchPopularAdapter searchPopularAdapter = new SearchPopularAdapter();
    private List<String> mPopularList = new ArrayList<>();//已选择话题

    public static void start(Activity context, List<String> popularList, int code) {
        Intent starter = new Intent(context, EditPopularActivity.class);
        starter.putExtra("popularList", (Serializable) popularList);
        context.startActivityForResult(starter, code);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_popular;
    }

    @Override
    protected void assignViews() {
        llEditPopularTop = (RelativeLayout) findViewById(R.id.ll_edit_popular_top);
        ivEditPopularBack = (ImageView) findViewById(R.id.iv_edit_popular_back);
        etEditPopularContent = (EditText) findViewById(R.id.et_edit_popular_content);
        ivEditPopularClean = (ImageView) findViewById(R.id.iv_edit_popular_clean);
        tvEditPopularButton = (TextView) findViewById(R.id.tv_edit_popular_button);
        tvEditPopularSelf = (TextView) findViewById(R.id.tv_edit_popular_self);
        recyclerEditPopularSelf = (RecyclerView) findViewById(R.id.recycler_edit_popular_self);
        lineEditPopularPopular = (View) findViewById(R.id.line_edit_popular_popular);
        tvEditPopularPopular = (TextView) findViewById(R.id.tv_edit_popular_popular);
        foldEditPopularPopular = (TBFoldLayout) findViewById(R.id.fold_edit_popular_popular);

        StatusBarUtil.setPaddingSmart(this, llEditPopularTop);

        recyclerEditPopularSelf.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerEditPopularSelf.addItemDecoration(new DividerItemDecoration(mContext, Color.parseColor("#CDD9DB"), 1, SizeUtils.dp2px(6), SizeUtils.dp2px(6), false));
        popularSelfAdapter.setOnDeleteClick(this);
        recyclerEditPopularSelf.setAdapter(popularSelfAdapter);

        searchPopularAdapter.setOnChoiceClick(this);
        foldEditPopularPopular.setAdapter(searchPopularAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivEditPopularBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftInput(etEditPopularContent);
                Intent intent = new Intent();
                intent.putStringArrayListExtra(KEY_POPULAR_ID, (ArrayList<String>) mPopularList);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        etEditPopularContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString().trim();
                if (text.isEmpty()) {
                    ivEditPopularClean.setVisibility(View.GONE);
                } else {
                    ivEditPopularClean.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ClickUtils.applySingleDebouncing(ivEditPopularClean, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEditPopularContent.setText("");
            }
        });

        ClickUtils.applySingleDebouncing(tvEditPopularButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchResult = etEditPopularContent.getText().toString().trim();
                if (!searchResult.isEmpty()) {
                    KeyboardUtils.hideSoftInput(etEditPopularContent);
                    mPopularList.add(searchResult);
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra(KEY_POPULAR_ID, (ArrayList<String>) mPopularList);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    KeyboardUtils.hideSoftInput(etEditPopularContent);
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra(KEY_POPULAR_ID, (ArrayList<String>) mPopularList);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        if (bundle.get("popularList") != null) {
            mPopularList = (List<String>) bundle.get("popularList");
        }
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        if (mPopularList != null && mPopularList.size() > 0) {
            popularSelfAdapter.setList(mPopularList);
            tvEditPopularSelf.setVisibility(View.VISIBLE);
            recyclerEditPopularSelf.setVisibility(View.VISIBLE);
        } else {
            tvEditPopularSelf.setVisibility(View.GONE);
            recyclerEditPopularSelf.setVisibility(View.GONE);
        }

        initSearchPopularLog();
    }

    private void initSearchPopularLog() {
        EasyHttp.post(this)
                .api(new AppVipSearchPopularApi())
                .request(new HttpCallback<HttpData<AppVipSearchPopularApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AppVipSearchPopularApi.Bean> result) {
                        if (result.getData() != null && result.getData().getTags() != null && result.getData().getTags().size() > 0) {
                            lineEditPopularPopular.setVisibility(View.VISIBLE);
                            tvEditPopularPopular.setVisibility(View.VISIBLE);
                            searchPopularAdapter.setNewData(result.getData().getTags());
                        } else {
                            lineEditPopularPopular.setVisibility(View.GONE);
                            tvEditPopularPopular.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public void choice(String search) {
//        etEditPopularContent.setText("");
        etEditPopularContent.setText(search);
    }

    @Override
    public void delete(int position) {
        mPopularList.remove(position);
        if (mPopularList != null && mPopularList.size() > 0) {
            popularSelfAdapter.setList(mPopularList);
            tvEditPopularSelf.setVisibility(View.VISIBLE);
            recyclerEditPopularSelf.setVisibility(View.VISIBLE);
        } else {
            tvEditPopularSelf.setVisibility(View.GONE);
            recyclerEditPopularSelf.setVisibility(View.GONE);
        }
    }
}
