package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.SearchHotAdapter;
import com.moyun.modelclass.adapter.SearchPopularAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.AppVipSearchHotApi;
import com.moyun.modelclass.http.api.AppVipSearchPopularApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.TBFoldLayout;

/**
 * 作者：Meteor
 * tip：搜索
 */
public class SearchActivity extends BaseActivity implements SearchHotAdapter.OnChoiceClick, SearchPopularAdapter.OnChoiceClick {
    private RelativeLayout llSearchTop;
    private ImageView ivSearchBack;
    private EditText etSearchContent;
    private TextView tvSearchButton;
    private TextView tvSearchReplace;
    private RecyclerView recyclerSearchHot;
    private TBFoldLayout foldSearchPopular;

    private SearchHotAdapter searchHotAdapter = new SearchHotAdapter();
    private SearchPopularAdapter searchPopularAdapter = new SearchPopularAdapter();

    public static void start(Context context) {
        Intent starter = new Intent(context, SearchActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_search;
    }

    @Override
    protected void assignViews() {
        llSearchTop = (RelativeLayout) findViewById(R.id.ll_search_top);
        ivSearchBack = (ImageView) findViewById(R.id.iv_search_back);
        etSearchContent = (EditText) findViewById(R.id.et_search_content);
        tvSearchButton = (TextView) findViewById(R.id.tv_search_button);
        tvSearchReplace = (TextView) findViewById(R.id.tv_search_replace);
        recyclerSearchHot = (RecyclerView) findViewById(R.id.recycler_search_hot);
        foldSearchPopular = (TBFoldLayout) findViewById(R.id.fold_search_popular);

        StatusBarUtil.setPaddingSmart(this, llSearchTop);

        recyclerSearchHot.setLayoutManager(new GridLayoutManager(mContext, 2));
        searchHotAdapter.setOnChoiceClick(this);
        recyclerSearchHot.setAdapter(searchHotAdapter);

        searchPopularAdapter.setOnChoiceClick(this);
        foldSearchPopular.setAdapter(searchPopularAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivSearchBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvSearchButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchResult = etSearchContent.getText().toString().trim();
                KeyboardUtils.hideSoftInput(etSearchContent);
                SearchResultActivity.start(SearchActivity.this, searchResult);
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvSearchReplace, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //换一换
                initSearchHotLog();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        initSearchHotLog();
        initSearchPopularLog();
    }

    private void initSearchHotLog() {
        EasyHttp.post(this)
                .api(new AppVipSearchHotApi())
                .request(new HttpCallback<HttpData<AppVipSearchHotApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AppVipSearchHotApi.Bean> result) {
                        if (result.getData() != null && result.getData().getSearches() != null && result.getData().getSearches().size() > 0) {
                            searchHotAdapter.setNewInstance(result.getData().getSearches());
                        }
                    }
                });
    }

    private void initSearchPopularLog() {
        EasyHttp.post(this)
                .api(new AppVipSearchPopularApi())
                .request(new HttpCallback<HttpData<AppVipSearchPopularApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AppVipSearchPopularApi.Bean> result) {
                        if (result.getData() != null && result.getData().getTags() != null && result.getData().getTags().size() > 0) {
                            searchPopularAdapter.setNewData(result.getData().getTags());
                        }
                    }
                });
    }

    @Override
    public void choice(String search) {
        KeyboardUtils.hideSoftInput(etSearchContent);
        SearchResultActivity.start(SearchActivity.this, search);
        finish();
    }
}
