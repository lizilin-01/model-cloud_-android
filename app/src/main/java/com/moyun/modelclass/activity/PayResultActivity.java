package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.R;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.utils.StatusBarUtil;

/**
 * 作者：Meteor
 * tip：支付结果
 */
public class PayResultActivity extends BaseActivity {
    private RelativeLayout llPayResultTop;
    private ImageView ivPayResultBack;
    private TextView tvPayResultTitle;
    private ImageView ivPayResultState;
    private TextView tvPayResultHome;
    private TextView tvPayResultOrder;
    private boolean mPayState = false;
    private String mOrderId = "";//订单号

    public static void start(Context context, boolean payState, String orderId) {
        Intent starter = new Intent(context, PayResultActivity.class);
        starter.putExtra("payState", payState);
        starter.putExtra("orderId", orderId);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_pay_result;
    }

    @Override
    protected void assignViews() {
        llPayResultTop = (RelativeLayout) findViewById(R.id.ll_pay_result_top);
        ivPayResultBack = (ImageView) findViewById(R.id.iv_pay_result_back);
        tvPayResultTitle = (TextView) findViewById(R.id.tv_pay_result_title);
        ivPayResultState = (ImageView) findViewById(R.id.iv_pay_result_state);
        tvPayResultHome = (TextView) findViewById(R.id.tv_pay_result_home);
        tvPayResultOrder = (TextView) findViewById(R.id.tv_pay_result_order);

        StatusBarUtil.setPaddingSmart(this, llPayResultTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivPayResultBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvPayResultHome, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SplashActivity.start(PayResultActivity.this);
                ActivityUtils.finishAllActivities();
            }
        });

        ClickUtils.applySingleDebouncing(tvPayResultOrder, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv) && !mOrderId.isEmpty()) {
                    OrderDetailsActivity.start(PayResultActivity.this, mOrderId);
                } else {
//                    ToastUtils.showShort(getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(PayResultActivity.this);
                }
                finish();
            }
        });

    }

    @Override
    protected void getExtras(Bundle bundle) {
        mPayState = bundle.getBoolean("payState", false);
        mOrderId = bundle.getString("orderId", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        if (mPayState) {
            tvPayResultTitle.setText("支付成功");
            ivPayResultState.setImageResource(R.drawable.icon_success);
        } else {
            tvPayResultTitle.setText("支付失败");
            ivPayResultState.setImageResource(R.drawable.icon_fails);
        }
    }
}
