package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.MyOrderAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.OrderCancelApi;
import com.moyun.modelclass.http.api.OrderDeleteApi;
import com.moyun.modelclass.http.api.OrderListApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * 日期：2022/1/19 23:25
 * tip：我的订单列表
 */
public class OrderSearchActivity extends BaseActivity implements MyOrderAdapter.OnFindClick {
    private RelativeLayout rlOrderSearchTop;
    private ImageView ivOrderSearchBack;
    private EditText etOrderSearchContent;
    private TextView tvOrderSearchButton;
    private SmartRefreshLayout orderSearchListRefresh;
    private RecyclerView orderSearchListRecycler;

    private MyOrderAdapter myOrderAdapter = new MyOrderAdapter();
    private String mLastId = "";
    private int mOrderType = 0;

    public static void start(Context context) {
        Intent starter = new Intent(context, OrderSearchActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_order_list;
    }

    @Override
    protected void assignViews() {
        rlOrderSearchTop = (RelativeLayout) findViewById(R.id.rl_order_search_top);
        ivOrderSearchBack = (ImageView) findViewById(R.id.iv_order_search_back);
        etOrderSearchContent = (EditText) findViewById(R.id.et_order_search_content);
        tvOrderSearchButton = (TextView) findViewById(R.id.tv_order_search_button);
        orderSearchListRefresh = (SmartRefreshLayout) findViewById(R.id.order_search_list_refresh);
        orderSearchListRecycler = (RecyclerView) findViewById(R.id.order_search_list_recycler);

        orderSearchListRecycler.setLayoutManager(new LinearLayoutManager(this));
        myOrderAdapter.setOnFindClick(this);
        orderSearchListRecycler.setAdapter(myOrderAdapter);

        StatusBarUtil.setPaddingSmart(this, rlOrderSearchTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivOrderSearchBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvOrderSearchButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftInput(etOrderSearchContent);
                orderListData(true);
            }
        });

        etOrderSearchContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        KeyboardUtils.hideSoftInput(etOrderSearchContent);
                        orderListData(true);
                        break;
                }
                return true;
            }
        });

        orderSearchListRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                orderListData(true);
            }
        });
        orderSearchListRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                orderListData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        orderListData(true);
    }

    private void orderListData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new OrderListApi().setOrderStateVO(mOrderType)
                        .setPageSize(CommonConfiguration.page)
                        .setLastId(mLastId)
                        .setSearchText(etOrderSearchContent.getText().toString().trim()))
                .request(new HttpCallback<HttpData<OrderListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<OrderListApi.Bean> result) {
                        orderSearchListRefresh.finishRefresh();
                        orderSearchListRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                myOrderAdapter.setList(result.getData().getList());
                            } else {
                                myOrderAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                myOrderAdapter.setList(new ArrayList<>());
                                myOrderAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }

    @Override
    public void close(String orderId) {
        //关闭订单
        EasyHttp.post(this)
                .api(new OrderCancelApi().setOrderId(orderId))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        ToastUtils.showShort("关闭订单成功");
                        orderListData(true);
                    }
                });
    }

    @Override
    public void pay(String orderId) {
        //立即支付
    }

    @Override
    public void details(String orderId) {
        //查看详情
        OrderDetailsActivity.start(OrderSearchActivity.this, orderId);
    }

    @Override
    public void delete(String orderId) {
        //删除订单
        EasyHttp.post(this)
                .api(new OrderDeleteApi().setOrderId(orderId))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        ToastUtils.showShort("删除订单成功");
                        orderListData(true);
                    }
                });
    }

    @Override
    public void evaluate(String orderId) {
        //立即评价
        OrderEvaluateActivity.start(OrderSearchActivity.this, orderId);
    }
}
