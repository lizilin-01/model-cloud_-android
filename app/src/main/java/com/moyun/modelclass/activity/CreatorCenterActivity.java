package com.moyun.modelclass.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.CreatorCenterAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.FundInfoApi;
import com.moyun.modelclass.http.api.FundWithdrawalOrderListApi;
import com.moyun.modelclass.http.api.UserInfoApi;
import com.moyun.modelclass.http.bean.LoginInfoResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * 作者：Meteor
 * tip：创作者中心
 */
public class CreatorCenterActivity extends BaseActivity {
    private RelativeLayout rlCreatorCenterTop;
    private ImageView ivCreatorCenterBack;
    private TextView tvCreatorCenterZsy;
    private TextView tvCreatorCenterBysy;
    private TextView tvCreatorCenterKtx;
    private SmartRefreshLayout srlCreatorCenterRefresh;
    private RecyclerView rvCreatorCenterRecycler;
    private TextView tvCreatorCenterButton;

    private CreatorCenterAdapter creatorCenterAdapter = new CreatorCenterAdapter();
    private final DecimalFormat df = new DecimalFormat("￥##0.00");
    private String mLastId = "";
    private String mUserId = "";
    private boolean isBindWechat = false;//是否绑定微信
    private String mPhoneNumber = "";//手机号
    private int mWithdrawalMoney = 0;//提现金额

    public static void start(Context context, String userId) {
        Intent starter = new Intent(context, CreatorCenterActivity.class);
        starter.putExtra("userId", userId);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_creator_center;
    }

    @Override
    protected void assignViews() {
        rlCreatorCenterTop = (RelativeLayout) findViewById(R.id.rl_creator_center_top);
        ivCreatorCenterBack = (ImageView) findViewById(R.id.iv_creator_center_back);
        tvCreatorCenterZsy = (TextView) findViewById(R.id.tv_creator_center_zsy);
        tvCreatorCenterBysy = (TextView) findViewById(R.id.tv_creator_center_bysy);
        tvCreatorCenterKtx = (TextView) findViewById(R.id.tv_creator_center_ktx);
        srlCreatorCenterRefresh = (SmartRefreshLayout) findViewById(R.id.srl_creator_center_refresh);
        rvCreatorCenterRecycler = (RecyclerView) findViewById(R.id.rv_creator_center_recycler);
        tvCreatorCenterButton = (TextView) findViewById(R.id.tv_creator_center_button);

        StatusBarUtil.setPaddingSmart(this, rlCreatorCenterTop);
        rvCreatorCenterRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        rvCreatorCenterRecycler.setAdapter(creatorCenterAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivCreatorCenterBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        srlCreatorCenterRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                initFundInfo();
                fundWithdrawalOrderListData(true);
            }
        });

        srlCreatorCenterRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                fundWithdrawalOrderListData(false);
            }
        });

        ClickUtils.applySingleDebouncing(tvCreatorCenterButton, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBindWechat) {
                    WithdrawalActivity.start(CreatorCenterActivity.this, mWithdrawalMoney,mPhoneNumber);
                } else {
                    BindWechatActivity.start(CreatorCenterActivity.this);
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mUserId = bundle.getString("userId", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        initFundInfo();
        fundWithdrawalOrderListData(true);
    }

    //查看资金详情
    @SuppressLint("SetTextI18n")
    private void initFundInfo() {
        EasyHttp.post(this)
                .api(new FundInfoApi())
                .request(new HttpCallback<HttpData<FundInfoApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<FundInfoApi.Bean> result) {
                        if (result.getData() != null) {
                            mWithdrawalMoney = result.getData().getCanWithdrawal();
                            tvCreatorCenterZsy.setText(df.format(result.getData().getTotalFund() * 0.01));
                            tvCreatorCenterBysy.setText(df.format(result.getData().getMonthFund() * 0.01));
                            tvCreatorCenterKtx.setText(df.format(result.getData().getCanWithdrawal() * 0.01));
                        }
                    }
                });
    }

    private void fundWithdrawalOrderListData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new FundWithdrawalOrderListApi().setLastId(mLastId).setPageSize(CommonConfiguration.page))
                .request(new HttpCallback<HttpData<FundWithdrawalOrderListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<FundWithdrawalOrderListApi.Bean> result) {
                        srlCreatorCenterRefresh.finishRefresh();
                        srlCreatorCenterRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                creatorCenterAdapter.setList(result.getData().getList());
                            } else {
                                creatorCenterAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                creatorCenterAdapter.setList(new ArrayList<>());
                                creatorCenterAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUserInfo();
    }

    private void initUserInfo() {
        if (mUserId.isEmpty()) {
            return;
        }
        EasyHttp.post(this)
                .api(new UserInfoApi().setUserId(mUserId))
                .request(new HttpCallback<HttpData<LoginInfoResponse>>(this) {
                    @Override
                    public void onSucceed(HttpData<LoginInfoResponse> result) {
                        if (result.getData() != null) {
                            isBindWechat = result.getData().isHasBindWechat();
                            mPhoneNumber = result.getData().getMobile();
                            if (isBindWechat) {
                                tvCreatorCenterButton.setText("提现");
                                tvCreatorCenterButton.setBackgroundResource(R.drawable.background_ff6600_25);
                            } else {
                                tvCreatorCenterButton.setText("绑定微信去提现");
                                tvCreatorCenterButton.setBackgroundResource(R.drawable.background_055d6c_25);
                            }
                        }
                    }
                });
    }
}
