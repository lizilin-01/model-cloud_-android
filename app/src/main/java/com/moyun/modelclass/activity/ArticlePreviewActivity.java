package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.ArticleDetailDetailsAdapter;
import com.moyun.modelclass.adapter.ModelBuyAdapter;
import com.moyun.modelclass.adapter.ParentCommentAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.AppModelListApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.NonScrollRecyclerView;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArticlePreviewActivity extends BaseActivity {
    private RelativeLayout rlArticlePreviewTop;
    private ImageView ivArticlePreviewBack;
    private TextView tvArticlePreviewTitle;

    private RecyclerView recyclerArticlePreviewDetails;
    private TextView tvArticlePreviewSubject;
    private RecyclerView recyclerArticlePreviewModel;
    private NonScrollRecyclerView recyclerArticlePreviewComments;

    private ArticleDetailDetailsAdapter articleDetailDetailsAdapter = new ArticleDetailDetailsAdapter();
    private ModelBuyAdapter modelBuyAdapter = new ModelBuyAdapter();
    private ParentCommentAdapter parentCommentAdapter = new ParentCommentAdapter();

    private String mDetailsTitle = "";//文章标题
    private String mDetailsContent = "";//文章内容
    private List<String> mDetailsModelList = new ArrayList<>();//模型列表
    private List<String> mDetailsTagList = new ArrayList<>();//话题列表

    public static void start(Context context, String detailsTitle, String detailsContent, List<String> detailsTagList, List<String> detailsModelList) {
        Intent starter = new Intent(context, ArticlePreviewActivity.class);
        starter.putExtra("detailsTitle", detailsTitle);
        starter.putExtra("detailsContent", detailsContent);
        starter.putExtra("detailsTagList", (Serializable) detailsTagList);
        starter.putExtra("detailsModelList", (Serializable) detailsModelList);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_article_preview;
    }

    @Override
    protected void assignViews() {
        rlArticlePreviewTop = (RelativeLayout) findViewById(R.id.rl_article_preview_top);
        ivArticlePreviewBack = (ImageView) findViewById(R.id.iv_article_preview_back);
        tvArticlePreviewTitle = (TextView) findViewById(R.id.tv_article_preview_title);
        recyclerArticlePreviewDetails = (RecyclerView) findViewById(R.id.recycler_article_preview_details);
        tvArticlePreviewSubject = (TextView) findViewById(R.id.tv_article_preview_subject);
        recyclerArticlePreviewModel = (RecyclerView) findViewById(R.id.recycler_article_preview_model);
        recyclerArticlePreviewComments = (NonScrollRecyclerView) findViewById(R.id.recycler_article_preview_comments);

        StatusBarUtil.setPaddingSmart(this, rlArticlePreviewTop);

        recyclerArticlePreviewDetails.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerArticlePreviewDetails.setAdapter(articleDetailDetailsAdapter);

        recyclerArticlePreviewModel.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerArticlePreviewModel.addItemDecoration(new DividerItemDecoration(mContext, Color.parseColor("#EFEFEF"), 1, SizeUtils.dp2px(16), SizeUtils.dp2px(16), false));
        recyclerArticlePreviewModel.setAdapter(modelBuyAdapter);

        recyclerArticlePreviewComments.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerArticlePreviewComments.setAdapter(parentCommentAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivArticlePreviewBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mDetailsTitle = bundle.getString("detailsTitle", "");
        mDetailsContent = bundle.getString("detailsContent", "");
        mDetailsTagList = (List<String>) bundle.get("detailsTagList");
        mDetailsModelList = (List<String>) bundle.get("detailsModelList");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        tvArticlePreviewTitle.setText(mDetailsTitle);

        setContent(mDetailsContent);
        StringBuilder tags = new StringBuilder();
        if (mDetailsTagList != null && mDetailsTagList.size() > 0) {
            tvArticlePreviewSubject.setVisibility(View.VISIBLE);
            for (int i = 0; i < mDetailsTagList.size(); i++) {
                //证明是最后一个
                if (i == mDetailsTagList.size() - 1) {
                    tags.append("#").append(mDetailsTagList.get(i));
                } else {
                    tags.append("#").append(mDetailsTagList.get(i)).append(",");
                }
            }
        } else {
            tvArticlePreviewSubject.setVisibility(View.GONE);
        }
        tvArticlePreviewSubject.setText(tags);
        if (mDetailsModelList != null && mDetailsModelList.size() > 0) {
            initModelListLog(mDetailsModelList);
        }
        parentCommentAdapter.setEmptyView(R.layout.empty_comment_layout);
    }

    //批量获取模型数据
    private void initModelListLog(List<String> modelIdList) {
        if (modelIdList.size() <= 0) {
            ToastUtils.showShort("模型id列表为空");
            return;
        }
        EasyHttp.post(this)
                .api(new AppModelListApi().setModelIdList(modelIdList).setScene(1))
                .request(new HttpCallback<HttpData<AppModelListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AppModelListApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            modelBuyAdapter.setList(result.getData().getList());
                        }
                    }
                });
    }

    private void setContent(String content) {
        //正则表达式匹配文本内容
        String stringRegex = "[^\\[\\]]+";
        Pattern stringPattern = Pattern.compile(stringRegex);
        Matcher stringMatcher = stringPattern.matcher(content);

        // 存储文本内容的列表
        List<String> stringContents = new ArrayList<>();

        // 匹配文本内容并存储到列表中
        while (stringMatcher.find()) {
            stringContents.add(stringMatcher.group().trim());
        }
        articleDetailDetailsAdapter.setList(stringContents);
    }
}
