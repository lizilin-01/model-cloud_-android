package com.moyun.modelclass.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.ShippingAddressListAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.AddressListApi;
import com.moyun.modelclass.http.bean.AddressResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * tip：选择收货地址列表
 */
public class ShippingAddressListActivity extends BaseActivity implements ShippingAddressListAdapter.OnItemSelectedChangedListener {
    private RelativeLayout rlShippingAddressTop;
    private ImageView ivShippingAddressBack;
    private ImageView ivShippingAddressAdd;
    private RecyclerView recyclerShippingAddress;
    private TextView tvShippingAddressSave;
    private ShippingAddressListAdapter shippingAddressListAdapter = new ShippingAddressListAdapter();
    private AddressResponse mBeanInfo = null;

    public static void start(Activity context) {
        Intent starter = new Intent(context, ShippingAddressListActivity.class);
        context.startActivity(starter);
    }

    public static void start(Activity context, int code) {
        Intent starter = new Intent(context, ShippingAddressListActivity.class);
        context.startActivityForResult(starter, code);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_shipping_address_list;
    }

    @Override
    protected void assignViews() {
        rlShippingAddressTop = (RelativeLayout) findViewById(R.id.rl_shipping_address_top);
        ivShippingAddressBack = (ImageView) findViewById(R.id.iv_shipping_address_back);
        ivShippingAddressAdd = (ImageView) findViewById(R.id.iv_shipping_address_add);
        recyclerShippingAddress = (RecyclerView) findViewById(R.id.recycler_shipping_address);
        tvShippingAddressSave = (TextView) findViewById(R.id.tv_shipping_address_save);

        StatusBarUtil.setPaddingSmart(this, rlShippingAddressTop);

        recyclerShippingAddress.setLayoutManager(new LinearLayoutManager(mContext));
        shippingAddressListAdapter.setOnItemSelectedChangedListener(this);
        recyclerShippingAddress.setAdapter(shippingAddressListAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivShippingAddressBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(ivShippingAddressAdd, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditAddressActivity.start(ShippingAddressListActivity.this, "", false);
            }
        });

        ClickUtils.applySingleDebouncing(tvShippingAddressSave, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBeanInfo != null) {
                    Intent intent = new Intent();
                    intent.putExtra(SubmitOrderActivity.KEY_ADDRESS_ID, mBeanInfo);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        addressList();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        addressList();
    }

    //获取地址列表
    private void addressList() {
        EasyHttp.post(this)
                .api(new AddressListApi())
                .request(new HttpCallback<HttpData<AddressListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AddressListApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mBeanInfo = result.getData().getList().get(0);
                            shippingAddressListAdapter.setList(result.getData().getList());
                        } else {
                            shippingAddressListAdapter.setList(new ArrayList<>());
                            shippingAddressListAdapter.setEmptyView(R.layout.empty_view_layout);
                        }
                    }
                });
    }

    @Override
    public void onItemSelected(AddressResponse beanInfo) {
        mBeanInfo = beanInfo;
    }

    @Override
    public void onItemEdit(String addressId, boolean isDefaultFlag) {
        EditAddressActivity.start(ShippingAddressListActivity.this, addressId, isDefaultFlag);
    }
}
