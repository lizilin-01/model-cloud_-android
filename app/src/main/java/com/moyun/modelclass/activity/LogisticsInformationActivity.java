package com.moyun.modelclass.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ClipboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.LogisticsInformationAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.AddressListApi;
import com.moyun.modelclass.http.api.OrderDeliveryInfoApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;

import java.util.ArrayList;

/**
 * Created by 星鲨
 * on 2021-01-06
 * Notes：查看物流信息
 */
public class LogisticsInformationActivity extends BaseActivity {
    private RelativeLayout llLogisticsInformationTop;
    private ImageView ivLogisticsInformationBack;
    private ImageView ivLogisticsInformationLogo;
    private TextView tvLogisticsInformationExpName;
    private TextView tvLogisticsInformationDeliveryNumber;
    private RecyclerView recyclerLogisticsInformation;

    private LogisticsInformationAdapter logisticsInformationAdapter = new LogisticsInformationAdapter();

    private String mOrderId = "";

    public static void start(Context mContext, String orderId) {
        Intent starter = new Intent(mContext, LogisticsInformationActivity.class);
        starter.putExtra("orderId", orderId);
        mContext.startActivity(starter);
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_logistics_information;
    }

    @Override
    protected void assignViews() {
        llLogisticsInformationTop = (RelativeLayout) findViewById(R.id.ll_logistics_information_top);
        ivLogisticsInformationBack = (ImageView) findViewById(R.id.iv_logistics_information_back);
        ivLogisticsInformationLogo = (ImageView) findViewById(R.id.iv_logistics_information_logo);
        tvLogisticsInformationExpName = (TextView) findViewById(R.id.tv_logistics_information_exp_name);
        tvLogisticsInformationDeliveryNumber = (TextView) findViewById(R.id.tv_logistics_information_delivery_number);
        recyclerLogisticsInformation = (RecyclerView) findViewById(R.id.recycler_logistics_information);

        StatusBarUtil.setPaddingSmart(this, llLogisticsInformationTop);

        recyclerLogisticsInformation.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerLogisticsInformation.setAdapter(logisticsInformationAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivLogisticsInformationBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mOrderId = bundle.getString("orderId", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        OrderDelivery();
    }

    //获取地址列表
    @SuppressLint("SetTextI18n")
    private void OrderDelivery() {
        if (mOrderId == null || mOrderId.isEmpty()) {
            return;
        }
        EasyHttp.post(this)
                .api(new OrderDeliveryInfoApi().setOrderId(mOrderId))
                .request(new HttpCallback<HttpData<OrderDeliveryInfoApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<OrderDeliveryInfoApi.Bean> result) {
                        Glide.with(LogisticsInformationActivity.this)
                                .load(result.getData().getLogo())
                                .centerInside()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(ivLogisticsInformationLogo);

                        tvLogisticsInformationExpName.setText(result.getData().getExpName());
                        tvLogisticsInformationDeliveryNumber.setText("快递单号：" + result.getData().getDeliveryNo());
                        if (result.getData() != null && result.getData().getItemList() != null && result.getData().getItemList().size() > 0) {
                            logisticsInformationAdapter.setList(result.getData().getItemList());
                        }

                        ClickUtils.applySingleDebouncing(tvLogisticsInformationDeliveryNumber, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //复制订单编号
                                if (result.getData() != null ) {
                                    ClipboardUtils.copyText("ddh", result.getData().getDeliveryNo());
                                    ToastUtils.showShort("复制成功");
                                }
                            }
                        });
                    }
                });
    }
}
