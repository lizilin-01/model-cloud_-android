package com.moyun.modelclass.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.base.BaseTwoActivity;
import com.moyun.modelclass.dialog.CommonBottomDialog;
import com.moyun.modelclass.dialog.ShareDialog;
import com.moyun.modelclass.fragment.model.ModelCommentFragment;
import com.moyun.modelclass.fragment.model.ModelDetailFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.AppModelDetailsApi;
import com.moyun.modelclass.http.api.ModelRemoveApi;
import com.moyun.modelclass.http.bean.CommonDialogBean;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.listener.OnContentListener;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.utils.WechatShareUtils;
import com.moyun.modelclass.view.NoScrollViewPager;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 作者：Meteor
 * 日期：2022/1/15 17:25
 * tip：模型详情
 */
public class ModelDetailsActivity extends BaseTwoActivity {
    private AppBarLayout modelDetailsAppbar;
    private TabLayout modelDetailsTabs;
    private ImageView modelDetailsBack;
    private ImageView modelDetailsShare;
    private NoScrollViewPager modelDetailsViewpager;

    private MinePagerAdapter minePagerAdapter;
    private String mModelId = "";//模型id
    private boolean mIsEdit = false;//是否可编辑

    public static void start(Context context, String articleId,boolean isEdit) {
        Intent starter = new Intent(context, ModelDetailsActivity.class);
        starter.putExtra("modelId", articleId);
        starter.putExtra("isEdit", isEdit);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_model_details;
    }

    @Override
    protected void assignViews() {
        modelDetailsAppbar = (AppBarLayout) findViewById(R.id.model_details_appbar);
        modelDetailsTabs = (TabLayout) findViewById(R.id.model_details_tabs);
        modelDetailsBack = (ImageView) findViewById(R.id.model_details_back);
        modelDetailsShare = (ImageView) findViewById(R.id.model_details_share);
        modelDetailsViewpager = (NoScrollViewPager) findViewById(R.id.model_details_viewpager);

        StatusBarUtil.setPaddingSmart(this, modelDetailsAppbar);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(modelDetailsBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(modelDetailsShare, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<CommonDialogBean> dataList = new ArrayList<>();
                dataList.add(new CommonDialogBean("1", "分享"));
                if (mIsEdit){
                    dataList.add(new CommonDialogBean("2", "编辑"));
                    dataList.add(new CommonDialogBean("3", "删除"));
                }
                dataList.add(new CommonDialogBean("-1", "取消"));
                CommonBottomDialog.getInstance(ModelDetailsActivity.this, "", dataList, new OnContentListener() {
                    @Override
                    public void content(int id, CommonDialogBean content) {
                        if (content.getMessage().equals("分享")) {
                            iniModelDetailsLog();
                        } else if (content.getMessage().equals("编辑")) {
                            if (mModelId != null && !mModelId.isEmpty()){
                                EditModelActivity.start(ModelDetailsActivity.this, mModelId);
                                finish();
                            }
                        }else if (content.getMessage().equals("删除")) {
                            if (mModelId != null && !mModelId.isEmpty()){
                                EasyHttp.post(ModelDetailsActivity.this)
                                        .api(new ModelRemoveApi().setModelId(mModelId))
                                        .request(new HttpCallback<HttpData<String>>(ModelDetailsActivity.this) {
                                            @Override
                                            public void onSucceed(HttpData<String> result) {
                                                ToastUtils.showShort("删除成功");
                                                finish();
                                            }
                                        });
                            }
                        }
                    }
                }).builder().show();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mModelId = bundle.getString("modelId", "");
        mIsEdit = bundle.getBoolean("isEdit", false);
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        minePagerAdapter = new MinePagerAdapter(getSupportFragmentManager(), mModelId);
        modelDetailsViewpager.setOffscreenPageLimit(2);
        modelDetailsViewpager.setAdapter(minePagerAdapter);
        modelDetailsTabs.setupWithViewPager(modelDetailsViewpager);

        modelDetailsTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        KeyboardUtils.hideSoftInput(ModelDetailsActivity.this);
                    }
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public static class MinePagerAdapter extends FragmentPagerAdapter {
        private Fragment[] fragments = null;
        private String[] titles = new String[]{"模型", "评论"};
        private String mModelId = "";

        public MinePagerAdapter(FragmentManager fm, String modelId) {
            super(fm);
            this.mModelId = modelId;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getCount() {
            fragments = new Fragment[]{ModelDetailFragment.newInstance(mModelId), ModelCommentFragment.newInstance(mModelId)};
            return fragments.length;
        }
    }

    //模型详情
    private void iniModelDetailsLog() {
        if (mModelId == null || mModelId.isEmpty()) {
            ToastUtils.showShort("模型id为空");
            return;
        }
        EasyHttp.post(ModelDetailsActivity.this)
                .api(new AppModelDetailsApi().setArticleId(mModelId))
                .request(new HttpCallback<HttpData<AppModelDetailsApi.Bean>>(this) {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSucceed(HttpData<AppModelDetailsApi.Bean> result) {
                        if (result != null && result.getData() != null) {
                            //分享
                            ShareDialog.getInstance(ModelDetailsActivity.this,
                                    CommonConfiguration.splitResUrl(5, result.getData().getCoverImage()),
                                    result.getData().getTitle(), new ShareDialog.OnShareListener() {
                                        @Override
                                        public void wx(Bitmap resource) {
                                            WechatShareUtils.wxShare(
                                                    ModelDetailsActivity.this,
                                                    result.getData().getTitle(),
                                                    result.getData().getDesc(),
                                                    resource);
                                        }

                                        @Override
                                        public void pyq(Bitmap resource) {
                                            WechatShareUtils.pyqShare(
                                                    ModelDetailsActivity.this,
                                                    result.getData().getTitle(),
                                                    result.getData().getDesc(),
                                                    resource);
                                        }
                                    }).builder().setNegativeButton().show();
                        }
                    }
                });
    }

    public void setTabSelect(int position) {
        Objects.requireNonNull(modelDetailsTabs.getTabAt(position)).select();
    }
}
