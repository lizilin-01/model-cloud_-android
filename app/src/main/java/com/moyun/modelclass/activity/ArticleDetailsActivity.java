package com.moyun.modelclass.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.base.BaseTwoActivity;
import com.moyun.modelclass.dialog.ShareDialog;
import com.moyun.modelclass.fragment.article.CommentFragment;
import com.moyun.modelclass.fragment.article.GoodsDetailFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.AppArticleDetailsApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.utils.WechatShareUtils;
import com.moyun.modelclass.view.NoScrollViewPager;

import java.util.Objects;

/**
 * 作者：Meteor
 * 日期：2022/1/15 17:25
 * tip：文章详情
 */
public class ArticleDetailsActivity extends BaseTwoActivity {
    private AppBarLayout articleDetailsAppbar;
    private TabLayout articleDetailsTabs;
    private ImageView articleDetailsBack;
    private ImageView articleDetailsShare;
    private NoScrollViewPager articleDetailsViewpager;

    private MinePagerAdapter minePagerAdapter;
    private String mArticleId = "";//文章id
    private boolean mIsShowState = false;//是否显示状态

    public static void start(Context context, String articleId, boolean isShowState) {
        Intent starter = new Intent(context, ArticleDetailsActivity.class);
        starter.putExtra("articleId", articleId);
        starter.putExtra("isShowState", isShowState);
        context.startActivity(starter);
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_article_details;
    }

    @Override
    protected void assignViews() {
        articleDetailsAppbar = (AppBarLayout) findViewById(R.id.article_details_appbar);
        articleDetailsTabs = (TabLayout) findViewById(R.id.article_details_tabs);
        articleDetailsBack = (ImageView) findViewById(R.id.article_details_back);
        articleDetailsShare = (ImageView) findViewById(R.id.article_details_share);
        articleDetailsViewpager = (NoScrollViewPager) findViewById(R.id.article_details_viewpager);

        StatusBarUtil.setPaddingSmart(this, articleDetailsAppbar);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(articleDetailsBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(articleDetailsShare, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initArticleDetailsLog();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mArticleId = bundle.getString("articleId", "");
        mIsShowState = bundle.getBoolean("isShowState", false);
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        minePagerAdapter = new MinePagerAdapter(getSupportFragmentManager(), mArticleId, mIsShowState);
        articleDetailsViewpager.setOffscreenPageLimit(2);
        articleDetailsViewpager.setAdapter(minePagerAdapter);
        articleDetailsTabs.setupWithViewPager(articleDetailsViewpager);
        articleDetailsTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        KeyboardUtils.hideSoftInput(ArticleDetailsActivity.this);
                    }
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    //文章详情
    private void initArticleDetailsLog() {
        if (mArticleId == null || mArticleId.isEmpty()) {
            ToastUtils.showShort("文章id为空");
            return;
        }
        EasyHttp.post(this)
                .api(new AppArticleDetailsApi().setArticleId(mArticleId))
                .request(new HttpCallback<HttpData<AppArticleDetailsApi.Bean>>(this) {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSucceed(HttpData<AppArticleDetailsApi.Bean> result) {
                        if (result != null && result.getData() != null) {
                            //分享
                            ShareDialog.getInstance(ArticleDetailsActivity.this,
                                    CommonConfiguration.splitResUrl(5, result.getData().getSimpleImage()),
                                    result.getData().getTitle(), new ShareDialog.OnShareListener() {
                                        @Override
                                        public void wx(Bitmap resource) {
                                            WechatShareUtils.wxShare(
                                                    ArticleDetailsActivity.this,
                                                    result.getData().getTitle(),
                                                    result.getData().getSummary(),
                                                    resource);
                                        }

                                        @Override
                                        public void pyq(Bitmap resource) {
                                            WechatShareUtils.pyqShare(
                                                    ArticleDetailsActivity.this,
                                                    result.getData().getTitle(),
                                                    result.getData().getSummary(),
                                                    resource);
                                        }
                                    }).builder().setNegativeButton().show();
                        }
                    }
                });
    }

    public static class MinePagerAdapter extends FragmentPagerAdapter {
        private Fragment[] fragments = null;
        private String[] titles = new String[]{"文章", "评论"};
        private String mArticleId = "";
        private boolean mIsShowState = false;//是否显示状态

        public MinePagerAdapter(FragmentManager fm, String mArticleId, boolean isShowState) {
            super(fm);
            this.mArticleId = mArticleId;
            this.mIsShowState = isShowState;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getCount() {
            fragments = new Fragment[]{GoodsDetailFragment.newInstance(mArticleId, mIsShowState), CommentFragment.newInstance(mArticleId, mIsShowState)};
            return fragments.length;
        }
    }

    public void setTabSelect(int position) {
        Objects.requireNonNull(articleDetailsTabs.getTabAt(position)).select();
    }
}
