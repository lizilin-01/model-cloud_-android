package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.RelatedUsersAllAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckFollowListUserApi;
import com.moyun.modelclass.http.api.FollowUserApi;
import com.moyun.modelclass.http.api.UserSearchApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：搜索结果：全部用户
 */
public class RelatedUsersAllActivity extends BaseActivity implements RelatedUsersAllAdapter.OnFindClick {
    private RelativeLayout llRelatedUsersAllTop;
    private ImageView ivRelatedUsersAllBack;
    private SmartRefreshLayout relatedUsersAllRefresh;
    private RecyclerView recyclerRelatedUsersAllResult;

    private RelatedUsersAllAdapter relatedUsersAllAdapter = new RelatedUsersAllAdapter();
    private String searchInfo = "";//搜索词
    private String mLastId = "";

    private List<String> userIdList = new ArrayList<>();


    public static void start(Context context, String search) {
        Intent starter = new Intent(context, RelatedUsersAllActivity.class);
        starter.putExtra("searchInfo", search);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_related_users_all;
    }

    @Override
    protected void assignViews() {
        llRelatedUsersAllTop = (RelativeLayout) findViewById(R.id.ll_related_users_all_top);
        ivRelatedUsersAllBack = (ImageView) findViewById(R.id.iv_related_users_all_back);
        relatedUsersAllRefresh = (SmartRefreshLayout) findViewById(R.id.related_users_all_refresh);
        recyclerRelatedUsersAllResult = (RecyclerView) findViewById(R.id.recycler_related_users_all_result);

        StatusBarUtil.setPaddingSmart(this, llRelatedUsersAllTop);

        recyclerRelatedUsersAllResult.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
        relatedUsersAllAdapter.setOnFindClick(this);
        recyclerRelatedUsersAllResult.setAdapter(relatedUsersAllAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivRelatedUsersAllBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        relatedUsersAllRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                userIdList.clear();
                initRelatedUsersLog(true);
            }
        });
        relatedUsersAllRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                initRelatedUsersLog(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        searchInfo = bundle.getString("searchInfo", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        initRelatedUsersLog(true);
    }

    private void initRelatedUsersLog(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new UserSearchApi().setLastId("").setPageSize(3).setSearchText(searchInfo))
                .request(new HttpCallback<HttpData<UserSearchApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<UserSearchApi.Bean> result) {
                        relatedUsersAllRefresh.finishRefresh();
                        relatedUsersAllRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            if (isRefresh) {
                                relatedUsersAllAdapter.setList(result.getData().getList());
                            } else {
                                relatedUsersAllAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                relatedUsersAllAdapter.setList(new ArrayList<>());
                                relatedUsersAllAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }

                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            for (UserSearchApi.BeanInfo item : result.getData().getList()) {
                                userIdList.add(item.getUserId());
                            }
                        }
                        checkUserListFollow();
                    }
                });
    }

    private void checkUserListFollow() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckFollowListUserApi().setUserIdList(userIdList))
                    .request(new HttpCallback<HttpData<CheckFollowListUserApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckFollowListUserApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                                relatedUsersAllAdapter.setFollowState(result.getData().getList());
                            }
                        }
                    });
        }
    }

    @Override
    public void userInfo(String userId) {
        UserInfoActivity.start(RelatedUsersAllActivity.this, userId);
    }

    @Override
    public void follow(String userId, boolean isFollow) {
        EasyHttp.post(this)
                .api(new FollowUserApi().setUserId(userId).setFollow(isFollow))
                .request(new HttpCallback<HttpData<FollowUserApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<FollowUserApi.Bean> result) {
                        checkUserListFollow();
                        if (isFollow) {
                            ToastUtils.showShort("已关注");
                        } else {
                            ToastUtils.showShort("取消关注");
                        }
                    }
                });
    }
}
