package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.ModelPagerAdapter;
import com.moyun.modelclass.adapter.ModelTypeAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.fragment.model.ModelSearchListFragment;
import com.moyun.modelclass.http.api.ModelTypeApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.listener.RecyclerItemClickListener;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：模型搜索
 */
public class ModelSearchActivity extends BaseActivity{
    private RelativeLayout llModelSearchTop;
    private ImageView ivModelSearchBack;
    private EditText etModelSearchContent;
    private TextView tvModelSearchButton;
    private RecyclerView mainModelSearchRecycler;
    private NoScrollViewPager mainModelSearchVp;

    private ModelPagerAdapter modelPagerAdapter = null;

    private ModelTypeAdapter modelTypeAdapter = new ModelTypeAdapter();
    private List<Fragment> mFragmentArrays = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, ModelSearchActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_model_search;
    }

    @Override
    protected void assignViews() {
        llModelSearchTop = (RelativeLayout) findViewById(R.id.ll_model_search_top);
        ivModelSearchBack = (ImageView) findViewById(R.id.iv_model_search_back);
        etModelSearchContent = (EditText) findViewById(R.id.et_model_search_content);
        tvModelSearchButton = (TextView) findViewById(R.id.tv_model_search_button);
        mainModelSearchRecycler = (RecyclerView) findViewById(R.id.main_model_search_recycler);
        mainModelSearchVp = (NoScrollViewPager) findViewById(R.id.main_model_search_vp);

        StatusBarUtil.setPaddingSmart(this, llModelSearchTop);

        mainModelSearchRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mainModelSearchRecycler.setAdapter(modelTypeAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivModelSearchBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        ClickUtils.applySingleDebouncing(tvModelSearchButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchResult = etModelSearchContent.getText().toString().trim();
                KeyboardUtils.hideSoftInput(etModelSearchContent);
                modelTypeData(searchResult);
            }
        });

        mainModelSearchRecycler.addOnItemTouchListener(new RecyclerItemClickListener(this, mainModelSearchRecycler, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                modelTypeAdapter.setSelected(position);
                mainModelSearchVp.setCurrentItem(position, true);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        mainModelSearchVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                modelTypeAdapter.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {

    }

    private void modelTypeData(String searchWord) {
        EasyHttp.post(this)
                .api(new ModelTypeApi())
                .request(new HttpCallback<HttpData<ModelTypeApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<ModelTypeApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            modelTypeAdapter.setList(result.getData().getList());
                            mFragmentArrays.clear();
                            for (ModelTypeApi.BeanInfo data : result.getData().getList()) {
                                mFragmentArrays.add(ModelSearchListFragment.newInstance(data.getCategoryId(),searchWord));
                            }

                            modelPagerAdapter = new ModelPagerAdapter(getSupportFragmentManager(), mFragmentArrays);
                            mainModelSearchVp.setAdapter(modelPagerAdapter);
                        }
                    }
                });
    }
}
