package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;

/**
 * 作者：Meteor
 * tip：文章待审核
 */
public class ArticleAuditActivity extends BaseActivity {
    private RelativeLayout llArticleAuditTop;
    private ImageView ivArticleAuditBack;
    private TextView tvArticleAuditHome;
    private TextView tvArticleAuditMyArticle;
    public static void start(Context context) {
        Intent starter = new Intent(context, ArticleAuditActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_article_audit;
    }

    @Override
    protected void assignViews() {
        llArticleAuditTop = (RelativeLayout) findViewById(R.id.ll_article_audit_top);
        ivArticleAuditBack = (ImageView) findViewById(R.id.iv_article_audit_back);
        tvArticleAuditHome = (TextView) findViewById(R.id.tv_article_audit_home);
        tvArticleAuditMyArticle = (TextView) findViewById(R.id.tv_article_audit_my_article);

        StatusBarUtil.setPaddingSmart(this, llArticleAuditTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivArticleAuditBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvArticleAuditHome, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SplashActivity.start(ArticleAuditActivity.this);
                ActivityUtils.finishAllActivities();
            }
        });

        ClickUtils.applySingleDebouncing(tvArticleAuditMyArticle, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    MyArticleActivity.start(ArticleAuditActivity.this);
                } else {
//                    ToastUtils.showShort(getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(ArticleAuditActivity.this);
                }
                finish();
            }
        });

    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {

    }
}
