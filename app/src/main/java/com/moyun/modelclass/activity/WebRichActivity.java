package com.moyun.modelclass.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.DocumentGetApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.utils.WebRichEditor;

/**
 * 作者：Meteor
 * 日期：2022/2/287:58 下午
 * tip：H5页面:公共
 */
public class WebRichActivity extends BaseActivity {
    private RelativeLayout rlWebTop;
    private ImageView ivWebBack;
    private TextView tvWebTitle;
    private WebRichEditor reWebContent;

    private static final String KEY_URL = "key_url";
    private String mKey = "";


    public static void start(Context context, String url) {
        Intent starter = new Intent(context, WebRichActivity.class);
        starter.putExtra(KEY_URL, url);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_web_rich;
    }

    @Override
    protected void assignViews() {
        rlWebTop = (RelativeLayout) findViewById(R.id.rl_web_top);
        ivWebBack = (ImageView) findViewById(R.id.iv_web_back);
        tvWebTitle = (TextView) findViewById(R.id.tv_web_title);
        reWebContent = (WebRichEditor) findViewById(R.id.re_web_content);

        StatusBarUtil.setPaddingSmart(this, rlWebTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivWebBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mKey = bundle.getString(KEY_URL, "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        documentGet();
    }

    //获取地址列表
    private void documentGet() {
        EasyHttp.post(this)
                .api(new DocumentGetApi().setKey(mKey))
                .request(new HttpCallback<HttpData<DocumentGetApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<DocumentGetApi.Bean> result) {
                        if (result != null && result.getData() != null) {
                            tvWebTitle.setText(result.getData().getTitle());
                            reWebContent.setHtml(result.getData().getContent());
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
