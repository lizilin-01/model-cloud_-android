package com.moyun.modelclass.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.InviteInfoApi;
import com.moyun.modelclass.http.api.InviteUseCodeApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.MyEditText;

/**
 * 作者：Meteor
 * tip：编辑个人信息
 */
public class EditPersonalInformationActivity extends BaseActivity {
    private RelativeLayout rlEditPersonalInformationTop;
    private ImageView ivEditPersonalInformationBack;
    private TextView ivEditPersonalInformationTitle;
    private MyEditText etEditPersonalInformationInput;
    private TextView tvEditPersonalInformationLogin;
    private int mEditType = 1;//1:昵称；2：职业；3：专业领域/爱好；4：个人介绍
    private String mCompleted = "";

    public static void start(Activity context, int editType, String completed, int code) {
        Intent starter = new Intent(context, EditPersonalInformationActivity.class);
        starter.putExtra("editType", editType);
        starter.putExtra("completed", completed);
        context.startActivityForResult(starter, code);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_personal_information;
    }

    @Override
    protected void assignViews() {
        rlEditPersonalInformationTop = (RelativeLayout) findViewById(R.id.rl_edit_personal_information_top);
        ivEditPersonalInformationBack = (ImageView) findViewById(R.id.iv_edit_personal_information_back);
        ivEditPersonalInformationTitle = (TextView) findViewById(R.id.iv_edit_personal_information_title);
        etEditPersonalInformationInput = (MyEditText) findViewById(R.id.et_edit_personal_information_input);
        tvEditPersonalInformationLogin = (TextView) findViewById(R.id.tv_edit_personal_information_login);

        StatusBarUtil.setPaddingSmart(this, rlEditPersonalInformationTop);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivEditPersonalInformationBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvEditPersonalInformationLogin, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = etEditPersonalInformationInput.getText().toString().trim();
                if (code.isEmpty()) {
                    if (mEditType == 1) {
                        ToastUtils.showShort("请输入昵称");
                    } else if (mEditType == 2) {
                        ToastUtils.showShort("请输入职业");
                    } else if (mEditType == 3) {
                        ToastUtils.showShort("请输入专业领域/爱好");
                    } else if (mEditType == 4) {
                        ToastUtils.showShort("请输入个人介绍");
                    }
                } else {
                    KeyboardUtils.hideSoftInput(EditPersonalInformationActivity.this);
                    Intent intent = new Intent();
                    intent.putExtra(PersonalInformationActivity.KEY_PERSONAL_INFORMATION_ID, code);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mEditType = bundle.getInt("editType", 1);
        mCompleted = bundle.getString("completed", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        if (mEditType == 1) {
            ivEditPersonalInformationTitle.setText("编辑昵称");
            etEditPersonalInformationInput.setHint("请输入昵称");
            etEditPersonalInformationInput.setText(mCompleted);
        } else if (mEditType == 2) {
            ivEditPersonalInformationTitle.setText("编辑职业");
            etEditPersonalInformationInput.setHint("请输入职业");
            etEditPersonalInformationInput.setText(mCompleted);
        } else if (mEditType == 3) {
            ivEditPersonalInformationTitle.setText("编辑专业领域/爱好");
            etEditPersonalInformationInput.setHint("请输入专业领域/爱好");
            etEditPersonalInformationInput.setText(mCompleted);
        } else if (mEditType == 4) {
            ivEditPersonalInformationTitle.setText("编辑个人介绍");
            etEditPersonalInformationInput.setHint("请输入个人介绍");
            etEditPersonalInformationInput.setText(mCompleted);
        }
    }
}
