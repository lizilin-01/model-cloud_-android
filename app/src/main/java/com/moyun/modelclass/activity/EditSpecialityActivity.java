package com.moyun.modelclass.activity;

import static com.moyun.modelclass.activity.PersonalInformationActivity.KEY_PERSONAL_INFORMATION_ID;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.adapter.PopularSelfAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：编辑专业领域/爱好
 */
public class EditSpecialityActivity extends BaseActivity implements PopularSelfAdapter.OnDeleteClick {
    private RelativeLayout llEditSpecialityTop;
    private ImageView ivEditSpecialityBack;
    private EditText etEditSpecialityContent;
    private ImageView ivEditSpecialityClean;
    private TextView tvEditSpecialityButton;
    private TextView tvEditSpecialitySelf;
    private RecyclerView recyclerEditSpecialitySelf;

    private PopularSelfAdapter specialitySelfAdapter = new PopularSelfAdapter();

    private List<String> mSpecialityList = new ArrayList<>();//已编辑专业领域/爱好

    public static void start(Activity context, List<String> specialityList, int code) {
        Intent starter = new Intent(context, EditSpecialityActivity.class);
        starter.putExtra("specialityList", (Serializable) specialityList);
        context.startActivityForResult(starter, code);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_speciality;
    }

    @Override
    protected void assignViews() {
        llEditSpecialityTop = (RelativeLayout) findViewById(R.id.ll_edit_speciality_top);
        ivEditSpecialityBack = (ImageView) findViewById(R.id.iv_edit_speciality_back);
        etEditSpecialityContent = (EditText) findViewById(R.id.et_edit_speciality_content);
        ivEditSpecialityClean = (ImageView) findViewById(R.id.iv_edit_speciality_clean);
        tvEditSpecialityButton = (TextView) findViewById(R.id.tv_edit_speciality_button);
        tvEditSpecialitySelf = (TextView) findViewById(R.id.tv_edit_speciality_self);
        recyclerEditSpecialitySelf = (RecyclerView) findViewById(R.id.recycler_edit_speciality_self);

        StatusBarUtil.setPaddingSmart(this, llEditSpecialityTop);
        recyclerEditSpecialitySelf.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerEditSpecialitySelf.addItemDecoration(new DividerItemDecoration(mContext, Color.parseColor("#CDD9DB"), 1, SizeUtils.dp2px(6), SizeUtils.dp2px(6), true));
        specialitySelfAdapter.setOnDeleteClick(this);
        recyclerEditSpecialitySelf.setAdapter(specialitySelfAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivEditSpecialityBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftInput(etEditSpecialityContent);
                Intent intent = new Intent();
                intent.putStringArrayListExtra(KEY_PERSONAL_INFORMATION_ID, (ArrayList<String>) mSpecialityList);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        etEditSpecialityContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString().trim();
                if (text.isEmpty()) {
                    ivEditSpecialityClean.setVisibility(View.GONE);
                } else {
                    ivEditSpecialityClean.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ClickUtils.applySingleDebouncing(ivEditSpecialityClean, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEditSpecialityContent.setText("");
            }
        });

        ClickUtils.applySingleDebouncing(tvEditSpecialityButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchResult = etEditSpecialityContent.getText().toString().trim();
                if (!searchResult.isEmpty()) {
                    KeyboardUtils.hideSoftInput(etEditSpecialityContent);
                    mSpecialityList.add(searchResult);
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra(KEY_PERSONAL_INFORMATION_ID, (ArrayList<String>) mSpecialityList);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    KeyboardUtils.hideSoftInput(etEditSpecialityContent);
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra(KEY_PERSONAL_INFORMATION_ID, (ArrayList<String>) mSpecialityList);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        if (bundle.get("specialityList") != null) {
            mSpecialityList = (List<String>) bundle.get("specialityList");
        }
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        if (mSpecialityList != null && mSpecialityList.size() > 0) {
            specialitySelfAdapter.setList(mSpecialityList);
            tvEditSpecialitySelf.setVisibility(View.VISIBLE);
            recyclerEditSpecialitySelf.setVisibility(View.VISIBLE);
        } else {
            tvEditSpecialitySelf.setVisibility(View.GONE);
            recyclerEditSpecialitySelf.setVisibility(View.GONE);
        }
    }

    @Override
    public void delete(int position) {
        mSpecialityList.remove(position);
        if (mSpecialityList != null && mSpecialityList.size() > 0) {
            specialitySelfAdapter.setList(mSpecialityList);
            tvEditSpecialitySelf.setVisibility(View.VISIBLE);
            recyclerEditSpecialitySelf.setVisibility(View.VISIBLE);
        } else {
            tvEditSpecialitySelf.setVisibility(View.GONE);
            recyclerEditSpecialitySelf.setVisibility(View.GONE);
        }
    }
}
