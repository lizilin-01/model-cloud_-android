package com.moyun.modelclass.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.blankj.utilcode.util.ClickUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.AddressListAdapter;
import com.moyun.modelclass.adapter.ShippingAddressListAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.http.api.AddressListApi;
import com.moyun.modelclass.http.bean.AddressResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * tip：收货地址列表
 */
public class AddressListActivity extends BaseActivity implements AddressListAdapter.OnItemSelectedChangedListener {
    private RelativeLayout rlAddressTop;
    private ImageView ivAddressBack;
    private ImageView ivAddressAdd;
    private RecyclerView recyclerAddress;


    private AddressListAdapter addressListAdapter = new AddressListAdapter();

    public static void start(Activity context) {
        Intent starter = new Intent(context, AddressListActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_address_list;
    }

    @Override
    protected void assignViews() {
        rlAddressTop = (RelativeLayout) findViewById(R.id.rl_address_top);
        ivAddressBack = (ImageView) findViewById(R.id.iv_address_back);
        ivAddressAdd = (ImageView) findViewById(R.id.iv_address_add);
        recyclerAddress = (RecyclerView) findViewById(R.id.recycler_address);

        StatusBarUtil.setPaddingSmart(this, rlAddressTop);

        recyclerAddress.setLayoutManager(new LinearLayoutManager(mContext));
        addressListAdapter.setOnItemSelectedChangedListener(this);
        recyclerAddress.setAdapter(addressListAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivAddressBack, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(ivAddressAdd, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditAddressActivity.start(AddressListActivity.this, "", false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        addressList();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        addressList();
    }

    //获取地址列表
    private void addressList() {
        EasyHttp.post(this)
                .api(new AddressListApi())
                .request(new HttpCallback<HttpData<AddressListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AddressListApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            addressListAdapter.setList(result.getData().getList());
                        } else {
                            addressListAdapter.setList(new ArrayList<>());
                            addressListAdapter.setEmptyView(R.layout.empty_view_layout);
                        }
                    }
                });
    }

    @Override
    public void onItemEdit(String addressId, boolean isDefaultFlag) {
        EditAddressActivity.start(AddressListActivity.this, addressId, isDefaultFlag);
    }
}
