package com.moyun.modelclass.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alipay.sdk.app.PayTask;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ClipboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.MyOrderModelAdapter;
import com.moyun.modelclass.adapter.SubmitOrderPriceAdapter;
import com.moyun.modelclass.base.BaseActivity;
import com.moyun.modelclass.dialog.CashierDialog;
import com.moyun.modelclass.http.api.ConfigPrintParamsApi;
import com.moyun.modelclass.http.api.OrderCancelApi;
import com.moyun.modelclass.http.api.OrderDetailsApi;
import com.moyun.modelclass.http.api.OrderInfoApi;
import com.moyun.modelclass.http.api.OrderUrgeDeliveryApi;
import com.moyun.modelclass.http.api.PayParamsApi;
import com.moyun.modelclass.http.bean.OrderDetailsResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.listener.PayResult;
import com.moyun.modelclass.utils.StatusBarUtil;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.text.DecimalFormat;
import java.util.Map;

/**
 * 作者：Meteor
 * tip：订单详情
 */
public class OrderDetailsActivity extends BaseActivity {
    private RelativeLayout rlOrderDetailsTop;
    private ImageView ivOrderDetailsBack;
    private LinearLayout llOrderDetailsStateAll;
    private TextView tvOrderDetailsStateOne;
    private TextView tvOrderDetailsStateTwo;
    private TextView tvOrderDetailsStateThree;
    private TextView tvOrderDetailsStateFour;

    private RelativeLayout rlOrderDetailsDeliveryAll;
    private TextView tvOrderDetailsDeliveryNumber;
    private TextView tvOrderDetailsDeliveryDetails;

    private TextView tvOrderDetailsName;
    private TextView tvOrderDetailsPhoneNumber;
    private TextView tvOrderDetailsAddress;
    private RecyclerView recyclerOrderDetailsModelList;
    private TextView tvOrderDetailsPrintOne;
    private TextView tvOrderDetailsPrintTwo;
    private TextView tvOrderDetailsPrintThree;
    private TextView tvOrderDetailsPrintFour;
    //    private RecyclerView recyclerOrderDetailsPrice;
    private TextView tvOrderDetailsPriceOne;
    private TextView tvOrderDetailsPriceTitleTwo;
    private TextView tvOrderDetailsPriceTwo;
    private TextView tvOrderDetailsPriceThree;

    private TextView tvOrderDetailsNotes;
    private TextView tvOrderDetailsDdbh;
    private TextView tvOrderDetailsWlbh;
    private TextView tvOrderDetailsXdsj;
    private TextView tvOrderDetailsFksj;
    private TextView tvOrderDetailsCancel;
    private TextView tvOrderDetailsAmountTitle;
    private TextView tvOrderDetailsAmountMoney;
    private TextView tvOrderDetailsPay;

    private TextView tvOrderDetailsUrgeShipment;
    private TextView tvOrderDetailsServiceTip;
    private TextView tvOrderDetailsService;
    private TextView tvOrderDetailsEvaluate;
    private MyOrderModelAdapter myOrderModelAdapter = new MyOrderModelAdapter();
    private SubmitOrderPriceAdapter submitOrderPriceAdapter = new SubmitOrderPriceAdapter();//价格
    private String mOrderId = "";
    private OrderDetailsResponse orderDetailsResponse = null;
    private DecimalFormat df = new DecimalFormat("￥##0.00");

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            //对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
            @SuppressWarnings("unchecked")
            PayResult payResult = new PayResult((Map<String, String>) msg.obj);
            // 判断resultStatus 为9000则代表支付成功
            if (TextUtils.equals(payResult.getResultStatus(), "9000")) {
                // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                ToastUtils.showShort("支付成功" + payResult.getMemo());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                initOrderDetailsApi(mOrderId);
            } else {
                // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                ToastUtils.showShort("支付失败" + payResult.getMemo());
            }
        }
    };


    public static void start(Context context, String orderId) {
        Intent starter = new Intent(context, OrderDetailsActivity.class);
        starter.putExtra("orderId", orderId);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_order_details;
    }

    @Override
    protected void assignViews() {
        rlOrderDetailsTop = (RelativeLayout) findViewById(R.id.rl_order_details_top);
        ivOrderDetailsBack = (ImageView) findViewById(R.id.iv_order_details_back);

        llOrderDetailsStateAll = (LinearLayout) findViewById(R.id.ll_order_details_state_all);
        tvOrderDetailsStateOne = (TextView) findViewById(R.id.tv_order_details_state_one);
        tvOrderDetailsStateTwo = (TextView) findViewById(R.id.tv_order_details_state_two);
        tvOrderDetailsStateThree = (TextView) findViewById(R.id.tv_order_details_state_three);
        tvOrderDetailsStateFour = (TextView) findViewById(R.id.tv_order_details_state_four);

        rlOrderDetailsDeliveryAll = (RelativeLayout) findViewById(R.id.rl_order_details_delivery_all);
        tvOrderDetailsDeliveryNumber = (TextView) findViewById(R.id.tv_order_details_delivery_number);
        tvOrderDetailsDeliveryDetails = (TextView) findViewById(R.id.tv_order_details_delivery_details);

        tvOrderDetailsName = (TextView) findViewById(R.id.tv_order_details_name);
        tvOrderDetailsPhoneNumber = (TextView) findViewById(R.id.tv_order_details_phone_number);
        tvOrderDetailsAddress = (TextView) findViewById(R.id.tv_order_details_address);
        recyclerOrderDetailsModelList = (RecyclerView) findViewById(R.id.recycler_order_details_model_list);
        tvOrderDetailsPrintOne = (TextView) findViewById(R.id.tv_order_details_print_one);
        tvOrderDetailsPrintTwo = (TextView) findViewById(R.id.tv_order_details_print_two);
        tvOrderDetailsPrintThree = (TextView) findViewById(R.id.tv_order_details_print_three);
        tvOrderDetailsPrintFour = (TextView) findViewById(R.id.tv_order_details_print_four);

//        recyclerOrderDetailsPrice = (RecyclerView) findViewById(R.id.recycler_order_details_price);
        tvOrderDetailsPriceOne = (TextView) findViewById(R.id.tv_order_details_price_one);
        tvOrderDetailsPriceTitleTwo = (TextView) findViewById(R.id.tv_order_details_price_title_two);
        tvOrderDetailsPriceTwo = (TextView) findViewById(R.id.tv_order_details_price_two);
        tvOrderDetailsPriceThree = (TextView) findViewById(R.id.tv_order_details_price_three);

        tvOrderDetailsNotes = (TextView) findViewById(R.id.tv_order_details_notes);
        tvOrderDetailsDdbh = (TextView) findViewById(R.id.tv_order_details_ddbh);
        tvOrderDetailsWlbh = (TextView) findViewById(R.id.tv_order_details_wlbh);
        tvOrderDetailsXdsj = (TextView) findViewById(R.id.tv_order_details_xdsj);
        tvOrderDetailsFksj = (TextView) findViewById(R.id.tv_order_details_fksj);
        tvOrderDetailsCancel = (TextView) findViewById(R.id.tv_order_details_cancel);
        tvOrderDetailsAmountTitle = (TextView) findViewById(R.id.tv_order_details_amount_title);
        tvOrderDetailsAmountMoney = (TextView) findViewById(R.id.tv_order_details_amount_money);
        tvOrderDetailsPay = (TextView) findViewById(R.id.tv_order_details_pay);

        tvOrderDetailsUrgeShipment = (TextView) findViewById(R.id.tv_order_details_urge_shipment);
        tvOrderDetailsServiceTip = (TextView) findViewById(R.id.tv_order_details_service_tip);
        tvOrderDetailsService = (TextView) findViewById(R.id.tv_order_details_service);
        tvOrderDetailsEvaluate = (TextView) findViewById(R.id.tv_order_details_evaluate);

        StatusBarUtil.setPaddingSmart(this, rlOrderDetailsTop);

        recyclerOrderDetailsModelList.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerOrderDetailsModelList.addItemDecoration(new DividerItemDecoration(mContext, Color.parseColor("#EFEFEF"), 1, 0, 0, true));
        recyclerOrderDetailsModelList.setAdapter(myOrderModelAdapter);

//        recyclerOrderDetailsPrice.setLayoutManager(new LinearLayoutManager(mContext));
//        recyclerOrderDetailsPrice.setAdapter(submitOrderPriceAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(ivOrderDetailsBack, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ClickUtils.applySingleDebouncing(tvOrderDetailsDeliveryDetails, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //物流详情
                if (mOrderId.isEmpty()) {
                    return;
                }
                LogisticsInformationActivity.start(OrderDetailsActivity.this, mOrderId);
            }
        });
        ClickUtils.applySingleDebouncing(tvOrderDetailsDdbh, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //复制订单编号
                if (orderDetailsResponse != null && !orderDetailsResponse.getOrderNo().isEmpty()) {
                    ClipboardUtils.copyText("tvOrderDetailsDdbh", orderDetailsResponse.getOrderNo());
                    ToastUtils.showShort("复制成功");
                }

            }
        });

        ClickUtils.applySingleDebouncing(tvOrderDetailsWlbh, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //复制物流编号
                if (orderDetailsResponse != null && !orderDetailsResponse.getDeliveryNo().isEmpty()) {
                    ClipboardUtils.copyText("tvOrderDetailsDdbh", orderDetailsResponse.getDeliveryNo());
                    ToastUtils.showShort("复制成功");
                }
            }
        });


        ClickUtils.applySingleDebouncing(tvOrderDetailsCancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //取消订单
                if (mOrderId.isEmpty()) {
                    return;
                }
                EasyHttp.post(OrderDetailsActivity.this)
                        .api(new OrderCancelApi().setOrderId(mOrderId))
                        .request(new HttpCallback<HttpData<String>>(OrderDetailsActivity.this) {
                            @Override
                            public void onSucceed(HttpData<String> result) {
                                finish();
                            }
                        });
            }
        });

        ClickUtils.applySingleDebouncing(tvOrderDetailsPay, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //立即支付
                if (orderDetailsResponse != null && !orderDetailsResponse.getPayOrderId().isEmpty()) {
                    EasyHttp.post(OrderDetailsActivity.this)
                            .api(new OrderInfoApi().setPayOrderId(orderDetailsResponse.getPayOrderId()))
                            .request(new HttpCallback<HttpData<OrderInfoApi.Bean>>(OrderDetailsActivity.this) {
                                @Override
                                public void onSucceed(HttpData<OrderInfoApi.Bean> orderInfoResult) {
                                    if (orderInfoResult != null && orderInfoResult.getData() != null) {
                                        OrderInfoApi.ChannelBean channelBean = orderInfoResult.getData().getChannelList().get(1);
                                        CashierDialog dialog = CashierDialog.getInstance(OrderDetailsActivity.this, orderDetailsResponse.getTotalPrice(), channelBean, new CashierDialog.OnCashierListener() {
                                            @Override
                                            public void pay() {
                                                EasyHttp.post(OrderDetailsActivity.this)
                                                        .api(new PayParamsApi()
                                                                .setPayOrderId(orderDetailsResponse.getPayOrderId())
                                                                .setChannelId(channelBean.getChannelId()))
                                                        .request(new HttpCallback<HttpData<PayParamsApi.Bean>>(OrderDetailsActivity.this) {
                                                            @Override
                                                            public void onSucceed(HttpData<PayParamsApi.Bean> payParamsResult) {
                                                                if (payParamsResult != null && payParamsResult.getData() != null) {
//                                                                            IWXAPI api = WXAPIFactory.createWXAPI(mActivity, BaseConfig.WX_APPID, true);
//                                                                            boolean clientValid = api.isWXAppInstalled();
//                                                                            if (clientValid) {
////                                                                                WXPayRequest wxPayRequest = GsonUtils.fromJson(payParamsResult.getData().getWxParams(), WXPayRequest.class);
//                                                                                PayReq request = new PayReq();
//                                                                                request.appId = payParamsResult.getData().getWxParams().getAppId();
//                                                                                request.partnerId = payParamsResult.getData().getWxParams().getPartnerid();
//                                                                                request.prepayId = payParamsResult.getData().getWxParams().getPrepayid();
//                                                                                request.packageValue = payParamsResult.getData().getWxParams().getPackagee();
//                                                                                request.nonceStr = payParamsResult.getData().getWxParams().getNoncestr();
//                                                                                request.timeStamp = payParamsResult.getData().getWxParams().getTimestamp();
//                                                                                request.sign = payParamsResult.getData().getWxParams().getSign();
//                                                                                api.sendReq(request);
//                                                                            } else {
//                                                                                ToastUtils.showShort("请安装微信客户端");
//                                                                            }

                                                                    final String orderInfo = payParamsResult.getData().getAliParams().getOrderStr();   // 订单信息
                                                                    Runnable payRunnable = new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            PayTask alipay = new PayTask(OrderDetailsActivity.this);
                                                                            Map<String, String> result = alipay.payV2(orderInfo, true);
                                                                            Message msg = new Message();
                                                                            msg.obj = result;
                                                                            mHandler.sendMessage(msg);
                                                                        }
                                                                    };
                                                                    // 必须异步调用
                                                                    Thread payThread = new Thread(payRunnable);
                                                                    payThread.start();
                                                                }
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void cancel() {
                                            }
                                        }).builder();
                                        dialog.show();
                                    }
                                }
                            });
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvOrderDetailsUrgeShipment, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //催发货
                if (mOrderId.isEmpty()) {
                    return;
                }
                EasyHttp.post(OrderDetailsActivity.this)
                        .api(new OrderUrgeDeliveryApi().setOrderId(mOrderId))
                        .request(new HttpCallback<HttpData<String>>(OrderDetailsActivity.this) {
                            @Override
                            public void onSucceed(HttpData<String> result) {
                                ToastUtils.showShort("催发货成功");
                            }
                        });
            }
        });

        ClickUtils.applySingleDebouncing(tvOrderDetailsService, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //联系客服
                if (orderDetailsResponse != null && !orderDetailsResponse.getReceiveManufacturerUserId().isEmpty()) {
                    ChatDetailActivity.start(OrderDetailsActivity.this, orderDetailsResponse.getReceiveManufacturerUserId());
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvOrderDetailsEvaluate, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //评价
                if (mOrderId.isEmpty()) {
                    return;
                }
                OrderEvaluateActivity.start(OrderDetailsActivity.this, mOrderId);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mOrderId = bundle.getString("orderId", "");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        initOrderDetailsApi(mOrderId);
    }

    //订单详情
    @SuppressLint("SetTextI18n")
    private void initOrderDetailsApi(String mOrderId) {
        if (mOrderId.isEmpty()) {
            return;
        }
        EasyHttp.post(this)
                .api(new OrderDetailsApi().setOrderId(mOrderId))
                .request(new HttpCallback<HttpData<OrderDetailsResponse>>(this) {
                    @Override
                    public void onSucceed(HttpData<OrderDetailsResponse> result) {
                        if (result != null && result.getData() != null) {
                            orderDetailsResponse = result.getData();
                            orderState(result.getData().getOrderStateVO());
                            recipientAddress(result.getData().getRecipient());

                            myOrderModelAdapter.setList(result.getData().getModelList());

                            initConfigPrintParamsLog(result.getData().getPrintParams());
                            tvOrderDetailsPrintFour.setText(result.getData().getPrintParams());

                            tvOrderDetailsPriceOne.setText(df.format(result.getData().getProductPrice() * 0.01));
                            tvOrderDetailsPriceTwo.setText(df.format(result.getData().getDeliveryPrice() * 0.01));
                            tvOrderDetailsPriceThree.setText(df.format(result.getData().getMaterialPrice() * 0.01));
                            if (result.getData().getRemark() == null || result.getData().getRemark().isEmpty()) {
                                tvOrderDetailsNotes.setText("无");
                            } else {
                                tvOrderDetailsNotes.setText(result.getData().getRemark());
                            }


                            tvOrderDetailsDdbh.setText("订单编号：" + result.getData().getOrderNo());
                            if (result.getData().getOrderStateVO() == 3 || result.getData().getOrderStateVO() == 4 || result.getData().getOrderStateVO() == 5) {
                                rlOrderDetailsDeliveryAll.setVisibility(View.VISIBLE);
                                tvOrderDetailsWlbh.setVisibility(View.VISIBLE);
                                tvOrderDetailsDeliveryNumber.setText("快递单号：" + result.getData().getDeliveryNo());
                                tvOrderDetailsWlbh.setText("物流编号：" + result.getData().getDeliveryNo());
                            } else {
                                rlOrderDetailsDeliveryAll.setVisibility(View.GONE);
                                tvOrderDetailsWlbh.setVisibility(View.GONE);
                            }
                            tvOrderDetailsXdsj.setText("下单时间：" + result.getData().getCreateTime());
                            if (result.getData().getOrderStateVO() == 2 || result.getData().getOrderStateVO() == 3 || result.getData().getOrderStateVO() == 4 || result.getData().getOrderStateVO() == 5) {
                                tvOrderDetailsFksj.setVisibility(View.VISIBLE);
                                tvOrderDetailsFksj.setText("付款时间：" + result.getData().getPayTime());
                            } else {
                                tvOrderDetailsFksj.setVisibility(View.GONE);
                            }

                            tvOrderDetailsAmountMoney.setText(df.format(result.getData().getTotalPrice() * 0.01));//总价格
                        }
                    }
                });
    }

    //订单状态
    private void orderState(int orderState) {
        if (orderState == 1) {
            llOrderDetailsStateAll.setVisibility(View.VISIBLE);
            tvOrderDetailsStateOne.setTextColor(Color.parseColor("#FF6600"));
            tvOrderDetailsStateOne.setTypeface(null, Typeface.BOLD);

            tvOrderDetailsStateTwo.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateTwo.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateThree.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateThree.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateFour.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateFour.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsCancel.setVisibility(View.VISIBLE);
            tvOrderDetailsAmountTitle.setVisibility(View.VISIBLE);
            tvOrderDetailsAmountMoney.setVisibility(View.VISIBLE);
            tvOrderDetailsPay.setVisibility(View.VISIBLE);

            tvOrderDetailsUrgeShipment.setVisibility(View.GONE);
            tvOrderDetailsServiceTip.setVisibility(View.GONE);
            tvOrderDetailsService.setVisibility(View.GONE);
            tvOrderDetailsEvaluate.setVisibility(View.GONE);
        } else if (orderState == 2) {
            llOrderDetailsStateAll.setVisibility(View.VISIBLE);
            tvOrderDetailsStateOne.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateOne.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateTwo.setTextColor(Color.parseColor("#FF6600"));
            tvOrderDetailsStateTwo.setTypeface(null, Typeface.BOLD);

            tvOrderDetailsStateThree.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateThree.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateFour.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateFour.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsCancel.setVisibility(View.GONE);
            tvOrderDetailsAmountTitle.setVisibility(View.GONE);
            tvOrderDetailsAmountMoney.setVisibility(View.GONE);
            tvOrderDetailsPay.setVisibility(View.GONE);

            tvOrderDetailsUrgeShipment.setVisibility(View.VISIBLE);
            tvOrderDetailsServiceTip.setVisibility(View.GONE);
            tvOrderDetailsService.setVisibility(View.GONE);
            tvOrderDetailsEvaluate.setVisibility(View.GONE);
        } else if (orderState == 3) {
            llOrderDetailsStateAll.setVisibility(View.VISIBLE);
            tvOrderDetailsStateOne.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateOne.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateTwo.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateTwo.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateThree.setTextColor(Color.parseColor("#FF6600"));
            tvOrderDetailsStateThree.setTypeface(null, Typeface.BOLD);

            tvOrderDetailsStateFour.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateFour.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsCancel.setVisibility(View.GONE);
            tvOrderDetailsAmountTitle.setVisibility(View.GONE);
            tvOrderDetailsAmountMoney.setVisibility(View.GONE);
            tvOrderDetailsPay.setVisibility(View.GONE);

            tvOrderDetailsUrgeShipment.setVisibility(View.GONE);
            tvOrderDetailsServiceTip.setVisibility(View.GONE);
            tvOrderDetailsService.setVisibility(View.GONE);
            tvOrderDetailsEvaluate.setVisibility(View.GONE);
        } else if (orderState == 4) {
            llOrderDetailsStateAll.setVisibility(View.VISIBLE);
            tvOrderDetailsStateOne.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateOne.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateTwo.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateTwo.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateThree.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateThree.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateFour.setTextColor(Color.parseColor("#FF6600"));
            tvOrderDetailsStateFour.setTypeface(null, Typeface.BOLD);

            tvOrderDetailsCancel.setVisibility(View.GONE);
            tvOrderDetailsAmountTitle.setVisibility(View.GONE);
            tvOrderDetailsAmountMoney.setVisibility(View.GONE);
            tvOrderDetailsPay.setVisibility(View.GONE);

            tvOrderDetailsUrgeShipment.setVisibility(View.GONE);
            tvOrderDetailsServiceTip.setVisibility(View.VISIBLE);
            tvOrderDetailsService.setVisibility(View.VISIBLE);
            tvOrderDetailsEvaluate.setVisibility(View.VISIBLE);
        } else if (orderState == 5) {
            //已完成
            llOrderDetailsStateAll.setVisibility(View.GONE);
            tvOrderDetailsStateOne.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateOne.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateTwo.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateTwo.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateThree.setTextColor(Color.parseColor("#767676"));
            tvOrderDetailsStateThree.setTypeface(null, Typeface.NORMAL);

            tvOrderDetailsStateFour.setTextColor(Color.parseColor("#FF6600"));
            tvOrderDetailsStateFour.setTypeface(null, Typeface.BOLD);

            tvOrderDetailsCancel.setVisibility(View.GONE);
            tvOrderDetailsAmountTitle.setVisibility(View.GONE);
            tvOrderDetailsAmountMoney.setVisibility(View.GONE);
            tvOrderDetailsPay.setVisibility(View.GONE);

            tvOrderDetailsUrgeShipment.setVisibility(View.GONE);
            tvOrderDetailsServiceTip.setVisibility(View.VISIBLE);
            tvOrderDetailsService.setVisibility(View.VISIBLE);
            tvOrderDetailsEvaluate.setVisibility(View.GONE);
        } else if (orderState == 6) {
            //已取消
            llOrderDetailsStateAll.setVisibility(View.GONE);
            tvOrderDetailsCancel.setVisibility(View.GONE);
            tvOrderDetailsAmountTitle.setVisibility(View.GONE);
            tvOrderDetailsAmountMoney.setVisibility(View.GONE);
            tvOrderDetailsPay.setVisibility(View.GONE);

            tvOrderDetailsUrgeShipment.setVisibility(View.GONE);
            tvOrderDetailsServiceTip.setVisibility(View.GONE);
            tvOrderDetailsService.setVisibility(View.GONE);
            tvOrderDetailsEvaluate.setVisibility(View.GONE);
        }
    }

    //收货人地址
    private void recipientAddress(OrderDetailsResponse.RecipientResponse recipient) {
        tvOrderDetailsName.setText(recipient.getUserName());
        tvOrderDetailsPhoneNumber.setText(recipient.getMobile());
        tvOrderDetailsAddress.setText(recipient.getAddress());
    }

    //打印相关参数
    private void initConfigPrintParamsLog(String printParams) {
        // 检查字符串长度是否能被三等分
        if (printParams.isEmpty()) {
            return;
        }
        EasyHttp.post(this)
                .api(new ConfigPrintParamsApi())
                .request(new HttpCallback<HttpData<ConfigPrintParamsApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<ConfigPrintParamsApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            int partLength = printParams.length() / 3;
                            int positionOne = 0;//第一层的位置
                            int positionTwo = 0;//第二层的位置
                            // 使用substring方法截取字符串
                            String levelOne = printParams.substring(0, partLength);
                            String levelTwo = printParams.substring(0, partLength * 2);
                            String levelThree = printParams;

                            for (int i = 0; i < result.getData().getList().size(); i++) {
                                if (levelOne.equals(result.getData().getList().get(i).paramId)) {
                                    tvOrderDetailsPrintOne.setText(result.getData().getList().get(i).paramLabel);
                                    positionOne = i;
                                }
                            }

                            for (int j = 0; j < result.getData().getList().get(positionOne).getOptionValue().size(); j++) {
                                if (levelTwo.equals(result.getData().getList().get(positionOne).getOptionValue().get(j).paramId)) {
                                    tvOrderDetailsPrintTwo.setText(result.getData().getList().get(positionOne).getOptionValue().get(j).paramLabel);
                                    positionTwo = j;
                                }
                            }
                            for (int k = 0; k < result.getData().getList().get(positionOne).getOptionValue().get(positionTwo).getOptionValue().size(); k++) {
                                if (levelThree.equals(result.getData().getList().get(positionOne).getOptionValue().get(positionTwo).getOptionValue().get(k).paramId)) {
                                    tvOrderDetailsPrintThree.setText(result.getData().getList().get(positionOne).getOptionValue().get(positionTwo).getOptionValue().get(k).paramLabel);
                                }
                            }

//                            configPrintParamsList = result.getData().getList();
//                            tvSubmitOrderPrintOne.setText(configPrintParamsList.get(0).paramLabel);
//                            tvSubmitOrderPrintTwo.setText(configPrintParamsList.get(0).getOptionValue().get(0).getParamLabel());
//                            tvSubmitOrderPrintThree.setText(configPrintParamsList.get(0).getOptionValue().get(0).getOptionValue().get(0).getParamLabel());
//                            printParamsThree = configPrintParamsList.get(0).getOptionValue().get(0).getOptionValue().get(0);
//
//                            tvOrderDetailsPrintOne.setText();
//                            tvOrderDetailsPrintTwo.setText(result.getData().getPrintParams());
//                            tvOrderDetailsPrintThree.setText(result.getData().getPrintParams());
                        }
                    }
                });
    }
}
