package com.moyun.modelclass.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.base.BaseConfig;
import com.moyun.modelclass.event.WxPayEvent;
import com.moyun.modelclass.R;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;

/**
 * 作者：Meteor
 * 日期：2022/3/1011:06 下午
 * tip：
 */
public class WXPayEntryActivity extends AppCompatActivity implements IWXAPIEventHandler {
    private IWXAPI api;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, BaseConfig.WX_APPID, false);
        try {
            Intent intent = getIntent();
            api.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
        Log.i("Wx=====", "errStr:");
        finish();
    }

    @Override
    public void onResp(BaseResp baseResp) {
        if (baseResp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            switch (baseResp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    EventBus.getDefault().post(new WxPayEvent());
                    ToastUtils.showShort(getString(R.string.pay_code_success));
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    ToastUtils.showShort(getString(R.string.pay_code_cancel));
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    ToastUtils.showShort(getString(R.string.pay_code_deny));
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_UNSUPPORT:
                    ToastUtils.showShort(getString(R.string.pay_code_unsupported));
                    finish();
                    break;
                default:
                    ToastUtils.showShort(getString(R.string.pay_code_unknown));
                    finish();
                    break;
            }
        }
    }
}
