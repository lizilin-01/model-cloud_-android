package com.moyun.modelclass.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hjq.http.listener.OnHttpListener;
import com.tencent.mmkv.MMKV;

import org.greenrobot.eventbus.EventBus;

/**
 * 作者：Meteor
 * 日期：2022/1/19 23:21
 * tip：
 */
public abstract class BaseFragment extends Fragment implements IView,OnHttpListener<Object>  {
    protected ProgressDialog mProgressDialog;
    protected Context mContext;
    protected Activity mActivity;
    protected MMKV kv;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getLayoutResId() != 0) {
            return inflater.inflate(getLayoutResId(), container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressDialog();
        assignViews(view);
        registerListeners();
        Bundle extras = getArguments();
        if (extras != null) {
            getExtras(extras);
        }
        if (isRegisterEventBus()) {
            EventBus.getDefault().register(this);
        }
        doAction();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = (Activity) context;
        kv = MMKV.defaultMMKV();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onDestroyView() {
        if (isRegisterEventBus()) {
            EventBus.getDefault().unregister(this);
        }

        dismissProgressDialog();
        super.onDestroyView();
    }

    protected void initProgressDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle(null);
    }

    @Override
    public void showProgressDialog() {
        showProgressDialog(null);
    }

    @Override
    public void showProgressDialog(String message) {
        showProgressDialog(message, false);
    }

    @Override
    public void showProgressDialog(String message, boolean cancelAble) {
        showProgressDialog(null, message, cancelAble);
    }

    @Override
    public void showProgressDialog(String title, String message, boolean cancelAble) {
        mProgressDialog.setTitle(title);
        mProgressDialog.setCancelable(cancelAble);
        if (TextUtils.isEmpty(message)) {
            message = "加载中";
        }
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onSucceed(Object result) {

    }

    @Override
    public void onFail(Exception e) {

    }

    protected abstract int getLayoutResId();

    protected abstract void assignViews(View view);

    protected abstract void registerListeners();

    protected abstract void getExtras(Bundle bundle);

    protected abstract boolean isRegisterEventBus();

    protected abstract void doAction();
}
