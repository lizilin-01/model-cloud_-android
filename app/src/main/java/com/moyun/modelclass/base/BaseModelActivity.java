package com.moyun.modelclass.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.WindowCompat;

import com.gyf.immersionbar.ImmersionBar;
import com.hjq.http.listener.OnHttpListener;
import com.tencent.mmkv.MMKV;

import org.greenrobot.eventbus.EventBus;

import okhttp3.Call;

/**
 * 作者：Meteor
 * 日期：2022-01-12 21:53
 * tip：
 */
public abstract class BaseModelActivity extends AppCompatActivity implements IView, OnHttpListener<Object> {
    protected ProgressDialog mProgressDialog;
    protected Context mContext;
    protected Activity mActivity;
    protected MMKV kv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;
        kv = MMKV.defaultMMKV();
        if (getLayoutResId() != 0) {
            setContentView(getLayoutResId());
        }
        ImmersionBar.with(mActivity).statusBarDarkFont(false).init();
        if (!ImmersionBar.hasNavigationBar(this)){
            WindowCompat.setDecorFitsSystemWindows(this.getWindow(), false);
            this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            this.getWindow().setNavigationBarColor(Color.TRANSPARENT);
        }
        initProgressDialog();
        assignViews();
        registerListeners();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            getExtras(extras);
        }

        if (isRegisterEventBus()) {
            EventBus.getDefault().register(this);
        }

        doAction();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        hideSoftInputMethod();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        hideSoftInputMethod();
    }

    @Override
    protected void onDestroy() {
        if (isRegisterEventBus()) {
            EventBus.getDefault().unregister(this);
        }

        dismissProgressDialog();

        super.onDestroy();
    }

    protected void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle(null);
    }

    public void hideSoftInputMethod() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void showProgressDialog() {
        showProgressDialog(null);
    }

    @Override
    public void showProgressDialog(String message) {
        showProgressDialog(message, false);
    }

    @Override
    public void showProgressDialog(String message, boolean cancelAble) {
        showProgressDialog(null, message, cancelAble);
    }

    @Override
    public void showProgressDialog(String title, String message, boolean cancelAble) {
        mProgressDialog.setTitle(title);
        mProgressDialog.setCancelable(cancelAble);
        if (TextUtils.isEmpty(message)) {
            message = "加载中";
        }
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        onStateNotSaved();
    }

    @Override
    public void onStart(Call call) {
        showProgressDialog();
        Log.e("BaseActivity", "onStart");
    }

    @Override
    public void onSucceed(Object result) {
        Log.e("BaseActivity", "onSucceed");
    }

    @Override
    public void onFail(Exception e) {
        Log.e("BaseActivity", "onFail");
    }

    @Override
    public void onEnd(Call call) {
        dismissProgressDialog();
        Log.e("BaseActivity", "onEnd");
    }

    protected abstract int getLayoutResId();

    protected abstract void assignViews();

    protected abstract void registerListeners();

    protected abstract void getExtras(Bundle bundle);

    protected abstract boolean isRegisterEventBus();

    protected abstract void doAction();
}
