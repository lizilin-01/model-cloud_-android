package com.moyun.modelclass.base;

/**
 * 作者：Meteor
 * 日期：2022-01-12 22:01
 * tip：
 */
public interface IView {
    void showProgressDialog();

    void showProgressDialog(String message);

    void showProgressDialog(String message, boolean cancelAble);

    void showProgressDialog(String title, String message, boolean cancelAble);

    void dismissProgressDialog();
}
