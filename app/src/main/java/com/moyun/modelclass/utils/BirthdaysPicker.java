package com.moyun.modelclass.utils;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;

import com.github.gzuliyujiang.wheelpicker.DatePicker;
import com.github.gzuliyujiang.wheelpicker.annotation.DateMode;
import com.github.gzuliyujiang.wheelpicker.entity.DateEntity;
import com.github.gzuliyujiang.wheelpicker.impl.BirthdayFormatter;

/**
 * author ： 许明荣
 * time ： 2021/12/14
 * desc ：
 */
public class BirthdaysPicker extends DatePicker {
    public static final int MIN_AGE = 18;
    public static final int MAX_AGE = 100;
    private DateEntity oldValue;//开始日期(老的日期)
    private DateEntity defaultValue;//默认选择日期
    private DateEntity newValue;//结束日期(最新的日期 )
    private boolean initialized = false;

    public BirthdaysPicker(@NonNull Activity activity) {
        super(activity);
    }

    public BirthdaysPicker(@NonNull Activity activity, @StyleRes int themeResId) {
        super(activity, themeResId);
    }

    @Override
    protected void initData() {
        super.initData();
        initialized = true;
        wheelLayout.setRange(oldValue, newValue, defaultValue);
        wheelLayout.setDateMode(DateMode.YEAR_MONTH_DAY);
        wheelLayout.setDateFormatter(new BirthdayFormatter());
    }

    public void setDefaultValue(DateEntity oldDateEntity, DateEntity defaultDateEntity, DateEntity newDateEntity) {
        oldValue = oldDateEntity;
        defaultValue = defaultDateEntity;
        newValue = newDateEntity;
        if (initialized) {
            wheelLayout.setDefaultValue(defaultValue);
        }
    }

}
