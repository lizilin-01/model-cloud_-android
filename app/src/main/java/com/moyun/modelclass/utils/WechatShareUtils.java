package com.moyun.modelclass.utils;

import android.app.Activity;
import android.graphics.Bitmap;

import com.moyun.modelclass.activity.ArticleDetailsActivity;
import com.moyun.modelclass.base.BaseConfig;
import com.moyun.modelclass.http.CommonConfiguration;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

public class WechatShareUtils {

    public static void wxShare(Activity activity,String title,String summary,Bitmap resource){
        IWXAPI api = WXAPIFactory.createWXAPI(activity, BaseConfig.WX_APPID, true);
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = CommonConfiguration.getShareUrl();
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = title;
        msg.description = summary;
        Bitmap thumbBmp = Bitmap.createScaledBitmap(resource, 150, 150, true);
        resource.recycle();
        msg.thumbData = Util.bmpToByteArray(thumbBmp, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
    }

    public static void pyqShare(Activity activity,String title,String summary,Bitmap resource){
        IWXAPI api = WXAPIFactory.createWXAPI(activity, BaseConfig.WX_APPID, true);
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = CommonConfiguration.getShareUrl();
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = title;
        msg.description = summary;
        Bitmap thumbBmp = Bitmap.createScaledBitmap(resource, 150, 150, true);
        resource.recycle();
        msg.thumbData = Util.bmpToByteArray(thumbBmp, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneTimeline;
        api.sendReq(req);
    }

    private static String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }
}
