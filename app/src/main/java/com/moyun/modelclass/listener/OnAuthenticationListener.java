package com.moyun.modelclass.listener;


/**
 * 作者：Meteor
 * 日期：2022/1/15 17:25
 * tip：认证后获取个人信息监听回调
 */
public interface OnAuthenticationListener {
    void Authentication();
}
