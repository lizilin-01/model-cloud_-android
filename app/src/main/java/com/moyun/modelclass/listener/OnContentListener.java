package com.moyun.modelclass.listener;

import com.moyun.modelclass.http.bean.CommonDialogBean;

/**
 * 作者：Meteor
 * 日期：2022/2/52:37 上午
 * tip：
 */
public interface OnContentListener {
    void content(int id, CommonDialogBean content);
}