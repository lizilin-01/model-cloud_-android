package com.moyun.modelclass.comment.bean;

import java.util.List;

/**
 * @author ganhuanhui
 * 时间：2019/12/11
 * 描述：
 */
public class CommentEntity {

    public static final int TYPE_COMMENT_PARENT = 1;
    public static final int TYPE_COMMENT_CHILD = 2;
    public static final int TYPE_COMMENT_MORE = 3;
    public static final int TYPE_COMMENT_EMPTY = 4;
    public static final int TYPE_COMMENT_OTHER = 5;

    private List<FirstLevelBean> comments;
    private int totalComment;//总数量

    public List<FirstLevelBean> getComments() {
        return comments;
    }

    public int getTotalComment() {
        return totalComment;
    }

    public void setComments(List<FirstLevelBean> comments) {
        this.comments = comments;
    }

    public void setTotalComment(int totalComment) {
        this.totalComment = totalComment;
    }

    @Override
    public String toString() {
        return "CommentEntity{" +
                "comments=" + comments +
                ", totalComment=" + totalComment +
                '}';
    }
}
