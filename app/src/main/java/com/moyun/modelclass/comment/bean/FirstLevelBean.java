package com.moyun.modelclass.comment.bean;

import static com.moyun.modelclass.comment.bean.CommentEntity.TYPE_COMMENT_PARENT;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class FirstLevelBean implements MultiItemEntity {
    private List<SecondLevelBean> children;//子评论
    private String id;//评论id
    private UserInfoResponse userInfo;//创建人的信息
    private String content;//评论内容
    private int star;//评论星级
    private int type;//评论内容类型
    private String objectId;//评论对象
    private UserInfoResponse parentUserInfo;//创建人的信息
    private String[] images;//图片集合
    private String createTime;//评论时间
    private int likes = 0;//点赞数
    private boolean isSelfLike = false;//自己是否点赞

    //当前评论的总条数（包括这条一级评论）ps:处于未使用状态
    private long totalCount;
    //当前一级评论的位置（下标）
    private int position;
    //当前一级评论所在的位置(下标)
    private int positionCount;


    public List<SecondLevelBean> getChildren() {
        return children;
    }

    public void setChildren(List<SecondLevelBean> children) {
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserInfoResponse getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoResponse userInfo) {
        this.userInfo = userInfo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public UserInfoResponse getParentUserInfo() {
        return parentUserInfo;
    }

    public void setParentUserInfo(UserInfoResponse parentUserInfo) {
        this.parentUserInfo = parentUserInfo;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isSelfLike() {
        return isSelfLike;
    }

    public void setSelfLike(boolean selfLike) {
        isSelfLike = selfLike;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPositionCount() {
        return positionCount;
    }

    public void setPositionCount(int positionCount) {
        this.positionCount = positionCount;
    }

    @Override
    public String toString() {
        return "FirstLevelBean{" +
                "children=" + children +
                ", id='" + id + '\'' +
                ", userInfo=" + userInfo +
                ", content='" + content + '\'' +
                ", star=" + star +
                ", type=" + type +
                ", objectId='" + objectId + '\'' +
                ", parentUserInfo=" + parentUserInfo +
                ", images=" + Arrays.toString(images) +
                ", createTime='" + createTime + '\'' +
                ", likes=" + likes +
                ", isSelfLike=" + isSelfLike +
                ", totalCount=" + totalCount +
                ", position=" + position +
                ", positionCount=" + positionCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FirstLevelBean that = (FirstLevelBean) o;
        return star == that.star && type == that.type && likes == that.likes && isSelfLike == that.isSelfLike && totalCount == that.totalCount && position == that.position && positionCount == that.positionCount && Objects.equals(children, that.children) && Objects.equals(id, that.id) && Objects.equals(userInfo, that.userInfo) && Objects.equals(content, that.content) && Objects.equals(objectId, that.objectId) && Objects.equals(parentUserInfo, that.parentUserInfo) && Arrays.equals(images, that.images) && Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(children, id, userInfo, content, star, type, objectId, parentUserInfo, createTime, likes, isSelfLike, totalCount, position, positionCount);
        result = 31 * result + Arrays.hashCode(images);
        return result;
    }

    @Override
    public int getItemType() {
        return TYPE_COMMENT_PARENT;
    }
}
