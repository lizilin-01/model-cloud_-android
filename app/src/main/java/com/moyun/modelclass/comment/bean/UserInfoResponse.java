package com.moyun.modelclass.comment.bean;

public class UserInfoResponse {
    private String userId;//用户userId
    private String nickName;//用户昵称
    private String avatar;//用户头像
    private String desc;//用户描述

    public String getUserId() {
        return userId;
    }

    public String getNickName() {
        return nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getDesc() {
        return desc;
    }
}
