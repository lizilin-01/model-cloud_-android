package com.moyun.modelclass.comment.bean;

import static com.moyun.modelclass.comment.bean.CommentEntity.TYPE_COMMENT_MORE;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * @author ganhuanhui
 * 时间：2019/12/12 0012
 * 描述：更多item
 */
public class CommentMoreBean implements MultiItemEntity {
    private String objectId;//评论对象id
    private String parentCommentId;//父评论id
    private long totalCount;
    private long position;
    private long positionCount;


    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(String parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public long getPositionCount() {
        return positionCount;
    }

    public void setPositionCount(long positionCount) {
        this.positionCount = positionCount;
    }

    @Override
    public int getItemType() {
        return TYPE_COMMENT_MORE;
    }
}
