package com.moyun.modelclass.fragment.order;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.activity.OrderDetailsActivity;
import com.moyun.modelclass.activity.OrderEvaluateActivity;
import com.moyun.modelclass.adapter.MyOrderAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.event.OrderSearchEvent;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.OrderCancelApi;
import com.moyun.modelclass.http.api.OrderDeleteApi;
import com.moyun.modelclass.http.api.OrderListApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.ArrayList;

/**
 * 作者：Meteor
 * 日期：2022/1/19 23:25
 * tip：我的订单列表
 */
public class OrderListFragment extends BaseFragment implements MyOrderAdapter.OnFindClick {
    private SmartRefreshLayout orderListRefresh;
    private RecyclerView orderListRecycler;

    private MyOrderAdapter myOrderAdapter = new MyOrderAdapter();
    private String mLastId = "";
    private int mOrderType = 0;
    private String mSearch = "";

    public static OrderListFragment newInstance(String orderType) {
        Bundle args = new Bundle();
        args.putString("orderType", orderType);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_order_list;
    }

    @Override
    protected void assignViews(View view) {
        orderListRefresh = (SmartRefreshLayout) view.findViewById(R.id.order_list_refresh);
        orderListRecycler = (RecyclerView) view.findViewById(R.id.order_list_recycler);

        orderListRecycler.setLayoutManager(new LinearLayoutManager(requireActivity()));
        myOrderAdapter.setOnFindClick(this);
        orderListRecycler.setAdapter(myOrderAdapter);
    }

    @Override
    protected void registerListeners() {
        orderListRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                orderListData(true);
            }
        });
        orderListRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                orderListData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        String orderType = bundle.getString("orderType", "");
        if (!orderType.isEmpty()) {
            switch (orderType) {
                case "全部":
                    mOrderType = 0;
                    break;
                case "待付款":
                    mOrderType = 1;
                    break;
                case "待发货":
                    mOrderType = 2;
                    break;
                case "待收货":
                    mOrderType = 3;
                    break;
                case "待评价":
                    mOrderType = 4;
                    break;

            }
        }
    }

    @Override
    protected boolean isRegisterEventBus() {
        return true;
    }

    @Override
    protected void doAction() {
//        orderListData(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        orderListData(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderListEvent(OrderSearchEvent search) {
        mSearch = search.getSearch();
        orderListData(true);
    }

    private void orderListData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new OrderListApi().setOrderStateVO(mOrderType).setPageSize(CommonConfiguration.page).setLastId(mLastId).setSearchText(mSearch))
                .request(new HttpCallback<HttpData<OrderListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<OrderListApi.Bean> result) {
                        orderListRefresh.finishRefresh();
                        orderListRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                myOrderAdapter.setList(result.getData().getList());
                            } else {
                                myOrderAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                myOrderAdapter.setList(new ArrayList<>());
                                myOrderAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }

    @Override
    public void close(String orderId) {
        //关闭订单
        EasyHttp.post(this)
                .api(new OrderCancelApi().setOrderId(orderId))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        ToastUtils.showShort("关闭订单成功");
                        orderListData(true);
                    }
                });
    }

    @Override
    public void pay(String orderId) {
        //立即支付
        OrderDetailsActivity.start(requireActivity(), orderId);
    }

    @Override
    public void details(String orderId) {
        //查看详情
        OrderDetailsActivity.start(requireActivity(), orderId);
    }

    @Override
    public void delete(String orderId) {
        //删除订单
        EasyHttp.post(this)
                .api(new OrderDeleteApi().setOrderId(orderId))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        ToastUtils.showShort("删除订单成功");
                        orderListData(true);
                    }
                });
    }

    @Override
    public void evaluate(String orderId) {
        //立即评价
        OrderEvaluateActivity.start(requireActivity(),orderId);
    }
}
