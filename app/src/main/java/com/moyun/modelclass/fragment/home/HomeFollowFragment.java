package com.moyun.modelclass.fragment.home;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.activity.ArticleDetailsActivity;
import com.moyun.modelclass.activity.UserInfoActivity;
import com.moyun.modelclass.adapter.HomeFollowAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckCollectListUserApi;
import com.moyun.modelclass.http.api.CollectArticleApi;
import com.moyun.modelclass.http.api.HomeFollowApi;
import com.moyun.modelclass.http.bean.ArticleResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页-关注
 */
public class HomeFollowFragment extends BaseFragment implements HomeFollowAdapter.OnFollowClick {
    private HomeFollowAdapter homeFollowAdapter = new HomeFollowAdapter();
    private SmartRefreshLayout mainHomeFollowRefresh;
    private RecyclerView mainHomeFollowRecycler;

    private List<String> articleIdList = new ArrayList<>();

    private String mLastId = "";

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home_follow;
    }

    @Override
    protected void assignViews(View view) {
        mainHomeFollowRefresh = (SmartRefreshLayout) view.findViewById(R.id.main_home_follow_refresh);
        mainHomeFollowRecycler = (RecyclerView) view.findViewById(R.id.main_home_follow_recycler);

        mainHomeFollowRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        homeFollowAdapter.setOnFollowClick(this);
        mainHomeFollowRecycler.setAdapter(homeFollowAdapter);
    }

    @Override
    protected void registerListeners() {
        mainHomeFollowRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                articleIdList.clear();
                homeFollowData(true);
            }
        });
        mainHomeFollowRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                homeFollowData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        homeFollowData(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        homeFollowData(true);
    }

    private void homeFollowData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new HomeFollowApi().setLastId(mLastId).setPageSize(CommonConfiguration.page).setSearchText("").setAuthorUserId("").setSortType(1))
                .request(new HttpCallback<HttpData<HomeFollowApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<HomeFollowApi.Bean> result) {
                        mainHomeFollowRefresh.finishRefresh();
                        mainHomeFollowRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                homeFollowAdapter.setList(result.getData().getList());
                            } else {
                                homeFollowAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                homeFollowAdapter.setList(new ArrayList<>());
                                homeFollowAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }

                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            for (ArticleResponse item : result.getData().getList()) {
                                articleIdList.add(item.getId());
                            }
                        }
                        checkUserListCollect();
                    }
                });
    }

    private void checkUserListCollect() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckCollectListUserApi().setArticleIdList(articleIdList))
                    .request(new HttpCallback<HttpData<CheckCollectListUserApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckCollectListUserApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getLikes().size() > 0) {
                                homeFollowAdapter.setArticleState(result.getData().getLikes());
                            }
                        }
                    });
        }
    }

    @Override
    public void userInfo(String userId) {
        UserInfoActivity.start(requireActivity(), userId);
    }

    @Override
    public void details(String articleId) {
        ArticleDetailsActivity.start(requireActivity(), articleId,false);
    }

    @Override
    public void imageUrl(String url) {
//        ScanImageDialog.getInstance(mContext, url).builder().setNegativeButton().show();
    }

    @Override
    public void collect(String articleId, boolean isCollect) {
        EasyHttp.post(requireActivity())
                .api(new CollectArticleApi().setArticleId(articleId).setLikes(isCollect))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
//                        checkUserListCollect();
                        if (isCollect) {
                            ToastUtils.showShort("已收藏");
                        } else {
                            ToastUtils.showShort("取消收藏");
                        }
                    }
                });
    }
}
