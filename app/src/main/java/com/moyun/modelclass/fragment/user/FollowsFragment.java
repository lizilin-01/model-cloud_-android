package com.moyun.modelclass.fragment.user;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SizeUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.FansAndFollowsAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.GetFollowApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * tip：关注者
 */

public class FollowsFragment extends BaseFragment {
    private SmartRefreshLayout fragmentFollowsRefresh;
    private RecyclerView fragmentFollowsComments;
    private String mLastId = "";

    private FansAndFollowsAdapter fansAndFollowsAdapter = new FansAndFollowsAdapter();

    public static FollowsFragment newInstance() {
        return new FollowsFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_follows;
    }

    @Override
    protected void assignViews(View view) {
        fragmentFollowsRefresh = (SmartRefreshLayout) view.findViewById(R.id.fragment_follows_refresh);
        fragmentFollowsComments = (RecyclerView) view.findViewById(R.id.fragment_follows_comments);

        fragmentFollowsComments.setLayoutManager(new LinearLayoutManager(mContext));
        fragmentFollowsComments.addItemDecoration(new DividerItemDecoration(
                requireActivity(),
                Color.parseColor("#EFEFEF"),
                1,
                SizeUtils.dp2px(15),
                SizeUtils.dp2px(15), false));
        fragmentFollowsComments.setAdapter(fansAndFollowsAdapter);
    }

    @Override
    protected void registerListeners() {
        fragmentFollowsRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                followData(true);
            }
        });
        fragmentFollowsRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                followData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        followData(true);
    }

    private void followData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new GetFollowApi().setLastId(mLastId).setPageSize(CommonConfiguration.page))
                .request(new HttpCallback<HttpData<GetFollowApi.Bean>>(null) {
                    @Override
                    public void onSucceed(HttpData<GetFollowApi.Bean> result) {
                        fragmentFollowsRefresh.finishRefresh();
                        fragmentFollowsRefresh.finishLoadMore();

                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getUserId();
                            if (isRefresh) {
                                fansAndFollowsAdapter.setList(result.getData().getList());
                            } else {
                                fansAndFollowsAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                fansAndFollowsAdapter.setList(new ArrayList<>());
                                fansAndFollowsAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }

                    }
                });
    }
}
