package com.moyun.modelclass.fragment.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.activity.ArticleDetailsActivity;
import com.moyun.modelclass.activity.SearchActivity;
import com.moyun.modelclass.activity.UserInfoActivity;
import com.moyun.modelclass.adapter.HomeFindAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.CheckCollectListUserApi;
import com.moyun.modelclass.http.api.CheckFollowListUserApi;
import com.moyun.modelclass.http.api.CollectArticleApi;
import com.moyun.modelclass.http.api.FollowUserApi;
import com.moyun.modelclass.http.api.HomeFindApi;
import com.moyun.modelclass.http.bean.ArticleResponse;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页-发现
 */
public class HomeFindFragment extends BaseFragment implements HomeFindAdapter.OnFindClick {
    private HomeFindAdapter homeFindAdapter = new HomeFindAdapter();
    private FrameLayout mainHomeSearchContent;

    private SmartRefreshLayout mainHomeFindRefresh;
    private RecyclerView mainHomeFindRecycler;
    private String mLastId = "";
    private List<String> userIdList = new ArrayList<>();
    private List<String> articleIdList = new ArrayList<>();

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home_find;
    }

    @Override
    protected void assignViews(View view) {
        mainHomeFindRefresh = (SmartRefreshLayout) view.findViewById(R.id.main_home_find_refresh);
        mainHomeFindRecycler = (RecyclerView) view.findViewById(R.id.main_home_find_recycler);

        mainHomeFindRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        homeFindAdapter.setOnFindClick(this);
        chatHeaderView();
        mainHomeFindRecycler.setAdapter(homeFindAdapter);

    }

    private void chatHeaderView() {
        View header = LayoutInflater.from(mContext).inflate(R.layout.head_search, null);
        mainHomeSearchContent = (FrameLayout) header.findViewById(R.id.fl_search_content);
        homeFindAdapter.addHeaderView(header);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(mainHomeSearchContent, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchActivity.start(requireActivity());
            }
        });

        mainHomeFindRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                userIdList.clear();
                articleIdList.clear();
                homeFindData(true);
            }
        });
        mainHomeFindRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                homeFindData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        homeFindData(true);
    }


    private void homeFindData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new HomeFindApi().setLastId(mLastId).setPageSize(CommonConfiguration.page).setSearchText("").setAuthorUserId("").setSortType(1))
                .request(new HttpCallback<HttpData<HomeFindApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<HomeFindApi.Bean> result) {
                        mainHomeFindRefresh.finishRefresh();
                        mainHomeFindRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                homeFindAdapter.setList(result.getData().getList());
                            } else {
                                homeFindAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                homeFindAdapter.setList(new ArrayList<>());
                                homeFindAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            for (ArticleResponse item : result.getData().getList()) {
                                userIdList.add(item.getCreateUser());
                                articleIdList.add(item.getId());
                            }
                        }
                        checkUserListFollow();
                        checkUserListCollect();
                    }
                });
    }

    private void checkUserListFollow() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckFollowListUserApi().setUserIdList(userIdList))
                    .request(new HttpCallback<HttpData<CheckFollowListUserApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckFollowListUserApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                                homeFindAdapter.setFollowState(result.getData().getList());
                            }
                        }
                    });
        }
    }

    private void checkUserListCollect() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckCollectListUserApi().setArticleIdList(articleIdList))
                    .request(new HttpCallback<HttpData<CheckCollectListUserApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckCollectListUserApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getLikes().size() > 0) {
                                homeFindAdapter.setArticleState(result.getData().getLikes());
                            }
                        }
                    });
        }
    }

    @Override
    public void userInfo(String userId) {
        UserInfoActivity.start(requireActivity(), userId);
    }

    @Override
    public void details(String articleId) {
        ArticleDetailsActivity.start(requireActivity(), articleId, false);
    }

    @Override
    public void imageUrl(String url) {
//        ScanImageDialog.getInstance(mContext, url).builder().setNegativeButton().show();
    }

    @Override
    public void follow(String userId, boolean isFollow) {
        EasyHttp.post(this)
                .api(new FollowUserApi().setUserId(userId).setFollow(isFollow))
                .request(new HttpCallback<HttpData<FollowUserApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<FollowUserApi.Bean> result) {
                        checkUserListFollow();
                        if (isFollow) {
                            ToastUtils.showShort("已关注");
                        } else {
                            ToastUtils.showShort("取消关注");
                        }
                    }
                });
    }

    @Override
    public void collect(String articleId, boolean isCollect) {
        EasyHttp.post(requireActivity())
                .api(new CollectArticleApi().setArticleId(articleId).setLikes(isCollect))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
//                        checkUserListCollect();
                        if (isCollect) {
                            ToastUtils.showShort("已收藏");
                        } else {
                            ToastUtils.showShort("取消收藏");
                        }
                    }
                });
    }
}
