package com.moyun.modelclass.fragment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.activity.LoginVerificationCodeActivity;
import com.moyun.modelclass.activity.MyModelActivity;
import com.moyun.modelclass.adapter.ModelPagerAdapter;
import com.moyun.modelclass.adapter.ModelTypeAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.event.ModelSearchEvent;
import com.moyun.modelclass.fragment.model.ModelListFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.ModelTypeApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.listener.RecyclerItemClickListener;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.StatusBarUtil;
import org.greenrobot.eventbus.EventBus;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：Meteor
 * tip：模型
 */
public class MainModelFragment extends BaseFragment {
    private RelativeLayout rlMainModelTop;
    private TextView tvMainModelMy;
    private EditText etMainModelSearchContent;
    private TextView tvMainModelSearchButton;
    private RecyclerView mainModelRecycler;
    private ViewPager mainModelVp;

    private ModelPagerAdapter modelPagerAdapter = null;

    private ModelTypeAdapter modelTypeAdapter = new ModelTypeAdapter();
    private List<Fragment> mFragmentArrays = new ArrayList<>();

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_model;
    }

    @Override
    protected void assignViews(View view) {
        rlMainModelTop = (RelativeLayout) view.findViewById(R.id.rl_main_model_top);
        tvMainModelMy = (TextView) view.findViewById(R.id.tv_main_model_my);
        etMainModelSearchContent = (EditText) view.findViewById(R.id.et_main_model_search_content);
        tvMainModelSearchButton = (TextView) view.findViewById(R.id.tv_main_model_search_button);
        mainModelRecycler = (RecyclerView) view.findViewById(R.id.main_model_recycler);
        mainModelVp = (ViewPager) view.findViewById(R.id.main_model_vp);
        StatusBarUtil.setPaddingSmart(requireActivity(), rlMainModelTop);

        mainModelRecycler.setLayoutManager(new LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false));
        mainModelRecycler.setAdapter(modelTypeAdapter);
    }

    @Override
    protected void registerListeners() {

        ClickUtils.applySingleDebouncing(tvMainModelSearchButton, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftInput(etMainModelSearchContent);
                EventBus.getDefault().post(new ModelSearchEvent(etMainModelSearchContent.getText().toString().trim()));
                etMainModelSearchContent.clearFocus();
            }
        });

        etMainModelSearchContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        KeyboardUtils.hideSoftInput(etMainModelSearchContent);
                        EventBus.getDefault().post(new ModelSearchEvent(etMainModelSearchContent.getText().toString().trim()));
                        etMainModelSearchContent.clearFocus();
                        break;
                }
                return true;
            }
        });

        ClickUtils.applySingleDebouncing(tvMainModelMy, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    MyModelActivity.start(requireActivity());
                } else {
//                    ToastUtils.showShort(getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(requireActivity());
                }
            }
        });

        mainModelRecycler.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mainModelRecycler, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                modelTypeAdapter.setSelected(position);
                mainModelVp.setCurrentItem(position, true);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
        mainModelVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                modelTypeAdapter.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        modelTypeData();
    }

    private void modelTypeData() {
        EasyHttp.post(this)
                .api(new ModelTypeApi())
                .request(new HttpCallback<HttpData<ModelTypeApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<ModelTypeApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            modelTypeAdapter.setList(result.getData().getList());
                            mFragmentArrays.clear();
                            for (ModelTypeApi.BeanInfo data : result.getData().getList()) {
                                mFragmentArrays.add(ModelListFragment.newInstance(data.getCategoryId()));
                            }

                            modelPagerAdapter = new ModelPagerAdapter(getChildFragmentManager(), mFragmentArrays);
                            mainModelVp.setAdapter(modelPagerAdapter);
                        }
                    }
                });
    }
}
