package com.moyun.modelclass.fragment.user;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SizeUtils;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.adapter.FansAndFollowsAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.GetFansApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * tip：粉丝
 */

public class FansFragment extends BaseFragment {
    private SmartRefreshLayout fragmentFansRefresh;
    private RecyclerView fragmentFansComments;
    private String mLastId = "";

    private FansAndFollowsAdapter fansAndFollowsAdapter = new FansAndFollowsAdapter();

    public static FansFragment newInstance() {
        return new FansFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_fans;
    }

    @Override
    protected void assignViews(View view) {
        fragmentFansRefresh = (SmartRefreshLayout) view.findViewById(R.id.fragment_fans_refresh);
        fragmentFansComments = (RecyclerView) view.findViewById(R.id.fragment_fans_comments);

        fragmentFansComments.setLayoutManager(new LinearLayoutManager(mContext));
        fragmentFansComments.addItemDecoration(new DividerItemDecoration(
                requireActivity(),
                Color.parseColor("#EFEFEF"),
                1,
                SizeUtils.dp2px(15),
                SizeUtils.dp2px(15), false));
        fragmentFansComments.setAdapter(fansAndFollowsAdapter);
    }

    @Override
    protected void registerListeners() {

        fragmentFansRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                fansData(true);
            }
        });
        fragmentFansRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                fansData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {

    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        fansData(true);
    }

    private void fansData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new GetFansApi().setLastId(mLastId).setPageSize(CommonConfiguration.page))
                .request(new HttpCallback<HttpData<GetFansApi.Bean>>(null) {
                    @Override
                    public void onSucceed(HttpData<GetFansApi.Bean> result) {
                        fragmentFansRefresh.finishRefresh();
                        fragmentFansRefresh.finishLoadMore();

                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getUserId();
                            if (isRefresh) {
                                fansAndFollowsAdapter.setList(result.getData().getList());
                            } else {
                                fansAndFollowsAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                fansAndFollowsAdapter.setList(new ArrayList<>());
                                fansAndFollowsAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }
}
