package com.moyun.modelclass.fragment.article;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.moyun.modelclass.activity.ArticleDetailsActivity;
import com.moyun.modelclass.activity.EditArticleActivity;
import com.moyun.modelclass.activity.LoginVerificationCodeActivity;
import com.moyun.modelclass.activity.ModelDetailsActivity;
import com.moyun.modelclass.activity.SearchActivity;
import com.moyun.modelclass.activity.SubmitOrderActivity;
import com.moyun.modelclass.activity.UserInfoActivity;
import com.moyun.modelclass.adapter.ArticleDetailDetailsAdapter;
import com.moyun.modelclass.adapter.ParentCommentAdapter;
import com.moyun.modelclass.adapter.ModelBuyAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.AppAddShoppingApi;
import com.moyun.modelclass.http.api.AppArticleDetailsApi;
import com.moyun.modelclass.http.api.AppHotCommentApi;
import com.moyun.modelclass.http.api.AppModelListApi;
import com.moyun.modelclass.http.api.ArticleBackToDraftApi;
import com.moyun.modelclass.http.api.CheckArticlesCollectApi;
import com.moyun.modelclass.http.api.CheckCommentsLikesApi;
import com.moyun.modelclass.http.api.CollectArticleApi;
import com.moyun.modelclass.http.api.CommentMakeApi;
import com.moyun.modelclass.http.api.LikeCommentApi;
import com.moyun.modelclass.http.api.UpdateImageApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.CreateFileUtils;
import com.moyun.modelclass.utils.DataUtil;
import com.moyun.modelclass.utils.GlideEngine;
import com.moyun.modelclass.view.AndroidBugInputBoxAndSoftKeyboard;
import com.moyun.modelclass.view.recyclerview.DividerItemDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;


public class GoodsDetailFragment extends BaseFragment implements ModelBuyAdapter.OnBuyClick, ParentCommentAdapter.OnCommentClick {
    private TextView goodsDetailCommentsState;
    private TextView tvArticleDetailTitle;
    private ImageView ivArticleDetailHeadPortrait;
    private TextView tvArticleDetailName;
    private TextView tvArticleDetailTime;
    private TextView tvArticleDetailRead;
    private RecyclerView recyclerArticleDetailDetails;
    private TextView tvArticleDetailSubject;
    private RecyclerView recyclerGoodsDetail;
    private TextView goodsDetailCommentsNumber;
    private TextView tvGoodsDetailCheck;
    private RecyclerView recyclerGoodsDetailComments;

    private TextView goodsDetailCollect;
    private TextView goodsDetailAddShoppingCart;
    private TextView goodsDetailBuy;

    private RelativeLayout rlGoodsDetailShoppingAll;
    private RelativeLayout rlGoodsDetailCommentAll;
    private ImageView ivCommentSendImage;
    private EditText etCommentContent;
    private TextView tvCommentSendContent;
    private FrameLayout flCommentPreviewImage;
    private ImageView tvCommentPreviewImage;
    private ImageView tvCommentPreviewImageDelete;

    private TextView goodsDetailEdit;
    private TextView goodsDetailCancel;

    private ArticleDetailDetailsAdapter articleDetailDetailsAdapter = new ArticleDetailDetailsAdapter();
    private ModelBuyAdapter modelBuyAdapter = new ModelBuyAdapter();
    private ParentCommentAdapter parentCommentAdapter = new ParentCommentAdapter();

    private String mArticleId = "";//文章id
    private boolean mIsShowState = false;//是否显示状态
    private List<CommentMakeApi.CommentMakeRequest> commentList = new ArrayList<>();
    private File commentImageFile = null;//头像
    private UpdateImageApi.Bean commentImageBean = null;//评论图片
    private String mCommentId = "";//回复评论id

    private String mUserId = "";//作者的userId
    private boolean isCollect = false;//是否收藏
    private List<String> modelList = new ArrayList<>();
    private List<String> articleIdlList = new ArrayList<>();

    private List<String> commentIdList = new ArrayList<>();

    private boolean isShowSoftKeyboard = false;

    public static GoodsDetailFragment newInstance(String articleId, boolean isShowState) {
        Bundle args = new Bundle();
        args.putString("articleId", articleId);
        args.putBoolean("isShowState", isShowState);
        GoodsDetailFragment fragment = new GoodsDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_goods_detail;
    }

    @Override
    protected void assignViews(View view) {
        goodsDetailCommentsState = (TextView) view.findViewById(R.id.goods_detail_comments_state);
        tvArticleDetailTitle = (TextView) view.findViewById(R.id.tv_article_detail_title);
        ivArticleDetailHeadPortrait = (ImageView) view.findViewById(R.id.iv_article_detail_head_portrait);
        tvArticleDetailName = (TextView) view.findViewById(R.id.tv_article_detail_name);
        tvArticleDetailTime = (TextView) view.findViewById(R.id.tv_article_detail_time);
        tvArticleDetailRead = (TextView) view.findViewById(R.id.tv_article_detail_read);
        recyclerArticleDetailDetails = (RecyclerView) view.findViewById(R.id.recycler_article_detail_details);
        tvArticleDetailSubject = (TextView) view.findViewById(R.id.tv_article_detail_subject);
        recyclerGoodsDetail = (RecyclerView) view.findViewById(R.id.recycler_goods_detail);
        goodsDetailCommentsNumber = (TextView) view.findViewById(R.id.goods_detail_comments_number);
        tvGoodsDetailCheck = (TextView) view.findViewById(R.id.tv_goods_detail_check);
        recyclerGoodsDetailComments = (RecyclerView) view.findViewById(R.id.recycler_goods_detail_comments);
        goodsDetailCollect = (TextView) view.findViewById(R.id.goods_detail_collect);
        goodsDetailAddShoppingCart = (TextView) view.findViewById(R.id.goods_detail_add_shopping_cart);
        goodsDetailBuy = (TextView) view.findViewById(R.id.goods_detail_buy);

        rlGoodsDetailShoppingAll = (RelativeLayout) view.findViewById(R.id.rl_goods_detail_shopping_all);
        rlGoodsDetailCommentAll = (RelativeLayout) view.findViewById(R.id.rl_goods_detail_comment_all);
        ivCommentSendImage = (ImageView) view.findViewById(R.id.iv_comment_send_image);
        etCommentContent = (EditText) view.findViewById(R.id.et_comment_content);
        tvCommentSendContent = (TextView) view.findViewById(R.id.tv_comment_send_content);
        flCommentPreviewImage = (FrameLayout) view.findViewById(R.id.fl_comment_preview_image);
        tvCommentPreviewImage = (ImageView) view.findViewById(R.id.tv_comment_preview_image);
        tvCommentPreviewImageDelete = (ImageView) view.findViewById(R.id.tv_comment_preview_image_delete);
        goodsDetailEdit = (TextView) view.findViewById(R.id.goods_detail_edit);
        goodsDetailCancel = (TextView) view.findViewById(R.id.goods_detail_cancel);


        recyclerArticleDetailDetails.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerArticleDetailDetails.setAdapter(articleDetailDetailsAdapter);

        recyclerGoodsDetail.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerGoodsDetail.addItemDecoration(new DividerItemDecoration(mContext, Color.parseColor("#EFEFEF"), 1, SizeUtils.dp2px(16), SizeUtils.dp2px(16), false));
        modelBuyAdapter.setOnBuyClick(this);
        recyclerGoodsDetail.setAdapter(modelBuyAdapter);

        recyclerGoodsDetailComments.setLayoutManager(new LinearLayoutManager(mContext));
        parentCommentAdapter.setOnCommentClick(this);
        recyclerGoodsDetailComments.setAdapter(parentCommentAdapter);
    }

    @Override
    protected void registerListeners() {
        ClickUtils.applySingleDebouncing(goodsDetailAddShoppingCart, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShoppingLog(modelList);
            }
        });

        ClickUtils.applySingleDebouncing(ivArticleDetailHeadPortrait, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mUserId.isEmpty()) {
                    UserInfoActivity.start(requireActivity(), mUserId);
                }
            }
        });

        ClickUtils.applySingleDebouncing(goodsDetailBuy, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    SubmitOrderActivity.start(requireActivity(), modelList);
                } else {
//                    ToastUtils.showShort(getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(requireActivity());
                }
            }
        });

        ClickUtils.applySingleDebouncing(tvGoodsDetailCheck, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    // 调用Activity中的方法
                    ((ArticleDetailsActivity) getActivity()).setTabSelect(1);
                }
            }
        });

        ClickUtils.applySingleDebouncing(goodsDetailCollect, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeArticlesCollect(!isCollect);
            }
        });

        ClickUtils.applySingleDebouncing(tvArticleDetailSubject, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchActivity.start(requireActivity());
            }
        });

        AndroidBugInputBoxAndSoftKeyboard.assistActivity(requireActivity(), true, new AndroidBugInputBoxAndSoftKeyboard.OnChangeListener() {
            @Override
            public void showChange(boolean isShow) {
                if (isShow) {
                    if (CommonConfiguration.isLogin(kv)) {
                        etCommentContent.setCursorVisible(true);
                    } else {
//                        ToastUtils.showShort(getContext().getString(R.string.http_token_error));
                        LoginVerificationCodeActivity.start(requireActivity());
                    }
                } else {
                    if (!isShowSoftKeyboard) {
                        rlGoodsDetailShoppingAll.setVisibility(View.VISIBLE);
                        rlGoodsDetailCommentAll.setVisibility(View.GONE);
                        etCommentContent.clearFocus();
                        Glide.with(requireActivity()).clear(tvCommentPreviewImage);
                        commentImageBean = null;
                        etCommentContent.setText("");
                        flCommentPreviewImage.setVisibility(View.GONE);
                    }
                }
            }
        });
        ClickUtils.applySingleDebouncing(tvCommentPreviewImageDelete, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(requireActivity()).clear(tvCommentPreviewImage);
                commentImageBean = null;
                flCommentPreviewImage.setVisibility(View.GONE);
            }
        });

        ClickUtils.applySingleDebouncing(tvCommentSendContent, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = etCommentContent.getText().toString();
                if (!content.isEmpty()) {
                    commentList.clear();
                    CommentMakeApi.CommentMakeRequest comment = new CommentMakeApi.CommentMakeRequest();
                    comment.setCommentContent(content);
                    if (commentImageBean != null) {
                        List<String> image = new ArrayList<>();
                        image.add(commentImageBean.getUrl());
                        comment.setImages(image);
                    }
                    comment.setObjectId(mArticleId);
                    comment.setTargetId(mCommentId);
                    commentList.add(comment);
                    initCommentMakeApi(commentList);
                } else {
                    ToastUtils.showShort("请输入评论内容");
                }
            }
        });

        ClickUtils.applySingleDebouncing(ivCommentSendImage, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowSoftKeyboard = true;
                KeyboardUtils.hideSoftInput(etCommentContent);
                XXPermissions.with(requireActivity()).permission(Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_EXTERNAL_STORAGE)
                        .request(new OnPermissionCallback() {
                            @Override
                            public void onGranted(List<String> permissions, boolean all) {
                                PictureSelector.create(requireActivity())
                                        .openGallery(SelectMimeType.ofImage())
                                        .setImageEngine(GlideEngine.createGlideEngine())
                                        .setMaxSelectNum(1)
                                        .forResult(new OnResultCallbackListener<LocalMedia>() {
                                            @Override
                                            public void onResult(ArrayList<LocalMedia> result) {
                                                for (LocalMedia media : result) {
                                                    Luban.with(requireActivity())
                                                            .load(media.getRealPath())
                                                            .ignoreBy(50)
                                                            .setTargetDir(CreateFileUtils.getSandboxPath(requireActivity()))
                                                            .setCompressListener(new OnCompressListener() {
                                                                                     @Override
                                                                                     public void onStart() {
                                                                                     }

                                                                                     @Override
                                                                                     public void onSuccess(int index, File compressFile) {
                                                                                         commentImageFile = compressFile;
                                                                                         Glide.with(mContext)
                                                                                                 .asBitmap()
                                                                                                 .centerCrop()
                                                                                                 .load(PictureMimeType.isContent(media.getPath()) && !media.isCut() && !media.isCompressed() ? Uri.parse(media.getPath()) : media.getPath())
                                                                                                 .placeholder(R.drawable.icon_head_portrait)
                                                                                                 .error(R.drawable.icon_head_portrait)
                                                                                                 .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                                                                                                 .into(new BitmapImageViewTarget(tvCommentPreviewImage) {
                                                                                                     @Override
                                                                                                     protected void setResource(Bitmap resource) {
                                                                                                         super.setResource(resource);
                                                                                                         tvCommentPreviewImage.setImageBitmap(ImageUtils.toRound(resource));
                                                                                                     }
                                                                                                 });
                                                                                         initUpdateImageApi(true);
                                                                                     }

                                                                                     @Override
                                                                                     public void onError(int index, Throwable e) {
                                                                                         ToastUtils.showShort("选择图片失败");
                                                                                     }
                                                                                 }
                                                            ).launch();
                                                }
                                            }

                                            @Override
                                            public void onCancel() {

                                            }
                                        });
                            }

                            @Override
                            public void onDenied(List<String> permissions, boolean never) {
                                ToastUtils.showShort("获取存储权限失败");
                            }
                        });
            }
        });

        ClickUtils.applySingleDebouncing(goodsDetailEdit, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditArticleActivity.start(requireActivity(), mArticleId);
                requireActivity().finish();
            }
        });

        ClickUtils.applySingleDebouncing(goodsDetailCancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消发布
                removeArticle();
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mArticleId = bundle.getString("articleId", "");
        mIsShowState = bundle.getBoolean("isShowState", false);
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        initArticleDetailsLog();
        initCommentsLog();
        checkArticlesCollect();
    }

    private void initUpdateImageApi(boolean isWatermark) {
        isShowSoftKeyboard = false;
        EasyHttp.post(this).api(new UpdateImageApi()
                        .setFileName(UUID.randomUUID() + ".jpg")
                        .setFile(commentImageFile)
                        .setWatermark(isWatermark))
                .request(new HttpCallback<HttpData<UpdateImageApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<UpdateImageApi.Bean> result) {
                        flCommentPreviewImage.setVisibility(View.VISIBLE);
                        commentImageBean = result.getData();
                    }
                });
    }

    private void initCommentMakeApi(List<CommentMakeApi.CommentMakeRequest> commentList) {
        EasyHttp.post(this).api(new CommentMakeApi().setType(1).setCommentObjectList(commentList))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        mCommentId = "";
                        initCommentsLog();
                        ToastUtils.showShort("回复成功");
                        rlGoodsDetailShoppingAll.setVisibility(View.VISIBLE);
                        rlGoodsDetailCommentAll.setVisibility(View.GONE);
                        KeyboardUtils.hideSoftInput(mActivity);
                        etCommentContent.clearFocus();
                        Glide.with(requireActivity()).clear(tvCommentPreviewImage);
                        commentImageBean = null;
                        etCommentContent.setText("");
                        flCommentPreviewImage.setVisibility(View.GONE);
                    }
                });
    }

    //文章详情
    private void initArticleDetailsLog() {
        if (mArticleId == null || mArticleId.isEmpty()) {
            ToastUtils.showShort("文章id为空");
            return;
        }
        EasyHttp.post(requireActivity())
                .api(new AppArticleDetailsApi().setArticleId(mArticleId))
                .request(new HttpCallback<HttpData<AppArticleDetailsApi.Bean>>(this) {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSucceed(HttpData<AppArticleDetailsApi.Bean> result) {
                        if (result != null && result.getData() != null) {
                            mUserId = result.getData().getCreateUser();
                            if (mIsShowState) {
                                if (result.getData().getArticleState() == 1) {
                                    goodsDetailCommentsState.setVisibility(View.GONE);
                                } else if (result.getData().getArticleState() == 2) {
                                    goodsDetailCommentsState.setVisibility(View.VISIBLE);
                                    goodsDetailCommentsState.setText("审核中，请耐心等候！");
                                    goodsDetailCommentsState.setBackgroundColor(Color.parseColor("#00BC77"));

                                    goodsDetailEdit.setVisibility(View.GONE);
                                    goodsDetailCancel.setVisibility(View.GONE);
                                    rlGoodsDetailShoppingAll.setVisibility(View.GONE);
                                    rlGoodsDetailCommentAll.setVisibility(View.GONE);
                                } else if (result.getData().getArticleState() == 3) {
                                    goodsDetailCommentsState.setVisibility(View.VISIBLE);
                                    goodsDetailCommentsState.setText("内容已成功发布！");
                                    goodsDetailCommentsState.setBackgroundColor(Color.parseColor("#FF6600"));

                                    goodsDetailEdit.setVisibility(View.GONE);
                                    goodsDetailCancel.setVisibility(View.VISIBLE);
                                    rlGoodsDetailShoppingAll.setVisibility(View.GONE);
                                    rlGoodsDetailCommentAll.setVisibility(View.GONE);
                                } else if (result.getData().getArticleState() == 4) {
                                    goodsDetailCommentsState.setVisibility(View.VISIBLE);
                                    goodsDetailCommentsState.setText("已驳回，建议您修改后重新提交!");
                                    goodsDetailCommentsState.setBackgroundColor(Color.parseColor("#FF0000"));

                                    goodsDetailEdit.setVisibility(View.VISIBLE);
                                    goodsDetailCancel.setVisibility(View.GONE);
                                    rlGoodsDetailShoppingAll.setVisibility(View.GONE);
                                    rlGoodsDetailCommentAll.setVisibility(View.GONE);
                                }
                            } else {
                                goodsDetailCommentsState.setVisibility(View.GONE);
                                rlGoodsDetailShoppingAll.setVisibility(View.VISIBLE);
                            }

                            modelList = result.getData().getModelList();
                            initModelListLog(modelList);
                            Glide.with(requireActivity())
                                    .asBitmap()
                                    .load(CommonConfiguration.splitResUrl(3, result.getData().getCreateUserAvatar()))
                                    .placeholder(R.drawable.icon_head_portrait)
                                    .error(R.drawable.icon_head_portrait)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                                    .into(new BitmapImageViewTarget(ivArticleDetailHeadPortrait) {
                                        @Override
                                        protected void setResource(Bitmap resource) {
                                            super.setResource(resource);
                                            ivArticleDetailHeadPortrait.setImageBitmap(ImageUtils.toRound(resource));
                                        }
                                    });
                            tvArticleDetailName.setText(result.getData().getNickName());
                            tvArticleDetailTitle.setText(result.getData().getTitle());
                            tvArticleDetailTime.setText(DataUtil.getTimeShowString(TimeUtils.string2Millis(result.getData().getUpdateTime()), false) + "发布");
                            tvArticleDetailRead.setText("浏览" + result.getData().getScanCount());
                            setContent(result.getData().getContent());
                            StringBuilder tags = new StringBuilder();
                            if (result.getData().getTags() != null && !result.getData().getTags().isEmpty()) {
                                tvArticleDetailSubject.setVisibility(View.VISIBLE);
                                for (int i = 0; i < result.getData().getTags().size(); i++) {
                                    //证明是最后一个
                                    if (i == result.getData().getTags().size() - 1) {
                                        tags.append("#").append(result.getData().getTags().get(i));
                                    } else {
                                        tags.append("#").append(result.getData().getTags().get(i)).append(",");
                                    }
                                }
                            } else {
                                tvArticleDetailSubject.setVisibility(View.GONE);
                            }
                            tvArticleDetailSubject.setText(tags);
                        }
                    }
                });
    }

    private void setContent(String content) {
        //正则表达式匹配文本内容
        String stringRegex = "[^\\[\\]]+";
        Pattern stringPattern = Pattern.compile(stringRegex);
        Matcher stringMatcher = stringPattern.matcher(content);

        // 存储文本内容的列表
        List<String> stringContents = new ArrayList<>();

        // 匹配文本内容并存储到列表中
        while (stringMatcher.find()) {
            stringContents.add(stringMatcher.group().trim());
        }
        articleDetailDetailsAdapter.setList(stringContents);
    }

    //批量获取模型数据
    private void initModelListLog(List<String> modelIdList) {
        if (modelIdList.size() <= 0) {
            ToastUtils.showShort("模型id列表为空");
            return;
        }
        EasyHttp.post(requireActivity())
                .api(new AppModelListApi().setModelIdList(modelIdList).setScene(1))
                .request(new HttpCallback<HttpData<AppModelListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AppModelListApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            modelBuyAdapter.setList(result.getData().getList());
                        }
                    }
                });
    }

    //热评列表
    private void initCommentsLog() {
        if (mArticleId == null || mArticleId.isEmpty()) {
            ToastUtils.showShort("文章id为空");
            return;
        }
        EasyHttp.post(requireActivity())
                .api(new AppHotCommentApi().setType(1).setObjectId(mArticleId))
                .request(new HttpCallback<HttpData<AppHotCommentApi.Bean>>(this) {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSucceed(HttpData<AppHotCommentApi.Bean> result) {
                        if (result != null && result.getData() != null && result.getData().getComments().size() > 0) {
                            goodsDetailCommentsNumber.setText("评论   " + result.getData().getTotalComment());
                            parentCommentAdapter.setList(result.getData().getComments());
                        } else {
                            goodsDetailCommentsNumber.setText("评论   0");
                            parentCommentAdapter.setEmptyView(R.layout.empty_comment_layout);
                        }

                        if (result != null && result.getData() != null && result.getData().getComments().size() > 0) {
                            for (int i = 0; i < result.getData().getComments().size(); i++) {
                                commentIdList.add(result.getData().getComments().get(i).getId());
                                if (result.getData().getComments().get(i).getChildren() != null && result.getData().getComments().get(i).getChildren().size() > 0) {
                                    for (int j = 0; j < result.getData().getComments().get(i).getChildren().size(); j++) {
                                        commentIdList.add(result.getData().getComments().get(i).getChildren().get(j).getId());
                                    }
                                }
                            }
                        }
                        checkComments();
                    }
                });
    }

    private void checkComments() {
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(this)
                    .api(new CheckCommentsLikesApi().setCommentIdList(commentIdList))
                    .request(new HttpCallback<HttpData<CheckCommentsLikesApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckCommentsLikesApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getLikes().size() > 0) {
                                parentCommentAdapter.setZanState(result.getData().getLikes());
                            }
                        }
                    });
        }
    }

    //检查文章是否点赞或者收藏
    private void checkArticlesCollect() {
        if (mArticleId == null || mArticleId.isEmpty()) {
            ToastUtils.showShort("文章id为空");
            return;
        }
        articleIdlList.clear();
        articleIdlList.add(mArticleId);
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(requireActivity())
                    .api(new CheckArticlesCollectApi().setArticleIdList(articleIdlList))
                    .request(new HttpCallback<HttpData<CheckArticlesCollectApi.Bean>>(this) {
                        @Override
                        public void onSucceed(HttpData<CheckArticlesCollectApi.Bean> result) {
                            if (result != null && result.getData() != null && result.getData().getLikes().size() > 0) {
                                if (result.getData().getLikes().get(0).isLikes()) {
                                    isCollect = true;
                                    Drawable drawable = ContextCompat.getDrawable(requireActivity(), R.drawable.icon_collect_yes_big);
                                    goodsDetailCollect.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
                                    goodsDetailCollect.setText("已收藏");
                                } else {
                                    isCollect = false;
                                    Drawable drawable = ContextCompat.getDrawable(requireActivity(), R.drawable.icon_collect_not_big);
                                    goodsDetailCollect.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
                                    goodsDetailCollect.setText("收藏");
                                }
                            }
                        }
                    });
        }
    }

    //收藏或取消收藏文章
    private void changeArticlesCollect(boolean isLike) {
        if (mArticleId == null || mArticleId.isEmpty()) {
            ToastUtils.showShort("文章id为空");
            return;
        }
        if (CommonConfiguration.isLogin(kv)) {
            EasyHttp.post(requireActivity())
                    .api(new CollectArticleApi().setArticleId(mArticleId).setLikes(isLike))
                    .request(new HttpCallback<HttpData<String>>(this) {
                        @Override
                        public void onSucceed(HttpData<String> result) {
                            if (isLike) {
                                isCollect = true;
                                Drawable drawable = ContextCompat.getDrawable(requireActivity(), R.drawable.icon_collect_yes_big);
                                goodsDetailCollect.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
                                goodsDetailCollect.setText("已收藏");
                            } else {
                                isCollect = false;
                                Drawable drawable = ContextCompat.getDrawable(requireActivity(), R.drawable.icon_collect_not_big);
                                goodsDetailCollect.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
                                goodsDetailCollect.setText("收藏");
                            }
                        }
                    });
        } else {
//            ToastUtils.showShort(getString(R.string.http_token_error));
            LoginVerificationCodeActivity.start(requireActivity());
        }
    }

    @Override
    public void buy(String modelId) {
        List<String> buyList = new ArrayList<>();
        buyList.add(modelId);
        addShoppingLog(buyList);
    }

    @Override
    public void scanModel(String modelId) {
        ModelDetailsActivity.start(requireActivity(), modelId, false);
    }

    //加入购物车
    private void addShoppingLog(List<String> buyList) {
        if (buyList.size() == 0) {
            ToastUtils.showShort("模型id列表为空");
            return;
        }

        EasyHttp.post(requireActivity())
                .api(new AppAddShoppingApi().setModelIdList(buyList))
                .request(new HttpCallback<HttpData<AppAddShoppingApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<AppAddShoppingApi.Bean> result) {
                        ToastUtils.showShort("加入购物车成功");
                    }
                });
    }

    private void removeArticle() {
        EasyHttp.post(requireActivity())
                .api(new ArticleBackToDraftApi().setArticleId(mArticleId))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        ToastUtils.showShort("取消发布成功");
                        requireActivity().finish();
                    }
                });
    }

    @Override
    public void zan(String commentId, boolean isLike) {
        EasyHttp.post(requireActivity())
                .api(new LikeCommentApi().setCommentId(commentId).setLikes(isLike))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onSucceed(HttpData<String> result) {
//                        initCommentsLog();
                    }
                });
    }

    @Override
    public void replay(String commentId) {
        this.mCommentId = commentId;
        rlGoodsDetailShoppingAll.setVisibility(View.GONE);
        rlGoodsDetailCommentAll.setVisibility(View.VISIBLE);
        KeyboardUtils.showSoftInput(mActivity);
        etCommentContent.requestFocus();
    }
}
