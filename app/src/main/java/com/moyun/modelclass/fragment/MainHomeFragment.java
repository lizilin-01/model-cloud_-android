package com.moyun.modelclass.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.IntRange;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.moyun.modelclass.activity.EditArticleActivity;
import com.moyun.modelclass.activity.LoginVerificationCodeActivity;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.fragment.home.HomeFindFragment;
import com.moyun.modelclass.fragment.home.HomeFollowFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.R;
import com.moyun.modelclass.utils.FragmentUtils;
import com.moyun.modelclass.utils.StatusBarUtil;

import java.util.List;

/**
 * 作者：Meteor
 * 日期：2022/1/19 23:25
 * tip：首页
 */
public class MainHomeFragment extends BaseFragment {
    private RelativeLayout llMainHomeTop;
    private RadioGroup rgMainHomeType;
    private RadioButton rbMainHomeFind;
    private RadioButton rbMainHomeFollow;
    private ImageView ivMainEditArticle;

    private final Fragment[] mFragments = new Fragment[2];
    private int mCurrentIndex = 0;
    private Fragment mCurrentFragment;
    private FragmentManager mFragmentManager;
    private Bundle mSavedInstanceState;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_home;
    }

    @Override
    protected void assignViews(View view) {
        llMainHomeTop = (RelativeLayout) view.findViewById(R.id.ll_main_home_top);
        rgMainHomeType = (RadioGroup) view.findViewById(R.id.rg_main_home_type);
        rbMainHomeFind = (RadioButton) view.findViewById(R.id.rb_main_home_find);
        rbMainHomeFollow = (RadioButton) view.findViewById(R.id.rb_main_home_follow);
        ivMainEditArticle = (ImageView) view.findViewById(R.id.iv_main_edit_article);

        StatusBarUtil.setPaddingSmart(requireActivity(), llMainHomeTop);

        mFragmentManager = requireActivity().getSupportFragmentManager();
        if (mSavedInstanceState != null) {
//            mCurrentIndex = mSavedInstanceState.getInt(KEY_SELECT_INDEX, 0);
            releaseFragment();
        }

        mFragments[0] = new HomeFindFragment();
        mFragments[1] = new HomeFollowFragment();

        rgMainHomeType.setSelected(true);
        rbMainHomeFind.setSelected(false);
        doOnTabSelected(mCurrentIndex);
    }

    @Override
    protected void registerListeners() {
        rgMainHomeType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_main_home_find) {
                    //发现
                    rbMainHomeFind.setSelected(true);
                    rbMainHomeFollow.setSelected(false);
                    doOnTabSelected(0);
                } else if (checkedId == R.id.rb_main_home_follow) {
                    //关注
                    if (CommonConfiguration.isLogin(kv)) {
                        rbMainHomeFind.setSelected(false);
                        rbMainHomeFollow.setSelected(true);
                        doOnTabSelected(1);
                    } else {
                        rgMainHomeType.check(rbMainHomeFind.getId());
//                        ToastUtils.showShort(getString(R.string.http_token_error));
                        LoginVerificationCodeActivity.start(requireActivity());
                    }
                }
            }
        });

        ClickUtils.applySingleDebouncing(ivMainEditArticle, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonConfiguration.isLogin(kv)) {
                    EditArticleActivity.start(requireActivity(),"");
                } else {
//                    ToastUtils.showShort(getString(R.string.http_token_error));
                    LoginVerificationCodeActivity.start(requireActivity());
                }
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {

    }

    //移除fragment，避免出现fragment重叠的问题
    private void releaseFragment() {
        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        List<Fragment> fragments = requireActivity().getSupportFragmentManager().getFragments();
        for (Fragment f : fragments) {
            if (f != null) {
                transaction.detach(f);
                transaction.remove(f);
            }
        }
        transaction.commitNowAllowingStateLoss();
    }

    private void doOnTabSelected(@IntRange(from = 0, to = 1) int position) {
        mCurrentFragment = FragmentUtils.switchContent(mFragmentManager, mCurrentFragment, mFragments[position], R.id.fl_main_home, position, false);
        mCurrentIndex = position;
    }
}
