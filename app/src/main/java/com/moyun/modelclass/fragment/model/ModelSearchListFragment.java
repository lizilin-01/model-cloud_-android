package com.moyun.modelclass.fragment.model;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.activity.ModelDetailsActivity;
import com.moyun.modelclass.adapter.ModelListAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.ModelListApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * 日期：2022/1/19 23:25
 * tip：模型列表
 */
public class ModelSearchListFragment extends BaseFragment implements ModelListAdapter.OnItemSelectedChangedListener {
    private SmartRefreshLayout modelListRefresh;
    private RecyclerView modelListRecycler;

    private ModelListAdapter modelListAdapter = new ModelListAdapter();
    private String mLastId = "";
    private String mCategoryId = "";
    private String mSearchWord = "";

    public static ModelSearchListFragment newInstance(String categoryId,String searchWord) {
        Bundle args = new Bundle();
        args.putString("categoryId", categoryId);
        args.putString("searchWord", searchWord);
        ModelSearchListFragment fragment = new ModelSearchListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_model_list;
    }

    @Override
    protected void assignViews(View view) {
        modelListRefresh = (SmartRefreshLayout) view.findViewById(R.id.model_list_refresh);
        modelListRecycler = (RecyclerView) view.findViewById(R.id.model_list_recycler);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        modelListRecycler.setLayoutManager(layoutManager);
        modelListAdapter.setOnItemSelectedChangedListener(this);
        modelListRecycler.setAdapter(modelListAdapter);
    }

    @Override
    protected void registerListeners() {
        modelListRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                modelListData(true);
            }
        });
        modelListRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                modelListData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        mCategoryId =  bundle.getString("categoryId","");
        mSearchWord =  bundle.getString("searchWord","");
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
        modelListData(true);
    }

    private void modelListData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        if (mCategoryId == null ||mCategoryId.isEmpty()){
            return;
        }
        EasyHttp.post(this)
                .api(new ModelListApi().setType(Integer.parseInt(mCategoryId)).setLastId(mLastId).setPageSize(CommonConfiguration.page).setSearchText(mSearchWord))
                .request(new HttpCallback<HttpData<ModelListApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<ModelListApi.Bean> result) {
                        modelListRefresh.finishRefresh();
                        modelListRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                modelListAdapter.setList(result.getData().getList());
                            } else {
                                modelListAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                modelListAdapter.setList(new ArrayList<>());
                                modelListAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }

    @Override
    public void details(String modelId) {
        ModelDetailsActivity.start(requireActivity(), modelId,false);
    }
}
