package com.moyun.modelclass.fragment.article;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.moyun.modelclass.activity.ArticleDetailsActivity;
import com.moyun.modelclass.activity.EditArticleActivity;
import com.moyun.modelclass.adapter.MyArticleAdapter;
import com.moyun.modelclass.base.BaseFragment;
import com.moyun.modelclass.dialog.ScanImageDialog;
import com.moyun.modelclass.http.CommonConfiguration;
import com.moyun.modelclass.http.api.ArticleOnesPostApi;
import com.moyun.modelclass.http.model.HttpData;
import com.moyun.modelclass.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

/**
 * 作者：Meteor
 * 日期：2022/1/19 23:25
 * tip：我的文章列表
 */
public class ArticleListFragment extends BaseFragment implements MyArticleAdapter.OnFindClick {
    private SmartRefreshLayout articleListRefresh;
    private RecyclerView articleListRecycler;

    private MyArticleAdapter myArticleAdapter = new MyArticleAdapter();
    private String mLastId = "";
    private int mArticleType = 0;

    public static ArticleListFragment newInstance(String articleType) {
        Bundle args = new Bundle();
        args.putString("articleType", articleType);
        ArticleListFragment fragment = new ArticleListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_article_list;
    }

    @Override
    protected void assignViews(View view) {
        articleListRefresh = (SmartRefreshLayout) view.findViewById(R.id.article_list_refresh);
        articleListRecycler = (RecyclerView) view.findViewById(R.id.article_list_recycler);

        articleListRecycler.setLayoutManager(new LinearLayoutManager(requireActivity()));
        myArticleAdapter.setOnFindClick(this);
        articleListRecycler.setAdapter(myArticleAdapter);
    }

    @Override
    protected void registerListeners() {
        articleListRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mLastId = "";
                modelListData(true);
            }
        });
        articleListRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                modelListData(false);
            }
        });
    }

    @Override
    protected void getExtras(Bundle bundle) {
        String articleType = bundle.getString("articleType", "");
        if (!articleType.isEmpty()) {
            switch (articleType) {
                case "全部":
                    mArticleType = 0;
                    break;
                case "审核中":
                    mArticleType = 2;
                    break;
                case "已发布":
                    mArticleType = 3;
                    break;
                case "审核驳回":
                    mArticleType = 4;
                    break;
                case "草稿箱":
                    mArticleType = 1;
                    break;
            }
        }
    }

    @Override
    protected boolean isRegisterEventBus() {
        return false;
    }

    @Override
    protected void doAction() {
//        modelListData(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        modelListData(true);
    }

    private void modelListData(boolean isRefresh) {
        mLastId = isRefresh ? "" : mLastId;
        EasyHttp.post(this)
                .api(new ArticleOnesPostApi().setUserId(CommonConfiguration.getUseId(kv)).setLastId(mLastId).setPageSize(CommonConfiguration.page).setArticleState(mArticleType))
                .request(new HttpCallback<HttpData<ArticleOnesPostApi.Bean>>(this) {
                    @Override
                    public void onSucceed(HttpData<ArticleOnesPostApi.Bean> result) {
                        articleListRefresh.finishRefresh();
                        articleListRefresh.finishLoadMore();
                        if (result != null && result.getData() != null && result.getData().getList().size() > 0) {
                            mLastId = result.getData().getList().get(result.getData().getList().size() - 1).getId();
                            if (isRefresh) {
                                myArticleAdapter.setList(result.getData().getList());
                            } else {
                                myArticleAdapter.addData(result.getData().getList());
                            }
                        } else {
                            if (isRefresh) {
                                myArticleAdapter.setList(new ArrayList<>());
                                myArticleAdapter.setEmptyView(R.layout.empty_view_layout);
                            }
                        }
                    }
                });
    }

    @Override
    public void details(String articleId) {
        ArticleDetailsActivity.start(requireActivity(), articleId, true);
    }

    @Override
    public void edit(String articleId) {
        EditArticleActivity.start(requireActivity(), articleId);
    }

    @Override
    public void imageUrl(String url) {
        ScanImageDialog.getInstance(mContext, url).builder().setNegativeButton().show();
    }
}
